<?php

use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locale = $this->command->ask("Enter language to migrate the data in Projects module");
        $projects = \TCG\Voyager\Models\Page::whereNotIn('id', array('1','2','3','4','6','8'))->get()->translate('locale', $locale);
        if(count($projects)>0){
            foreach ($projects as $item) {

                $array = array('9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27');
                if (in_array($item->id, $array)) {
                    $product = \App\Models\Products::where('title', 'Лизингтік қаржыландыру шеңберінде бизнес-ұсыныстардың алдын ала талқылауы')->first();
                    $product_id = $product->id;
                } else {
                    $product = \App\Models\Products::where('title', 'Инвестициялық жобаларды қаржыландыру')->first();
                    $product_id = $product->id;
                }

                if ($locale == 'kz') {
                    \App\Models\Projects::create([
                        'title' => $item->title,
                        'description' => $item->body,
                        'status' => 'ACTIVE',
                        'product_id' => $product_id,
                    ]);
                }

                if($locale == 'en')
                {
                    $projects = \App\Models\Projects::get();
                    foreach ($projects as $project) {

                        $translationTable = \App\Models\Translations::where('table_name','projects')->where('foreign_key',$project->id)->where('locale')->first();
                        if(!empty($translationTable)){
                            $translationModel = new \App\Models\Translations();
                            $translationModel->create([
                                'table_name' => 'projects',
                                'foreign_key' => $project->id,
                                'column_name' => 'title',
                                'locale' => $locale,
                                'value' => $item->title,
                            ]);

                            $translationModel->create([
                                'table_name' => 'projects',
                                'foreign_key' => $project->id,
                                'column_name' => 'description',
                                'locale' => $locale,
                                'value' => $item->body,
                            ]);
                        } else {
                            break;
                        }
                    }

                }

            }
        }

    }
}
