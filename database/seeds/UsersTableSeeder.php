<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use TCG\Voyager\Models\Role;
use TCG\Voyager\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::firstOrCreate([
            'slug' => 'admin',
            'display_name' => 'Admin',
            'name' => 'admin',
        ]);

        User::firstOrCreate([
            'name'           => 'Admin',
            'last_name'      => 'Admin',
            'email'          => 'admin@yopmail.com',
            'password'       => bcrypt('123456'),
            'remember_token' => Str::random(60),
            'role_id'        => $role->id,
            'gender'        => 'Male',
            'address'       => 'Test Address',
            'contact'       => '1234567890',
            'postcode'      => '12345',
            'city'          => 'test',
        ]);

        $this->call(VoyagerDatabaseSeeder::class);
    }
}
