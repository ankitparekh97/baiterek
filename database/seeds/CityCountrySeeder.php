<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CityCountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('set foreign_key_checks=0');
        DB::table('cities')->truncate();
        DB::table('regions')->truncate();
        DB::table('countries')->truncate();
        DB::statement('set foreign_key_checks=1');
        DB::table('countries')->insert([
            array('en_name' => 'kazakhstan',  'ru_name' => 'Казахстан', 'kz_name' => 'Қазақстан')
        ]);
        DB::table('regions')->insert([
            array('id' => '1', 'country_id' => '1', 'en_name' => '', 'ru_name' => 'Область', 'kz_name' => ''),
            array('id' => '2', 'country_id' => '1', 'en_name' => '', 'ru_name' => 'Нур-Султан', 'kz_name' => ''),
            array('id' => '3', 'country_id' => '1', 'en_name' => '', 'ru_name' => 'Алматы', 'kz_name' => ''),
            array('id' => '4', 'country_id' => '1', 'en_name' => '', 'ru_name' => 'Шымкент', 'kz_name' => ''),
            array('id' => '5', 'country_id' => '1', 'en_name' => '', 'ru_name' => 'Акмолинская область', 'kz_name' => ''),
            array('id' => '6', 'country_id' => '1', 'en_name' => '', 'ru_name' => 'Актюбинская область', 'kz_name' => ''),
            array('id' => '7', 'country_id' => '1', 'en_name' => '', 'ru_name' => 'Алматинская область', 'kz_name' => ''),
            array('id' => '8', 'country_id' => '1', 'en_name' => '', 'ru_name' => 'Атырауская область', 'kz_name' => ''),
            array('id' => '9', 'country_id' => '1', 'en_name' => '', 'ru_name' => 'Восточно-Казахстанская область', 'kz_name' => ''),
            array('id' => '10', 'country_id' => '1', 'en_name' => '', 'ru_name' => 'Жамбыльская область', 'kz_name' => ''),
            array('id' => '11', 'country_id' => '1', 'en_name' => '', 'ru_name' => 'Западно-Казахстанская область', 'kz_name' => ''),
            array('id' => '12', 'country_id' => '1', 'en_name' => '', 'ru_name' => 'Карагандинская область', 'kz_name' => ''),
            array('id' => '13', 'country_id' => '1', 'en_name' => '', 'ru_name' => 'Костанайская область', 'kz_name' => ''),
            array('id' => '14', 'country_id' => '1', 'en_name' => '', 'ru_name' => 'Кызылординская область', 'kz_name' => ''),
            array('id' => '15', 'country_id' => '1', 'en_name' => '', 'ru_name' => 'Мангистауская область', 'kz_name' => ''),
            array('id' => '16', 'country_id' => '1', 'en_name' => '', 'ru_name' => 'Павлодарская область', 'kz_name' => ''),
            array('id' => '17', 'country_id' => '1', 'en_name' => '', 'ru_name' => 'Северо-Казахстанская область', 'kz_name' => ''),
        ]);
        DB::table('cities')->insert([
            array('region_id' => '1', 'en_name' => '', 'ru_name' => 'Нур-Султан', 'kz_name' => ''),
            array('region_id' => '2', 'en_name' => '', 'ru_name' => 'Алматы', 'kz_name' => ''),
            array('region_id' => '3', 'en_name' => '', 'ru_name' => 'Шымкент', 'kz_name' => ''),

            array('region_id' => '4', 'en_name' => '', 'ru_name' => 'Акколь', 'kz_name' => ''),
            array('region_id' => '4', 'en_name' => '', 'ru_name' => 'Атбасар', 'kz_name' => ''),
            array('region_id' => '4', 'en_name' => '', 'ru_name' => 'Державинск', 'kz_name' => ''),
            array('region_id' => '4', 'en_name' => '', 'ru_name' => 'Ерейментау', 'kz_name' => ''),
            array('region_id' => '4', 'en_name' => '', 'ru_name' => 'Есиль', 'kz_name' => ''),
            array('region_id' => '4', 'en_name' => '', 'ru_name' => 'Кокшетау', 'kz_name' => ''),
            array('region_id' => '4', 'en_name' => '', 'ru_name' => 'Макинск', 'kz_name' => ''),
            array('region_id' => '4', 'en_name' => '', 'ru_name' => 'Степногорск', 'kz_name' => ''),
            array('region_id' => '4', 'en_name' => '', 'ru_name' => 'Степняк', 'kz_name' => ''),
            array('region_id' => '4', 'en_name' => '', 'ru_name' => 'Щучинск', 'kz_name' => ''),

            array('region_id' => '5', 'en_name' => '', 'ru_name' => 'Актобе', 'kz_name' => ''),
            array('region_id' => '5', 'en_name' => '', 'ru_name' => 'Алга', 'kz_name' => ''),
            array('region_id' => '5', 'en_name' => '', 'ru_name' => 'Жем', 'kz_name' => ''),
            array('region_id' => '5', 'en_name' => '', 'ru_name' => 'Кандыагаш', 'kz_name' => ''),
            array('region_id' => '5', 'en_name' => '', 'ru_name' => 'Темир', 'kz_name' => ''),
            array('region_id' => '5', 'en_name' => '', 'ru_name' => 'Хромтау', 'kz_name' => ''),
            array('region_id' => '5', 'en_name' => '', 'ru_name' => 'Шалкар', 'kz_name' => ''),
            array('region_id' => '5', 'en_name' => '', 'ru_name' => 'Эмба', 'kz_name' => ''),

            array('region_id' => '6', 'en_name' => '', 'ru_name' => 'Есик', 'kz_name' => ''),
            array('region_id' => '6', 'en_name' => '', 'ru_name' => 'Жаркент', 'kz_name' => ''),
            array('region_id' => '6', 'en_name' => '', 'ru_name' => 'Капчагай', 'kz_name' => ''),
            array('region_id' => '6', 'en_name' => '', 'ru_name' => 'Каскелен', 'kz_name' => ''),
            array('region_id' => '6', 'en_name' => '', 'ru_name' => 'Сарканд', 'kz_name' => ''),
            array('region_id' => '6', 'en_name' => '', 'ru_name' => 'Талгар', 'kz_name' => ''),
            array('region_id' => '6', 'en_name' => '', 'ru_name' => 'Талдыкорган', 'kz_name' => ''),
            array('region_id' => '6', 'en_name' => '', 'ru_name' => 'Текели', 'kz_name' => ''),
            array('region_id' => '6', 'en_name' => '', 'ru_name' => 'Ушарал', 'kz_name' => ''),
            array('region_id' => '6', 'en_name' => '', 'ru_name' => 'Уштобе', 'kz_name' => ''),

            array('region_id' => '7', 'en_name' => '', 'ru_name' => 'Атырау', 'kz_name' => ''),
            array('region_id' => '7', 'en_name' => '', 'ru_name' => 'Кульсары', 'kz_name' => ''),

            array('region_id' => '8', 'en_name' => '', 'ru_name' => 'Аягоз', 'kz_name' => ''),
            array('region_id' => '8', 'en_name' => '', 'ru_name' => 'Зайсан', 'kz_name' => ''),
            array('region_id' => '8', 'en_name' => '', 'ru_name' => 'Зыряновск', 'kz_name' => ''),
            array('region_id' => '8', 'en_name' => '', 'ru_name' => 'Курчатов', 'kz_name' => ''),
            array('region_id' => '8', 'en_name' => '', 'ru_name' => 'Риддер', 'kz_name' => ''),
            array('region_id' => '8', 'en_name' => '', 'ru_name' => 'Семей', 'kz_name' => ''),
            array('region_id' => '8', 'en_name' => '', 'ru_name' => 'Серебрянск', 'kz_name' => ''),
            array('region_id' => '8', 'en_name' => '', 'ru_name' => 'Усть-Каменогорск', 'kz_name' => ''),
            array('region_id' => '8', 'en_name' => '', 'ru_name' => 'Шар', 'kz_name' => ''),
            array('region_id' => '8', 'en_name' => '', 'ru_name' => 'Шемонаиха', 'kz_name' => ''),

            array('region_id' => '9', 'en_name' => '', 'ru_name' => 'Жанатас', 'kz_name' => ''),
            array('region_id' => '9', 'en_name' => '', 'ru_name' => 'Каратау', 'kz_name' => ''),
            array('region_id' => '9', 'en_name' => '', 'ru_name' => 'Тараз', 'kz_name' => ''),
            array('region_id' => '9', 'en_name' => '', 'ru_name' => 'Шу', 'kz_name' => ''),

            array('region_id' => '10', 'en_name' => '', 'ru_name' => 'Аксай', 'kz_name' => ''),
            array('region_id' => '10', 'en_name' => '', 'ru_name' => 'Уральск', 'kz_name' => ''),

            array('region_id' => '11', 'en_name' => '', 'ru_name' => 'Абай', 'kz_name' => ''),
            array('region_id' => '11', 'en_name' => '', 'ru_name' => 'Балхаш', 'kz_name' => ''),
            array('region_id' => '11', 'en_name' => '', 'ru_name' => 'Жезказган', 'kz_name' => ''),
            array('region_id' => '11', 'en_name' => '', 'ru_name' => 'Караганда', 'kz_name' => ''),
            array('region_id' => '11', 'en_name' => '', 'ru_name' => 'Каражал', 'kz_name' => ''),
            array('region_id' => '11', 'en_name' => '', 'ru_name' => 'Каркаралинск', 'kz_name' => ''),
            array('region_id' => '11', 'en_name' => '', 'ru_name' => 'Приозёрск', 'kz_name' => ''),
            array('region_id' => '11', 'en_name' => '', 'ru_name' => 'Сарань', 'kz_name' => ''),
            array('region_id' => '11', 'en_name' => '', 'ru_name' => 'Сатпаев', 'kz_name' => ''),
            array('region_id' => '11', 'en_name' => '', 'ru_name' => 'Темиртау', 'kz_name' => ''),
            array('region_id' => '11', 'en_name' => '', 'ru_name' => 'Шахтинск', 'kz_name' => ''),

            array('region_id' => '12', 'en_name' => '', 'ru_name' => 'Аркалык', 'kz_name' => ''),
            array('region_id' => '12', 'en_name' => '', 'ru_name' => 'Житикара', 'kz_name' => ''),
            array('region_id' => '12', 'en_name' => '', 'ru_name' => 'Костанай', 'kz_name' => ''),
            array('region_id' => '12', 'en_name' => '', 'ru_name' => 'Лисаковск', 'kz_name' => ''),
            array('region_id' => '12', 'en_name' => '', 'ru_name' => 'Рудный', 'kz_name' => ''),
            array('region_id' => '12', 'en_name' => '', 'ru_name' => 'Тобыл', 'kz_name' => ''),

            array('region_id' => '13', 'en_name' => '', 'ru_name' => 'Аральск', 'kz_name' => ''),
            array('region_id' => '13', 'en_name' => '', 'ru_name' => 'Байконыр', 'kz_name' => ''),
            array('region_id' => '13', 'en_name' => '', 'ru_name' => 'Казалинск', 'kz_name' => ''),
            array('region_id' => '13', 'en_name' => '', 'ru_name' => 'Кызылорда', 'kz_name' => ''),

            array('region_id' => '14', 'en_name' => '', 'ru_name' => 'Актау', 'kz_name' => ''),
            array('region_id' => '14', 'en_name' => '', 'ru_name' => 'Жанаозен', 'kz_name' => ''),
            array('region_id' => '14', 'en_name' => '', 'ru_name' => 'Форт-Шевченко', 'kz_name' => ''),

            array('region_id' => '15', 'en_name' => '', 'ru_name' => 'Аксу', 'kz_name' => ''),
            array('region_id' => '15', 'en_name' => '', 'ru_name' => 'Павлодар', 'kz_name' => ''),
            array('region_id' => '15', 'en_name' => '', 'ru_name' => 'Экибастуз', 'kz_name' => ''),

            array('region_id' => '16', 'en_name' => '', 'ru_name' => 'Булаево', 'kz_name' => ''),
            array('region_id' => '16', 'en_name' => '', 'ru_name' => 'Мамлютка', 'kz_name' => ''),
            array('region_id' => '16', 'en_name' => '', 'ru_name' => 'Петропавловск', 'kz_name' => ''),
            array('region_id' => '16', 'en_name' => '', 'ru_name' => 'Сергеевка', 'kz_name' => ''),
            array('region_id' => '16', 'en_name' => '', 'ru_name' => 'Тайынша', 'kz_name' => ''),

            array('region_id' => '17', 'en_name' => '', 'ru_name' => 'Арысь', 'kz_name' => ''),
            array('region_id' => '17', 'en_name' => '', 'ru_name' => 'Жетысай', 'kz_name' => ''),
            array('region_id' => '17', 'en_name' => '', 'ru_name' => 'Кентау', 'kz_name' => ''),
            array('region_id' => '17', 'en_name' => '', 'ru_name' => 'Ленгер', 'kz_name' => ''),
            array('region_id' => '17', 'en_name' => '', 'ru_name' => 'Сарыагаш', 'kz_name' => ''),
            array('region_id' => '17', 'en_name' => '', 'ru_name' => 'Туркестан', 'kz_name' => ''),
            array('region_id' => '17', 'en_name' => '', 'ru_name' => 'Шардара', 'kz_name' => ''),
        ]);
    }
}
