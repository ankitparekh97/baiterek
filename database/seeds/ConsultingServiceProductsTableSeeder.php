<?php

use Illuminate\Database\Seeder;

class ConsultingServiceProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = \App\Models\Products::whereIn('id',array('58','59','61','62','63'))->get();
        if(count($products)>0){
            foreach($products as $item){
                \App\Models\ConsultingServiceProducts::firstOrCreate([
                    'product_id' => $item->id,
                ]);
            }
        }
    }
}
