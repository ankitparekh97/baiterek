<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropServiceInquiryIdToServiceInquiryVerifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_inquiry_verifications', function (Blueprint $table) {
            $table->dropForeign('service_inquiry_verifications_service_inquiry_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_inquiry_verifications', function (Blueprint $table) {
            $table->foreign('service_inquiry_id')->references('id')->on('service_inquiries');
        });
    }
}
