<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCustomizationFieldsToBannerImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('banner_images', function (Blueprint $table) {
            $table->text('title_color')->nullable()->after('title');
            $table->text('url_color')->nullable()->after('url');
            $table->text('url_text_color')->nullable()->after('url');
            $table->text('url_text')->nullable()->after('url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('banner_images', function (Blueprint $table) {
            $table->dropColumn('title_color');
            $table->dropColumn('url_color');
            $table->dropColumn('url_text_color');
            $table->dropColumn('url_text');
        });
    }
}
