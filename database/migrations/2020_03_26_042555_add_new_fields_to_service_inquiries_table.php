<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldsToServiceInquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_inquiries', function (Blueprint $table) {
            $table->string('dataUUID')->nullable();
            $table->longText('old_json_data')->nullable();
            $table->tinyInteger('is_migrated')->default(0)->comment('0 = No, 1 = Yes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_inquiries', function (Blueprint $table) {
            $table->dropColumn('dataUUID');
            $table->dropColumn('old_json_data');
            $table->dropColumn('is_migrated');
        });
    }
}
