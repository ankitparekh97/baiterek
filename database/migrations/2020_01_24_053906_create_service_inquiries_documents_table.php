<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceInquiriesDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_inquiries_documents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('document');

            $type = \Illuminate\Support\Facades\DB::connection()->getDoctrineColumn(\Illuminate\Support\Facades\DB::getTablePrefix().'service_inquiries', 'id')->getType()->getName();
            if ($type == 'bigint') {
                $table->bigInteger('service_inquiries_id')->unsigned()->index();
            } else {
                $table->integer('service_inquiries_id')->unsigned()->index();
            }

            $table->foreign('service_inquiries_id')->references('id')->on('service_inquiries')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_inquiries_documents');
    }
}
