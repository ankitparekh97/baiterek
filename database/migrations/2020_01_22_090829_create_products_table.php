<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            $table->text('code')->nullable();
            $table->text('description')->nullable();

            $table->unsignedInteger('productCategory_id');
            $table->unsignedInteger('subsidiary_id');
            $type = \Illuminate\Support\Facades\DB::connection()->getDoctrineColumn(\Illuminate\Support\Facades\DB::getTablePrefix().'users', 'id')->getType()->getName();
            if ($type == 'bigint') {
                $table->bigInteger('user_id')->unsigned()->index();
            } else {
                $table->integer('user_id')->unsigned()->index();
            }

            $table->text('logo')->nullable();
            $table->text('organization')->nullable();
            $table->text('can_apply')->nullable();
            $table->timestamps();

            $table->foreign('productCategory_id')->references('id')->on('product_categories')->onDelete('cascade');
            $table->foreign('subsidiary_id')->references('id')->on('subsidiaries')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
