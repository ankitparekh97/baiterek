<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCommentToServiceInquiryVerifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_inquiry_verifications', function (Blueprint $table) {
            $table->longText('comment')->after('action_taken_at')->nullable();
            $table->longText('path')->after('comment')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_inquiry_verifications', function (Blueprint $table) {
            $table->dropColumn('comment');
            $table->dropColumn('path');
        });
    }
}
