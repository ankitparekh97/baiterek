<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSlugToFooterWidgets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('footer_widgets', function (Blueprint $table) {
            $table->text('slug')->nullable();
        });
        \App\Helper\Helper::updateSlugForFooterWidgets();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('footer_widgets', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
    }
}
