<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddApplicationFormIdToServiceInquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_inquiries', function (Blueprint $table) {
            $type = \Illuminate\Support\Facades\DB::connection()->getDoctrineColumn(\Illuminate\Support\Facades\DB::getTablePrefix().'application_forms', 'id')->getType()->getName();
            if ($type == 'bigint') {
                $table->bigInteger('application_form_id')->nullable()->unsigned()->index()->after('fields');
            } else {
                $table->integer('application_form_id')->nullable()->unsigned()->index()->after('fields');
            }
            $table->foreign('application_form_id')->references('id')->on('application_forms')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_inquiries', function (Blueprint $table) {
            //
        });
    }
}
