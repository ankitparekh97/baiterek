<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveForeignKeyToServiceInquiryActivities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_inquiry_activities', function (Blueprint $table) {
            $table->dropForeign('service_inquiry_activities_user_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_inquiry_activities', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }
}
