<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->char('userID')->nullable()->comment('UUID пользователя');
            $table->char('patronymic')->nullable()->comment('Отчество');
            $table->char('login')->nullable()->comment('Логин');
            $table->date('finished')->nullable()->comment('Дата удаления из системы');
            $table->tinyInteger('isadmin')->nullable()->comment('1 - является методологом, 0 - в противном случае');
            $table->tinyInteger('isconfigurator')->nullable()->comment('1 - является методологом, 0 - в противном случае');
            $table->tinyInteger('webDeveloper')->default(0)->nullable();
            $table->tinyInteger('pointersAccess')->default(0)->nullable()->comment('1 - имеет доступ в справочник показателей, 0 - в противном случае');
            $table->integer('photo_width')->nullable()->comment('Ширина фотографии');
            $table->integer('photo_height')->nullable()->comment('Высота фотографии');
            $table->char('locale')->nullable();
            $table->integer('update_interval')->default(15)->nullable()->comment('Интервал обновления открытой вкладки в минутах');
            $table->char('getctag')->default(123)->nullable()->comment('Поле не используется');
            $table->char('pointer_code')->nullable()->comment('Код показателя, используется при создании системных показателей пользователей');
            $table->date('expire')->nullable()->comment('Срок действия учетной записи');
            $table->text('homedirectory')->nullable()->comment('Путь к домашней директории пользователя в хранилище');
            $table->char('homedirectoryid')->nullable()->comment('ID домашней директории пользователя в хранилище');
            $table->char('jid')->nullable();
            $table->tinyInteger('strategy_access')->default(0)->nullable();
            $table->string('ldap_host')->nullable();
            $table->string('ldap_dn')->nullable();
            $table->string('ldap_domain')->nullable();
            $table->char('version')->nullable()->comment('Версия приложения которое пользователь загрузил в последний раз');
            $table->tinyInteger('read_getting_started')->nullable();
            $table->char('calendarID')->nullable()->comment('User private calendarID');
            $table->char('emailForInvites')->nullable()->comment('email для приглашений мобильного приложения');
            $table->tinyInteger('source')->default(0)->nullable()->comment('1 = Import, 0 = Website');
        });
        DB::statement("ALTER TABLE users ADD photo MEDIUMBLOB COMMENT 'Файл фотографии' AFTER pointersAccess");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
