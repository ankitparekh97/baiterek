<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('gender');
            $table->date('dob')->nullable();
            $table->text('address');
            $table->text('address2')->nullable();
            $table->text('contact');
            $table->text('city');
            $table->text('postcode');
            $table->boolean('is_active')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('gender');
            $table->dropColumn('dob');
            $table->dropColumn('address');
            $table->dropColumn('address2');
            $table->dropColumn('contact');
            $table->dropColumn('city');
            $table->dropColumn('postcode');
        });
    }
}
