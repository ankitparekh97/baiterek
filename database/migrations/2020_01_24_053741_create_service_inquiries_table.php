<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceInquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_inquiries', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->text('last_name');
            $table->text('contact')->nullable();
            $table->text('patronymiv_name')->nullable();
            $table->text('email');
            $table->text('email_of_recipient')->nullable();
            $table->text('gender');
            $table->text('marital_status');
            $table->text('application_type');
            $table->text('address')->nullable();
            $table->text('message')->nullable();
            $table->string('bin')->nullable();
            $table->text('application_status')->nullable();

            $type = \Illuminate\Support\Facades\DB::connection()->getDoctrineColumn(\Illuminate\Support\Facades\DB::getTablePrefix().'users', 'id')->getType()->getName();
            if ($type == 'bigint') {
                $table->bigInteger('user_id')->unsigned()->index();
            } else {
                $table->integer('user_id')->unsigned()->index();
            }

            $table->integer('product_id')->unsigned()->index();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_inquiries');
    }
}
