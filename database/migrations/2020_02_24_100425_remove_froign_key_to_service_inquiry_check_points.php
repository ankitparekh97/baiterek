<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class RemoveFroignKeyToServiceInquiryCheckPoints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_inquiry_check_points', function (Blueprint $table) {
            $table->dropForeign('service_inquiry_check_points_product_check_point_id_foreign');
            $table->dropForeign('service_inquiry_check_points_service_inquiry_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_inquiry_check_points', function (Blueprint $table) {
            $table->foreign('service_inquiry_id')->references('id')->on('service_inquiries');
            $table->foreign('product_check_point_id')->references('id')->on('product_check_points');
        });
    }
}
