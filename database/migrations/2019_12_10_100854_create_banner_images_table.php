<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannerImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('banner_images');
        Schema::create('banner_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('banner_masters_id')->unsigned();
            $table->text('image');
            $table->text('title')->nullable();
            $table->text('primary_text')->nullable();
            $table->text('secondary_text')->nullable();
            $table->text('url')->nullable();
            $table->integer('order');
            $table->timestamps();
        });

        Schema::table('banner_images', function($table) {
            $table->foreign('banner_masters_id')->references('id')->on('banner_masters')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner_images');
    }
}
