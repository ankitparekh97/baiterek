<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceInquiryVerificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_inquiry_verifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('service_inquiry_id')->unsigned();
            $table->foreign('service_inquiry_id')->references('id')->on('service_inquiries');
            $table->string('email');
            $table->string('link');
            $table->tinyInteger('is_verified')->comment('0 : No, 1 : Yes')->nullable();
            $table->string('inquiry_status');
            $table->dateTime('action_taken_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_inquiry_verifications');
    }
}
