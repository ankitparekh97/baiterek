<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('gender')->nullable()->change();
            $table->text('address')->nullable()->change();
            $table->text('city')->nullable()->change();
            $table->text('postcode')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('gender')->change();
            $table->text('address')->change();
            $table->text('city')->change();
            $table->text('postcode')->change();
        });
    }
}
