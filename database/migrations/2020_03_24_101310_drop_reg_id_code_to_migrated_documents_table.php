<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropRegIdCodeToMigratedDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('migrated_documents', function (Blueprint $table) {
            $table->dropColumn('registryID');
            $table->dropColumn('registry_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('migrated_documents', function (Blueprint $table) {
            $table->string('registryID')->nullable();
            $table->string('registry_code')->nullable();
        });
    }
}
