<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceInquiryCheckPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_inquiry_check_points', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('service_inquiry_id')->unsigned();
            $table->foreign('service_inquiry_id')->references('id')->on('service_inquiries');
            $table->bigInteger('product_check_point_id')->unsigned();
            $table->foreign('product_check_point_id')->references('id')->on('product_check_points');
            $table->tinyInteger('status')->default(0)->comment('0 : False , 1 : True');
            $table->text('application_status');
            $table->text('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_inquiry_check_points');
    }
}
