<div class="container">
    <div class="row">
        <div class="col-md-12">
            <ul class="footer-links">
                <li class="footer-links-li">
                    @if(!empty(\App\Helper\Helper::getFooterWidget('digital-baiterek')))
                        <p>{!! \App\Helper\Helper::getFooterWidget('digital-baiterek')->content !!}</p>
                    @endif
                </li>
                <li class="footer-links-li">
                    {{-- <h6 class="color-white">Explore</h6> --}}
                    {{menu('Footer menu Block 1','bootstrap')}}
                </li>
                <li class="footer-links-li">
                    @if(!empty(\App\Helper\Helper::getFooterWidget('visit')))
                        {{-- <h6 class="color-white">{{\App\Helper\Helper::getFooterWidget('visit')->title}}</h6> --}}
                        <p>{!! \App\Helper\Helper::getFooterWidget('visit')->content !!}</p>
                    @endif
                </li>
                {{-- <li class="footer-links-li">
                    @if(!empty(\App\Helper\Helper::getFooterWidget('new-business')))
                        <h6 class="color-white">{{\App\Helper\Helper::getFooterWidget('new-business')->title}}</h6>
                        <p>{!! \App\Helper\Helper::getFooterWidget('new-business')->content !!}</p>
                    @endif
                </li> --}}
                <li class="footer-links-li">
                    {{-- <h6 class="color-white">legal</h6> --}}
                    {{menu('Footer menu Block 2','bootstrap')}}
                </li>
                <li class="footer-links-li">
                    @if(!empty(\App\Helper\Helper::getFooterWidget('contact')))
                        {{-- <h6 class="color-white">{!! \App\Helper\Helper::getFooterWidget('contact')->title !!}</h6> --}}
                        <p>{!! \App\Helper\Helper::getFooterWidget('contact')->content !!}</p>
                    @endif
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="footer-copyright">
                <p>© @lang('content.copy_rights') {{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', \Carbon\Carbon::now())->year}} @lang('content.all_rights')</p>
                {{-- <p>Created by <a href="">AVDEVS Solution</a></p> --}}
            </div>
        </div>
    </div>
</div>
