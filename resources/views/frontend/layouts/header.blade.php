<div class="profile-language">
    @if(\Illuminate\Support\Facades\Auth::user())

    {{--<form id="" class="">--}}
{{--        {{csrf_field()}}--}}
        <input type="search" placeholder="Search">
    {{--</form>--}}
    @endif

    <div class="dropdown profile-dropdown">
        @if(\Illuminate\Support\Facades\Auth::user())
            <a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                @if(\Illuminate\Support\Facades\Auth::check() == 0)
                    <img src="{{asset('storage/user-img.png')}}" class="img-fluid" alt="">
                @else
                    <img src="{{asset('storage/'.\Illuminate\Support\Facades\Auth::user()->avatar)}}" class="img-fluid" alt="">
                @endif
               {!! \Illuminate\Support\Facades\Auth::check() ? '<span>'.ucfirst(\Illuminate\Support\Facades\Auth::user()->name).'</span>' : '' !!}
            </a>
        @else
           <ul class="navbar-nav">
               <li class="nav-item">
                   <a class="btn" href="{{route('frontend.login')}}">
                       @lang('auth.login')
                   </a>
               </li>
           </ul>
        @endif

        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <a class="dropdown-item" href="{{ route('voyager.dashboard') }}" target="_blank"><i class="fa fa-id-card" aria-hidden="true"></i> @lang('content.cabinet')</a>
            <a class="dropdown-item" href="{{route('frontend.logout')}}"><i class="fa fa-lock fa-1x"></i> @lang('auth.logout')</a>
        </div>

    </div>

    <div class="dropdown language-dropdown">
        <a class="btn dropdown-toggle" href="{{ url('locale/en') }}" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           {{ (\Illuminate\Support\Facades\Session::get('locale') ? strtoupper(\Illuminate\Support\Facades\Session::get('locale')) : config('baiterek.locales.KZ')) }}
        </a>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
            @foreach(config('baiterek.locales') as $langs)
                @if(!empty(\Illuminate\Support\Facades\Session::get('locale')))
                    @if(strtoupper($langs) != strtoupper(\Illuminate\Support\Facades\Session::get('locale')))
                        <a class="dropdown-item" href="{{ url('locale/'.$langs) }}">{{strtoupper($langs)}}</a>
                    @endif
                @else
                    @if(strtoupper($langs)!='KZ')
                        <a class="dropdown-item" href="{{ url('locale/'.$langs) }}">{{strtoupper($langs)}}</a>
                    @endif
                @endif
            @endforeach
        </div>
    </div>
</div>
