@if(!empty(\App\Helper\Helper::getBannerCategoryBySlug('home-page-middle-block')))
<div class="business-banner container-fluid" style="background-image:url({{asset('storage/' . \App\Helper\Helper::getBannersImages('home-page-middle-block')[0]->image)}})">
    <div class="row h-100">
        <div class="offset-lg-8 offset-md-6"></div>
        <div class="col-lg-4 col-md-6 black-div">
            @foreach(\App\Helper\Helper::getBannersImages('home-page-middle-block') as $homepagemiddleblock)
                <h3>{!! $homepagemiddleblock->primary_text !!}</h3>
                <p>{!! $homepagemiddleblock->secondary_text !!}</p>
            @endforeach
        </div>
    </div>
</div>
@endif
