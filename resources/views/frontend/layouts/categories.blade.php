<div class="col-md-12">
    <h3 class="section-header">@lang('fields.Browse Services')</h3>
    {{-- <p class="section-para">Imperdiet pulvinar ut nunc pellentesque.</p> --}}

    {{--<form class="">--}}
{{--        {{csrf_field()}}--}}
        <input class="form-control search-here" id="search-category" type="search" placeholder="@lang('fields.Search')" aria-label="Search">
    {{--</form>--}}
    <div class="services-list categoryList">
        <ul class="owl-carousel myCarousel owl_catagory">
            @if(!empty(\App\Http\Controllers\Services::getAllProductCategories('4')))
                @foreach(\App\Http\Controllers\Services::getAllProductCategories('4') as $productCategory)
                    <li class="item">
                        <a href="{{url('category', ['id' => $productCategory->id])}}">
                            <div class="service-box">
                                <div class="serice-icon"> <i class="icon-{{$productCategory->logo}}"></i></div>
                                <h5>{{$productCategory->title}}</h5>
                                <p>{{$productCategory->description}}</p>
                                <div class="background-icon"><i class="icon-{{$productCategory->logo}}"></i></div>
                            </div>
                        </a>
                    </li>
                @endforeach
            @endif
        </ul>
        <a href="{{url('categories')}}" class="view-all btn">@lang('fields.View All')</a>
    </div>
</div>