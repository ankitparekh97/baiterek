@if(count(\App\Helper\Helper::getNews())>0)
<style>
    .long-desc {
        min-height: max-content !important;
        margin-bottom: 10px !important;
    }
    /*.latest_news .news-list ul {*/
        /*flex-wrap: inherit !important;*/
    /*}*/
    /*.owl-nav{*/
        /*display: none;*/
    /*}*/
</style>
<div class="col-md-12">
    <h3 class="section-header">@lang('fields.Latest News')</h3>
    <a href="{{route('frontend.news')}}" class="news_viewmore" style="float: right">@lang('fields.View All')</a>
    <div class="news-list">
        <ul class="owl-carousel myCarousel owl_catagory">
            @foreach(\App\Helper\Helper::getNews() as $news)
                <li class="item">
                    <a href="{{url('news', ['id' => $news->id])}}">
                        <div class="news-box">
                            <div class="news-img">
                                <img src="{{asset('storage/'.$news->image)}}" class="img-fluid" alt="">
                            </div>
                            <div class="news-content">
                                <div class="author-date">
                                    <div class="news-author">
                                        {{-- @if(\Illuminate\Support\Facades\Auth::check() == 0)
                                            <img src="{{asset('images/dafault_user.png')}}" class="img-fluid" alt="">
                                        @else
                                            <img src="{{asset('storage/'.\Illuminate\Support\Facades\Auth::user()->avatar)}}" class="img-fluid" alt="">
                                        @endif
                                        <h5>{{ucfirst($news->user->name) . ' ' . ucfirst($news->user->last_name)}} </h5> --}}
                                    </div>
                                    <div class="news-date">
                                        <p>{{\Carbon\Carbon::parse($news->created_at)->format('d M, Y')}}</p>
                                    </div>
                                </div>
                                <div class="news-desc">
                                    <h5>{!! $news->short_description !!} </h5>
                                    <p class="long-desc">{!! nl2br(str_limit(strip_tags($news->long_description), 150)) !!}</p>
                                    <a class="news_viewmore" href="{{url('news', ['id' => $news->id])}}">@lang('fields.Read More')</a>
                                    {{-- <p class="long-desc">{!! $news->long_description !!}</p>
                                    <a class="news_viewmore" href="{{url('news', ['id' => $news->id])}}">Read More</a> --}}
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
</div>
@endif


<script type="application/javascript">
    // fact owl
    $(function () {
        $('.owl_catagory').owlCarousel({
            loop:true,
            margin:0,
            autoplayTimeout:2000,
            autoplayHoverPause:true,
            responsive: {
                1630:{
                    items:4
                },
                1261:{
                    items:3
                },
                903:{
                    items:2
                },
                0: {
                    items: 1
                }
            }
        });
    })
</script>

