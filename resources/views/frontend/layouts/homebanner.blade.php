@if(!empty(\App\Helper\Helper::getBannerCategoryBySlug('home-banner')))
@php  $homeBanner =\App\Helper\Helper::getBannerCategoryBySlug('home-banner'); @endphp
<div class="owl-carousel owl-theme homebanner">
    @if(count(\App\Helper\Helper::getBannersImages('home-banner'))>0)
        @foreach(\App\Helper\Helper::getBannersImages('home-banner') as $homebanners)
        <div class="item banner_item">
        <div class="slider-content" style="width:{{$homeBanner->width}}px;height:{{$homeBanner->height}}px;background-image: url({{asset('storage/'. $homebanners->image)}});">
                <div class="content">
                    <h3 style="color:{{$homebanners->title_color}}">{{$homebanners->title}}</h3>
                    <h1>{!! $homebanners->primary_text !!} </h1>
                    <p>{!! $homebanners->secondary_text !!}</p>
                    <a href="{{$homebanners->url}}" target="_blank" class="btn" style="color:{{$homebanners->url_text_color}}; border: 2px solid {{$homebanners->url_color}};">@if($homebanners->url_text != '') {{$homebanners->url_text}} @else @lang('auth.giveItTry') @endif</a>
                </div>
            </div>
        </div>
        @endforeach
    @endif
</div>

<script type="application/javascript">
// home banner
$(function () {
 $('.homebanner').owlCarousel({
     loop:true,
     margin:0,
     nav:true,
     items: 1,
     responsive:{
         0:{
             items:1
         },
         600:{
             items:1
         },
         1000:{
             items:1
         }
     }
 })
})

</script>
@endif
