@if(!empty(\App\Helper\Helper::getBannerCategoryBySlug('company-logos')))
<div class="col-md-12">
    <ul>
        @foreach(\App\Helper\Helper::getBannersImages('company-logos') as $companybanners)
            <li>
                <img src="{{asset('storage/' . $companybanners->image)}}" class="img-fluid" alt="">
            </li>
        @endforeach
    </ul>
</div>
@endif