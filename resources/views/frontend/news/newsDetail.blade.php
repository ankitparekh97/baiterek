@extends('frontend.master')

@section('content')
    <section class="htmlContent">
        <div class="container-fluid">
            <div class="row">
                @if(!empty(\App\Helper\Helper::getNewsDetail($id)))
                 <h1 class="page_heading">{{\App\Helper\Helper::getNewsDetail($id)->short_description}}</h1>

                 <p> <img src="{{asset('storage/'.\App\Helper\Helper::getNewsDetail($id)->image)}}"></p>
                 <p>{!! nl2br(\App\Helper\Helper::getNewsDetail($id)->long_description) !!} </p>

                @endif
            </div>
        </div>
    </section>
@endsection