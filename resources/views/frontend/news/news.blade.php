@extends('frontend.master')

@section('content')

<style>
    .long-desc {
        min-height: max-content !important;
        margin-bottom: 10px !important;
    }
    h1.page_heading {
        text-align: center !important;
    }
</style>
<section class="latest_news ">
    <div class="container-fluid">
        <div class="row">
            @if(count(\App\Helper\Helper::getNews())>0)
                <div class="col-md-12">
                    <h1 class="page_heading">@lang('fields.News')</h1>
                    <?php
                    $newz = \App\Helper\Helper::getNews();
                    ?>
                    <div class="news-list">
                        <ul>
                            @foreach($newz as $news)
                                <li>
                                    {{-- <a href=""> --}}
                                        <div class="news-box">
                                            <div class="news-img">
                                                <img src="{{asset('storage/'.$news->image)}}" class="img-fluid" alt="">
                                            </div>
                                            <div class="news-content">
                                                <div class="author-date">
                                                    <div class="news-author">
                                                        {{-- @if(\Illuminate\Support\Facades\Auth::check() == 0)
                                                            <img src="{{asset('images/dafault_user.png')}}" class="img-fluid" alt="">
                                                        @else
                                                            <img src="{{asset('storage/'.\Illuminate\Support\Facades\Auth::user()->avatar)}}" class="img-fluid" alt="">
                                                        @endif
                                                        <h5>{{ucfirst($news->user->name) . ' ' . ucfirst($news->user->last_name)}} </h5> --}}
                                                    </div>
                                                    <div class="news-date">
                                                        <p>{{\Carbon\Carbon::parse($news->created_at)->format('d M, Y')}}</p>
                                                    </div>
                                                </div>
                                                <div class="news-desc">
                                                    <h5>{!! $news->short_description !!}</h5>
                                                    <p class="long-desc">{!! nl2br(str_limit(strip_tags($news->long_description), 150)) !!}</p>
                                                    <a class="news_viewmore" href="{{url('news', ['id' => $news->id])}}">@lang('fields.Read More')</a>
                                                    {{-- <p class="long-desc">{!! $news->long_description !!}</p>
                                                    <a class="news_viewmore" href="{{url('news', ['id' => $news->id])}}">Read More</a> --}}
                                                </div>
                                            </div>
                                        </div>
                                    {{-- </a> --}}
                                </li>
                            @endforeach
                        </ul>
                        <div style="margin: 10px; float: left">
                            {{ $newz->links() }}
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</section>

@endsection
