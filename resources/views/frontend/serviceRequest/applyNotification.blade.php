@extends('frontend.master')

@section('content')
    <style>
        .sucess_apply_notification_img, .alert-danger, .alert-success{
            margin-left: auto;
            margin-right: auto;
        }
        .button_cus{
            box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);
            border-radius: 5px;
            padding: 10px 20px;
            font-size: 18px;
            line-height: 21px;
            text-align: center;
            text-transform: capitalize;
            color: black;
            margin: 0 10px;
            background: transparent;
            margin-bottom: 10px;
        }
        .button_cus:hover{
            color:white;
            background-color: #BBBEC3;
        }
        .ext_button{
            margin-left: auto;
            margin-right: auto;
            margin-top: 30px;
        }
    </style>
    <section class="">
        <div class="container-fluid">
            <div class="login-container">
                <img class="sucess_apply_notification_img" src="{{asset('images/apply-notification.png')}}">
                @if (session('error'))
                    <div class="alert alert-danger" role="alert">
                        <i class="fa fa-exclamation-triangle fa-1x"></i> {{ session('error') }}
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        <i class="fa fa-check-circle fa-1x"></i> {{ session('success') }}
                    </div>
                    <div class="ext_button" role="alert">
                        <a href="{{ route('frontend.products') }}" class="button_cus">{{ __('content.apply_for_more') }}</a>
                        <a href="{{ route('voyager.service-inquiries.index') }}" class="button_cus">{{ __('content.see_application_status') }}</a>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection

<script>
    history.pushState(null, null, location.href);
    window.onpopstate = function (){
        history.go(1);
    };
</script>