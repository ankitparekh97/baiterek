@extends('frontend.master')

@section('content')
    <style>
        .btn-apply {
            background: #00803d !important;
            font-size: 14px;
            line-height: 21px;
            text-align: center;
            text-transform: capitalize;
            color: #ffffff;
            margin: 0 10px;
        }
        .btn-cancel {
            background: #c5c5c5 !important;
            height: 40px !important;
            font-size: 14px;
            border-radius: 3px;
            padding: 10px 20px;
            line-height: 21px;
            text-align: center;
            text-transform: capitalize;
            color: #ffffff;
            margin: 0 10px;
        }
        .preview-thumbnail {
            max-width:160px;
            height:auto;
            clear:both;
            display:block;
            padding:2px;
            border:1px solid #ddd;
        }
        .remove-preview {
            position: absolute;
            left: 25%;
            top: 30px;
            font-size: 16px;
            color: #c10000;
        }
    </style>
    <section class="">
        <div class="container-fluid">
            <div class="login-container">
                <h1 class="page_heading">@lang('fields.Apply Now') - <i>{{$product->title}}</i></h1>
                <form name="apply" id="apply" action="{{route('frontend.applyService')}}" method="POST" class="mt-3" enctype="multipart/form-data">
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <input type="hidden" name="service_inquiry_id" id="service_inquiry_id" value="{{$serviceInquiryDetails->id}}">
                        <input type="hidden" name="service_inquiry_document_ids" id="service_inquiry_document_ids">
                        <input type="hidden" name="product_id" id="product_id" value="{{$serviceInquiryDetails->product_id}}">
                        <input type="hidden" name="formFields" id="formFields">
                        <input type="hidden" name="application_form_id" id="application_form_id" value="{{$serviceInquiryDetails->application_form_id}}">
                        <input type="hidden" name="status_of_request" id="status_of_request" value="{{$serviceInquiryDetails->status_of_request}}">
                        <input type="hidden" name="file_count" id="file_count">
                    <div id="fb-render">
                    </div>
                    <div class="form-row mt-5">
                        <div class="form-group col-md-12 d-flex align-items-center">
                            <input type="button" name="applyNow" id="applyNow" value="@lang('fields.Apply Now')" class="btn-apply" style="margin-left: 0;">
                            <a href="{{route('voyager.service-inquiries.index')}}" class="btn-cancel">@lang('content.cancel')</a>
                        </div>
                    </div>
                    @if(count($product->productCheckPoints))
                        <h3 class="doc_file_title">Download document here</h3>
                        <div class="doc_row row">
                            @foreach($product->productCheckPoints as $key => $productCheckpoint)
                                @if($productCheckpoint->type == \App\Models\ProductCheckPoint::DOCUMENT)
                                    <div class="doc_files col-md-2">
                                        <center>
                                            <a class="doc_ico" href="{{ asset('storage/'.$productCheckpoint->checkpoint) }}" target="_blank"><img src="{{ asset('images/doc-thumbnail.svg') }}"></a>
                                            <a class="doc_name" href="{{ asset('storage/'.$productCheckpoint->checkpoint) }}" download="">{{ $productCheckpoint->document_name }} <i class="fa fa-download download"></i> </a>
                                        </center>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    @endif
                </form>
            </div>
        </div>
    </section>

    <script src="{{asset('js/form-builder.min.js')}}"></script>
    <script src="{{asset('js/form-render.min.js')}}"></script>
    <script type="application/javascript">
        var formRender;
        $('document').ready(function () {
            const fbRender = document.getElementById("fb-render");
            const formData = JSON.stringify({!! $serviceInquiryDetails->fields !!});
            formRender = $(fbRender).formRender({ formData });

            $('.rendered-form').addClass('form-row');
            $( ".rendered-form" ).each(function() {
                $(this).find('div').addClass('form-group col-md-6');
            });


            $( "div.col-md-6" ).each(function(key,value) {
                if($(this).find('h1').length == 1){
                    $(this).removeClass().addClass('col-md-12 h1');

                }
                if( $(this).find('h2').length == 1){
                    $(this).removeClass().addClass('col-md-12 h2');

                }
                if($(this).find('h3').length == 1){
                    $(this).removeClass().addClass('col-md-12');

                }
                if($(this).find('h4').length == 1){
                    $(this).removeClass().addClass('col-md-12');

                }
                if($(this).find('h5').length == 1){
                    $(this).removeClass().addClass('col-md-12');

                }
                if($(this).find('h6').length == 1){
                    $(this).removeClass().addClass('col-md-12');

                }
                if($(this).find('p').length == 1){
                    $(this).removeClass().addClass('col-md-12');

                }
            });

            $.ajax({
                url: "{{ route('frontend.getDraftData') }}",
                type: 'POST',
                data: {
                    "_token" : "{{ csrf_token() }}",
                    'id': $('#service_inquiry_id').val(),
                },
                success: function (data) {
                    var service_inquiry_document_ids = [];
                    var file_count = 0;
                    $.each(data.serviceInquiryDocumentsDetails, function( index, value ) {
                        service_inquiry_document_ids.push(value.id);
                        service_inquiry_document_id = value.id;
                        service_inquiry_document_name = value.document;
                        $('input[type="file"]').each(function(i,v) {
                            var $this = $(this);
                            $nameArray = service_inquiry_document_name.split('_');
                            if ($nameArray[0] == $this.attr('name')) {
                                fileExtension = service_inquiry_document_name.substr((service_inquiry_document_name.lastIndexOf('.') + 1));
                                if (fileExtension == 'jpg' || fileExtension == 'png') {
                                    var route = '{{asset('storage/documents')}}'+'/'+service_inquiry_document_name;
                                    $this.parent().css('position', 'relative');
                                    $('<img src='+route+' class="preview-thumbnail">').insertAfter($this.parent().find('label'));
                                    $('<a target="_blank" href="'+route+'">'+service_inquiry_document_name+'</a>').insertAfter($this.parent().find('img'));
                                }
                                else {
                                    var route = '{{asset('storage/documents')}}'+'/'+service_inquiry_document_name;
                                    $this.parent().css('position', 'relative');
                                    $('<img src="{{asset('images/doc-thumbnail.svg')}}" class="preview-thumbnail">').insertAfter($this.parent().find('label'));
                                    $('<a target="_blank" href="'+route+'">'+service_inquiry_document_name+'</a>').insertAfter($this.parent().find('img'));
                                }
                            }
                        });
                    });
                    $('input[type="file"]').each(function() {
                        file_count++;
                    });
                    $('#service_inquiry_document_ids').val(service_inquiry_document_ids.join());
                    $('#file_count').val(file_count);
                },
                error: function () {
                    toastr.error("Oops something went wrong!");
                }
            });

            setTimeout(function(){
                $('#apply').find('.form-control').each(function() {
                    if ($(this).parent().find('img').length > 0) {
                        $(this).removeAttr('required');
                    }
                    var name = $(this).attr('name');
                    var required = $(this).attr('required');
                    var type = $(this).attr('type');
                    if(required != undefined) {
                        $('#apply').validate();
                        if (type == 'file') {
                            $("[name='" + name + "']").rules("add", {
                                required:true,
                                accept:"jpg,png,jpeg,doc,docx,pdf,csv",
                                messages: {
                                    required: "{{ __('validation.default_required') }}",
                                    accept: "{{ __('validation.default_accept') }}"
                                }
                            });
                        }
                        else {
                            $("[name='" + name + "']").rules("add", {
                                required:true,
                                messages: {
                                    required: "{{ __('validation.default_required') }}"
                                }
                            });
                        }
                    }
                    else {
                        if (type == 'file') {
                            $("[name='" + name + "']").rules("add", {
                                accept:"jpg,png,jpeg,doc,docx,pdf,csv",
                                messages: {
                                    accept: "{{ __('validation.default_accept') }}"
                                }
                            });
                        }
                    }
                });
            },1000);

            $('#applyNow').on('click', function(){
                var form_fields = JSON.stringify(formRender.userData);
                $('#formFields').val(form_fields);
                $('#status_of_request').val("{{ \App\Models\ServiceInquiries::APPLICATION_RECEIVED }}");
                if ($('#apply').valid()) {
                    $('form').submit();
                }
                else {
                    return false;
                }
            });

            $(document).on('click','.remove-preview',function(){
                var instance = $(this);
                var id = $(this).data('id');
                $.ajax({
                    url: "{{ route('frontend.deleteDocument') }}",
                    type: 'POST',
                    data: {
                        'id': id,
                    },
                    success: function (data) {
                        if (data.status) {
                            instance.parent().parent().find('img').remove();
                            instance.remove();
                        }
                        else {
                            console.log('Not able to delete');
                        }
                    },
                    error: function () {
                        toastr.error("Oops something went wrong!");
                    }
                });
            });
        });

    </script>
@endsection
