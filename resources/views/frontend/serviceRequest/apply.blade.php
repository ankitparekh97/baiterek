@extends('frontend.master')

@section('css')
    <?php app()->setLocale(\Session::get('locale')) ?>
    <style>
        blockquote {
            font-size: 20px !important;
            color: #526069 !important;
            padding: 10px 20px !important;
            margin: 0 0 20px !important;
            border-left: 2px solid #eee !important;
        }
        /* .col-md-6, .col-md-4, .col-md-3{
            display: inline-block !important;
        } */
    </style>
@endsection
@section('content')
    <section class="">
        <div class="container-fluid">
            <div class="login-container">
                <h1 class="page_heading">@lang('fields.Apply Now') - <i>{{$product->title}}</i></h1>
                <form name="apply" id="apply" action="{{route('frontend.submitRequest')}}" method="POST" class="mt-3" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{-- <input type="hidden" name="service_inquiry_id" id="service_inquiry_id"> --}}
                    <input type="hidden" name="product_id" value="{{$id}}">
                    <input type="hidden" name="formFields" id="formFields">
                    <input type="hidden" name="application_form_id" id="application_form_id" value="{{$application_form_id}}">
                    <input type="hidden" name="status_of_request" id="status_of_request">
                    <div id="fb-render">
                    </div>
                    <div class="form-row mt-5">
                        <div class="form-group col-md-12 d-flex align-items-center">
                            <input type="button" name="saveDraft" id="saveDraft" value="@lang('fields.Save Draft')" class="btn-draft" style="margin-left: 0;">
                            <input type="button" name="applyNow" id="applyNow" value="@lang('fields.Apply Now')" class="btn-apply" style="margin-left: 0;">
                        </div>
                    </div>
                    @if(count($product->productCheckPoints))
                        <h3 class="doc_file_title">{{ __('portal.front_end.service_documents') }}</h3>
                        <div class="doc_row row">
                            @foreach($product->productCheckPoints as $key => $productCheckpoint)
                                @if($productCheckpoint->type == \App\Models\ProductCheckPoint::DOCUMENT)
                                    <div class="doc_files col-md-2">
                                        <center>
                                            <a class="doc_ico" href="{{ asset('storage/'.$productCheckpoint->checkpoint) }}" target="_blank"><img src="{{ asset('images/doc-thumbnail.svg') }}"></a>
                                            <a class="doc_name" href="{{ asset('storage/'.$productCheckpoint->checkpoint) }}" download="">{{ $productCheckpoint->document_name }} <i class="fa fa-download download"></i> </a>
                                        </center>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    @endif
                </form>
            </div>
        </div>
    </section>

    @if(!empty($consultingServiceProduct))
       <script src="//code.jivosite.com/widget/miPUBgm94z" async></script>
    @endif

    <script src="{{asset('js/form-builder-oldercode.min.js')}}"></script>
    <script src="{{asset('js/form-render-current.min.js')}}"></script>
    <script type="application/javascript">
        var formRender;
        $('document').ready(function () {
            const fbRender = document.getElementById("fb-render");
            jQuery(function($) {
                const formData = JSON.stringify({!! $fields !!});
                formRender = $(fbRender).formRender({ formData });
                    // $('.rendered-form').addClass('form-row');
                    // $( ".rendered-form" ).each(function() {
                    //   $(this).find('div').addClass('form-group col-md-6');
                    //  });
                    // $( "div.col-md-6" ).each(function() {
                    //     if($(this).find('h1').length == 1){
                    //         $(this).removeClass().addClass('col-md-12');
                    //     }
                    //     if( $(this).find('h2').length == 1){
                    //         $(this).removeClass().addClass('col-md-12');
                    //     }
                    //     if($(this).find('h3').length == 1){
                    //         $(this).removeClass().addClass('col-md-12');
                    //     }
                    //     if($(this).find('h4').length == 1){
                    //         $(this).removeClass().addClass('col-md-12');
                    //     }
                    //     if($(this).find('h5').length == 1){
                    //         $(this).removeClass().addClass('col-md-12');
                    //     }
                    //     if($(this).find('h6').length == 1){
                    //         $(this).removeClass().addClass('col-md-12');
                    //     }
                    //     if($(this).find('p').length == 1){
                    //         $(this).removeClass().addClass('col-md-12');
                    //     }
                    // });
            });

            setTimeout(function(){
                $('.form-control').each(function() {
                    var name = $(this).attr('name');
                    var required = $(this).attr('required');
                    var type = $(this).attr('type');
                    if(required != undefined) {
                        $('#apply').validate();
                        if (type == 'file') {
                            $("[name='" + name + "']").rules("add", {
                                required:true,
//                                accept:"jpg,png,jpeg,doc,docx,pdf,csv",
                                accept:"jpg,png,jpeg,pdf",
                                messages: {
                                    required: "{{ __('validation.default_required') }}",
                                    accept: "{{ __('validation.default_accept') }}"
                                }
                            });
                        }
                        else {
                            $("[name='" + name + "']").rules("add", {
                                required:true,
                                messages: {
                                    required: "{{ __('validation.default_required') }}"
                                }
                            });
                        }
                    }
                    else {

                        if (type == 'file') {
                            $("[name='" + name + "']").rules("add", {
                                accept:"jpg,png,jpeg,pdf",
                                messages: {
                                    accept: "{{ __('validation.default_accept') }}"
                                }
                            });
                        }
                    }
                });
            },100);

            $('#applyNow').on('click', function(){
                var form_fields = JSON.stringify(formRender.userData);
                $('#formFields').val(form_fields);
                $('#status_of_request').val("{{ \App\Models\ServiceInquiries::APPLICATION_RECEIVED  }}");
                if ($('#apply').valid()) {
                    showLoading();
                    $('form').submit();
                }
                else {
                    hideLoading();
                    return false;
                }
            });

            $('#saveDraft').on('click', function(){
                showLoading();
                var form_fields = JSON.stringify(formRender.userData);
                $('#formFields').val(form_fields);
                $('#status_of_request').val("Draft");
                var form = $('#apply')[0];
                var formData = new FormData(form);
                $.ajax({
                    type: 'POST',
                    url: "{{route('frontend.submitRequest')}}",
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function (data){
                        hideLoading();
                        $('#apply').append('<input type="hidden" name="service_inquiry_id" id="service_inquiry_id" value="'+data.service_inquiry_id+'">');
                        if(typeof(data.service_inquiry_document_id) != "undefined" && data.service_inquiry_document_id !== null) {
                            $('#apply').append('<input type="hidden" name="service_inquiry_document_ids" id="service_inquiry_document_ids" value="'+data.service_inquiry_document_id.join()+'">');
                        }
                        toastr.success(data.message);
                    },
                    error: function(e) {
                        hideLoading();
                        console.log(e);
                    }
                });
            });
        });
        $('body').on('change', $('input[type="file"]'), function(e){
            var fileName = e.target.files[0].name;
//            var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
            var fileExtension = ['jpeg', 'jpg', 'png', 'pdf'];
            if ($.inArray(fileName.split('.').pop().toLowerCase(), fileExtension) == -1) {
                alert("{{ __('portal.only_formats_are_allowed') }} : "+fileExtension.join(', '));
                $(e.target).val(null)
            }
        });
    </script>
@endsection
