@extends('frontend.master')

@section('content')
    <style>
        .btn-disabled {
            background: #353d4769 !important;
            border-radius: 5px;
            padding: 10px 20px;
            font-size: 18px;
            line-height: 21px;
            text-align: center;
            text-transform: capitalize;
            color: #ffffff;
            margin: 0 10px;
            background: transparent;
            margin-bottom: 10px;
            cursor: not-allowed;
        }
        .doc_td img{
            width: 26px;
            display: inline-block;
            margin-left: 5px;
        }
        .tab-pane td{
            padding: 5px 0;
        }
    </style>
    <?php app()->setLocale(\Session::get('locale')) ?>
    <section class="product_category ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="section-header">{{ __('content.verification_data') }}</h3>
                    <div class="row padding-35">
                        <div class="col-lg-12 col-md-12" >
                            <div class="product-view">
                                <div class="tab-content" id="v-pills-tabContent">
                                    <div class="tab-pane fade show active" id="v-pills-tab" role="tabpanel" aria-labelledby="v-pills-tab">
                                        <div class="view-filter">
                                            <div class="tab-content" id="pills-tabContent">
                                                <div class="tab-pane fade show active" role="tabpanel" aria-labelledby="pills-gridview-tab">
                                                    <table >
                                                        @if(isset($service_form_data))
                                                        <thead>
                                                            <tr>
                                                                <td>{{ __('content.title') }}</td>
                                                                <td style="padding: 0 7px;"></td>
                                                                <td>{{ __('content.data') }}</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($service_form_data as $form_data)
                                                            @if ($form_data['type'] == 'file')
                                                                <tr><td></td></tr>
                                                                <tr>
                                                                <td>{!! $form_data['title'] !!} </td>
                                                                <td style="padding: 0 7px;">:</td>
                                                                <td class="doc_td">
                                                                    <a href="{{ $form_data['value'] }}" target="_blank">
                                                                        <img src="{{ asset('images/doc-thumbnail.svg') }}">
                                                                    </a>
                                                                </td>
                                                                </tr>
                                                            @elseif($form_data['type'] == 'paragraph' || $form_data['type'] == 'header' || $form_data['type'] == 'hidden' || $form_data['type'] == 'button')
                                                            @else
                                                            <tr>
                                                                <td>{!! $form_data['title'] !!}  </td>
                                                                <td style="padding: 0 7px;">:</td>
                                                                <td>{!! $form_data['value'] !!}</td>
                                                            </tr>
                                                            @endif
                                                        @endforeach
                                                        </tbody>
                                                        @endif
                                                    </table>

                                                    @if(!is_null($service_data->action_taken_at))
                                                        <br><div><b style="font-weight: bold; color: green">Verified at : {{ \Carbon\Carbon::parse($service_data->action_taken_at)->toDayDateTimeString() }}</b></div>
                                                    @endif
                                                    <br>
                                                    <form action="{{ route('actionServiceInquiryVerification',['id' => $service_data->id ]) }}" method="post" enctype="multipart/form-data">
                                                        @csrf
                                                        @if(empty($service_data->action_taken_at))
                                                            <label>@lang('fields.comment')</label>
                                                            <textarea name="comment" class="form-control"></textarea>
                                                            <label style="margin-top:30px">@lang('fields.externalDoc') : </label>
                                                            <input type="file" name="external_doc[]" multiple="" class="form-control">
                                                        @endif
                                                        <div class="d-flex ">
                                                            <button style="margin-right: 10px" name="status" class="view-all text btn @if(!is_null($service_data->action_taken_at)) btn-disabled @endif" @if(is_null($service_data->action_taken_at)) value="1" @endif>{{ __('content.Accepted') }}</button>
                                                            <button name="status" class="view-all text btn @if(!is_null($service_data->action_taken_at)) btn-disabled @endif" @if(is_null($service_data->action_taken_at)) value="0" @endif>{{ __('content.Rejected') }}</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
