<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="_url" content="{{ URL('') }}">
    <link rel="icon" href="{{asset('images/Frame.png') }}" type="image/png" >

    {{--<link rel="stylesheet" href="https://use.typekit.net/jyl6cwe.css">--}}
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
{{--        <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">--}}
{{--        <link rel="stylesheet" href="{{asset('css/owl.theme.default.css')}}">--}}
{{--        <link rel="stylesheet" href="{{asset('css/toastr.min.css')}}">--}}

        <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
{{--        <script src="{{asset('js/jquery-ui.min.js')}}"></script>--}}

{{--        <link rel="stylesheet" href="{{asset('fonts/icomoon/style.css')}}">--}}
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
{{--        <link rel="stylesheet" href="{{asset('css/trix.css')}}">--}}

    {{--    <style>--}}
    {{--        #toast-container {--}}
    {{--            position: fixed;--}}
    {{--            z-index: 999999;--}}
    {{--            pointer-events: none;--}}
    {{--            left:50% !important;--}}
    {{--            transform: translate(-50%, 0px);--}}
    {{--        }--}}
    {{--        .toast-top-right {--}}
    {{--            right: auto !important;--}}
    {{--        }--}}
    {{--    </style>--}}

    {{--    <title>{{ Voyager::setting("site.title") }}</title>--}}
    <title>{{ __('content.title_statement') }}</title>
    {{--    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">--}}
</head>

<body>
<header>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="/">
                <?php $site_logo_img = Voyager::setting('site.logo', ''); ?>
                @if($site_logo_img == '')
                    <img src="{{asset('images/logo.svg') }}" alt="{{ Voyager::setting("site.title") }}" title="{{ __('content.title_statement') }}" {{--title="{{ Voyager::setting("site.title") }}"--}}>
                @else
                    <img src="{{ Voyager::image($site_logo_img) }}" alt="{{ Voyager::setting("site.title") }}" title="{{ __('content.title_statement') }}" {{--title="{{ Voyager::setting("site.title") }}"--}}>
                @endif
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            @include('frontend.layouts.menu')
            @include('frontend.layouts.header')

        </nav>
    </div>
</header>
<div id="loader" style="display: none;">
    <img src="{{asset('images/loader.gif')}}" alt="Loader">
</div>
<main class="main-body">
    @yield('content')
    <section class="htmlContent">
        <div class="container-fluid">
            <div class="row">
                <h3 class="page_heading">Something went wrong</h3>
            </div>
        </div>
    </section>
</main>

<footer>
{{--        <script src="{{asset('js/popper.min.js')}}"></script>--}}
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
{{--        <script src="{{asset('js/owl.carousel.min.js')}}"></script>--}}
{{--        <script src="{{asset('js/moment.js')}}"></script>--}}
{{--        <script src="{{asset('js/jquery.validate.min.js')}}"></script>--}}
{{--        <script src="{{asset('js/toastr.min.js')}}"></script>--}}
{{--        <script src="{{asset('js/trix.js')}}"></script>--}}
</footer>

{{--<script defer src="{{asset('js/frontend.js')}}"></script>--}}

{{--<script type="application/javascript">--}}
{{--    var baseUrl = $('meta[name="_url"]').attr('content');--}}
{{--    var assetBaseUrl = "{{ asset('') }}";--}}
{{--    var disabledBtn = "@lang('fields.Coming soon')";--}}


{{--</script>--}}
</body>
</html>

