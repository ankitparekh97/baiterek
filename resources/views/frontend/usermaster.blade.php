<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="_url" content="{{ URL('') }}">
    <link rel="icon" href="{{asset('images/Frame.png') }}" type="image/png" >

{{--    <title>{{ Voyager::setting("site.title") }}</title>--}}
    <title>{{ __('content.title_statement') }}</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-datetimepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/toastr.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.theme.default.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">

    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    
    <style>
        #toast-container {
            position: fixed;
            z-index: 999999;
            pointer-events: none;
            left:50% !important;
            transform: translate(-50%, 0px);
        }
        .toast-top-right {
            right: auto !important;
        }
    </style>
</head>

<body>

    <div id="loader" style="display: none;">
        <img src="{{asset('images/loader.gif')}}" alt="Loader">
    </div>
    <main>
        <section class="login-register">
            <div class="container-fluid">
                <div class="row">
                    @yield('customer')
                </div>
            </div>
        </section>
    </main>


    <script type="application/javascript">
        var baseUrl = $('meta[name="_url"]').attr('content');
        var assetBaseUrl = "{{ asset('') }}";

        @if(session('success'))
            toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

            toastr.success("{{ \Illuminate\Support\Facades\Session::get('success') }}");
        @endif

        @if(session('error'))
            toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

            toastr.error("{{ \Illuminate\Support\Facades\Session::get('error') }}");
        @endif


        // login register
        $(function () {
            $('.owl-carousel').owlCarousel({
                loop:true,
                margin:0,
                nav:false,
                dots:true,
                items: 1,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    1000:{
                        items:1
                    }
                }
            })
        });

    </script>

</body>
<footer>
    <script src="{{asset('js/popper.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/moment.js')}}"></script>
    <script src="{{asset('js/bootstrap-datetimepicker.js')}}"></script>
    <script src="{{asset('js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('js/toastr.min.js')}}"></script>
    <script src="{{asset('js/frontend.js')}}"></script>
</footer>
</html>
