@extends('frontend.master')

@section('content')
    <section class="htmlContent">
        <div class="container-fluid">
            <div class="row">
                @if(!empty($pages))
                <h1 class="page_heading">{{$pages->translate(\Session::get('locale'))->title}}</h1>
                <p>
                     {!! $pages->translate(\Session::get('locale'))->body !!}
                 </p>
                @endif
            </div>
        </div>
    </section>
@endsection