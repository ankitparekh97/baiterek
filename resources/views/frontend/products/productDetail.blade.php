@extends('frontend.master')

@section('content')
    <section class="htmlContent">
        <div class="container-fluid">
            <div class="row">
                @if(!empty($product))
                     <h1 class="page_heading">{{$product->title}}</h1>

                     <p><img src="{{asset('storage/'.$product->logo)}}"></p>
                     <p>{{$product->code}}</p>
                     <p>{{$product->subsidiaryId->title}}</p>
                     <p>{{$product->productCategoryId->title}} </p>
                     <p>{{$product->organization}}</p>
                     <p>{!! nl2br($product->description) !!} </p>
                @endif
            </div>
        </div>
    </section>
@endsection