@extends('frontend.master')

@section('content')
    <section class="product_category ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="section-header">@lang('fields.Top Products') - {{$productCategory->title}}</h3>
                    <div class="row padding-35">
                        <div class="col-lg-12 col-md-7">
                            <div class="product-view">
                                <div class="view-filter">
                                    <div class="grid-view gridResults">
                                        <ul>
                                            @if(!empty($products))
                                                @foreach($products as $product)
                                                    <li>
                                                        <div class="grid-box">
                                                            <img src="{{asset('storage/'.$product->logo)}}" class="img-fluid" alt="">
                                                            <h5>{{$product->title}}</h5>
                                                            <div class="hover-div">
                                                                <a href="{{url('services',['id' => $product->id])}}" class="btn more-btn">@lang('fields.More')</a>
                                                                @if(in_array($product->id, $relatedProductIds) && $product->can_apply == 'Yes')
                                                                    <a href="{{url('apply',['id' => $product->id])}}" class="btn apply-btn">@lang('fields.Apply')</a>
                                                                @else
                                                                    <button class="btn-disabled">@lang('fields.Coming soon')</button>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endforeach

                                            @else
                                                @lang('content.No Match')
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection