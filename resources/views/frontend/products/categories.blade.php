@extends('frontend.master')

@section('content')

<section class="our_services">
    <div class="container-fluid">
        <div class="row">
            <div class="services-list">
                <ul>
                    @if(!empty($categories))
                        @foreach($categories as $productCategory)
                            <li>
                                <a href="{{url('category', ['id' => $productCategory->id])}}">
                                    <div class="service-box">
                                        <div class="serice-icon"> <i class="icon-{{$productCategory->logo}}"></i></div>
                                        <h5>{{$productCategory->title}}</h5>
                                        <p>{{$productCategory->description}}</p>
                                        <div class="background-icon"><i class="icon-{{$productCategory->logo}}"></i></div>
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </div>
</section>

@endsection
