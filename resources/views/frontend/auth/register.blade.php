@extends('frontend.usermaster')

@section('customer')

    <div class="col-xs-12 col-md-6 col-lg-8 login-sidebar">
        <div class="login-container">
            <a href="{{ url('/') }}"> <img src="{{asset('images/logo.svg')}}" class="img-fluid mb-5" alt="" title=""></a>
            <div>
                <a class="btn-group btn-group-toggle" data-toggle="buttons">
                    @foreach(config('voyager.multilingual.locales') as $lang)
                        <a href="{{ url('locale/'.$lang) }}" class="btn btn-success {{ ($lang === \Illuminate\Support\Facades\Session::get('locale')) ? " active" : "" }}">{{ strtoupper($lang) }}</a>
                    @endforeach
                </a>
            </div>
            <br>

            <h2>@lang('auth.sign up')</h2>
            <p>@lang('auth.takes few minutes')</p>

            <form name="register" id="register" class="validation" method="post" action="{{route('frontend.save')}}" class="mt-3">
                @csrf

                <div id="regStatus">
                    <div class="form-row border-top pt-5 mt-4">
                        <div class="form-group col-lg-4 col-md-12 mb-4">
                            <label>@lang('auth.first name')</label>
                            <div class="controls">
                                <input type="text" name="name" id="name" placeholder="@lang('auth.first name')" class="form-control" required autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group col-lg-4 col-md-12 mb-4">
                            <label>@lang('auth.last name')</label>
                            <div class="controls">
                                <input type="text" name="last_name" id="last_name" placeholder="@lang('auth.last name')" class="form-control" required autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group col-lg-4 col-md-12 mb-4">
                            <label>@lang('auth.patronymic name')</label>
                            <div class="controls">
                                <input type="text" name="patronymic" id="patronymic" placeholder="@lang('auth.patronymic name')" class="form-control" autocomplete="off">
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-lg-6 col-md-12 mb-4">
                            <label>@lang('auth.email')</label>
                            <div class="controls">
                                <input type="text" name="email" id="email" value="" placeholder="@lang('auth.email')" class="form-control" required autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group col-lg-6 col-md-12 mb-4" id="contact">
                            <label>@lang('auth.confirm email')</label>
                            <div class="controls">
                                <input type="text" name="confirm_email" id="confirm_email" value="" placeholder="@lang('auth.confirm email')" class="form-control" required autocomplete="off">
                            </div>
                        </div>
                    </div>

                    {{--<div class="form-row">--}}
                        {{--<div class="form-group col-lg-6 col-md-12 mb-4">--}}
                            {{--<label class="mr-sm-2" for="gender">@lang('auth.gender')</label>--}}
                            {{--<select class="custom-select mr-sm-2 form-control" id="gender" name="gender" required>--}}
                                {{--<option selected value="">@lang('fields.Choose')</option>--}}
                                {{--<option value="Male">@lang('fields.Male')</option>--}}
                                {{--<option value="Female">@lang('fields.Female')</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        {{--<div class="form-group col-lg-6 col-md-12 mb-4">--}}
                            {{--<label>@lang('auth.date of birth')</label>--}}
                            {{--<div class="controls">--}}
                                {{--<input type="text" name="dob" id="dob" value="" placeholder="@lang('auth.date of birth')" class="form-control" required autocomplete="off">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group col-lg-6 col-md-12 mb-4">--}}
                            {{--<label>@lang('auth.VATno')</label>--}}
                            {{--<div class="controls">--}}
                                {{--<input type="text" name="VAT_no" id="VAT_no" value="" placeholder="@lang('auth.VATno')" class="form-control" required autocomplete="off">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="form-row">
                        {{--<div class="form-group col-lg-6 col-md-12 mb-4">--}}
                            {{--<label>@lang('auth.address')</label>--}}
                            {{--<div class="controls">--}}
                                {{--<textarea name="address" id="address" class="form-control" rows="2" required></textarea>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="form-group col-lg-12 col-md-12 mb-4">
                            <label>@lang('auth.phone number')</label>
                            <div class="controls">
                                <input type="text" name="contact" id="contact" value="" placeholder="@lang('auth.phone number')" class="form-control" required autocomplete="off">
                            </div>
                        </div>
                    </div>
                    {{--<div class="form-row">--}}
                        {{--<div class="form-group col-lg-4 col-md-12 mb-4">--}}
                            {{--<label>@lang('auth.region')</label>--}}
                            {{--<div class="controls">--}}
                                {{--<select name="region" id="region" class="form-control" required >--}}
                                    {{--<option value="">@lang('fields.Choose')</option>--}}
                                    {{--@foreach($regions as $k => $v)--}}
                                        {{--<option value="{{ $k }}"> {{ $v }} </option>--}}
                                    {{--@endforeach--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group col-lg-4 col-md-12 mb-4">--}}
                            {{--<label>@lang('auth.town/city')</label>--}}
                            {{--<div class="controls" id="cityDiv">--}}
                                {{--<select name="city" id="city" class="form-control" required >--}}
                                    {{--<option value="">@lang('fields.Choose')</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group col-lg-4 col-md-12 mb-4">--}}
                            {{--<label>@lang('auth.post code')</label>--}}
                            {{--<div class="controls">--}}
                                {{--<input type="text" name="postcode" id="postcode" value="" placeholder="@lang('auth.post code')" class="form-control" required autocomplete="off">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="form-row border-bottom pb-4">
                        <div class="form-group col-md-12 mt-3" id="acceptGroup">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="accept" id="accept" required>
                                @if(\Session::get('locale') == 'kz')
                                    <label class="custom-control-label" for="accept">@lang('auth.condition_first') <a href="{{ url('terms') }}">@lang('auth.condition_second')</a> @lang('auth.terms') </label>
                                @else
                                    <label class="custom-control-label" for="accept">@lang('auth.terms') <a href="{{ url('terms') }}">@lang('auth.condition')</a></label>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-row  mt-5">
                        <div class="form-group col-md-12 d-flex justify-content-between align-items-center">
                            <p class="mb-0">@lang('auth.already an account')&nbsp;<a href="{{route('frontend.login')}}">@lang('auth.sign_page')</a></p>
                            <input type="submit" name="Sign Up" id="signup" value="@lang('auth.sign up')" class="btn btn-block login-button">
                        </div>
                    </div>
                </div>

                <div class="alert alert-success success" role="alert" style="display: none !important;">
                    <i class="fa fa-check-circle-o fa-1x"></i>
                </div>

                <div class="alert alert-danger failure" role="alert" style="display: none !important;">
                    <i class="fa fa-check-circle-o fa-1x"></i>
                </div>

            </form>
        </div>
    </div>

    <div class="col-md-6 col-lg-4 right-banner side-banner green-gradient">
        <div class="side-banner-content">
            <div class="bg-img">
                <img src="{{asset('images/leaf-bg.png')}}" class="img-fluid">
            </div>
            <div class="slider">
                <div class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="slider-text">
                            {{--<img src="{{asset('images/logo.svg')}}" alt="Baiterek" title="Baiterek">--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="application/javascript">
        $(function () {
            $('#dob').datetimepicker({
                'format': 'YYYY/MM/DD'
            });

            $("#register").validate({
                rules: {
                    email: {
                        required: true,
                        email: true,
                        remote: {
                            url: baseUrl+"/checkEmail",
                            type: "get",
                            email: function() {
                                return $( "#email" ).val();
                            }
                        }
                    },
//                    VAT_no: {
//                        required: true,
//                        remote: {
//                            url: baseUrl+"/checkVAT_no",
//                            type: "get",
//                            VAT_no: function() {
//                                return $( "#VAT_no" ).val();
//                            }
//                        }
//                    },
                    confirm_email: {
                        equalTo: "#email"
                    },
                    contact: {
                        required: true,
                        digits: true,
                        maxlength: 11
                    }
                },
                messages: {
                    name: {
                        required: "{{ __('validation.required',['attribute' => "first name"]) }}",
                    },
                    last_name: {
                        required: "{{ __('validation.required',['attribute' => "last name"]) }}",
                    },
                    {{--patronymic: {--}}
                        {{--required: "{{ __('validation.required',['attribute' => "patronymic name"]) }}",--}}
                    {{--},--}}
                    email: {
                        required: "{{ __('validation.required',['attribute' => "email address"]) }}",
                        email: "{{ __('validation.email',['attribute' => "email address"]) }}",
                        remote: "{{ __('validation.unique',['attribute' => "email address"]) }}",
                    },
                    confirm_email: {
                        required: "{{ __('validation.required',['attribute' => "confirm email"]) }}",
                        equalTo: "{{ __('validation.equalTo') }}",
                    },
                    gender: {
                        required: "{{ __('validation.required',['attribute' => "gender"]) }}",
                    },
                    dob: {
                        required: "{{ __('validation.required',['attribute' => "your date of birth"]) }}",
                    },
                    {{--VAT_no: {--}}
                        {{--required: "{{ __('validation.required',['attribute' => "VAT no"]) }}",--}}
                        {{--remote: "{{ __('validation.unique',['attribute' => "VAT no"]) }}",--}}
                    {{--},--}}
                    address: {
                        required: "{{ __('validation.required',['attribute' => "your address"]) }}",
                    },
                    contact: {
                        required: "{{ __('validation.required',['attribute' => "your contact information"]) }}",
                        digits: "{{ __('validation.valid contact') }}",
                        maxlength: "{{ __('validation.valid contact') }}",
                    },
                    {{--city: {--}}
                        {{--required: "{{ __('validation.required',['attribute' => "town/city"]) }}",--}}
                    {{--},--}}
                    {{--region: {--}}
                        {{--required: "{{ __('validation.required',['attribute' => "region"]) }}",--}}
                    {{--},--}}
                    {{--postcode: {--}}
                        {{--required: "{{ __('validation.required',['attribute' => "your postcode"]) }}",--}}
                    {{--},--}}
                    accept: {
                        required: "{{ __('validation.terms') }}",
                    },
                },
                submitHandler: function(form) {
                    form.submit();
                    showLoading();
                },
                errorPlacement: function(error, element) {
                    if (element.attr("name") == "accept") {
                        var $parentDiv = $(element).parent();
                        error.insertAfter($parentDiv);
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        });


        $('body').on('change', '#region', function(){
            $.ajax({
                url: "{{ route('voyager.service-inquiries.ajaxCityList') }}",
                type: 'GET',
                dataType: 'json',
                contentType: false,
                data: {
                    "dataId" : $('#region').val(),
                    "_token" : '{{ csrf_token() }}'
                },
                beforeSend: function (msg) {

                },
                success: function (response) {
                    $('.ajax_add_option').remove();
                    $.each(response.data, function(index, value) {
                        var html = '<option class="ajax_add_option" value="'+index+'">'+value+'</option>' ;
                        $('#city').append(html);
                    });
                },
                error: function () {
                    alert('request error');
                }
            });

        });


    </script>

@endsection
