@extends('frontend.usermaster')

@section('customer')

    <div class="col-md-6 col-lg-4 left-banner side-banner yellow-gradient">
        <div class="side-banner-content">
            <div class="bg-img">
                <img src="{{asset('images/leaf-bg.png')}}" class="img-fluid">
            </div>
{{--            <div class="slider">--}}
{{--                <div class="owl-carousel owl-theme">--}}
{{--                    <div class="item">--}}
{{--                        <div class="slider-text">--}}
{{--                            <h6>Nunc euismod tellus ut quis.</h6>--}}
{{--                            <p>Commodo eget sit enim, aliquam ut sagittis. Platea nisi, dictum volutpat feugiat.</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="item">--}}
{{--                        <div class="slider-text">--}}
{{--                            <h6>Nunc euismod tellus ut quis.</h6>--}}
{{--                            <p>Commodo eget sit enim, aliquam ut sagittis. Platea nisi, dictum volutpat feugiat.</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
    </div>

    <div class="col-xs-12 col-md-6 col-lg-8 login-sidebar">
        <div class="login-container">
            <a href="{{ url('/') }}"><img src="{{asset('images/logo.svg')}}" class="img-fluid mb-5" alt="" title=""></a>
            <div>
                <a class="btn-group btn-group-toggle" data-toggle="buttons">
                    @foreach(config('voyager.multilingual.locales') as $lang)
                        <a href="{{ url('locale/'.$lang) }}" class="btn btn-success {{ ($lang === \Illuminate\Support\Facades\Session::get('locale')) ? " active" : "" }}">{{ strtoupper($lang) }}</a>
                    @endforeach
                </a>
            </div>
            <br/>
            <h2>{{ __('passwords.resetPassword') }}</h2>
            <p>{{ __('passwords.Please reset your password to login your account') }}</p>

            <form name="savePassword" id="savePassword" method="post" action="{{route('frontend.savePassword')}}">
                {{ csrf_field() }}

                <input type="hidden" name="token" value="{{ $token }}">

                @if (\Illuminate\Support\Facades\Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        <i class="fa fa-exclamation-triangle fa-1x"></i> {{ \Illuminate\Support\Facades\Session::get('error') }}
                    </div>
                @endif

                @if (\Illuminate\Support\Facades\Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        <i class="fa fa-check-circle"></i> {{ \Illuminate\Support\Facades\Session::get('success') }}
                    </div>
                @endif

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label>{{ __('passwords.newPassword') }}</label>
                        <div class="controls">
                            <input type="password" name="password" id="password" placeholder="{{ __('passwords.newPassword') }}" class="form-control" autocomplete="off">
                        </div>
                    </div>
                    @if ($errors->has('password'))
                        <span class="baiterek-help-block" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                    @endif
                </div>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label>{{ __('passwords.confirmNewPassword') }}</label>
                        <div class="controls">
                            <input type="password" name="confirm_password" id="confirm_password" placeholder="{{ __('passwords.confirmNewPassword') }}" class="form-control" autocomplete="off">
                        </div>
                    </div>
                    @if ($errors->has('confirm_password'))
                        <span class="baiterek-help-block" role="alert">
                    <strong>{{ $errors->first('confirm_password') }}</strong>
                </span>
                    @endif
                </div>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <hr />
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6"></div>
                    <div class="form-group col-md-4"></div>
                    <div class="form-group col-md-2">
                        <div class="controls">
                            <input type="submit" name="changePassword" id="changePassword" value="{{ __('passwords.resetPassword') }}" class="btn btn-block login-button forgotpassword-btn">
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>


@endsection
