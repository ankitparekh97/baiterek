@extends('frontend.usermaster')

@section('customer')

    <div class="col-md-6 col-lg-4 left-banner side-banner green-gradient">
        <div class="side-banner-content">
            <div class="bg-img">
                <img src="{{asset('images/leaf-bg.png')}}" class="img-fluid">
            </div>
            <div class="slider">
                <div class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="slider-text">
{{--                            <img src="{{asset('images/logo.svg')}}" alt="Baiterek" title="Baiterek">--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-md-6 col-lg-8 login-sidebar">
        <div class="login-container">
            <a href="{{ url('/') }}"><img src="{{asset('images/logo.svg')}}" class="img-fluid mb-5" alt="" title=""></a>
            <div>
                <a class="btn-group btn-group-toggle" data-toggle="buttons">
                    @foreach(config('voyager.multilingual.locales') as $lang)
                        <a href="{{ url('locale/'.$lang) }}" class="btn btn-success {{ ($lang === \Illuminate\Support\Facades\Session::get('locale')) ? " active" : "" }}">{{ strtoupper($lang) }}</a>
                    @endforeach
                </a>
            </div>
            <br>
            <h2>@lang('auth.welcome')</h2>
            <p>@lang('auth.sign in to account')</p>
            <p>@lang('auth.no account') <span style="color: #2D803D;"><a href="{{route('frontend.register')}}">@lang('auth.sign up now')</a></span></p>

            <form name="login" id="login" action="{{route('frontend.authCheck')}}" method="POST" class="mt-3">
                {{ csrf_field() }}

                @if (session('error'))
                    <div class="alert alert-danger" role="alert">
                        <i class="fa fa-exclamation-triangle fa-1x"></i> {{ session('error') }}
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        <i class="fa fa-check-circle fa-1x"></i> {{ session('success') }}
                    </div>
                @endif

                <div class="form-row border-top pt-5">
                    <div class="form-group col-lg-4 col-md-12">
                        <label for="email">@lang('auth.email')</label>
                        <input type="text" class="form-control" value="{{old('email')}}" id="email" name="email" placeholder="@lang('auth.email')" autocomplete="off">
                        @if ($errors->has('email'))
                            <span class="baiterek-help-block" role="alert">
                        <span>{{ $errors->first('email') }}</span>
                    </span>
                        @endif
                    </div>
                    <div class="form-group col-lg-4 col-md-12">
                        <label for="password">@lang('auth.password')</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="@lang('auth.password here')" autocomplete="off">
                        @if ($errors->has('password'))
                            <span class="baiterek-help-block" role="alert">
                        <span>{{ $errors->first('password') }}</span>
                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-row border-bottom pb-4">
                    <div class="form-group col-md-12 mt-3">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="remember" id="remember" value="1">
                            <label class="custom-control-label" for="remember">@lang('auth.keep logged in')</label>
                        </div>
                    </div>
                </div>

                <div class="form-row  mt-5">
                    <div class="form-group col-md-12 d-flex justify-content-end align-items-center">
                        <a href="{{route('frontend.forgotPassword')}}" class="forgot-pswd">@lang('auth.forgot password?')</a>
                        <input type="submit" name="login" id="Login" value="@lang('auth.login')" class="btn btn-block login-button">
                    </div>
                </div>

            </form>

        </div>

    </div>

@endsection
