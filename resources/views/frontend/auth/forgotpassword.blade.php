@extends('frontend.usermaster')

@section('customer')

    <div class="col-md-6 col-lg-4 left-banner side-banner green-gradient">
        <div class="side-banner-content">
            <div class="bg-img">
                <img src="{{asset('images/leaf-bg.png')}}" class="img-fluid">
            </div>
            <div class="slider">
                <div class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="slider-text">
{{--                            <img src="{{asset('images/logo.svg')}}" alt="Baiterek" title="Baiterek">--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-md-6 col-lg-8 login-sidebar">
        <div class="login-container">
            <a href="{{ url('/') }}"><img src="{{asset('images/logo.svg')}}" class="img-fluid mb-5" alt="" title=""></a>
            <div>
                <a class="btn-group btn-group-toggle" data-toggle="buttons">
                    @foreach(config('voyager.multilingual.locales') as $lang)
                        <a href="{{ url('locale/'.$lang) }}" class="btn btn-success {{ ($lang === \Illuminate\Support\Facades\Session::get('locale')) ? " active" : "" }}">{{ strtoupper($lang) }}</a>
                    @endforeach
                </a>
            </div>
            <br>
            <h2>@lang('auth.forgot password')</h2>
            <p>@lang('auth.forgot password instructions')</p>

            <form name="forgotPassword" id="forgotPassword" action="{{route('frontend.emailResetPasswordLink')}}" method="POST">
                {{ csrf_field() }}

                @if (session('error'))
                    <div class="alert alert-danger" role="alert">
                        <i class="fa fa-exclamation-triangle fa-1x"></i> {{ session('error') }}
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        <i class="fa fa-check-circle fa-1x"></i> {{ session('success') }}
                    </div>
                @endif

                <div class="form-row py-4 mt-4 border-bottom border-top">
                    <div class="form-group col-lg-6 col-md-12">
                        <label for="email">@lang('auth.email')</label>
                        <input type="email" class="form-control" value="{{old('email')}}" id="email" name="email" placeholder="@lang('auth.email')" autocomplete="off">
                        @if ($errors->has('email'))
                            <span class="baiterek-help-block" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-row mt-5">
                    <div class="form-group col-md-12 d-flex justify-content-end align-items-center">
                        <a href="{{route('frontend.login')}}" class="forgot-pswd">@lang('auth.login here')</a>
                        <input type="submit" name="sendLink" id="sendLink" value="@lang('auth.send link')" class="btn btn-block login-button">
                    </div>
                </div>

            </form>

        </div>

    </div>

@endsection
