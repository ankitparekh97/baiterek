@extends('frontend.master')

@section('content')
    <section class="product_category ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
{{--                    <h3 class="section-header">@lang('fields.Top Products')</h3>--}}
                    <div class="row padding-35">
                        <div class="col-lg-3 col-md-5">
                            <div class="category-list">

                                {{--<form class="">--}}
{{--                                    {{csrf_field()}}--}}
                                    <input class="form-control search-here" id="search-product" type="search" placeholder="@lang('fields.Search')" aria-label="Search">
                                {{--</form>--}}
                                <div class="nav flex-column nav-pills" id="v-pills-tab1" role="tablist" aria-orientation="vertical">
                                    @if(!empty($productCategories))
                                        @foreach($productCategories as $productCategory)
                                            <a class="nav-link" id="v-pills-{{$productCategory->logo}}-tab" data-toggle="pill" href="#v-pills-{{$productCategory->logo}}" role="tab" aria-controls="v-pills-{{$productCategory->logo}}" data-id="{{$productCategory->id}}" aria-selected="true">
                                                {{$productCategory->title}} <i class="icon-{{$productCategory->logo}}"></i>
                                            </a>
                                        @endforeach
                                    @endif
                                    <a href="{{url('categories')}}" class="view-all btn">@lang('fields.View All')</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-7">
                            <div class="product-view">
                                <div class="tab-content" id="v-pills-tabContent">
                                    <div class="tab-pane fade show active" id="v-pills-tab" role="tabpanel" aria-labelledby="v-pills-tab">

                                        <div class="view-filter">
                                            <ul class="nav nav-pills justify-content-end" id="pills-tab" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link list" id="pills-listview-tab" data-toggle="pill" href="#pills-listview" role="tab" aria-controls="pills-listview-investor" aria-selected="false">
                                                        <img src="{{asset('images/list.svg')}}" class="img-fluid" alt="">
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link grid active" id="pills-gridview-tab" data-toggle="pill" href="#pills-gridview" role="tab" aria-controls="pills-gridview" aria-selected="true">
                                                        <img src="{{asset('images/grid.svg')}}" class="img-fluid" alt="">
                                                    </a>
                                                </li>
                                            </ul>

                                            <div class="tab-content" id="pills-tabContent">
                                                <div class="tab-pane fade" id="pills-listview" role="tabpanel" aria-labelledby="pills-listview-tab">
                                                    <div class="list-view viewResults">
                                                        <ul>
                                                            @if(!empty($products))
                                                                @foreach($products as $product)
                                                                    <li>
                                                                        <div class="list-box">
                                                                            <img src="{{asset('storage/'.$product->logo)}}" class="img-fluid" alt="">
                                                                            <h5>{{$product->title}}</h5>
                                                                            <div class="hover-div">
                                                                                <a href="{{url('projects',['id' => $product->id])}}" class="btn more-btn">@lang('fields.ViewProjects')</a>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                @endforeach
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade show active" id="pills-gridview" role="tabpanel" aria-labelledby="pills-gridview-tab">
                                                    <div class="grid-view gridResults">
                                                        <ul>
                                                            @if(!empty($products))
                                                                @foreach($products as $product)
                                                                <li>
                                                                    <div class="grid-box">
                                                                        <img src="{{asset('storage/'.$product->logo)}}" class="img-fluid" alt="">
                                                                        <h5>{{$product->title}}</h5>
                                                                        <div class="hover-div">
                                                                           <a href="{{url('projects',['id' => $product->id])}}" class="btn more-btn">@lang('fields.ViewProjects')</a>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                @endforeach
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
