@extends('frontend.master')

@section('content')
    <section class="htmlContent">
        <div class="container-fluid">
            <div class="row">
                @if(!empty($projectDetail))
                     <h1 class="page_heading">{{$projectDetail->title}}</h1>

{{--                     <p><img src="{{asset('storage/'.$product->logo)}}"></p>--}}
                     <p>{!! nl2br($projectDetail->description) !!} </p>
                @endif
            </div>
        </div>
    </section>
@endsection