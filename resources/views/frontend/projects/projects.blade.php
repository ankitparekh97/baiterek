@extends('frontend.master')

@section('css')
    <style>
        .latest_news .news-list ul li .news-box {
            height: 300px !important;
            background: #e8e9ea47 !important;
            border: 2px solid rgba(229, 229, 229, 0.35) !important;
        }
        .latest_news .news-list ul li .news-box .news-content .news-desc h5{
            font-weight: bold !important;
        }
        .latest_news .news-list{
            margin-top: 0 !important;
        }
        .latest_news .news-list ul li .news-box .news-content .news-desc h5:before {
            content: normal !important;
        }
        .latest_news .news-list ul li .news-box .news-content .news-desc h5:after {
            content: normal !important;
            background: #e8e9ea47 !important;
        }
        .latest_news .news-list ul li .news-box .news-content .news-desc p:after {
            background: #e8e9ea47 !important;
        }
    </style>
@stop

@section('content')
    <section class="product_category ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="section-header">@lang('fields.Projects')</h3>
                    <div class="row">
                        {{--<div class="col-lg-3 col-md-5">--}}
                            {{--<div class="category-list">--}}

                                {{--<form class="">--}}
{{--                                    {{csrf_field()}}--}}
                                    {{--<input class="form-control search-here" id="search-product" type="search" placeholder="@lang('fields.Search')" aria-label="Search">--}}
                                {{--</form>--}}
                                {{--<div class="nav flex-column nav-pills" id="v-pills-tab1" role="tablist" aria-orientation="vertical">--}}
                                    {{--@if(!empty($productCategories))--}}
                                        {{--@foreach($productCategories as $productCategory)--}}
                                            {{--<a class="nav-link" id="v-pills-{{$productCategory->logo}}-tab" data-toggle="pill" href="#v-pills-{{$productCategory->logo}}" role="tab" aria-controls="v-pills-{{$productCategory->logo}}" data-id="{{$productCategory->id}}" aria-selected="true">--}}
                                                {{--{{$productCategory->title}} <i class="icon-{{$productCategory->logo}}"></i>--}}
                                            {{--</a>--}}
                                        {{--@endforeach--}}
                                    {{--@endif--}}
                                    {{--<a href="{{url('categories')}}" class="view-all btn">@lang('fields.View All')</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="col-lg-12 col-md-12">
                            <div class="latest_news">
                                <div class="news-list">
                                    <ul>
                                        @if(count($projects)>0)
                                            @foreach($projects as $project)
                                                <li>
                                                    <div class="news-box">
                                                        <div class="news-content">
                                                            <div class="news-desc">
                                                                <h5>{{$project->title}}</h5>
                                                                <p class="long-desc">{!! nl2br(str_limit(strip_tags($project->description), 150)) !!}</p>
                                                                <a class="news_viewmore" href="{{url('projectDetail', ['id' => $project->id])}}">@lang('fields.Read More')</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        @else
                                            @lang('fields.NoProjectsAvailable')
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
