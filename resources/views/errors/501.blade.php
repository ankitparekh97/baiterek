@extends('frontend.frontend')

@section('content')
    <section class="htmlContent">
        <div class="container-fluid">
            <div class="row">
                <h3 class="page_heading">@lang('fields.somethingWentWrong')</h3>
            </div>
        </div>
    </section>
@endsection
