<input type="date" class="form-control" name="{{ $row->field }}"
       placeholder="{{ $row->display_name }}"
       value="@if(isset($dataTypeContent->{$row->field})){{ \Carbon\Carbon::parse(old($row->field, $dataTypeContent->{$row->field}))->format(App\Helper\Helper::getDateFormat('php')) }}@else{{old($row->field)}}@endif">
