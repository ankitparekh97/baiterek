@extends('voyager::master')
@section('page_title','User Profile')

@section('css')
    <style>
        .user-email {
            font-size: .85rem;
            margin-bottom: 1.5em;
        }
        .tooltip-inner{
            max-width: 100% !important;
            width: 100% !important;
        }
        strong {
            font-weight: bold !important;
            font-size: 14px !important;
        }
        .progress {
            height: 10px !important;
            margin-bottom: 0px !important;
        }

        .image-progress {
            height: 25px !important;
            margin-top: 15px !important;
        }

        .file-button {
            background-color: Transparent;
            background-repeat:no-repeat;
            border: none;
            cursor:pointer;
            overflow: hidden;
            outline:none;
        }
        ul.gender li {
            width: 25% !important;
            border-bottom: 0 !important;
        }
        .img-circle {
            border-radius: 50% !important;
        }
        .change-pic {
            padding-bottom: 10px;
        }
        #avImg{
            width: 100%;
        }
        .page-title {
            height: 0 !important;
        }
    </style>
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-person"></i>
        {{ __('My Profile') }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')

    <div class="page-content container-fluid">
        <form class="" role="form" id="frmProfile" action="{{ route('voyager.update') }}" method="POST" enctype="multipart/form-data" autocomplete="off">
            {{ csrf_field() }}

            <input type="hidden" name="address" value="Test">
            <input type="hidden" name="gender" value="Male">
            <input type="hidden" name="city" value="Test">
            <input type="hidden" name="postcode" value="000">

            <div class="row">
                <div class="col-md-12">
                    <div id="uploadPreview"></div>
                    <div id="uploadProgress" class="image-progress active progress-striped">
                        <div class="progress-bar progress-bar-success" style="width: 0;"></div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="panel panel-bordered">
                        <div class="panel-body">

                            @if(count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <input type="hidden" id="user_id" name="user_id" value="{{ (isset($dataTypeContent->id))?$dataTypeContent->id:'' }}">
                            <input type="hidden" id="role_id" name="role_id" value="{{ (isset($dataTypeContent->role_id))?$dataTypeContent->role_id:'' }}">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name">{{ __('First Name') }}</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="{{ __('First Name') }}" 
                                                value="{{ old('name',isset($dataTypeContent->name)?$dataTypeContent->name:'') }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group ">
                                        <label for="last_name">{{ __('Last Name') }}</label>
                                        <input type="text" class="form-control" id="last_name" name="last_name" placeholder="{{ __('Last Name') }}" 
                                                value="{{ old('last_name',isset($dataTypeContent->last_name)?$dataTypeContent->last_name:'') }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="phone">{{ __('Email') }}</label>
                                        <input type="email" class="form-control" id="email" name="email" readonly placeholder="{{ __('Email') }}" value="{{ isset($dataTypeContent->email)? $dataTypeContent->email : '' }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                {{--<div class="col-md-6">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="dob">{{__('Date Of Birth')}}</label>--}}
                                        {{--<div class='input-group date' id='dob'>--}}
                                        {{--<input type='text' autocomplete="off" class="form-control" name="dob" value="@if(!empty($dataTypeContent->dob)){{\App\Helper\Helper::getFormattedDate($dataTypeContent->dob) }}@else{{ old('dob') }}@endif " />--}}
                                            {{--<span class="input-group-addon">--}}
                                                {{--<span class="glyphicon glyphicon-calendar"></span>--}}
                                            {{--</span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="phone">{{ __('VAT no') }}</label>--}}
                                        {{--<input type="text" class="form-control" id="VAT_no" name="VAT_no" placeholder="{{ __('VAT_no') }}" value="{{ isset($dataTypeContent->VAT_no)? $dataTypeContent->VAT_no : '' }}">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-6">--}}
                                    {{--<label class="control-label" for="gender">Gender</label>--}}
                                    {{--<ul class="radio gender">--}}
                                        {{--<li>--}}
                                            {{--<input type="radio" class="form-check-input" id="gender_male" name="gender" value="Male" {{ isset($dataTypeContent->gender) ? $dataTypeContent->gender == "Male" ? 'checked': '' : old('gender') == "Male" ? 'checked': '' }}>--}}
                                            {{--<label for="gender_male">Male</label>--}}
                                            {{--<div class="check"></div>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<input type="radio" class="form-check-input" id="gender_female" name="gender" value="Female" {{ isset($dataTypeContent->gender) ? $dataTypeContent->gender == "Female" ? 'checked': '' : old('gender') == "Female" ? 'checked': '' }}>--}}
                                            {{--<label for="gender_female">Female</label>--}}
                                            {{--<div class="check"></div>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                            </div>
                            <div class="row password-status-container">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password">Password </label> <span style="color: red;">[Leave blank to use current]</span>
                                        <input id="password" name="password" type="password" class="form-control">
                                        <span style="font-style: italic; color: #000;">Password should be minimum 8 characters and must contain one upper case, one lowercase character, one digit and a special character</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="confirm_password">Confirm Password </label>
                                        <input id="confirm_password" name="confirm_password" type="password" class="form-control" {{ empty(old('password')) ? 'disabled' : '' }}>
                                        <span style="font-style: italic;color: #000;">Confirm password should be same as password</span>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">--}}
                                {{--<div class="col-md-6">--}}
                                    {{--<div class="form-group">--}}
                                       {{--<label for="address">Address </label>--}}
                                       {{--<textarea name="address" id="address" class="form-control" rows="2" cols="2" style="width: 100%">{{ isset($dataTypeContent->address)? $dataTypeContent->address : '' }}</textarea>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-6">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="address">Address 2</label>--}}
                                        {{--<textarea name="address2" id="address2" class="form-control" rows="2" cols="2" style="width: 100%">{{ isset($dataTypeContent->address2)? $dataTypeContent->address2 : '' }}</textarea>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="contact">Contact </label>
                                        <input name="contact" id="contact" class="form-control" value="{{ isset($dataTypeContent->contact)? $dataTypeContent->contact : '' }}">
                                    </div>
                                </div>
                                {{--<div class="col-md-4">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="city">City </label>--}}
                                        {{--<input name="city" id="city" class="form-control" value="{{ isset($dataTypeContent->city)? $dataTypeContent->city : '' }}">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="postcode">Post Code </label>--}}
                                        {{--<input name="postcode" id="postcode" class="form-control" value="{{ isset($dataTypeContent->postcode)? $dataTypeContent->postcode : '' }}">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary save">
                                        @if(isset($dataTypeContent->id)) Update @else{{ __('voyager::generic.save') }}@endif
                                    </button>
                                    <a class="btn btn-primary cancel" href="{{url('admin/')}}">
                                        Cancel
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel panel-bordered panel-warning">
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="update-pic">
                                    <div class="img-circle">
                                        <div class="change-pic">
                                            <button type="button" class="file-button" data-name="avatar" name="avatar" id="avatarImage">Change</button>
                                        </div>
                                        @if(isset($dataTypeContent->avatar))
                                            <img id="avImg" src="{{ filter_var($dataTypeContent->avatar, FILTER_VALIDATE_URL) ? $dataTypeContent->avatar : Voyager::image( $dataTypeContent->avatar ) }}" />
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <!-- Crop Image Modal -->
    <div class="modal fade modal-warning" id="confirm_crop_modal" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::media.crop_image') }}</h4>
                </div>

                <form id="myForm" action="{{ route('voyager.image-upload') }}" name="avatarUpload" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="crop-container">
                            <input type="hidden" id="user_id" name="user_id" value="{{ (isset($dataTypeContent->id))?$dataTypeContent->id:'' }}">
                            <input type="hidden" id="x" name="x">
                            <input type="hidden" id="y" name="y">
                            <input type="hidden" id="height" name="height">
                            <input type="hidden" id="width" name="width">
                            <input type="hidden" id="user_id" name="user_id" value="{{ (isset($dataTypeContent->id))?$dataTypeContent->id:'' }}">
                            <input type="file" class="hidden" data-name="avatar" name="avatar" onclick="this.value=null;" id="inputImage"/>
                            <img id="cropping-image" class="img img-responsive"/>
                        </div>
                        <div class="new-image-info">
                            {{ __('voyager::media.width') }} <span id="new-image-width"></span>, {{ __('voyager::media.height') }}<span id="new-image-height"></span>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                        <button type="submit" class="btn btn-warning" id="save_btn">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Crop Image Modal -->

@stop
@section('javascript')
    <script>
        $('document').ready(function () {
            $('#dob').datetimepicker({
                format: 'YYYY/MM/DD',
            });

            var date = new Date();
//            $('#dob').data("DateTimePicker").maxDate(date);
//            date.setFullYear(date.getFullYear() - 90);
//            $('#dob').data("DateTimePicker").minDate(date);

            $('body').on('keyup','#password', function () {
                var newPassword = $("#password").val();
                if(newPassword!=''){
                    $("#confirm_password").removeAttr('disabled');
                } else {
                    $("#confirm_password").attr('disabled','disabled');
                }
            })

            $(document).on('click', '#avatarImage', function () {
                $('#inputImage').trigger("click");
            });

            $(document).on('change', '#inputImage', function () {

                if (this.files && this.files[0]) {
                    if (this.files[0].type == "image/jpeg" || this.files[0].type == "image/png") {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $('#cropping-image').attr('src', e.target.result);
                        }
                        reader.readAsDataURL(this.files[0]);
                        $('#confirm_crop_modal').modal('show');
                    }
                    else {
                        toastr.error("Only image of type such as jpg/png/jpeg are allowed");
                    }
                }
            });

            // Cropper must init after the modal shown
            $('#confirm_crop_modal').on('shown.bs.modal', function (e) {
                var croppingImage = document.getElementById('cropping-image');
                cropper = new Cropper(croppingImage, {
                    aspectRatio: 1 / 1,
                    crop: function(e) {
                        document.getElementById('new-image-width').innerText = Math.round(e.detail.width) + 'px';
                        document.getElementById('new-image-height').innerText = Math.round(e.detail.height) + 'px';
                        $("#x").val(e.detail.x);
                        $("#y").val(e.detail.y);
                        $("#height").val(e.detail.height);
                        $("#width").val(e.detail.width);
                    }
                });
            });

            document.avatarUpload.addEventListener('submit', function(e) {
                e.preventDefault();
                var actionURL = this.action; // will get the form action url
                uploadFile(actionURL); // your upload event with request url
            });

        });

        function uploadFile(url) {
            var myForm = document.getElementById('myForm');
            var formdata = new FormData(myForm);
            var ajax = new XMLHttpRequest();
            ajax.upload.addEventListener("progress", progressHandler, false);
            ajax.responseType = 'json';
            ajax.open("post", url);
            ajax.onreadystatechange = function() {
                if (ajax.readyState == 4 && ajax.status == 200) {
                    if(ajax.response.success){
                        $('#avImg').attr('src', ajax.response.path);
                        $('.profile-img').attr('src', ajax.response.path);
                    } else {
                        toastr.error(ajax.response.message);
                    }
                    $('.save').prop('disabled',false);
                    $('.cancel').prop('disabled',false);
                }
            };
            ajax.send(formdata);
        }

        function progressHandler(event) {
            $('.save').prop('disabled',true);
            $('.cancel').prop('disabled',true);
            $('#uploadProgress').fadeIn();
            $('#confirm_crop_modal').modal('hide');
            var percent = (event.loaded / event.total) * 100;
            $('#uploadProgress .progress-bar').css('width', percent + '%');
            if(percent == 100){
                $('#uploadProgress').delay(1500).slideUp(function(){
                    $('#uploadProgress .progress-bar').css('width', '0%');
                });
            }
        }
    </script>
@stop