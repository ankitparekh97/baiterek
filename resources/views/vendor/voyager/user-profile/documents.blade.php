@extends('voyager::master')

@section('page_title', __('My Documents'))

@section('head')
    <?php app()->setLocale(\Session::get('locale')) ?>
    <link rel="stylesheet" href="{{ asset('kanban/jkanban.min.css') }}">
    <link rel="stylesheet" href="{{ asset('typeahade/css/tokenfield-typeahead.css') }}">
    <link rel="stylesheet" href="{{ asset('typeahade/css/bootstrap-tokenfield.css') }}">
    <script src="{{ asset('kanban/jkanban.js') }}"></script>
    <style>
        #myKanban {
            overflow-x: auto;
            padding: 20px 0;
        }

        .primary {
            background: #f99fff;
            color: white;
        }

        .success {
            background: #00b961;
            color: white;
        }

        .info {
            background: #2a92bf;
            color: white;
        }

        .warning {
            background: #f4ce46;
            color: white;
        }

        .error {
            background: #fb7d44;
            color: white;
        }
        .all_lable{
            background-color: #2a323a;
            color: #ffffff;
        }
        .kanban-container{
            min-width: 100% !important;
            height: auto;
        }
        .kanban-board{
            width: 418px !important;
            overflow-y: auto;
            height: 40pc;
            background: #ebecf0;
            border: 1px solid #d9d9cd;
        }
        .kanban-board header {
            color: white;
        }
        .text_align_m{
            white-space: normal;
            text-align: left !important;
        }
        .kanban-item {
            border-radius: 15px;
            box-shadow: 0 1px 0 rgba(9,30,66,.25);
        }
        .kanban-item p {
            color: #172b4d;
        }
        a.exportData, a.exportData:hover {
            color: #49Bc9C;
            font-weight: bold;
        }
        .kanban-board header {
            font-size: 15px;
            padding: 10px;
        }

        .lineThrough {
            text-decoration: line-through;
        }

    </style>
@endsection
@section('page_header')
    <div class="container-fluid">
        <div class="row cci-page-header">
            {{--<div class="row cci-page-header">--}}
            <h1 class="page-title">
                <i class="{{ $dataType->icon }}"></i>{{ __('content.documents_migrated') }}
                @if($dataType->description != '')
                    <a href="#" data-toggle="tooltip" data-placement="bottom" class="cci-tooltip"
                       title="{{ $dataType->description }}">
                        <i class="voyager-question"></i>
                    </a>
                @endif
            </h1>
            {{--@if(auth()->user()->hasPermission('browse_audit_trails'))--}}
            {{--<a target="_blank" href="{{ url('admin/audit-trails/'.\App\Helpers\Helper::getAuditSectionId($dataType->model_name)) }}" class="btn btn-info btn-audit-trial">--}}
            {{--<i class="voyager-archive"></i> <span>{{ __('Audit Trails') }}</span>--}}
            {{--</a>--}}
            {{--@endif--}}
            @can('add', app($dataType->model_name))
                <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
                    <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
                </a>
            @endcan
            @can('delete', app($dataType->model_name))
                @include('voyager::partials.bulk-delete')
            @endcan
            @can('edit', app($dataType->model_name))
                @if(isset($dataType->order_column) && isset($dataType->order_display_column))
                    <a href="{{ route('voyager.'.$dataType->slug.'.order') }}" class="btn btn-primary hidden">
                        <i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>
                    </a>
                @endif
            @endcan

            <?php app()->setLocale(\Session::get('locale')) ?>
            <div class="language-selector">
                <div class="btn-group btn-group-sm" role="group">
                    @foreach(config('voyager.multilingual.locales') as $lang)
                        <a class="btn btn-primary  {{ ( $lang === \Illuminate\Support\Facades\Session::get('locale')) ? " active" : "" }}" href="{{ url('locale/'.$lang) }}"> {{ strtoupper($lang)}} </a>
                    @endforeach
                </div>
            </div>

        </div>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <table class="table table-bordered" id="table">
                            <thead>
                            <tr>
                                @if(auth()->user()->role->slug == 'admin')
                                <th>{{ __('content.userName') }}</th>
                                @endif
                                <th>{{ __('content.product_name') }}</th>
                                <th>{{ __('content.fileName') }}</th>
                                <th>{{ __('content.fileDate') }}</th>
                                <th>{{ __('content.download_migrated') }}</th>
                                {{--<th>{{ __('content.action') }}</th>--}}
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('javascript')
    <script type="text/javascript" src="{{ asset('typeahade/bootstrap-tokenfield.js') }}" charset="UTF-8"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $(function() {
            $('#table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('voyager.'.$dataType->slug.'.browse') }}',
                    type: 'POST',
                    data: {
                        'lang': '{{ \Illuminate\Support\Facades\Session::get('locale') }}'
                    }
                },
                "language": {
                    "sEmptyTable":     "{{ __('data-table.sEmptyTable') }}",
                    "sInfo":           "{{ __('data-table.sInfo') }}",
                    "sInfoEmpty":      "{{ __('data-table.sInfoEmpty') }}",
                    "sInfoFiltered":   "{{ __('data-table.sInfoFiltered') }}",
                    "sInfoPostFix":    "{{ __('data-table.sInfoPostFix') }}",
                    "sInfoThousands":  "{{ __('data-table.sInfoThousands') }}",
                    "sLengthMenu":     "{{ __('data-table.sLengthMenu') }}",
                    "sLoadingRecords": "{{ __('data-table.sLoadingRecords') }}",
                    "sProcessing":     "{{ __('data-table.sProcessing') }}",
                    "sSearch":         "{{ __('data-table.sSearch') }}",
                    "sZeroRecords":    "{{ __('data-table.sZeroRecords') }}",
                    "oPaginate": {
                        "sFirst":    "{{ __('data-table.sFirst') }}",
                        "sLast":     "{{ __('data-table.sLast') }}",
                        "sNext":     "{{ __('data-table.sNext') }}",
                        "sPrevious": "{{ __('data-table.sPrevious') }}"
                    },
                    "oAria": {
                        "sSortAscending":  "{{ __('data-table.sSortAscending') }}",
                        "sSortDescending": "{{ __('data-table.sSortDescending') }}"
                    }
                },


                columns: [
                        @if(auth()->user()->role->slug == 'admin')
                    {data: 'user_id', name: 'user_id'},
                        @endif
                    {data: 'service_inquiry_id', name: 'service_inquiry_id'},
                    {data: 'name', name: 'name'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'path', name: 'path'},
//                    {data: 'action', name: 'action'},
                ]
            });
        });
    </script>
@stop
