@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

@section('page_header')
    <div class="container-fluid">
        <div class="row cci-page-header">
            {{--<div class="row cci-page-header">--}}
                <h1 class="page-title">
                    <i class="{{ $dataType->icon }}"></i> {{ $dataType->display_name_plural }}
                    @if($dataType->description != '')
                        <a href="#" data-toggle="tooltip" data-placement="bottom" class="cci-tooltip"
                           title="{{ $dataType->description }}">
                            <i class="voyager-question"></i>
                        </a>
                    @endif
                </h1>

                {{--@if(auth()->user()->hasPermission('browse_audit_trails'))--}}
                {{--<a target="_blank" href="{{ url('admin/audit-trails/'.\App\Helpers\Helper::getAuditSectionId($dataType->model_name)) }}" class="btn btn-info btn-audit-trial">--}}
                {{--<i class="voyager-archive"></i> <span>{{ __('Audit Trails') }}</span>--}}
                {{--</a>--}}
                {{--@endif--}}

                {{--<a href="{{ route('voyager.banner-masters') }}" class="btn btn-primary">--}}
                    {{--<i class="voyager-window-list"></i> <span>{{ __('Back to Banner Master') }}</span>--}}
                {{--</a>--}}
                @can('add', app($dataType->model_name))
                    <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
                        <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
                    </a>
                @endcan
                @can('delete', app($dataType->model_name))
                    @include('voyager::partials.bulk-delete')
                @endcan
                @can('edit', app($dataType->model_name))
                    @if(isset($dataType->order_column) && isset($dataType->order_display_column))
                        <a href="{{ route('voyager.'.$dataType->slug.'.order') }}" class="btn btn-primary hidden">
                            <i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>
                        </a>
                    @endif
                @endcan

                @include('voyager::multilingual.language-selector')

            {{--</div>--}}
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group col-md-2">
                    <label for="report_type">Banner Category:</label>
                    <select class="form-control banner_category" name="banner_category" id="banner_category">
                        <option value="" selected="selected">ALL</option>
                        @if(isset($bannerCategory) && count($bannerCategory) != 0)
                            @foreach($bannerCategory as $values)
                                <option value="{{$values->id}}">{{$values->title}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')

    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                @include('voyager::alerts')
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-bordered">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="dataTable" class="table table-hover" style="width: 100%;">
                                        <thead>
                                        <tr>
                                            <th class="nosort">
                                                @can('delete',app($dataType->model_name))
                                                    <input type="checkbox" class="select_all">
                                                @endcan
                                            </th>
                                            <th>Category</th>
                                            <th>Title</th>
                                            <th>Image</th>
                                            <th>Primary Text</th>
                                            <th>Order</th>
                                            <th class="actions text-right nosort">{{ __('voyager::generic.actions') }}</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr class="cci-filters">
                                            <th>&nbsp;</th>
                                            @foreach($dataType->browseRows as $row)
                                                <th> {{ $row->display_name }} </th>
                                            @endforeach
                                            <th class="actions text-right">&nbsp;</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->display_name_singular) }}?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop


@section('javascript')
    <!-- DataTables -->
    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
    @endif
    <script type="application/javascript">
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();

            $('.banner_category').select2();

            var dataTable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('voyager.'.$dataType->slug.'.browse') }}',
                    method: 'post',
                    data: function(data){
                        // Append to data
                        data.banner_category = $('#banner_category').val();
                    }
                },
                columnDefs: [ {"targets": 'nosort',"orderable": false}],
                columns: [
                    {"data":"id","name":"id","searchable":false},
                    {"data":"banner_masters_id","name":"banner_masters_id","searchable":false},
                    {"data":"title","name":"title","searchable":true},
                    {
                        "name": "image",
                        "data": "image",
                        "render": function (data, type, full, meta) {
                            return "<img src=\"" + data + "\" height=\"50\"/>";
                        },
                        "title": "Banner",
                        "orderable": true,
                        "searchable": true
                    },
                    {"data":"primary_text","name":"primary_text","searchable":true},
                    {"data":"order","name":"order","searchable":true},
                    {"data":"action","name":"action","searchable":false},
                ],
                order : {!! json_encode($orderColumn) !!},
                pageLength: 25,
                initComplete: function () {

                },
                fnDrawCallback: function () {
                    $('#dataTable tbody tr').each(function () {
                        $(this).find('td:last').attr('id', 'bread-actions');
                    });
                },
//                dom: '<"top"l<"#cciAction.cci-action text-right">>rt<"bottom"ip<"clear">>'
            });

            @if ($isModelTranslatable)
            $('.side-body').multilingual();
            //Reinitialise the multilingual features when they change tab
            $('#dataTable').on('draw.dt', function(){
                $('.side-body').data('multilingual').init();
            })
            @endif
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });

            $('body').on('change','.banner_category',function(e){
                dataTable.draw();
            });
        });


        var deleteFormAction;
        $(document).on('click', 'a.delete', function (e) {
            e.preventDefault();
            $('#delete_form')[0].action = '{{ route('voyager.'.$dataType->slug.'.destroy', ['id' => '__id']) }}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });

    </script>
@stop
