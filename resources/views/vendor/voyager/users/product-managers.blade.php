@extends('voyager::master')
@section('page_title', __('voyager::generic.viewing').' Product Managers')
@section('page_header')
    <div class="container-fluid">
        <div class="row cci-page-header">
            {{--<div class="row cci-page-header">--}}
            <h1 class="page-title">
                <i class="voyager-people"></i> {{ __('portal.product_manager.product managers') }}
            </h1>

            <a href="{{ route('voyager.product-managers.create') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('portal.product_manager.add new') }}</span>
            </a>
            {{--@can('delete', app($dataType->model_name))--}}
                {{--@include('voyager::partials.bulk-delete')--}}
            {{--@endcan--}}
            {{--@can('edit', app($dataType->model_name))--}}
                {{--@if(isset($dataType->order_column) && isset($dataType->order_display_column))--}}
                    {{--<a href="{{ route('voyager.'.$dataType->slug.'.order') }}" class="btn btn-primary hidden">--}}
                        {{--<i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>--}}
                    {{--</a>--}}
                {{--@endif--}}
            {{--@endcan--}}
{{--            @include('voyager::multilingual.language-selector')--}}
            {{--</div>--}}
        </div>
    </div>
@stop

@section('content')
    <style>
        .p-r-3 {
            padding-right: 20px;
        }
    </style>
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="text-right p-r-3">
                        </div>
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" class="select_all">
                                    </th>
                                    <th>{{ __('portal.product_manager.first name') }}</th>
                                    <th>{{ __('portal.product_manager.last name') }}</th>
                                    <th>{{ __('portal.product_manager.gender') }}</th>
                                    <th>{{ __('portal.product_manager.email') }}</th>
                                    <th>{{ __('portal.product_manager.contact') }}</th>
                                    <th class="actions nosort text-center">{{ __('portal.product_manager.actions') }}</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ __('user') }}?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        @if(!empty($menuView) || $menuView == 'ProductManager' )
                            <input type="hidden" name="PM" value="1">
                        @endif
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@stop


@section('javascript')

    <script>
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();

            $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('voyager.product-managers.browse') }}',
                    method: 'POST'
                },
                columnDefs: [ {"targets": 'nosort',"orderable": false}],
                columns: [
                    {"data":"id","name":"id","searchable":false},
                    {"data":"name","name":"name","searchable":true},
                    {"data":"last_name","name":"last_name","searchable":true},
                    {"data":"gender","name":"gender","searchable":true},
                    {"data":"email","name":"email","searchable":true},
                    {"data":"contact","name":"contact","searchable":true},
                    {"data":"action","name":"action","searchable":false}
                ],
                "language": {
                "sEmptyTable":     "{{ __('data-table.sEmptyTable') }}",
                    "sInfo":           "{{ __('data-table.sInfo') }}",
                    "sInfoEmpty":      "{{ __('data-table.sInfoEmpty') }}",
                    "sInfoFiltered":   "{{ __('data-table.sInfoFiltered') }}",
                    "sInfoPostFix":    "{{ __('data-table.sInfoPostFix') }}",
                    "sInfoThousands":  "{{ __('data-table.sInfoThousands') }}",
                    "sLengthMenu":     "{{ __('data-table.sLengthMenu') }}",
                    "sLoadingRecords": "{{ __('data-table.sLoadingRecords') }}",
                    "sProcessing":     "{{ __('data-table.sProcessing') }}",
                    "sSearch":         "{{ __('data-table.sSearch') }}",
                    "sZeroRecords":    "{{ __('data-table.sZeroRecords') }}",
                    "oPaginate": {
                    "sFirst":    "{{ __('data-table.sFirst') }}",
                        "sLast":     "{{ __('data-table.sLast') }}",
                        "sNext":     "{{ __('data-table.sNext') }}",
                        "sPrevious": "{{ __('data-table.sPrevious') }}"
                },
                "oAria": {
                    "sSortAscending":  "{{ __('data-table.sSortAscending') }}",
                        "sSortDescending": "{{ __('data-table.sSortDescending') }}"
                }
            },

                pageLength: 10,
                initComplete: function () {
//                    cciInitComplete(this.api().columns());
                }
            });

        });

        var deleteFormAction;
        $(document).on('click', 'a.delete', function (e) {
            $('#delete_form')[0].action = '{{ route('voyager.product-managers.destroy', ['id' => '__id']) }}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });

    </script>
@stop
