@extends('voyager::master')
<?php app()->setLocale(\Session::get('locale')) ?>

@if(!empty($menuView) || $menuView == 'ProductManager' )
    @section('page_title', __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' Product Manager')
@else
    @section('page_title', __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))
@endif

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop
@php
$supervisor_role = Config::get('baiterek.roles.supervisor');
$addEdit = (empty($dataTypeContent->id) ? 'add' : 'edit');
@endphp
@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        @if(!empty($menuView) || $menuView == 'ProductManager' )
            {{ __('portal.product-managers.product-managers_'.(isset($dataTypeContent->id) ? 'edit' : 'add')) }}
        @else
{{--            {{ __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}--}}
            {{ __('portal.users.'.(isset($dataTypeContent->id) ? 'edit' : 'add').'_'.str_replace(' ', '_', strtolower($dataType->display_name_plural))) }}
        @endif
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
            @if(!empty($menuView) || $menuView == 'ProductManager' )
                <form class="form-edit-add" role="form"
                      action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.product-managers.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.product-managers.store') }}@endif"
                      method="POST" enctype="multipart/form-data" autocomplete="off">
            @else
                <form class="form-edit-add" role="form"
                      action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                      method="POST" enctype="multipart/form-data" autocomplete="off">
            @endif

            <!-- PUT Method if we are editing -->
            @if(isset($dataTypeContent->id))
                {{ method_field("PUT") }}
            @endif
            {{ csrf_field() }}

                <input type="hidden" name="address" value="Test">
                <input type="hidden" name="gender" value="Male">
                <input type="hidden" name="city" value="Test">
                <input type="hidden" name="VAT_no" value="{{uniqid()}}">
                <input type="hidden" name="postcode" value="000">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">
                        {{-- <div class="panel"> --}}
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="panel-body">
                            <div class="form-group">
                                <label for="name">{{ __('portal.users.name') }}</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="{{ __('portal.users.name') }}"
                                       value="{{ old('name', $dataTypeContent->name ?? '') }}">
                            </div>

                            <div class="form-group">
                                <label for="last_name">{{ __('portal.users.last_name') }}</label>
                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="{{ __('portal.users.last_name') }}"
                                       value="{{ old('last_name', $dataTypeContent->last_name ?? '') }}">
                            </div>

                            <div class="form-group">
                                <label for="email">{{ __('portal.users.email') }}</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="{{ __('portal.users.email') }}"
                                       value="{{ old('email', $dataTypeContent->email ?? '') }}">
                            </div>

                            @if(!empty($menuView) || $menuView == 'ProductManager' )
                                <input type="hidden" name="role_id" value="{{ (!empty($dataTypeContent->role_id) ? $dataTypeContent->role_id : $productManagerRole->id) }}">
                            @else
                                @can('editRoles', $dataTypeContent)
                                    <div class="form-group">
                                        <label for="default_role">{{ __('voyager::profile.role_default') }}</label>
                                        @php
                                            $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};
                                            $row     = $dataTypeRows->where('field', 'user_belongsto_role_relationship')->first();
                                            $options = $row->details;
                                        @endphp
                                        @include('voyager::formfields.relationship')
                                    </div>
                                    <div class="form-group">
                                        <label for="additional_roles">{{ __('voyager::profile.roles_additional') }}</label>
                                        @php
                                            $row     = $dataTypeRows->where('field', 'user_belongstomany_role_relationship')->first();
                                            $options = $row->details;
                                            $model = app($options->model);
                                            $query = $model::get();
                                        @endphp
                                        <select class="form-control select2" name="user_belongstomany_role_relationship[]" multiple>
                                            @foreach($query as $relationshipData)
                                                @if(isset($user_roles) && count($user_roles) != 0)
                                                    @foreach($user_roles as $user_role)
                                                        <option value="{{ $relationshipData->{$options->key} }}" @if(old('user_belongstomany_role_relationship') !== null) @if(in_array($relationshipData->{$options->key}, old('user_belongstomany_role_relationship'))){{ 'selected="selected"' }}@endif @else @if($user_role->role_id == $relationshipData->{$options->key}){{ 'selected="selected"' }}@endif @endif>{{ $relationshipData->{$options->label} }}</option>
                                                    @endforeach
                                                @else
                                                    <option value="{{ $relationshipData->{$options->key} }}" @if(old('user_belongstomany_role_relationship') !== null) @if(in_array($relationshipData->{$options->key}, old('user_belongstomany_role_relationship'))){{ 'selected="selected"' }}@endif @endif>{{ $relationshipData->{$options->label} }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                @endcan
                            @endif

                            <div class="form-group subsidiaryList">
                                <label for="report_type">{{ __('portal.subsidiaries.Еншілес_ұйымдар') }}: </label>
                                <select class="form-control" name="subsidiary_id" id="subsidiary_id">
                                    {{--<option value="" selected="selected">{{ __('portal.product.all') }} </option>--}}
                                    @if(isset($subsidiaries) && count($subsidiaries) != 0)
                                        @foreach($subsidiaries as $values)
                                            <option value="{{$values->id}}" @if(isset($subsidiaryUser->subsidiary_id) && $subsidiaryUser->subsidiary_id == $values->id) selected="selected" @endif>{{$values->title}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                            {{--<div class="form-group">--}}
                                {{--<label for="address">{{ __('Address') }}</label>--}}
                                {{--<textarea name="address" id="address" class="form-control" rows="2" cols="2">{{ old('address', $dataTypeContent->address ?? '') }}</textarea>--}}
                            {{--</div>--}}
                            <div class="form-group">
                                <label for="contact">{{ __('portal.users.contact') }}</label>
                                <input type="text" class="form-control" id="contact" name="contact" placeholder="{{ __('portal.users.contact') }}"
                                       value="{{ old('contact', $dataTypeContent->contact ?? '') }}">
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<label for="city">{{ __('City') }}</label>--}}
                                {{--<input type="text" class="form-control" id="city" name="city" placeholder="{{ __('City') }}"--}}
                                       {{--value="{{ old('city', $dataTypeContent->city ?? '') }}">--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="post_code">{{ __('Post Code') }}</label>--}}
                                {{--<input type="text" class="form-control" id="postcode" name="postcode" placeholder="{{ __('Post Code') }}"--}}
                                       {{--value="{{ old('postcode', $dataTypeContent->postcode ?? '') }}">--}}
                            {{--</div>--}}
                            {{--<div class="form-group col-md-12 ">--}}
                                {{--<label class="control-label" for="name">Gender</label>--}}
                                {{--<ul class="radio">--}}
                                    {{--<li>--}}
                                        {{--<input type="radio" id="gender_male" name="gender" value="Male" {{ isset($dataTypeContent->gender) ? $dataTypeContent->gender == "Male" ? 'checked': '' : old('gender') == "Male" ? 'checked': '' }}>--}}
                                        {{--<label for="gender_male">Male</label>--}}
                                        {{--<div class="check"></div>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<input type="radio" id="gender_female" name="gender" value="Female" {{ isset($dataTypeContent->gender) ? $dataTypeContent->gender == "Female" ? 'checked': '' : old('gender') == "Female" ? 'checked': '' }}>--}}
                                        {{--<label for="gender_female">Female</label>--}}
                                        {{--<div class="check"></div>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="VAT_no">{{ __('VAT No') }}</label>--}}
                                {{--<input type="text" class="form-control" id="VAT_no" name="VAT_no" placeholder="{{ __('VAT No') }}"--}}
                                       {{--value="{{ old('VAT_no', $dataTypeContent->VAT_no ?? '') }}">--}}
                            {{--</div>--}}
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary save">
                                        @if(isset($dataTypeContent->id)) Update @else{{ __('voyager::generic.save') }}@endif
                                    </button>
                                </div>
                            </div>
                            @php
                                if (isset($dataTypeContent->locale)) {
                                    $selected_locale = $dataTypeContent->locale;
                                } else {
                                    $selected_locale = config('app.locale', 'kz');
                                }
                            @endphp

                        </div>
                    </div>
                </div>
        </form>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
        </form>
    </div>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {

            $('.toggleswitch').bootstrapToggle();
            $('input[name=role_id]').select2();
            if('{{$addEdit}}' == 'edit'){
                if($("select[name=role_id] option:selected").text().toLowerCase() == '{{$supervisor_role}}'){
                    $('.subsidiaryList').show();
                    $('#subsidiary_id').select2();
                } else {
                    $('.subsidiaryList').hide();
                }

            } else {
                $('.subsidiaryList').hide();
            }

            var role;
            $('body').on('change','select[name=role_id]',function(e){
                role = $("select[name=role_id] option:selected").text().toLowerCase();
                if(role == '{{$supervisor_role}}'){

                    $('.subsidiaryList').show();
                    $('#subsidiary_id').select2();
                } else {
                    $('.subsidiaryList').hide();
                }
            });

        });
    </script>
@stop
