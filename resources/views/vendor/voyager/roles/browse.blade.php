@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

@section('page_header')
    <?php app()->setLocale(\Session::get('locale')) ?>
    <div class="container-fluid">
        <div class="row cci-page-header">
            {{--<div class="row cci-page-header">--}}
            <h1 class="page-title">
                <i class="{{ $dataType->icon }}"></i> {{ __('portal.roles.'.str_replace(' ', '_', strtolower($dataType->display_name_plural))) }}
                @if($dataType->description != '')
                    <a href="#" data-toggle="tooltip" data-placement="bottom" class="cci-tooltip"
                       title="{{ $dataType->description }}">
                        <i class="voyager-question"></i>
                    </a>
                @endif
            </h1>

            {{--@if(auth()->user()->hasPermission('browse_audit_trails'))--}}
            {{--<a target="_blank" href="{{ url('admin/audit-trails/'.\App\Helpers\Helper::getAuditSectionId($dataType->model_name)) }}" class="btn btn-info btn-audit-trial">--}}
            {{--<i class="voyager-archive"></i> <span>{{ __('Audit Trails') }}</span>--}}
            {{--</a>--}}
            {{--@endif--}}

            @can('add', app($dataType->model_name))
                <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
                    <i class="voyager-plus"></i> <span>{{ __('portal.roles.add_new') }}</span>
                </a>
            @endcan
            @can('delete', app($dataType->model_name))
                {{--                    @include('voyager::partials.bulk-delete')--}}
                @include('vendor.voyager.partials.bulk-delete')
            @endcan
            @can('edit', app($dataType->model_name))
                @if(isset($dataType->order_column) && isset($dataType->order_display_column))
                    <a href="{{ route('voyager.'.$dataType->slug.'.order') }}" class="btn btn-primary hidden">
                        <i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>
                    </a>
                @endif
            @endcan
            @include('voyager::multilingual.language-selector')
            {{--</div>--}}

        </div>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                <tr>
                                    <th class="nosort">
                                        @can('delete',app($dataType->model_name))
                                            <input type="checkbox" class="select_all">
                                        @endcan
                                    </th>
                                    @foreach($dataType->browseRows as $row)
                                        <th>
                                            {{ __('portal.roles.'.$row->display_name) }}
                                        </th>
                                    @endforeach
                                    <th class="actions text-right nosort">
                                        {{ __('portal.roles.actions_table') }}
                                    </th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr class="cci-filters">
                                    <th>&nbsp;</th>
                                    @foreach($dataType->browseRows as $row)
                                        <th> {{ $row->display_name }} </th>
                                    @endforeach
                                    <th class="actions text-right">&nbsp;</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('portal.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('portal.delete_question') }} {{ strtolower($dataType->display_name_singular) }}?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('portal.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('portal.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop


@section('javascript')
    <!-- DataTables -->
    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
    @endif
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();

            $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('voyager.'.$dataType->slug.'.browse') }}',
                    method: 'POST'
                },
                data: {
                    'lang': '{{ \Illuminate\Support\Facades\Session::get('locale') }}'
                },
                "language": {
                    "sEmptyTable":     "{{ __('data-table.sEmptyTable') }}",
                    "sInfo":           "{{ __('data-table.sInfo') }}",
                    "sInfoEmpty":      "{{ __('data-table.sInfoEmpty') }}",
                    "sInfoFiltered":   "{{ __('data-table.sInfoFiltered') }}",
                    "sInfoPostFix":    "{{ __('data-table.sInfoPostFix') }}",
                    "sInfoThousands":  "{{ __('data-table.sInfoThousands') }}",
                    "sLengthMenu":     "{{ __('data-table.sLengthMenu') }}",
                    "sLoadingRecords": "{{ __('data-table.sLoadingRecords') }}",
                    "sProcessing":     "{{ __('data-table.sProcessing') }}",
                    "sSearch":         "{{ __('data-table.sSearch') }}",
                    "sZeroRecords":    "{{ __('data-table.sZeroRecords') }}",
                    "oPaginate": {
                        "sFirst":    "{{ __('data-table.sFirst') }}",
                        "sLast":     "{{ __('data-table.sLast') }}",
                        "sNext":     "{{ __('data-table.sNext') }}",
                        "sPrevious": "{{ __('data-table.sPrevious') }}"
                    },
                    "oAria": {
                        "sSortAscending":  "{{ __('data-table.sSortAscending') }}",
                        "sSortDescending": "{{ __('data-table.sSortDescending') }}"
                    }
                },
                columnDefs: [ {"targets": 'nosort',"orderable": false}],
                columns: {!! \App\Helper\Helper::getBrowseColumns($dataType) !!},
                order : {!! json_encode($orderColumn) !!},
                pageLength: 25,
                initComplete: function () {
//                    cciInitComplete(this.api().columns());
                },
                fnDrawCallback: function () {
                    $('#dataTable tbody tr').each(function () {
                        $(this).find('td:last').attr('id', 'bread-actions');
                    });
                }
//                dom: '<"top"l<"#cciAction.cci-action text-right">>rt<"bottom"ip<"clear">>'
            });

            @if ($isModelTranslatable)
            $('.side-body').multilingual();
            //Reinitialise the multilingual features when they change tab
            $('#dataTable').on('draw.dt', function(){
                $('.side-body').data('multilingual').init();
            })
            @endif
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });
        });


        var deleteFormAction;
        $(document).on('click', 'a.delete', function (e) {
            $('#delete_form')[0].action = '{{ route('voyager.'.$dataType->slug.'.destroy', ['id' => '__id']) }}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });
    </script>
@stop
