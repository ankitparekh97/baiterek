@extends('voyager::master')

@section('page_title', __('voyager::generic.view').' '.$dataType->display_name_singular)

@section('page_header')
    <style>
        .cursor{
            cursor: pointer;
        }
        .statusReject{
            background-color: #ffb3b3 !important;
            padding: 20px 25px;
            color: black;
            margin-top: 15px;
            border-radius: 10px;
        }
        .statusAccept{
            background-color: #43d17f !important;
            padding: 20px 25px;
            color: black;
            margin-top: 15px;
            border-radius: 10px;
        }
        .inquirySend{
            background-color: #eaeaea;
            padding: 20px 25px;
            color: black;
            margin-top: 15px;
            border-radius: 10px;
        }
        .comment{
            background-color: wheat;
            padding: 20px 25px;
            color: black;
            margin-top: 15px;
            border-radius: 10px;
        }
    </style>
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> {{ __('portal.service_inquiry.viewing_service_inquiry') }}

        {{--@can('edit', $dataTypeContent)--}}
            {{--<a href="{{ route('voyager.'.$dataType->slug.'.edit', $dataTypeContent->getKey()) }}" class="btn btn-info">--}}
                {{--<span class="glyphicon glyphicon-pencil"></span>&nbsp;--}}
                {{--{{ __('voyager::generic.edit') }}--}}
            {{--</a>--}}
        {{--@endcan--}}
{{--        @can('delete', $dataTypeContent)--}}
            {{--<a href="javascript:;" title="{{ __('voyager::generic.delete') }}" class="btn btn-danger" data-id="{{ $dataTypeContent->getKey() }}" id="delete-{{ $dataTypeContent->getKey() }}">--}}
                {{--<i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">{{ __('voyager::generic.delete') }}</span>--}}
            {{--</a>--}}
        {{--@endcan--}}

        <a href="{{ route('voyager.'.$dataType->slug.'.index') }}" class="btn btn-warning">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            {{ __('portal.service_inquiry.return_to_list') }}
        </a>
    </h1>
    <?php app()->setLocale(\Session::get('locale')) ?>
    <div class="language-selector">
        <div class="btn-group btn-group-sm" role="group">
            @foreach(config('voyager.multilingual.locales') as $lang)
                <a class="btn btn-primary  {{ ( $lang === \Illuminate\Support\Facades\Session::get('locale')) ? " active" : "" }}" href="{{ url('locale/'.$lang) }}"> {{ strtoupper($lang)}} </a>
            @endforeach
        </div>
    </div>
@stop

@section('content')
    <style>
        .permissions-table {
            width: 75% !important;
        }
        a.exportData, a.exportData:hover {
            color: #49Bc9C;
            font-weight: bold;
        }
        #voyager-loader {
            background: transparent !important;
        }
    </style>
    <div class="page-content read container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered" style="padding-bottom:5px;">
                    <!-- form start -->


                                <div class="modal-header">
                                    <h4 class="modal-title" id="k-m-title">{!! $productTitle !!}</h4>
                                    {{--<h4 class="modal-title" id="k-m-title">{{ trans('content.ServiceInquiryDetail') }}</h4>--}}
                                </div>
                                <div class="modal-body">
                                    <div class="dataExport" align="center">
                                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i> <a href="{{ route('voyager.serviceInquiries.exportServicePdf') }}?type=pdf&id={{ $id }}" class="exportData" id="pdfClick" target="_blank">{{ trans('content.ExportToPDF') }}</a> |
                                        <i class="fa fa-file-excel-o" aria-hidden="true"></i> <a href="{{ route('voyager.serviceInquiries.exportServicePdf') }}?type=csv&id={{ $id }}" class="exportData" target="_blank" id="csvClick">{{ trans('content.ExportToCSV') }}</a>
                                    </div>
                                    <div id="content_detail">
                                        {!! $formHtml !!}
                                    </div>
                                    @if(auth()->user()->role->slug != 'customer')
                                        <hr>
                                        <div class="form-group form-inline">
                                            <div class="col-md-1" style="padding: 0; margin: 0;">
                                                <img src="{{ asset('storage/users/default.png') }}" style="border-radius: 50%;width: 40px;">
                                            </div>
                                            <div class="col-md-11" style="padding: 0; margin: 0;">
                                                <input type="hidden" id="msgBoxId" value="{{ $id }}">
                                                <textarea class="form-control" id="messageBox" placeholder="{{ __('content.write_a_message') }}" dir="auto" style="height: 40px;width: 100%; cursor: pointer"></textarea>
                                                <button type="button" id="saveBtn" class="btn btn-success pull-right">{{ trans('content.save') }}</button>
                                            </div>
                                        </div>
                                        <div class="col-md-12" style="margin:5px; padding: 2px;border: 1px solid #e4eaec;">
                                            <form enctype="multipart/form-data" method="post" action="{{ url('CustomerFile') }}">
                                                @csrf
                                                <input type="hidden" value="{{ $id }}" name="data_id">
                                                <div class="col-md-6">
                                                    <span class="form-control" style="border: none;">Document for customer</span>
                                                    <input multiple id="document_customer" name="document_customer[]" type="file" class="form-control" style="border: none;">
                                                </div>
                                                <div class="col-md-6">
                                                    <button type="submit" id="uploadBtn" style="float: right" class="btn btn-success">{{ trans('content.upload') }}</button>
                                                </div>
                                            </form>
                                        </div>
                                    @endif
                                    <div id="content_body">{!! $activityHtml !!}</div>
                                </div>

                </div>
            </div>
        </div>
    </div>

@stop

@section('javascript')
    @if ($isModelTranslatable)
        <script>
            // $("#messageBox").focusout(function(){
            //     $('#saveBtn').hide();
            // });
            // $('#messageBox').click(function(){
            //     $('#saveBtn').show();
            // });
            $(document).ready(function () {
                $('.side-body').multilingual();
            });
        </script>
        <script src="{{ voyager_asset('js/multilingual.js') }}"></script>
    @endif
    <script>
        @if(auth()->user()->role->slug != 'customer')
        $('#saveBtn').click(function(){
            // $('#saveBtn').hide();
            var message = $('#messageBox').val();
            $.ajax({
                url: "{{ route('voyager.'.$dataType->slug.'.ajaxMessageActivity') }}",
                type: 'POST',
                data: {
                    'message': message,
                    'data_id': $('#msgBoxId').val(),
                    'lang': '{{ \Illuminate\Support\Facades\Session::get('locale') }}',
                },
                success: function (data) {
                    toastr.clear();
                    if (data.status === true) {
                        $('#activityList').prepend(data.html);
                        $('#messageBox').val('');
                    } else {
                        toastr.warning(data.message);
                    }
                },
                error: function () {
                    console.log('request error');
                    toastr.error("Oops something went wrong!");
                }
            });
        });

        $('body').on('change','.checkPoint',function(){
            id = $(this).attr('data-id');
            if($(this).prop( "checked")){
                checkPointStatus("{{ \App\Models\ServiceInquiryCheckPoint::TRUE }}",id);
                $(this).next().addClass('lineThrough')
            }else{
                checkPointStatus("{{ \App\Models\ServiceInquiryCheckPoint::FALSE }}",id);
                $(this).next().removeClass('lineThrough')
            }
        });

        $('body').on('change','.checkPointComment',function(){
            comment = 'null';
            if($(this).val() !== ''){
                comment = $(this).val();
            }
            $.ajax({
                url: "{{ route('voyager.'.$dataType->slug.'.checkPointComment') }}",
                type: 'POST',
                data: {
                    'data_id': $(this).attr('data-id'),
                    'comment' : comment,
                    'lang': '{{ \Illuminate\Support\Facades\Session::get('locale') }}',
                },
                success: function (data) {
                    toastr.clear();
                    if (data.status === true) {
                        $('#activityList').prepend(data.html);
                    } else {
                        console.log(data.message);
                    }
                },
                error: function () {
                    console.log('request error');
                    toastr.error("Oops something went wrong!");
                }
            });
        });
        var documentSendArray = [];
        $('body').on('click','.inviteButton',function(){
            documentSendArray = [];
            $("input[name='document_send[]']:checked").each(function (key, value) {
                documentSendArray.push($(this).val());
            });
            var emails = $('.inviteUsers').val();
            $.ajax({
                url: "{{ route('voyager.'.$dataType->slug.'.ajaxInviteUsers') }}",
                async:false,
                type: 'POST',
                data: {
                    'emails': emails,
                    'data_id': $(this).attr('data-id'),
                    'documentsSend' : documentSendArray,
                    'lang': '{{ \Illuminate\Support\Facades\Session::get('locale') }}',
                },
                success: function (data) {
                    $('#voyager-loader').hide();
                    $("input[name='document_send[]']:checked").each(function (key, value) {
                        $(this).prop('checked',false);
                    });
                    $('.inviteUsers').val('');
                    toastr.clear();
                    if (data.status === true) {
                        $('#activityList').prepend(data.html);
                        toastr.success(data.message);
                    } else {
                        toastr.warning(data.message);
                        console.log(data.message);
                    }
                },
                error: function () {
                    console.log('request error');
                    toastr.error("Oops something went wrong!");
                }
            });
        });
        @endif

        function checkPointStatus(status, id){
            $.ajax({
                url: "{{ route('voyager.'.$dataType->slug.'.checkPointStatus') }}",
                type: 'POST',
                data: {
                    'data_id': id,
                    'status': status,
                    'lang': '{{ \Illuminate\Support\Facades\Session::get('locale') }}',
                },
                success: function (data) {
                    console.log(data);
                    toastr.clear();
                    if (data.status === true) {
                        $('#activityList').prepend(data.html);
                    } else {
                        console.log(data.message);
                    }
                },
                error: function () {
                    console.log('request error');
                    toastr.error("Oops something went wrong!");
                }
            });
        }

        $(document).on('click','#applyNow',function(){
            var form_fields = JSON.stringify(formRender.userData);
            $('#formFields').val(form_fields);
            $('#status_of_request').val("{{ \App\Models\ServiceInquiries::APPLICATION_RECEIVED }}");
            if ($('#apply').valid()) {
                $('form').submit();
            }
            else {
                return false;
            }
        });
        {{--$('body').on('change', '#document_customer', function(e){--}}
            {{--var fileName = e.target.files[0].name;--}}
{{--//            var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];--}}
            {{--var fileExtension = ['jpeg', 'jpg', 'png', 'pdf'];--}}
            {{--if ($.inArray(fileName.split('.').pop().toLowerCase(), fileExtension) == -1) {--}}
                {{--alert("{{ __('portal.only_formats_are_allowed') }} : "+fileExtension.join(', '));--}}
                {{--$(e.target).val(null)--}}
            {{--}--}}
        {{--});--}}
    </script>
@stop
