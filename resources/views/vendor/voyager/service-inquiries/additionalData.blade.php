@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

@section('page_header')
    <div class="container-fluid">
        <div class="row cci-page-header">
            {{--<div class="row cci-page-header">--}}
                <h1 class="page-title">
                    <i class="{{ $dataType->icon }}"></i> {{ $dataType->display_name_plural }}
                    @if($dataType->description != '')
                        <a href="#" data-toggle="tooltip" data-placement="bottom" class="cci-tooltip"
                           title="{{ $dataType->description }}">
                            <i class="voyager-question"></i>
                        </a>
                    @endif
                </h1>
                {{--@if(auth()->user()->hasPermission('browse_audit_trails'))--}}
                {{--<a target="_blank" href="{{ url('admin/audit-trails/'.\App\Helpers\Helper::getAuditSectionId($dataType->model_name)) }}" class="btn btn-info btn-audit-trial">--}}
                {{--<i class="voyager-archive"></i> <span>{{ __('Audit Trails') }}</span>--}}
                {{--</a>--}}
                {{--@endif--}}
                @can('add', app($dataType->model_name))
                    <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
                        <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
                    </a>
                @endcan
                @can('delete', app($dataType->model_name))
                    @include('voyager::partials.bulk-delete')
                @endcan
                @can('edit', app($dataType->model_name))
                    @if(isset($dataType->order_column) && isset($dataType->order_display_column))
                        <a href="{{ route('voyager.'.$dataType->slug.'.order') }}" class="btn btn-primary hidden">
                            <i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>
                        </a>
                    @endif
                @endcan
            {{--</div>--}}
        </div>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->display_name_singular) }}?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    {{-- Documents download modal --}}
    <div class="modal modal-success fade" tabindex="-1" id="documents_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><i class="voyager-download"></i> @lang('content.documents')</h4>
                </div>
                <div class="modal-body" id="content_body">

                </div>
                <div class="modal-footer" style="margin-top: 10px">
                    <button type="button" class="btn btn-default pull-right"
                            data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop


@section('javascript')
    <!-- DataTables -->
    <script src="https://use.fontawesome.com/eefee82593.js"></script>
    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
    @endif
    <script>
        $(document).ready(function () {
        });
    </script>
@stop
