<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="_url" content="{{ URL('') }}">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title>{{ Voyager::setting("site.title") }}</title>
    <style>
        @font-face {
            font-family: 'ru';
            font-weight: normal;
            font-style: normal;
            font-variant: normal;
            src: url("{{ storage_path('fonts/ru.ttf') }}") format("truetype");
        }
        body {
            font-family: ru;
        }
    </style>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <center>
                <h3>Application details for {{(auth()->user()->role->slug=='customer') ? $serviceInquiry->product->title : $serviceInquiry->user->name .' '. $serviceInquiry->user->last_name}} </h3>
            </center>
            <br>

            <div class="table-responsive">
                <table class="table table-striped table-hover table-condensed">
                    <thead>
                    @php
                        $fields = json_decode($serviceInquiry->fields)
                    @endphp
                    @foreach($fields as $index => $inquiry)
                        @if(isset($inquiry->userData))
                            <tr>
                                <td><strong>{{ isset($inquiry->userData) ? $inquiry->label ?? null : ''}}</strong></td>
                                @if(isset($inquiry->userData))
                                    @foreach($inquiry->userData as $userData)
                                        <td>{{ $userData }}</td>
                                    @endforeach
                                @endif
                            </tr>
                        @else
                            <tr>
                                <td><strong>{{ isset($inquiry->label) ? $inquiry->label : ''}}</strong></td>
                                <td></td>
                            </tr>
                        @endif
                    @endforeach
                    </thead>
                </table>
            </div>

        </div>
    </div>
</div>

</body>
</html>
