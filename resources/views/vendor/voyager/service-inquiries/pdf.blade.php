<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="_url" content="{{ URL('') }}">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title>{{ Voyager::setting("site.title") }}</title>
    <style>
        @font-face {
            font-family: 'ru';
            font-weight: normal;
            font-style: normal;
            font-variant: normal;
            src: url("{{ storage_path('fonts/ru.ttf') }}") format("truetype");
        }
        body {
            font-family: ru;
        }
        @font-face {
            font-family: 'ru-Bold';
            font-weight: normal;
            font-style: normal;
            font-variant: normal;
            src: url("{{ storage_path('fonts/ru-bold.ttf') }}") format("truetype");
        }
        .font_bold{
            font-family: ru-Bold;
        }
    </style>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <center>
                <h3>Application details for {{(auth()->user()->role->slug=='customer') ? $serviceInquiry->product->title : $serviceInquiry->user->name .' '. $serviceInquiry->user->last_name}} </h3>
            </center>
            <br>

            <div class="table-responsive">
                <table class="table table-striped table-hover table-condensed">
                    <thead>
                        @php
                            $fields = json_decode($serviceInquiry->fields)
                        @endphp
                        @foreach($fields as $index => $inquiry)
                            <tr>
                                @if(isset($inquiry->userData))
                                    <td style="font-family: ru !important;">{{ isset($inquiry->userData) ? $inquiry->label ?? null : ''}}</td>
                                    <td><strong>{{ isset($inquiry->userData) ? ':' : ''}} </strong></td>
                                    @if(isset($inquiry->userData))
                                        @foreach($inquiry->userData as $userData)
                                            <td>{{ $userData }}</td>
                                        @endforeach
                                    @endif
                                @else
                                    <td class="font_bold">{!!  isset($inquiry->label) ? $inquiry->label : '' !!}</td>
                                    <td></td>
                                @endif
                            </tr>
                        @endforeach
                    </thead>
                </table>
            </div>

        </div>
    </div>
</div>

</body>
</html>
