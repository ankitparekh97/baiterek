@extends('vendor.voyager.emails.default')

@section('content')
	<table border="0" cellpadding="0" cellspacing="0" width="460">

		<!-- Spacing -->
		<tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="50">&nbsp;</td></tr>
		<!-- Spacing -->

		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 600; font-size: 24px;line-height: 34px;">
				@lang('email.hello',['name' => $name ? $name : "Dear"])
			</td>
		</tr>
		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 600; font-size: 20px;line-height: 32px;">
				@lang('email.youReciveThisEmailBecause')
			</td>
		</tr>

		<!-- Spacing -->
		<tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="50">&nbsp;</td></tr>
		<!-- Spacing -->

		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 26px;">
				@lang('email.clickOnTheLinkBelow')
				<br/>
				<a style="color: #dc4e26; text-align:center;text-decoration: underline;" href="{{$actionUrl}}">@lang('email.setPassword')</a>.
			</td>
		</tr>

		<!-- Spacing -->
		<tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="15">&nbsp;</td></tr>
		<!-- Spacing -->

		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 40px;">
				@lang('email.ifYouDidNotRequestIgnoreEmail')
			</td>
		</tr>

		<!-- Spacing -->
		<tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="15">&nbsp;</td></tr>
		<!-- Spacing -->

		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 32px;">
				@lang('email.autoGeneratedMessage')
			</td>
		</tr>
		<tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="50">&nbsp;</td></tr>
	</table>
@endsection