@extends('vendor.voyager.emails.default')

@section('content')
	<table border="0" cellpadding="0" cellspacing="0" width="460">

		<!-- Spacing -->
		<tr><td style="font-size: 0; line-height: 0;" height="50">&nbsp;</td></tr>
		<!-- Spacing -->

		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 600; font-size: 24px;line-height: 34px;">
				Hi
			</td>
		</tr>
		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 600; font-size: 20px;line-height: 32px;">
				You are receiving this email because we received a password reset request for your account.
			</td>
		</tr>

		<!-- Spacing -->
		<tr><td style="font-size: 0; line-height: 0;" height="50">&nbsp;</td></tr>
		<!-- Spacing -->

		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 26px;">
				<a style="color: #dc4e26; text-align:center;text-decoration: underline;" href="{{ URL('password/reset/'.$token)}}" target="_blank"><span style="color: black">Set Password</span></a>
			</td>
		</tr>

		<!-- Spacing -->
		<tr><td style="font-size: 0; line-height: 0;" height="15">&nbsp;</td></tr>
		<!-- Spacing -->

		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 40px;">
				This password reset link will expire in 60 minutes.
			</td>
		</tr>

		<!-- Spacing -->
		<tr><td style="font-size: 0; line-height: 0;" height="15">&nbsp;</td></tr>
		<!-- Spacing -->

		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 32px;">
				If you did not request a password reset, no further action is required.
			</td>
		</tr>
		<tr><td style="font-size: 0; line-height: 0;" height="50">&nbsp;</td></tr>
	</table>
@endsection