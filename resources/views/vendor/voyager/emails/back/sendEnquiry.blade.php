@extends('vendor.voyager.emails.default')

@section('content')
    <table border="0" cellpadding="0" cellspacing="0" width="460">

        <!-- Spacing -->
        <tr><td style="font-size: 0; line-height: 0;" height="50">&nbsp;</td></tr>
        <!-- Spacing -->

        <tr>
            <td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 600; font-size: 24px;line-height: 34px;">
                Hello Administrator
            </td>
        </tr>
        <tr>
            <td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 600; font-size: 20px;line-height: 32px;">
                One new enquiry details received from {{ $name }} while enrolling on coaching from website. Here are the details.
            </td>
        </tr>

        <!-- Spacing -->
        <tr><td style="font-size: 0; line-height: 0;" height="50">&nbsp;</td></tr>
        <!-- Spacing -->

        <tr>
            <td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 40px;">
                User: <b style="color: #000000; font-weight: 600; font-size: 18px;line-height: 32px;">{{ $name }}</b>
            </td>
        </tr>

        <!-- Spacing -->
        <tr><td style="font-size: 0; line-height: 0;" height="15">&nbsp;</td></tr>
        <!-- Spacing -->

        <tr>
            <td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 40px;">
                Player: <b style="color: #000000; font-weight: 600; font-size: 18px;line-height: 32px;">{{ $player }}</b>
            </td>
        </tr>

        <!-- Spacing -->
        <tr><td style="font-size: 0; line-height: 0;" height="15">&nbsp;</td></tr>
        <!-- Spacing -->

        <tr>
            <td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 32px;">
                County: <b style="color: #000000; font-weight: 600; font-size: 18px;line-height: 32px;">{{ $county }}</b>
            </td>
        </tr>

        <!-- Spacing -->
        <tr><td style="font-size: 0; line-height: 0;" height="15">&nbsp;</td></tr>
        <!-- Spacing -->

        <tr>
            <td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 32px;">
                Age Range: <b style="color: #000000; font-weight: 600; font-size: 18px;line-height: 32px;">{{ $ageRange }}</b>
            </td>
        </tr>

        <!-- Spacing -->
        <tr><td style="font-size: 0; line-height: 0;" height="15">&nbsp;</td></tr>
        <!-- Spacing -->


        <tr><td style="font-size: 0; line-height: 0;" height="50">&nbsp;</td></tr>
    </table>
@endsection