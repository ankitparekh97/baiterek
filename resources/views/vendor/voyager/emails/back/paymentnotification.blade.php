@extends('vendor.voyager.emails.default')
@section('content')
    <table border="0" cellpadding="0" cellspacing="0" width="460">

        <!-- Spacing -->
        <tr><td style="font-size: 0; line-height: 0;" height="50">&nbsp;</td></tr>
        <!-- Spacing -->

        <tr>
            <td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 600; font-size: 24px;line-height: 34px;">
                Hi {{ $name }}
            </td>
        </tr>
        <tr>
            <td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 600; font-size: 20px;line-height: 32px;">
                Thank You for your payment to Complete Cricket’s online platform. Your transaction amount of £ {{$amount}} is successful.
            </td>
        </tr>

        <!-- Spacing -->
        <tr><td style="font-size: 0; line-height: 0;" height="50">&nbsp;</td></tr>
        <!-- Spacing -->

        <tr>
            <td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 26px;">
                For your reference, your Pound transaction id is: <span class="highlight">947356365</span>
            </td>
        </tr>
    </table>
@endsection