<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>CCI Email Template</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
</head>

<body style="margin: 0; padding: 0;">
<table border="0" cellpadding="0" cellspacing="0" width="100%" background="{{ url('images/grass-bg.jpg') }}">
    <!--[if gte mso 9]>
    <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="mso-width-percent:1000;">
        <v:fill type="tile" src="{{ url('images/grass-bg.jpg') }}" color="#7bceeb" />
        <v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
    <![endif]-->
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;" bgcolor="#ffffff">
                <tr>
                    <td align="center" bgcolor="#1161a8">
                        <table border="0" cellpadding="0" cellspacing="20">
                            <tr><td><img src="{{ url('images/logo.png') }}" alt="PROFESSIONAL CRICKET COACHING" width="200" height="auto" style="display:block; border:none; outline:none; text-decoration:none;" /></td></tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        @yield('content')
                    </td>
                </tr>

                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" background="{{ url('images/email-footer-bg.jpg') }}">
                            <!--[if gte mso 9]>
                            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="mso-width-percent:1000;">
                                <v:fill type="tile" src="http://cci.avdevs.com/images/email-footer-bg.jpg" color="#7bceeb" />
                                <v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
                            <![endif]-->

                            <!-- Spacing -->
                            <tr><td style="font-size: 0; line-height: 0;" height="20">&nbsp;</td></tr>
                            <!-- Spacing -->
                            <tr>
                                <td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 600; font-size: 24px;line-height: 30px;">Thank you</td>
                            </tr>
                            <!-- Spacing -->
                            <tr><td style="font-size: 0; line-height: 0;" height="5">&nbsp;</td></tr>
                            <!-- Spacing -->
                            <tr>
                                <td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 600; font-size: 18px;line-height: 28px;">The Complete Cricket Team</td>
                            </tr>
                            <!-- Spacing -->
                            <tr><td style="font-size: 0; line-height: 0;" height="20">&nbsp;</td></tr>
                            <!-- Spacing -->

                            <!--[if gte mso 9]>
                            </v:textbox>
                            </v:rect>
                            <![endif]-->
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="20" width="100%" bgcolor="#122e3c">
                            <tr>
                                <td align="center" style="color: #ffffff; font-family: 'Open Sans'; font-weight: 400; font-size: 14px;line-height: 20px;">Copyright Complete Cricket &copy; {{ date('Y') }}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <!--[if gte mso 9]>
    </v:textbox>
    </v:rect>
    <![endif]-->
</table>
</body>

</html>