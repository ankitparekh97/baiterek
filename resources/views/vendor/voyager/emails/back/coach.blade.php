@extends('vendor.voyager.emails.default')

@section('content')
	<table border="0" cellpadding="0" cellspacing="0" width="460">
		<!-- Spacing -->
		<tr><td style="font-size: 0; line-height: 0;" height="50">&nbsp;</td></tr>
		<!-- Spacing -->
		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 600; font-size: 24px;line-height: 34px;">
				Hi {{ $user->name ? $user->name : "Dear" }}
			</td>
		</tr>
		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 600; font-size: 20px;line-height: 32px;">
				Welcome to Complete Cricket's online platform. Your account has been created - to log in please <a style="color: #dc4e26; text-align:center;text-decoration: underline;" href="{{ $btnLink }}">click here</a>.
			</td>
		</tr>

		<!-- Spacing -->
		<tr><td style="font-size: 0; line-height: 0;" height="50">&nbsp;</td></tr>
		<!-- Spacing -->

		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 26px;">
				Here are your login details – please note that you will have to set your own secure password the first time you log in, as the password below is for temporary one-time use only.
			</td>
		</tr>

		<!-- Spacing -->
		<tr><td style="font-size: 0; line-height: 0;" height="15">&nbsp;</td></tr>
		<!-- Spacing -->

		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 40px;">
				Username: <a style="color: #1161a8; font-weight: 600; font-size: 18px;line-height: 40px; text-align:center;text-decoration: underline;" href="mailto:{{ $user->email }}">{{ $user->email }}</a>
			</td>
		</tr>

		<!-- Spacing -->
		<tr><td style="font-size: 0; line-height: 0;" height="15">&nbsp;</td></tr>
		<!-- Spacing -->

		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 32px;">
				Password: <b style="color: #000000; font-weight: 600; font-size: 18px;line-height: 32px;">{{ $password }}</b>
			</td>
		</tr>
		<tr><td style="font-size: 0; line-height: 0;" height="50">&nbsp;</td></tr>
	</table>
@endsection