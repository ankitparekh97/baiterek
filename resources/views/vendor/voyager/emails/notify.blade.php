@extends('vendor.voyager.emails.default')

@section('content')
	<table border="0" cellpadding="0" cellspacing="0" width="460">

		<!-- Spacing -->
		<tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="50">&nbsp;</td></tr>
		<!-- Spacing -->

		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 600; font-size: 24px;line-height: 34px;">
				Hi {{ $name ? $name : "Dear" }}
			</td>
		</tr>

		<!-- Spacing -->
		<tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="50">&nbsp;</td></tr>
		<!-- Spacing -->

		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 26px;">
				This mail is sent to notify you that your application status has been changed by the supervisor.
			</td>
		</tr>

		<!-- Spacing -->
		<tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="15">&nbsp;</td></tr>
		<!-- Spacing -->

		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 40px;">
				Old application status: <b style="color: #000000; font-weight: 600; font-size: 18px;line-height: 32px;">{{ $oldStatus }}<b>
			</td>
		</tr>

		<!-- Spacing -->
		<tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="15">&nbsp;</td></tr>
		<!-- Spacing -->

		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 32px;">
				New application status: <b style="color: #000000; font-weight: 600; font-size: 18px;line-height: 32px;">{{ $newStatus }}</b>
			</td>
		</tr>
		<tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="50">&nbsp;</td></tr>
	</table>
@endsection