@extends('vendor.voyager.emails.default')

@section('content')
    <table border="0" cellpadding="0" cellspacing="0" width="460">

        <!-- Spacing -->
        <tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="50">&nbsp;</td></tr>
        <!-- Spacing -->

        <tr>
            <td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 600; font-size: 24px;line-height: 34px;">
                Hi {{ $user->name ? $user->name : "Dear" }}
            </td>
        </tr>
        <tr>
            <td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 600; font-size: 20px;line-height: 32px;">
{{--                Welcome to Baiterek. Your account has been created - to log in please <a style="color: #dc4e26; text-align:center;text-decoration: underline;" href="{{ $btnLink }}">click here</a>.--}}
            </td>
        </tr>

        <!-- Spacing -->
        <tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="50">&nbsp;</td></tr>
        <!-- Spacing -->

        <tr>
            <td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 26px;">
                Here are your Product Name : {{ $serviceInquiry->product->title }}
            </td>
        </tr>

        <!-- Spacing -->
        <tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="15">&nbsp;</td></tr>
        <!-- Spacing -->

        <tr>
            <td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 40px;">
{{--                Username: {{ $user->email }}--}}
            </td>
        </tr>

        <!-- Spacing -->
        <tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="15">&nbsp;</td></tr>
        <!-- Spacing -->

        <tr>
            <td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 32px;">
{{--                Password: <b style="color: #000000; font-weight: 600; font-size: 18px;line-height: 32px;">{{ $password }}</b>--}}
            </td>
        </tr>
        <tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="50">&nbsp;</td></tr>
    </table>
@endsection
