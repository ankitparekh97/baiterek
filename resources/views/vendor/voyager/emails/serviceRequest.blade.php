@extends('vendor.voyager.emails.default')

@section('content')
	<table border="0" cellpadding="0" cellspacing="0" width="460">

		<!-- Spacing -->
		<tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="50">&nbsp;</td></tr>
		<!-- Spacing -->

		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 600; font-size: 24px;line-height: 34px;">
				@lang('email.hello',['name' => $user->name ? $user->name : "Dear"])
			</td>
		</tr>
		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 600; font-size: 20px;line-height: 32px;">
                {{ __('email.productManagerAlert', ['title' => $product->title]) }}
                <a style="color: #dc4e26; text-align:center;text-decoration: underline;" href="{{ $actionLink }}">{{ __('email.clickHere') }}</a>.
{{--				Вы получили новую заявку на услугу {{$product->title}} на портале Digital Baiterek. Для просмотра заявки, <a style="color: #dc4e26; text-align:center;text-decoration: underline;" href="{{ $actionLink }}">нажмите здесь</a>.--}}
			</td>
		</tr>

		<!-- Spacing -->
		<tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="50">&nbsp;</td></tr>
		<!-- Spacing -->

		{{--<tr>--}}
			{{--<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 26px;">--}}
				{{--Here are your details.--}}
			{{--</td>--}}
		{{--</tr>--}}

		{{--<!-- Spacing -->--}}
		{{--<tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="15">&nbsp;</td></tr>--}}
		{{--<!-- Spacing -->--}}

		{{--<tr>--}}
			{{--<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 40px;">--}}
				{{--Product : {{ $product->title }}--}}
			{{--</td>--}}
		{{--</tr>--}}

		{{--<!-- Spacing -->--}}
		{{--<tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="15">&nbsp;</td></tr>--}}
		{{--<!-- Spacing -->--}}

		{{--<tr>--}}
			{{--<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 32px;">--}}
				{{--Description: <b style="color: #000000; font-weight: 600; font-size: 18px;line-height: 32px;">{{ $product->description }}</b>--}}
			{{--</td>--}}
		{{--</tr>--}}

		{{--<!-- Spacing -->--}}
		{{--<tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="15">&nbsp;</td></tr>--}}
		{{--<!-- Spacing -->--}}

		{{--<tr>--}}
			{{--<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 32px;">--}}
				{{--Code: <b style="color: #000000; font-weight: 600; font-size: 18px;line-height: 32px;">{{ $product->code }}</b>--}}
			{{--</td>--}}
		{{--</tr>--}}

		{{--<!-- Spacing -->--}}
		{{--<tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="15">&nbsp;</td></tr>--}}
		{{--<!-- Spacing -->--}}

		{{--<tr>--}}
			{{--<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 32px;">--}}
				{{--Organization: <b style="color: #000000; font-weight: 600; font-size: 18px;line-height: 32px;">{{ $product->organization }}</b>--}}
			{{--</td>--}}
		{{--</tr>--}}
		<tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="50">&nbsp;</td></tr>
	</table>
@endsection
