<html>
<body style="background-color: #FFF;color: #333;">
Hi
<br/>
<br/>
We’ve had the following user requesting access to the Complete Cricket Platform:
<br/>
<br/>
<table width="350" style="color: #333">
    <tr>
        <td>First Name:</td>
        <td>{{ $accessDetail['first_name'] }}</td>
    </tr>
    <tr>
        <td>Last Name:</td>
        <td>{{ $accessDetail['last_name'] }}</td>
    </tr>
    <tr>
        <td>Email Address:</td>
        <td>{{ $accessDetail['email'] }}</td>
    </tr>
    <tr>
        <td>Phone:</td>
        <td>{{ $accessDetail['phone'] }}</td>
    </tr>
    <tr>
        <td>Comments:</td>
        <td>{{ $accessDetail['comment'] ? $accessDetail['comment'] : "N/A" }}</td>
    </tr>
</table>
<br/>
Please can you review this request and deal with it accordingly
<br/>
<br/>
Thank You,
<br/>
The Complete Cricket Platform Team.
</body>
</html>
