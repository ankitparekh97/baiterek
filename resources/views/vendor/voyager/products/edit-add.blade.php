@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp
@extends('voyager::master')
@section('css')
    <?php app()->setLocale(\Session::get('locale')) ?>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .documentError{
            color: #f55145;
            font-weight: bold;
        }
        #productApplicationFormView ul.nav > li > a {
            overflow: visible !important;
        }
        .btn-group.mr-2.sw-btn-group.justify-content-between.d-flex.w-100 {
            width: 100% !important;
        }
        button.btn.btn-secondary.sw-btn-next {
            float: right !important;
        }
        .sw-btn-group-extra {
            float: right !important;
            margin: -47px -5px 0 0 !important;
        }
        button#Save {
            float: right !important;
        }
        .form-wrap.form-builder .frmb{
            min-height: 522px !important;
        }
        .language_selector {
            text-align: center;
            margin: 15px 0 20px;
            padding: 0;
            width: 100%;
        }
        .language_selector li {
            display: inline-block;
            padding: 10px;
            cursor: pointer;
            background: #78c687;
            color: #ffffff;
            margin: 0;
            line-height: 18px;
        }
        .language_selector li.active {
            background:#28582C;
            color: #ffffff;
        }
        h3.heading {
            margin-top: 0px;
            padding-top: 20px;
        }
        .sw-theme-arrows > ul.step-anchor {
            background: #f6f6f6;
        }
        .sw-btn-group .btn-secondary {
            background: #2ecc71;
            color: #ffffff;
        }
        .sw-theme-arrows > ul.step-anchor > li > a, .sw-theme-arrows > ul.step-anchor > li > a:hover {
            color: #2d803d;
            background: #f3fcef;
        }
        .sw-theme-arrows > ul.step-anchor > li > a:after {
            border-left: 30px solid #f3fcef;
        }
        .sw-theme-arrows > ul.step-anchor > li.active > a {
            font-weight: bold;
        }

        .custom-card .nav-tabs { border-bottom: 0px solid #DDD; background: none; }
        .custom-card .nav-tabs > li.active > a, .custom-card .nav-tabs > li.active > a:focus, .custom-card .nav-tabs > li.active > a:hover { border-width: 0; }
        .custom-card .nav-tabs > li > a {
            border: none;
            color: #ffffff;
            background: #2D803D;
        }
        .custom-card .nav-tabs > li.active > a, .custom-card .nav-tabs > li > a:hover {
            border: none;
            color: #fff !important;
            background: #5CB85C;
        }
        .custom-card .nav-tabs > li > a::after {
            content: "";
            background: #5CB85C;
            height: 2px;
            position: absolute;
            width: 100%;
            left: 0px;
            bottom: -1px;
            transition: all 250ms ease 0s;
            transform: scale(0);
        }
        .custom-card .nav-tabs > li.active > a::after, .custom-card .nav-tabs > li:hover > a::after { transform: scale(1); }
        .custom-card .tab-nav > li > a::after {
            background: #5CB85C none repeat scroll 0% 0%;
            color: #fff;
        }
        .custom-card .tab-pane { padding: 15px 0; }
        .custom-card .tab-content {padding:20px}
        .custom-card .nav-tabs > li  {width:20%; text-align:center;}
        .custom-card {
            background: #FFF none repeat scroll 0% 0%;
            box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.3);
            margin-bottom: 30px;
        }

        .formBuilderTabs {
            justify-content: center;
            align-items: center;
            display: flex;
        }
        #productApplicationFormView ul.nav > li > a:hover {
            cursor: pointer;
            color: rgb(45, 128, 61) !important;
            /*background: #5CB85C;*/
        }

        .sw-theme-arrows > ul.step-anchor > li > a:hover:after {
            border-left-color: rgb(243, 252, 239);
        }
        .milestone {
            font-weight: bold;
        }

        @media all and (max-width:724px){
            .custom-card .nav-tabs > li > a > span {display:none;}
            .custom-card .nav-tabs > li > a {padding: 5px 5px;}
        }
        .mt-mb-10{
            margin-top: 10px;
            margin-bottom: 10px;
        }
        .mb-5{
            margin-bottom: 5px;
        }
        .mb-8{
            margin-bottom: 8px;
        }
        .admin_doc_file {
            margin-top: 10px;
        }
        .admin_doc_file a img{
            width: 30px;
            margin-right: 6px;
        }
        .removeDocumentPool {
            padding: 7px 12px 5px 12px;
            margin-left: 5px;
        }
        #loader {
            position: fixed;
            width: 67%;
            height: 100%;
            left: 304px;
            top: 0;
            z-index: 99;
            opacity: 1;
        }
        #fb-template{
            display:none;
        }
        .documentError{
            color: #f55145;
            font-weight: bold;
        }
        /* li.form-field { background-color: #eee !important; }
        .form-builder ul li { border: 1px solid #fff; }
        .col12 { width: 100%; }
        .col6 { width: 50%; }
        .d-flex{ display: flex; min-height: 30px; } */
    </style>
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('portal.product.'.str_replace(' ', '_', strtolower(($edit ? 'edit' : 'add').' '.$dataType->getTranslatedAttribute('display_name_singular')))) }}
{{--        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}--}}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')

    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <form role="form" class="form-edit-add" id="productForm" method="POST" enctype="multipart/form-data" action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif">

                        <!-- PUT Method if we are editing -->
                        @if($edit)
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        @if(auth()->user()->role->slug == 'product-manager')
                            <input type="hidden" name="englishFields" id="englishFields">
                            <input type="hidden" name="russianFields" id="russianFields">
                            <input type="hidden" name="kazakhFields" id="kazakhFields">

                            <div id="productApplicationFormView">
                                <ul>
                                    <li>
                                        <a href="#step-1"><span class="step1">@lang('fields.Step1')</span><br />
                                            <small class="productDetails">@lang('fields.ProductDetails')</small>
                                        </a>
                                    </li>
                                    <?php $show = auth()->user()->role->slug == 'product-manager' ?>
                                    <li class="{{ $show ? 'show' : 'hide' }}">
                                        <a href="#step-2"><span class="step2">@lang('fields.Step2')</span><br />
                                            <small class="formDetails">@lang('fields.ApplicantDetails')</small>
                                        </a>
                                    </li>
                                    <li class="{{ $show ? 'show' : 'hide' }}">
                                        <a href="#step-3"><span class="step3">@lang('fields.Step3')</span><br />
                                            <small class="checkpointDetails">@lang('fields.checkpoints')</small>
                                        </a>
                                    </li>
                                </ul>
                                <div>
                                    <div id="step-1" class="">
                                        <h3 class="border-bottom border-gray pb-2 heading productDetails">@lang('fields.ProductDetails')</h3>
                                        <div class="panel-body">
                                            <div id="showerror"></div>

                                            @if (count($errors) > 0)
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif

                                        <!-- Adding / Editing -->
                                            @php
                                                $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                                            @endphp

                                            @foreach($dataTypeRows as $row)
                                                {{--{{dump($row)}}--}}
                                            <!-- GET THE DISPLAY OPTIONS -->
                                                @php
                                                    $display_options = $row->details->display ?? NULL;
                                                    if ($dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')}) {
                                                        $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')};
                                                    }
                                                @endphp

                                                @if (isset($row->details->legend) && isset($row->details->legend->text))
                                                    <legend class="text-{{ $row->details->legend->align ?? 'center' }}" style="background-color: {{ $row->details->legend->bgcolor ?? '#f0f0f0' }};padding: 5px;">{{ $row->details->legend->text }}</legend>
                                                @endif

                                                <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width ?? 12 }} {{ $errors->has($row->field) ? 'has-error' : '' }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                                    {{ $row->slugify }}
                                                    <label class="control-label" for="name">{{ $row->getTranslatedAttribute('display_name') }}</label>
                                                    @include('voyager::multilingual.input-hidden-bread-edit-add')
                                                    @if (isset($row->details->view))
                                                        @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => ($edit ? 'edit' : 'add'), 'view' => ($edit ? 'edit' : 'add'), 'options' => $row->details])
                                                    @elseif ($row->type == 'relationship')
                                                        @include('voyager::formfields.relationship', ['options' => $row->details])
                                                    @else
                                                        {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                                    @endif

                                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                                    @endforeach
                                                    @if ($errors->has($row->field))
                                                        @foreach ($errors->get($row->field) as $error)
                                                            <span class="help-block">{{ $error }}</span>
                                                        @endforeach
                                                    @endif
                                                </div>
                                            @endforeach

                                            @if(\auth()->user()->role->slug == 'admin' || \auth()->user()->role->slug == 'supervisor')
                                                <div class="form-group col-md-12 ">
                                                    <label for="user_id">{{ __('portal.product.manager') }} </label>
                                                    @php
                                                        $row     = \TCG\Voyager\Models\DataRow::where('field', 'product_belongsto_user_relationship')->first();
                                                        $options = $row->details;

                                                        $model = app($options->model);
                                                        $role = \TCG\Voyager\Models\Role::where('slug','product-manager')->first();
                                                        $query = $model::where('role_id', $role->id)->get();

                                                    @endphp
                                                    <select class="form-control select2" name="user_id" id="user_id">
                                                        @foreach($query as $users)
                                                            <option value="{{ $users->{$options->key} }}" @if(old('user_id') !== null) @if(in_array($users->{$options->key}, old('user_id'))){{ 'selected="selected"' }}@endif @else @if($dataTypeContent->user_id == $users->{$options->key}){{ 'selected="selected"' }}@endif @endif>{{ $users->name . ' ' . $users->last_name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                    <div id="step-2" class="">
                                        <h3 class="border-bottom border-gray pb-2 formDetails">@lang('fields.ApplicantDetails')</h3>
                                        <div class="custom-card">
                                            <ul class="nav nav-tabs formBuilderTabs" role="tablist">
                                                <li role="presentation en" class="en"><a id="english_tab" href="#english" aria-controls="home" role="tab" data-toggle="tab"> <span>ENGLISH</span></a></li>
                                                <li role="presentation ru" class="ru"><a id="russian_tab" href="#russian" aria-controls="messages" role="tab" data-toggle="tab"><span>РУССКИЙ</span></a></li>
                                                <li role="presentation kz" class="kz active"><a id="kazakh_tab" href="#kazakh" aria-controls="profile" role="tab" data-toggle="tab"><span>ҚАЗАҚ</span></a></li>
                                            </ul>

                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane" id="english">
                                                    <div id="enFormBuilderArea" class="english"></div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="russian">
                                                    <div id="ruFormBuilderArea" class="russian"></div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane active" id="kazakh">
                                                    <div id="kzFormBuilderArea" class="kazakh"></div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div id="step-3" class="">

                                        <div class="row">
                                            <div class="col-md-12">
                                                <h4 class="border-bottom border-gray pb-2 selectMilestone">@lang('fields.selectMilestone')</h4>
                                                <select name="milestone" class="form-control">
                                                    @for($i=1; $i<=500; $i++)
                                                        <option value="{{ $i }}" {{ $dataTypeContent->milestone == $i ? 'selected' :'' }}> {{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <h4 class="border-bottom border-gray pb-2 externalVerification">@lang('fields.externalVerification')</h4>
                                            <select name="is_verification_required" class="form-control">
                                                <option value="{{ \App\Models\Products::VERIFICATION_NO }}" {{ $dataTypeContent->is_verification_required == \App\Models\Products::VERIFICATION_NO ? 'selected' :'' }}> No</option>
                                                <option value="{{ \App\Models\Products::VERIFICATION_YES }}" {{ $dataTypeContent->is_verification_required == \App\Models\Products::VERIFICATION_YES ? 'selected' :'' }}> Yes</option>
                                            </select>

                                        </div>
                                        <div class="col-md-6">
                                            <h3 class="border-bottom border-gray pb-2 checkpointDetails">@lang('fields.checkpoints')</h3>
                                            <?php
                                            $index = 0;
                                            ?>
                                            @if(isset($dataTypeContent->productCheckPoints) && count($dataTypeContent->productCheckPoints) > 0)
                                                <span id="divAddMoreCheckPoint">
                                                            @foreach($dataTypeContent->productCheckPoints as $key => $productCheckpoint)
                                                        @if($productCheckpoint->type == \App\Models\ProductCheckPoint::TEXT)
                                                            <span class="checkpointClass removeDivAddMoreCheckPoint{{ $productCheckpoint->id.'-'.$productCheckpoint->id }}">
                                                                        <div class="col-md-6 mt-mb-10">
                                                                            <input type="text" name="inputs[]" class="form-control" value="{{ $productCheckpoint->checkpoint }}" required>
                                                                        </div>
                                                                        <div class="col-md-5 mt-mb-10">
                                                                            <select name="checkpoint_step[]" class="form-control mb-8">
                                                                                <option value="">Select</option>
                                                                                @foreach(config('applicationStatus.serviceInquiries') as $serviceInquiry)
                                                                                    <option value="{{ $serviceInquiry }}" {{ $serviceInquiry == $productCheckpoint->status ? 'selected' : '' }}> {{ $serviceInquiry }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-1 mt-mb-10">
                                                                            <button type="button" class="btn btn-danger removeDivAddMoreCheckPoint" remove-id="removeDivAddMoreCheckPoint{{ $productCheckpoint->id.'-'.$productCheckpoint->id }}" ><i class="voyager-trash"></i></button>
                                                                        </div>
                                                                    </span>
                                                        @endif
                                                    @endforeach
                                                    <span class="checkpointClass">
                                                                <div class="col-md-6 mt-mb-10">
                                                                    <input type="text" name="inputs[]" class="form-control mb-8" required>
                                                                </div>
                                                                <div class="col-md-5 mt-mb-10">
                                                                    <select name="checkpoint_step[]" class="form-control mb-8">
                                                                        <option value="">Select</option>
                                                                        @foreach(config('applicationStatus.serviceInquiries') as $serviceInquiry)
                                                                            <option value="{{ $serviceInquiry }}"> {{ $serviceInquiry }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-1 mt-mb-10">
                                                                    <button type="button" class="btn btn-success addMoreCheckPointBtn" style="margin: 0"><i class="voyager-plus"></i></button>
                                                                </div>
                                                            </span>
                                                        </span>
                                            @else
                                                <span id="divAddMoreCheckPoint">
                                                            <span class="checkpointClass">
                                                                <div class="col-md-6 mt-mb-10">
                                                                    <input type="text" name="inputs[]" class="form-control mb-8" required>
                                                                </div>
                                                                <div class="col-md-5 mt-mb-10">
                                                                    <select name="checkpoint_step[]" class="form-control mb-8">
                                                                        <option value="">Select</option>
                                                                        @foreach(config('applicationStatus.serviceInquiries') as $serviceInquiry)
                                                                            <option value="{{ $serviceInquiry }}"> {{ $serviceInquiry }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-1 mt-mb-10">
                                                                    <button type="button" class="btn btn-success addMoreCheckPointBtn" style="margin: 0"><i class="voyager-plus"></i></button>
                                                                </div>
                                                            </span>
                                                        </span>
                                            @endif
                                        </div>
                                        <div class="col-md-6">
                                            <h3 class="border-bottom border-gray pb-2 documentPool">@lang('fields.documentPool')</h3>
                                            @if(isset($dataTypeContent->productCheckPoints) && count($dataTypeContent->productCheckPoints) > 0)
                                                <div class="row">
                                                    @foreach($dataTypeContent->productCheckPoints as $key => $productCheckpoint)
                                                        @if($productCheckpoint->type == \App\Models\ProductCheckPoint::DOCUMENT)
                                                            <div class="col-md-3 admin_doc_file removeDivDocumentPool{{ $productCheckpoint->id.'-'.$productCheckpoint->id }}">
                                                                <a href="{{ asset('storage/'.$productCheckpoint->checkpoint) }}" target="_blank">
                                                                    <img src="{{ asset('images/doc-thumbnail.svg') }}">
                                                                    <label> {{ $productCheckpoint->document_name }}</label>
                                                                </a>
                                                                {{--                                                                <button type="button" class="btn btn-success" id="documentPool"><i class="voyager-plus"></i></button>--}}
                                                                <button type="button" class="btn btn-danger removeDocumentPool" data-id="{{ $productCheckpoint->id }}" ><i class="voyager-trash"></i></button>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            @endif
                                            <div class="clearrix"></div>

                                            <span id="divAddMoreDocumentPool">
                                                <span class="documentPoolClass">
                                                    <div class="col-md-5 mt-mb-10">
                                                        <input type="text" name="document_name[]" class="form-control documentTypeName" placeholder="Document Name" required>
                                                    </div>
                                                    <div class="col-md-6 mt-mb-10">
                                                        <input type="file" name="documents[]" class="form-control documentTypeFile">
                                                    </div>
                                                    <div class="col-md-1 mt-mb-10">
                                                        <button type="button" class="btn btn-success documentPoolBtn"><i class="voyager-plus"></i></button>
                                                    </div>
                                                    <div class="col-md-12 documentError">
                                                    </div>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @else
                            <div class="panel-body">
                                <div id="showerror"></div>

                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                 <!-- Adding / Editing -->
                                @php
                                    $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                                @endphp

                                @foreach($dataTypeRows as $row)
                                     <!-- GET THE DISPLAY OPTIONS -->
                                    @php
                                        $display_options = $row->details->display ?? NULL;
                                        if ($dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')}) {
                                            $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')};
                                        }
                                    @endphp

                                    @if (isset($row->details->legend) && isset($row->details->legend->text))
                                        <legend class="text-{{ $row->details->legend->align ?? 'center' }}" style="background-color: {{ $row->details->legend->bgcolor ?? '#f0f0f0' }};padding: 5px;">{{ $row->details->legend->text }}</legend>
                                    @endif

                                    <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width ?? 12 }} {{ $errors->has($row->field) ? 'has-error' : '' }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                        {{ $row->slugify }}
                                        <label class="control-label" for="name">{{ $row->getTranslatedAttribute('display_name') }}</label>
                                        @include('voyager::multilingual.input-hidden-bread-edit-add')
                                        @if (isset($row->details->view))
                                            @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => ($edit ? 'edit' : 'add'), 'view' => ($edit ? 'edit' : 'add'), 'options' => $row->details])
                                        @elseif ($row->type == 'relationship')
                                            @include('voyager::formfields.relationship', ['options' => $row->details])
                                        @else
                                            {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                        @endif

                                        @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                            {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                        @endforeach
                                        @if ($errors->has($row->field))
                                            @foreach ($errors->get($row->field) as $error)
                                                <span class="help-block">{{ $error }}</span>
                                            @endforeach
                                        @endif
                                    </div>
                                @endforeach

                                <div class="form-group col-md-12 ">
                                    <label for="user_id">{{ __('portal.product.manager') }} </label>
                                    @php
                                        $row     = \TCG\Voyager\Models\DataRow::where('field', 'product_belongsto_user_relationship')->first();
                                        $options = $row->details;

                                        $model = app($options->model);
                                        $role = \TCG\Voyager\Models\Role::where('slug','product-manager')->first();
                                        $query = $model::where('role_id', $role->id)->get();

                                    @endphp
                                    <select class="form-control select2" name="user_id" id="user_id">
                                        @foreach($query as $users)
                                            <option value="{{ $users->{$options->key} }}" @if(old('user_id') !== null) @if(in_array($users->{$options->key}, old('user_id'))){{ 'selected="selected"' }}@endif @else @if($dataTypeContent->user_id == $users->{$options->key}){{ 'selected="selected"' }}@endif @endif>{{ $users->name . ' ' . $users->last_name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <button type="submit" id="save" class="btn btn-primary save">
                                            @if(isset($dataTypeContent->id)) Update @else{{ __('portal.product.save') }}@endif
                                        </button>
                                    </div>
                                </div>

                            </div>
                        @endif

                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
    <script type="application/javascript">
        var params = {};
        var $file;
        var fbInstances = new Array();
        var lang = 'kz';

        var errorEN = [];
        var errorRU = [];
        var errorKZ = [];

        function deleteHandler(tag, isMulti) {
            return function() {
                $file = $(this).siblings(tag);

                params = {
                    slug:   '{{ $dataType->slug }}',
                    filename:  $file.data('file-name'),
                    id:     $file.data('id'),
                    field:  $file.parent().data('field-name'),
                    multi: isMulti,
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text(params.filename);
                $('#confirm_delete_modal').modal('show');
            };
        }
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
            var englishOptions = {
                showActionButtons: false,
                i18n: {
                    locale: 'en-EN'
                },
                stickyControls: {
                    enable: true,
                    offset: {
                        top: 50,
                        right: 'auto',
                        left: 'auto'
                    }
                },
                disableFields: ['button','hidden'],
                formData: {!! !empty($formDataeN) ? $formDataeN : "''" !!}
            };

            var kazakhOptions = {
                showActionButtons: false,
                i18n: {
                    locale: 'kz-KZ'
                },
                disabledAttrs: [
                    ''
                ],
                //                fields: fields,
//                templates: templates,
                formData: {!! !empty($formDatakZ) ? $formDatakZ : "''" !!},
                stickyControls: {
                    enable: true,
                    offset: {
                        top: 50,
                        right: 'auto',
                        left: 'auto'
                    }
                },
//                disabledFieldButtons: {
//                    'header': ['copy'],
//                    'paragraph': ['copy'],
//                    'text': ['copy'],
//                    'date': ['copy'],
//                    'file': ['copy'],
//                    'number': ['copy'],
//                    'textarea': ['copy'],
//                    'radio': ['copy'],
//                    'radiogroup': ['copy'],
//                    'checkbox-group': ['copy'],
//                    'checkbox': ['copy'],
//                    'autocomplete': ['copy'],
//                    'select': ['copy']
//                },
                disableFields: ['button','hidden'],
                //                disableFields: ['button','text','select','header','radio','checkbox','autocomplete','file','textarea','date','paragraph','hidden','number','radio-group','checkbox-group'],
                //                typeUserEvents: {
//                    text: {
//                        onremove: function(fld) {
//                             console.log(fld,fld.remove());
//                        }
//                    }
//                },
            };

            var russianOptions = {
                showActionButtons: false,
                i18n: {
                    locale: 'ru-RU'
                },
                disabledAttrs: [
                    ''
                ],
                stickyControls: {
                    enable: true,
                    offset: {
                        top: 50,
                        right: 'auto',
                        left: 'auto'
                    }
                },
                disableFields: ['button','hidden'],
//                disabledFieldButtons: {
//                    'header': ['copy'],
//                    'paragraph': ['copy'],
//                    'text': ['copy'],
//                    'date': ['copy'],
//                    'file': ['copy'],
//                    'number': ['copy'],
//                    'textarea': ['copy'],
//                    'radio': ['copy'],
//                    'radiogroup': ['copy'],
//                    'checkbox-group': ['copy'],
//                    'checkbox': ['copy'],
//                    'autocomplete': ['copy'],
//                    'select': ['copy']
//                },
                formData: {!! !empty($formDatarU) ? $formDatarU : "''" !!},
//                onAddField: function(fieldId,control) {
//                    addTranslation(control);
//                },
            };

            $("#productApplicationFormView").on("showStep", function(e, anchorObject, stepNumber, stepDirection,stepPosition) {
                @if( auth()->user()->role->slug == 'supervisor')
                $(".sw-btn-prev").css({"opacity" : "0"});
                $(".sw-btn-next").hide();
                $('.sw-btn-group-extra').show();
                @elseif (auth()->user()->role->slug == 'product-manager')
                if(stepPosition === 'first'){
                    $("#prev-btn").addClass('disabled');
                    $(".sw-btn-next").removeClass('disabled');
                    $(".sw-btn-next").show();
                    $('.sw-btn-group-extra').hide();
                } else if(stepPosition === 'final'){
                    $(".sw-btn-next").hide();
                    $(".sw-btn-next").addClass('disabled');
                    $('.sw-btn-group-extra').show();

                } else {
                    $("#prev-btn").removeClass('disabled');
                    $(".sw-btn-next").show();
                    $(".sw-btn-next").removeClass('disabled');
                }
                @endif
            });

            var btnFinish = $('<button type="button" name="Save" id="save"></button>').text('Save').addClass('btn btn-success');
            $('#productApplicationFormView').smartWizard({
                selected: 0,
                theme: 'arrows',
                transitionEffect:'fade',
                showStepURLhash: false,
                enableFinishButton: false,
                validationEnabled: true,
                keyNavigation: false,
                autoAdjustHeight: false,
                lang: {         // Language variables for button
                    next: 'Next',
                    previous: 'Back'
                },
                toolbarSettings: {
                    toolbarPosition: 'bottom',
                    toolbarExtraButtons: [btnFinish],
                    toolbarButtonPosition: 'right', // left, right
                },
                anchorSettings: {
                    removeDoneStepOnNavigateBack: true,
                }
            });

            $("#productApplicationFormView").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
                $('#showerror').html('');
                errorEN = [];
                errorRU = [];
                errorKZ = [];
                var elmForm = $("#productForm");
                if(stepDirection === 'forward' && elmForm){
                    var titleData = JSON.parse($('input[name=title_i18n]').val());
                    $.each( titleData, function( key, value ) {
                        if(lang == key){
                            if($('input[name=title]').val() == '' || $('input[name=title]').val() == null){
                                errorEN.push({
                                    key : 'title',
                                    value  : 'empty',
                                    option : 'enter',
                                    lan : 'in ' + key,
                                    please : 'Please'
                                });
                                errorRU.push({
                                    key : 'заглавие',
                                    value  : 'empty',
                                    option : 'войти',
                                    lan : 'в ' + key,
                                    please : 'пожалуйста'
                                });
                                errorKZ.push({
                                    key : 'тақырыбы',
                                    value  : 'empty',
                                    option : 'енгізу',
                                    lan : 'ішінде ' + key,
                                    please : 'өтінемін'
                                });
                            }
                        }else{
                            if(value == '' || value == null){
                                errorEN.push({
                                    key : 'title',
                                    value  : 'empty',
                                    option : 'enter',
                                    lan : 'in ' + key,
                                    please : 'Please'
                                });
                                errorRU.push({
                                    key : 'заглавие',
                                    value  : 'empty',
                                    option : 'войти',
                                    lan : 'в ' + key,
                                    please : 'пожалуйста'
                                });
                                errorKZ.push({
                                    key : 'тақырыбы',
                                    value  : 'empty',
                                    option : 'енгізу',
                                    lan : 'ішінде ' + key,
                                    please : 'өтінемін'
                                });
                            }
                        }
                    });

                    var codeData = JSON.parse($('input[name=code_i18n]').val());
                    $.each( codeData, function( key, value ) {
                        if(lang == key){
                            if($('input[name=code]').val() == '' || $('input[name=code]').val() == null){
                                errorEN.push({
                                    key : 'code',
                                    value  : 'empty',
                                    option : 'enter',
                                    lan : 'in ' + key,
                                    please : 'Please'
                                });
                                errorRU.push({
                                    key : 'код',
                                    value  : 'empty',
                                    option : 'войти',
                                    lan : 'в ' + key,
                                    please : 'пожалуйста'
                                });
                                errorKZ.push({
                                    key : 'коды',
                                    value  : 'empty',
                                    option : 'енгізу',
                                    lan : 'ішінде ' + key,
                                    please : 'өтінемін'
                                });
                            }
                        }else{
                            if(value == '' || value == null){
                                errorEN.push({
                                    key : 'code',
                                    value  : 'empty',
                                    option : 'enter',
                                    lan : 'in ' + key,
                                    please : 'Please'
                                });
                                errorRU.push({
                                    key : 'код',
                                    value  : 'empty',
                                    option : 'войти',
                                    lan : 'в ' + key,
                                    please : 'пожалуйста'
                                });
                                errorKZ.push({
                                    key : 'коды',
                                    value  : 'empty',
                                    option : 'енгізу',
                                    lan : 'ішінде ' + key,
                                    please : 'өтінемін'
                                });
                            }
                        }
                    });

                    @if($edit == '')
                    if($('input[name=logo]').val() == '' || $('input[name=logo]').val() == null ){
                        errorEN.push({
                            key : 'logo',
                            value  : 'empty',
                            option : 'chose',
                            lan : '',
                            please : 'Please'
                        });
                        errorRU.push({
                            key : 'логотип',
                            value  : 'empty',
                            option : 'выбрал',
                            lan : '',
                            please : 'пожалуйста'
                        });
                        errorKZ.push({
                            key : 'логотипі',
                            value  : 'empty',
                            option : 'таңдады',
                            lan : '',
                            please : 'өтінемін'
                        });
                    }
                    @endif

                    // if($('input[name=milestone]').val() == '' || $('input[name=milestone]').val() == null ){
                    //     errorEN.push({
                    //         key : 'milestone',
                    //         value  : 'empty',
                    //         option : 'chose',
                    //         lan : '',
                    //         please : 'Please'
                    //     });
                    //     errorRU.push({
                    //         key : 'веха',
                    //         value  : 'empty',
                    //         option : 'выбрал',
                    //         lan : '',
                    //         please : 'пожалуйста'
                    //     });
                    //     errorKZ.push({
                    //         key : 'белес',
                    //         value  : 'empty',
                    //         option : 'таңдады',
                    //         lan : '',
                    //         please : 'өтінемін'
                    //     });
                    // }
                    if($('select.select2[name="productCategory_id"]').val() == '' || $('select.select2[name="productCategory_id"]').val() == null ){
                        errorEN.push({
                            key : 'Product Category',
                            value  : 'empty',
                            option : 'select',
                            lan : '',
                            please : 'Please'
                        });
                        errorRU.push({
                            key : 'категория продукта',
                            value  : 'empty',
                            option : 'таңдаңыз',
                            lan : '',
                            please : 'пожалуйста'
                        });
                        errorKZ.push({
                            key : 'Өнім категориясы',
                            value  : 'empty',
                            option : 'таңдаңыз',
                            lan : '',
                            please : 'өтінемін'
                        });
                    }
                    if($('select.select2[name="subsidiary_id"]').val() == '' || $('select.select2[name="subsidiary_id"]').val() == null ){
                        errorEN.push({
                            key : 'Subsidiary',
                            value  : 'empty',
                            option : 'select',
                            lan : '',
                            please : 'Please'
                        });
                        errorRU.push({
                            key : 'Көмекші',
                            value  : 'empty',
                            option : 'таңдаңыз',
                            lan : '',
                            please : 'өтінемін'
                        });
                        errorKZ.push({
                            key : 'Филиал',
                            value  : 'empty',
                            option : 'таңдаңыз',
                            lan : '',
                            please : 'пожалуйста'
                        });
                    }

                    var html = '<div id="errorMessage" class="alert alert-danger"><ul>';
                    if(lang == 'kz'){
                        $.each( errorKZ, function( key, value ) {
                            html += '<li>'+value['please']+' '+value['option']+' '+value['key']+' '+value['lan'] +' </li>';
                        });
                    }else if(lang == 'ru'){
                        $.each( errorRU, function( key, value ) {
                            html += '<li>'+value['please']+' '+value['option']+' '+value['key']+' '+value['lan'] +' </li>';
                        });
                    }else{
                        $.each( errorEN, function( key, value ) {
                            html += '<li>'+value['please']+' '+value['option']+' '+value['key']+' '+value['lan'] +' </li>';
                        });
                    }
                    html += '</ul></div >';

//                    elmForm.validator('validate');
//                    var elmErr = elmForm.children('.has-error');
                    if(errorEN.length > 0){
                        $('#showerror').prepend(html);
                        $(window).scrollTop(0);
                        return false;
                    }else{
                        errorEN = [];
                        errorRU = [];
                        errorKZ = [];
                        @if(auth()->user()->role->slug == 'supervisor')
                        $('#productForm').submit();
                        return false;
                        @endif
                    }
                }
                return true;
            });

            $('#productApplicationFormView .sw-toolbar .sw-btn-group').addClass('justify-content-between d-flex w-100');
            $('#productApplicationFormView .sw-toolbar .sw-btn-group-extra').addClass('justify-content-between d-flex w-100');

            fbInstances['en'] = $("#enFormBuilderArea").formBuilder(englishOptions);
            fbInstances['ru'] = $("#ruFormBuilderArea").formBuilderRu(russianOptions);
            fbInstances['kz'] = $("#kzFormBuilderArea").formBuilderKz(kazakhOptions);

            var status = 0;

            $('body').on('click','label.btn-primary', function(e){
                lang = $(this).find('input').attr('id');
                // if(status == 0){
                //     $('.documentError').html('');
                // }
                $(".documentPoolClass").each(function( index ) {
                    var documentTypeName = $(this).find('.documentTypeName').val();
                    var documentTypeFile = $(this).find('.documentTypeFile').val();
                    if((documentTypeName == ""|| documentTypeName == null) && (documentTypeFile == "" || documentTypeFile == null)){
                        $(this).find('.documentError').html('');
                    }
                    else if((documentTypeName == ""|| documentTypeName == null) && (documentTypeFile != "" || documentTypeFile != null)){
                        $(this).find('.documentError').html('{{ trans('fields.documentError') }}');
                        if(lang == 'kz') {
                            $(this).find('.documentError').html('Екі өріс міндетті');
                        }
                        else if(lang == 'en') {
                            $(this).find('.documentError').html('Both fields are required');
                        }
                        else if(lang == 'ru') {
                            $(this).find('.documentError').html('Оба поля обязательны для заполнения');
                        }
                    }
                    else if((documentTypeName != ""|| documentTypeName != null) && (documentTypeFile == "" || documentTypeFile == null)){
                        $(this).find('.documentError').html('{{ trans('fields.documentError') }}');
                        if(lang == 'kz') {
                            $(this).find('.documentError').html('Екі өріс міндетті');
                        }
                        else if(lang == 'en') {
                            $(this).find('.documentError').html('Both fields are required');
                        }
                        else if(lang == 'ru') {
                            $(this).find('.documentError').html('Оба поля обязательны для заполнения');
                        }
                    }
                    else{
                        $(this).find('.documentError').html('');
                    }
                });

                if(lang == 'en'){
                    $('#showerror').html('');
                    if(errorEN.length > 0) {
                        var html = '<div id="errorMessage" class="alert alert-danger"><ul>';
                        $.each(errorEN, function (key, value) {
                            html += '<li>' + value['please'] + ' ' + value['option'] + ' ' + value['key'] + ' ' + value['lan'] + ' </li>';
                        });
                        html += '</ul></div >';
                        $('#voyager-loader').hide();
                        $('#showerror').html(html);
                    }
                    $('#english_tab').click();
                }else if(lang == 'kz'){
                    $('#showerror').html('');
                    if(errorEN.length > 0) {
                        var html = '<div id="errorMessage" class="alert alert-danger"><ul>';
                        $.each(errorKZ, function (key, value) {
                            html += '<li>' + value['please'] + ' ' + value['option'] + ' ' + value['key'] + ' ' + value['lan'] + ' </li>';
                        });
                        html += '</ul></div >';
                        $('#voyager-loader').hide();
                        $('#showerror').html(html);
                    }

                    $('#kazakh_tab').click();
                }else if(lang == 'ru'){
                    // if(status == 1){
                    //     $('.documentError').html('Оба поля обязательны для заполнения');
                    // }
                    $('#showerror').html('');
                    if(errorEN.length > 0){
                        var html = '<div id="errorMessage" class="alert alert-danger"><ul>';
                        $.each( errorRU, function( key, value ) {
                            html += '<li>'+value['please']+' '+value['option']+' '+value['key']+' '+value['lan'] +' </li>';
                        });
                        html += '</ul></div >';
                        $('#voyager-loader').hide();
                        $('#showerror').html(html);
                    }

                    $('#russian_tab').click();
                }

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: '{{ route('getLabelTranslations') }}',
                    data:  {lang : lang},
                    dataType: 'json',
                    success: function (response) {
                        $('.productDetails').html(response.content.ProductDetails);
                        $('.formDetails').html(response.content.ApplicantDetails);
                        $('.checkpointDetails').html(response.content.checkpoints);
                        $('.documentPool').html(response.content.documentPool);
                        $('.addMoreBtn').html(response.content.addMoreBtn);
                        $('.step1').html(response.content.Step1);
                        $('.step2').html(response.content.Step2);
                        $('.step3').html(response.content.Step3);
                        $('.milestone').html(response.content.Milestone);
                        $('.selectMilestone').html(response.content.selectMilestone);
                        $('.externalVerification').html(response.content.externalVerification);
                    },
                    error: function (error) {
                        console.log('fail');
                    }
                });
            })

            $(document.getElementById("save")).click(function() {
                @if(auth()->user()->role->slug == 'supervisor')

                $('#showerror').html('');
                errorEN = [];
                errorRU = [];
                errorKZ = [];

                var titleData = JSON.parse($('input[name=title_i18n]').val());
                $.each( titleData, function( key, value ) {
                    if(lang == key){
                        if($('input[name=title]').val() == '' || $('input[name=title]').val() == null){
                            errorEN.push({
                                key : 'title',
                                value  : 'empty',
                                option : 'enter',
                                lan : 'in ' + key,
                                please : 'Please'
                            });
                            errorRU.push({
                                key : 'заглавие',
                                value  : 'empty',
                                option : 'войти',
                                lan : 'в ' + key,
                                please : 'пожалуйста'
                            });
                            errorKZ.push({
                                key : 'тақырыбы',
                                value  : 'empty',
                                option : 'енгізу',
                                lan : 'ішінде ' + key,
                                please : 'өтінемін'
                            });
                        }
                    }else{
                        if(value == '' || value == null){
                            errorEN.push({
                                key : 'title',
                                value  : 'empty',
                                option : 'enter',
                                lan : 'in ' + key,
                                please : 'Please'
                            });
                            errorRU.push({
                                key : 'заглавие',
                                value  : 'empty',
                                option : 'войти',
                                lan : 'в ' + key,
                                please : 'пожалуйста'
                            });
                            errorKZ.push({
                                key : 'тақырыбы',
                                value  : 'empty',
                                option : 'енгізу',
                                lan : 'ішінде ' + key,
                                please : 'өтінемін'
                            });
                        }
                    }
                });

                var codeData = JSON.parse($('input[name=code_i18n]').val());
                $.each( codeData, function( key, value ) {
                    if(lang == key){
                        if($('input[name=code]').val() == '' || $('input[name=code]').val() == null){
                            errorEN.push({
                                key : 'code',
                                value  : 'empty',
                                option : 'enter',
                                lan : 'in ' + key,
                                please : 'Please'
                            });
                            errorRU.push({
                                key : 'код',
                                value  : 'empty',
                                option : 'войти',
                                lan : 'в ' + key,
                                please : 'пожалуйста'
                            });
                            errorKZ.push({
                                key : 'коды',
                                value  : 'empty',
                                option : 'енгізу',
                                lan : 'ішінде ' + key,
                                please : 'өтінемін'
                            });
                        }
                    }else{
                        if(value == '' || value == null){
                            errorEN.push({
                                key : 'code',
                                value  : 'empty',
                                option : 'enter',
                                lan : 'in ' + key,
                                please : 'Please'
                            });
                            errorRU.push({
                                key : 'код',
                                value  : 'empty',
                                option : 'войти',
                                lan : 'в ' + key,
                                please : 'пожалуйста'
                            });
                            errorKZ.push({
                                key : 'коды',
                                value  : 'empty',
                                option : 'енгізу',
                                lan : 'ішінде ' + key,
                                please : 'өтінемін'
                            });
                        }
                    }
                });

                @if($edit == '')
                if($('input[name=logo]').val() == '' || $('input[name=logo]').val() == null){
                    errorEN.push({
                        key : 'logo',
                        value  : 'empty',
                        option : 'chose',
                        lan : '',
                        please : 'Please'
                    });
                    errorRU.push({
                        key : 'логотип',
                        value  : 'empty',
                        option : 'выбрал',
                        lan : '',
                        please : 'пожалуйста'
                    });
                    errorKZ.push({
                        key : 'логотипі',
                        value  : 'empty',
                        option : 'таңдады',
                        lan : '',
                        please : 'өтінемін'
                    });
                }
                @endif

                if($('select.select2[name="productCategory_id"]').val() == '' || $('select.select2[name="productCategory_id"]').val() == null ){
                    errorEN.push({
                        key : 'Product Category',
                        value  : 'empty',
                        option : 'select',
                        lan : '',
                        please : 'Please'
                    });
                    errorRU.push({
                        key : 'категория продукта',
                        value  : 'empty',
                        option : 'таңдаңыз',
                        lan : '',
                        please : 'пожалуйста'
                    });
                    errorKZ.push({
                        key : 'Өнім категориясы',
                        value  : 'empty',
                        option : 'таңдаңыз',
                        lan : '',
                        please : 'өтінемін'
                    });
                }
                if($('select.select2[name="subsidiary_id"]').val() == '' || $('select.select2[name="subsidiary_id"]').val() == null ){

                    errorEN.push({
                        key : 'Subsidiary',
                        value  : 'empty',
                        option : 'select',
                        lan : '',
                        please : 'Please'
                    });
                    errorRU.push({
                        key : 'Көмекші',
                        value  : 'empty',
                        option : 'таңдаңыз',
                        lan : '',
                        please : 'өтінемін'
                    });
                    errorKZ.push({
                        key : 'Филиал',
                        value  : 'empty',
                        option : 'таңдаңыз',
                        lan : '',
                        please : 'пожалуйста'
                    });
                }
                var html = '<div id="errorMessage" class="alert alert-danger"><ul>';
                if(lang == 'kz'){
                    $.each( errorKZ, function( key, value ) {
                        html += '<li>'+value['please']+' '+value['option']+' '+value['key']+' '+value['lan'] +' </li>';
                    });
                }else if(lang == 'ru'){
                    $.each( errorRU, function( key, value ) {
                        html += '<li>'+value['please']+' '+value['option']+' '+value['key']+' '+value['lan'] +' </li>';
                    });
                }else{
                    $.each( errorEN, function( key, value ) {
                        html += '<li>'+value['please']+' '+value['option']+' '+value['key']+' '+value['lan'] +' </li>';
                    });
                }
                html += '</ul></div >';

                // submit the form
                if(errorEN.length > 0){
                    $('#showerror').prepend(html);
                    $(window).scrollTop(0);
                    return false;
                }else{
                    errorEN = [];
                    errorRU = [];
                    errorKZ = [];
                    $('#voyager-loader').show();
                    $('#productForm').submit();
                    return false;
                }

                @else
                status = 0;
                $(".documentPoolClass").each(function( index ) {
                    var documentTypeName = $(this).find('.documentTypeName').val();
                    var documentTypeFile = $(this).find('.documentTypeFile').val();
                    if((documentTypeName == ""|| documentTypeName == null) && (documentTypeFile == "" || documentTypeFile == null)){
                        $(this).find('.documentError').html('');
                    }
                    else if((documentTypeName == ""|| documentTypeName == null) && (documentTypeFile != "" || documentTypeFile != null)){
                        if(lang == 'kz') {
                            $(this).find('.documentError').html('Екі өріс міндетті');
                        }
                        else if(lang == 'en') {
                            $(this).find('.documentError').html('Both fields are required');
                        }
                        else if(lang == 'ru') {
                            $(this).find('.documentError').html('Оба поля обязательны для заполнения');
                        }
                        status = 1;

                    }
                    else if((documentTypeName != ""|| documentTypeName != null) && (documentTypeFile == "" || documentTypeFile == null)){
                        if(lang == 'kz') {
                            $(this).find('.documentError').html('Екі өріс міндетті');
                        }
                        else if(lang == 'en') {
                            $(this).find('.documentError').html('Both fields are required');
                        }
                        else if(lang == 'ru') {
                            $(this).find('.documentError').html('Оба поля обязательны для заполнения');
                        }
                        status = 1;
                    }
                    else{
                        $(this).find('.documentError').html('');
                    }
                });
                if(status == 1){
                    return false;
                }
                $('#englishFields').val(JSON.stringify(fbInstances['en'].actions.getData()));
                $('#kazakhFields').val(JSON.stringify(fbInstances['kz'].actions.getData()));
                $('#russianFields').val(JSON.stringify(fbInstances['ru'].actions.getData()));
                $('#voyager-loader').show();
                $('#productForm').submit();

                @endif

            });

            //Init datepicker for date fields if data-datepicker attribute defined or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                } else if (elt.type != 'date') {
                    elt.type = 'text';
                    $(elt).datetimepicker({
                        format: 'L',
                        extraFormats: [ 'YYYY-MM-DD' ]
                    }).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
            $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();

        });

        var divAddMoreCheckPointCount = 1;
        var divAddMoreDocumentPoolCount = 1;

        $select = '<select name="checkpoint_step[]" class="form-control">'+
            '<option value="">Select</option>'+
                @foreach(config('applicationStatus.serviceInquiries') as $serviceInquiry)
                    '<option value="{{ $serviceInquiry }}"> {{ $serviceInquiry }}</option>'+
                @endforeach
                    '</select>';

        $('body').on('click','.addMoreCheckPointBtn',function () {
            $(this).removeClass('btn-success addMoreCheckPointBtn');
            $(this).addClass('btn-danger removeDivAddMoreCheckPoint');
            $(this).attr('remove-id','removeDivAddMoreCheckPoint'+divAddMoreCheckPointCount);
            $(this).parent().parent().addClass('removeDivAddMoreCheckPoint'+divAddMoreCheckPointCount);
            $(this).find('i').removeClass('voyager-plus').addClass('voyager-trash');
            var html = '<span class="checkpointClass">' +
                '<div class="col-md-6 mt-mb-10">\n' +
                '<input type="text" name="inputs[]" class="form-control" required>\n' +
                '</div>\n' +
                '<div class="col-md-5 mt-mb-10">\n' +
                $select+
                '</div>'+
                '<div class="col-md-1 mt-mb-10">\n' +
                // '<button type="button" class="btn btn-danger removeDivAddMoreCheckPoint" remove-id="removeDivAddMoreCheckPoint'+divAddMoreCheckPointCount+'"><i class="voyager-trash"></i></button>\n' +
                '<button type="button" class="btn btn-success addMoreCheckPointBtn"><i class="voyager-plus"></i></button>\n' +
                '</div>' +
                '</span>';
            $('#divAddMoreCheckPoint').append(html);
            divAddMoreCheckPointCount ++;
        });

        $('body').on('click','.removeDivAddMoreCheckPoint',function () {
            var remove = $(this).attr('remove-id');
            $('.'+remove).remove();
        });

        $('body').on('click','.documentPoolBtn',function () {
            $(this).removeClass('btn-success documentPoolBtn');
            $(this).addClass('btn-danger removeDivDocumentPool');
            $(this).attr('remove-id','removeDivDocumentPool'+divAddMoreDocumentPoolCount);
            $(this).parent().parent().addClass('removeDivDocumentPool'+divAddMoreDocumentPoolCount);
            $(this).find('i').removeClass('voyager-plus').addClass('voyager-trash');
            var html = '<span class="documentPoolClass">' +
                '<div class="col-md-5 mt-mb-10">\n' +
                '<input type="text" name="document_name[]" class="form-control documentTypeName" placeholder="Document Name">\n' +
                '</div>\n' +
                '<div class="col-md-6 mt-mb-10">\n' +
                '<input type="file" name="documents[]" class="form-control documentTypeFile">\n' +
                '</div>\n' +
                '<div class="col-md-1 mt-mb-10">\n' +
                // '<button type="button" class="btn btn-danger removeDivDocumentPool" remove-id="removeDivDocumentPool'+divAddMoreDocumentPoolCount+'"><i class="voyager-trash"></i></button>\n' +
                '<button type="button" class="btn btn-success documentPoolBtn"><i class="voyager-plus"></i></button>\n' +
                '</div>' +
                '<div class="col-md-12 documentError">' +
                '</div>' +
                '</span>';
            $('#divAddMoreDocumentPool').append(html);
            divAddMoreDocumentPoolCount ++;
        });

        $('body').on('click','.removeDivDocumentPool',function () {

            var remove = $(this).attr('remove-id');
            $('.'+remove).remove();
        });

        $('body').on('click','.removeDocumentPool',function () {
            var id = $(this).attr('data-id');
            var remove = "removeDivDocumentPool"+id+'-'+id;
            $.ajax({
                type: 'POST',
                url: '{{ route('removeCheckpointDocument') }}',
                data:  {id : id},
                dataType: 'json',
                success: function (response) {
                    if(response.status == true){
                        $('.'+remove).remove();
                    }
                },
                error: function (error) {
                    console.log('fail');
                }
            });
        });

        function addTranslation(control){

            try{
                var kz = fbInstances['kz'].actions.getData();
                var kzConcat = kz.concat(control);
                fbInstances['kz'].actions.setData(kzConcat);

                var en = fbInstances['en'].actions.getData();
                var enConcat = en.concat(control);
                fbInstances['en'].actions.setData(enConcat);
            }catch(e){
                console.log(e.message);
            }

        }
// english restructure code start 
// setTimeout(() => {
//     var a = $("ul.frmbenglish").children("li").children("ul");
//     var newObj = {};
//     if(typeof a != "undefined"){
//       $(a.children("li")).each(function(e){
//           var classname = $(a.children("li")[e]).find("input[name='className']").val();

//             newObj[e] = $(a.children("li")[e]).parent("ul").find("li:first-child").addClass("col-md-12 d-inline-block");
//             newObj[e]['class'] = 'col-md-12';
//             newObj[e]['index'] = e;

//           if(classname != ""){
//             var classexist = classname.match(/(col-md-\d*)/i);
//             if (typeof classexist[0] != "undefined") {
//               var currobj = $(a.children("li")[e]).parent("ul");
//               var nextobj = $(a.children("li")[e+1]).parent("ul");
//               $(a.children("li")[e]).parent("ul").addClass("d-flex");
//               $(a.children("li")[e]).parent("ul").find("li:first-child").addClass(classexist[0]+" d-inline-block");
//               newObj[e] = $(a.children("li")[e]);
//               newObj[e]['class'] = classexist[0];
//               newObj[e]['index'] = e;
//             }
//           }
//       });
//     }
//     if(!$.isEmptyObject(newObj)){
//         var finalObj = $("ul.frmbenglish");
//         finalObj.children().remove();
//         var formcolsmanage = {};
//         var design = '';
//         var md6 = 1,md4 = 1,md3 = 1;
//         var tempnewmd6, tempnewmd4, tempnewmd3;
//         $.each( newObj, function( index, value ){
//           if(newObj[index].class == "col-md-12"){
//               var li = $("<li/>").appendTo(finalObj);
//               var ul = $("<ul/>").addClass("drag-en ui-sortable d-flex").appendTo(li);
//               $(newObj[index]).clone().appendTo(ul);
//           }else if(newObj[index].class == "col-md-6"){
//             if(md6 == 1){
//               var li = $("<li/>").appendTo(finalObj);
//               var ul = $("<ul/>").addClass("drag-en ui-sortable d-flex").appendTo(li);
//               tempnewmd6 = ul;
//               $(newObj[index]).clone().appendTo(ul);
//             }else if(md6 == 2){ 
//                 $(newObj[index]).clone().appendTo(tempnewmd6); 
//                 md6 = 0; 
//                 tempnewmd6 = {};
//             }
//             md6++;
//           }else if(newObj[index].class == "col-md-4"){
//             if(md4 == 1){
//               var li = $("<li/>").appendTo(finalObj);
//               var ul = $("<ul/>").addClass("drag-en ui-sortable d-flex").appendTo(li);
//               tempnewmd4 = ul;
//               $(newObj[index]).clone().appendTo(ul);
//             }else if(md4 == 2){ 
//                 $(newObj[index]).clone().appendTo(tempnewmd4); 
//             }else if(md4 == 3){ 
//                 $(newObj[index]).clone().appendTo(tempnewmd4); 
//                 md4 = 0; 
//                 tempnewmd4 = {};
//             }
//             md4++;
//           }else if(newObj[index].class == "col-md-3"){
//             if(md3 == 1){
//               var li = $("<li/>").appendTo(finalObj);
//               var ul = $("<ul/>").addClass("drag-en ui-sortable d-flex").appendTo(li);
//               tempnewmd3 = ul;
//               $(newObj[index]).clone().appendTo(ul);
//             }else if(md3 == 2){ 
//                 $(newObj[index]).clone().appendTo(tempnewmd3); 
//             }else if(md3 == 3){ 
//                 $(newObj[index]).clone().appendTo(tempnewmd3); 
//             }else if(md3 == 4){ 
//                 $(newObj[index]).clone().appendTo(tempnewmd3); 
//                 md4 = 0; 
//                 tempnewmd3 = {};
//             }
//             md4++;
//           }
//         });
//     }
// }, 1000);
// english restructure code ends
// russia restructure code start
// setTimeout(() => {
//     var a = $("ul.frmbrussia").children("li").children("ul");
//     var newObj = {};
//     if(typeof a != "undefined"){
//       $(a.children("li")).each(function(e){
//           var classname = $(a.children("li")[e]).find("input[name='className']").val();

//             newObj[e] = $(a.children("li")[e]).parent("ul").find("li:first-child").addClass("col-md-12 d-inline-block");
//             newObj[e]['class'] = 'col-md-12';
//             newObj[e]['index'] = e;

//           if(classname != ""){
//             var classexist = classname.match(/(col-md-\d*)/i);
//             if (typeof classexist[0] != "undefined") {
//               var currobj = $(a.children("li")[e]).parent("ul");
//               var nextobj = $(a.children("li")[e+1]).parent("ul");
//               $(a.children("li")[e]).parent("ul").addClass("d-flex");
//               $(a.children("li")[e]).parent("ul").find("li:first-child").addClass(classexist[0]+" d-inline-block");
//               newObj[e] = $(a.children("li")[e]);
//               newObj[e]['class'] = classexist[0];
//               newObj[e]['index'] = e;
//             }
//           }
//       });
//     }
//     if(!$.isEmptyObject(newObj)){
//         var finalObj = $("ul.frmbrussia");
//         finalObj.children().remove();
//         var formcolsmanage = {};
//         var design = '';
//         var md6 = 1,md4 = 1,md3 = 1;
//         var tempnewmd6, tempnewmd4, tempnewmd3;
//         $.each( newObj, function( index, value ){
//           if(newObj[index].class == "col-md-12"){
//               var li = $("<li/>").appendTo(finalObj);
//               var ul = $("<ul/>").addClass("drag-en ui-sortable d-flex").appendTo(li);
//               $(newObj[index]).clone().appendTo(ul);
//           }else if(newObj[index].class == "col-md-6"){
//             if(md6 == 1){
//               var li = $("<li/>").appendTo(finalObj);
//               var ul = $("<ul/>").addClass("drag-en ui-sortable d-flex").appendTo(li);
//               tempnewmd6 = ul;
//               $(newObj[index]).clone().appendTo(ul);
//             }else if(md6 == 2){ 
//                 $(newObj[index]).clone().appendTo(tempnewmd6); 
//                 md6 = 0; 
//                 tempnewmd6 = {};
//             }
//             md6++;
//           }else if(newObj[index].class == "col-md-4"){
//             if(md4 == 1){
//               var li = $("<li/>").appendTo(finalObj);
//               var ul = $("<ul/>").addClass("drag-en ui-sortable d-flex").appendTo(li);
//               tempnewmd4 = ul;
//               $(newObj[index]).clone().appendTo(ul);
//             }else if(md4 == 2){ 
//                 $(newObj[index]).clone().appendTo(tempnewmd4); 
//             }else if(md4 == 3){ 
//                 $(newObj[index]).clone().appendTo(tempnewmd4); 
//                 md4 = 0; 
//                 tempnewmd4 = {};
//             }
//             md4++;
//           }else if(newObj[index].class == "col-md-3"){
//             if(md3 == 1){
//               var li = $("<li/>").appendTo(finalObj);
//               var ul = $("<ul/>").addClass("drag-en ui-sortable d-flex").appendTo(li);
//               tempnewmd3 = ul;
//               $(newObj[index]).clone().appendTo(ul);
//             }else if(md3 == 2){ 
//                 $(newObj[index]).clone().appendTo(tempnewmd3); 
//             }else if(md3 == 3){ 
//                 $(newObj[index]).clone().appendTo(tempnewmd3); 
//             }else if(md3 == 4){ 
//                 $(newObj[index]).clone().appendTo(tempnewmd3); 
//                 md4 = 0; 
//                 tempnewmd3 = {};
//             }
//             md4++;
//           }
//         });
//     }
// }, 1000);
// russia restructure code ends
// frmbkazakistan restructure code start
// setTimeout(() => {
//     var a = $("ul.frmbkazakistan").children("li").children("ul");
//     var newObj = {};
//     if(typeof a != "undefined"){
//       $(a.children("li")).each(function(e){
//           var classname = $(a.children("li")[e]).find("input[name='className']").val();

//             newObj[e] = $(a.children("li")[e]).parent("ul").find("li:first-child").addClass("col-md-12 d-inline-block");
//             newObj[e]['class'] = 'col-md-12';
//             newObj[e]['index'] = e;

//           if(classname != ""){
//             var classexist = classname.match(/(col-md-\d*)/i);
//             if (typeof classexist[0] != "undefined") {
//               var currobj = $(a.children("li")[e]).parent("ul");
//               var nextobj = $(a.children("li")[e+1]).parent("ul");
//               $(a.children("li")[e]).parent("ul").addClass("d-flex");
//               $(a.children("li")[e]).parent("ul").find("li:first-child").addClass(classexist[0]+" d-inline-block");
//               newObj[e] = $(a.children("li")[e]);
//               newObj[e]['class'] = classexist[0];
//               newObj[e]['index'] = e;
//             }
//           }
//       });
//     }
//     if(!$.isEmptyObject(newObj)){
//         var finalObj = $("ul.frmbkazakistan");
//         finalObj.children().remove();
//         var formcolsmanage = {};
//         var design = '';
//         var md6 = 1,md4 = 1,md3 = 1;
//         var tempnewmd6, tempnewmd4, tempnewmd3;
//         $.each( newObj, function( index, value ){
//           if(newObj[index].class == "col-md-12"){
//               var li = $("<li/>").appendTo(finalObj);
//               var ul = $("<ul/>").addClass("drag-en ui-sortable d-flex").appendTo(li);
//               $(newObj[index]).clone().appendTo(ul);
//           }else if(newObj[index].class == "col-md-6"){
//             if(md6 == 1){
//               var li = $("<li/>").appendTo(finalObj);
//               var ul = $("<ul/>").addClass("drag-en ui-sortable d-flex").appendTo(li);
//               tempnewmd6 = ul;
//               $(newObj[index]).clone().appendTo(ul);
//             }else if(md6 == 2){ 
//                 $(newObj[index]).clone().appendTo(tempnewmd6); 
//                 md6 = 0; 
//                 tempnewmd6 = {};
//             }
//             md6++;
//           }else if(newObj[index].class == "col-md-4"){
//             if(md4 == 1){
//               var li = $("<li/>").appendTo(finalObj);
//               var ul = $("<ul/>").addClass("drag-en ui-sortable d-flex").appendTo(li);
//               tempnewmd4 = ul;
//               $(newObj[index]).clone().appendTo(ul);
//             }else if(md4 == 2){ 
//                 $(newObj[index]).clone().appendTo(tempnewmd4); 
//             }else if(md4 == 3){ 
//                 $(newObj[index]).clone().appendTo(tempnewmd4); 
//                 md4 = 0; 
//                 tempnewmd4 = {};
//             }
//             md4++;
//           }else if(newObj[index].class == "col-md-3"){
//             if(md3 == 1){
//               var li = $("<li/>").appendTo(finalObj);
//               var ul = $("<ul/>").addClass("drag-en ui-sortable d-flex").appendTo(li);
//               tempnewmd3 = ul;
//               $(newObj[index]).clone().appendTo(ul);
//             }else if(md3 == 2){ 
//                 $(newObj[index]).clone().appendTo(tempnewmd3); 
//             }else if(md3 == 3){ 
//                 $(newObj[index]).clone().appendTo(tempnewmd3); 
//             }else if(md3 == 4){ 
//                 $(newObj[index]).clone().appendTo(tempnewmd3); 
//                 md4 = 0; 
//                 tempnewmd3 = {};
//             }
//             md4++;
//           }
//         });
//     }
// }, 1000);
// frmbkazakistan restructure code ends
    </script>
@stop
