<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Your password has been reset!',
    'sent' => 'We have e-mailed your password reset link!',
    'token' => 'This password reset token is invalid.',
    'user' => "We can't find a user with that e-mail address.",
    'throttled' => 'Please wait before retrying.',

    'resetPassword' => 'Сброс пароля',
    'please change your password to activate your account' => 'пожалуйста, измените свой пароль, чтобы активировать свой аккаунт.',
    'currentPassword' => 'Текущий пароль',
    'newPassword' => 'Новый пароль',
    'confirmNewPassword' => 'Подтвердите новый пароль',
    'changePassword' => 'сменить пароль',
    'New password set successfully' => 'Новый пароль успешно установлен',
    'Please reset your password to login your account' => 'Пожалуйста, сбросьте пароль для входа в свою учетную запись.',
    'resetPassword' => 'Сброс пароля'

];
