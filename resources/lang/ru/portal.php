<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Content Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'Documents' => 'Documents',
    'close' => 'близко',
    'delete_question' => 'Вы уверены, что хотите удалить это',
    'delete_confirm' => 'Да, удалить!',
    'cancel' => 'Отменить',

    'are_you_sure_delete' => 'Вы уверены, что хотите удалить',
    'bulk_delete_confirm' => 'Да, удалить эти',
    'bulk_delete_nothing' => 'Вы не выбрали ничего для удаления',

    'delete_selected' => 'Удалить выбранные',
    'view' => 'Вид',
    'edit' => 'Редактировать',
    'delete' => 'Удалить',
    'active' => 'Активный',
    'inactive' => 'Неактивный',

    'Service Inquiries' => 'Заявка на услугу',

    'banner_masters' => [
        'add_new' => 'Добавить',
        'Атауы' => 'Наименование',
        'Жарыққа шығару уақыты' => 'Дата создания',
        'Ені' => 'Ширина',
        'Биіктігі' => 'Высота',
        'Папка' => 'Папка',
        'URL-жол' => 'URL-путь',
        'actions' => 'Действия',
        'banner_masters_edit' => 'Редактировать Категория баннеров',
        'banner_masters_add' => 'добавлять Категория баннеров',
        'banner_masters' => 'Категория баннеров',
    ],

    'banner_images' => [
        'title' => 'Наименование',
        'add_new' => 'Добавить',
        'banner_images' => 'Изображения баннеров',
        'image' => 'Изображение',
        'created_at' => 'Дата создания',
        'banner_category' => 'Категория баннеров',
        'actions' => 'Действия',
        'primary_text' => 'Основной текст',
//        'button_url' => 'URL кнопки',
        'order' => 'Порядок',
        'Реті' => 'Порядок',
        'all' => 'Все',
        'secondary_text' => 'Вторичный текст',

        'Кескін' => 'Изображение',
        'Атауы' => 'Наименование',
        'Негізгі мәтін' => 'Основной текст',
        'Түйме мәтінінің түсі' => 'Цвет текста кнопки',
        'Түйменің түсі' => 'Цвет кнопки',
        'Реті' => 'Порядок',
        'Жарыққа шығару уақыты' => 'Дата создания',
        'Екінші реттік мәтін' => 'Вторичный текст',
        'Banner Category' => 'Категория баннеров',
        'Түйме URL' => 'URL кнопки',
        'banner_images_edit' => 'EРедактировать dit Изображения баннеров',
        'banner_images_add' => 'добавлять Изображения баннеров'
    ],

    'pages' => [
        'asdasd'=>'ssaaaaa',
        'add_new' => 'Добавить',
        'title' => 'Наименование',
        'status' => 'Статус',
        'actions_table' => 'Действия',
        'pages' => 'Страницы',
        'delete_confirm' => 'Да, удалить!',
        'cancel' => 'Отменить',
        'return_to_list' => 'Вернуться к списку',
        'viewing_page' => 'Просмотр страницы',
        'edit_page' => 'Редактирование страницы',
        'edit' => [
            'edit' => 'Редактирование страницы',
            'add' => 'добавлять Страницы',
            'Title' => 'Заголовок',
            'Excerpt' => 'Выдержка',
            'Body' => 'Основная часть',
            'Slug' => 'URL-путь',
            'Meta Description' => 'Meta Description',
            'Meta Keywords' => 'Meta Keywords',
            'Status' => 'Статус',
            'save' => 'Сохранить'
        ],
        'view' => 'Просмотр страницы',
        'edit_page_detail' => 'Редактировать',
        'delete' => 'Удалить',

        'Атауы' => 'Наименование',
        'Статус' => 'Статус',
        'delete_conformation' => 'Вы уверены, что хотите удалить следующие страницы?'
    ],

    'news' => [
        'news' => 'Новости',
        'add_new' => 'Добавить',
        'actions_table' => 'Действия',
        'delete_confirm' => 'Да, удалить!',
        'cancel' => 'Отменить',
        'Тақырып' => 'Заголовок ',
        'Кескін' => 'Изображение',
        'Created At' => 'Дата создания',
        'news_add' => 'добавлять Новости',
        'news_edit' => 'Редактировать Новости',
    ],

    'projects' => [
        'Жобалар' => 'проектов',
        'Атауы' => 'заглавие',
        'Описание' => 'Описание',
        'Күй' => 'Положение дел',
        'add_new' => 'Добавить',
        'actions_table' => 'Действия',
        'delete_confirm' => 'Да, удалить!',
        'cancel' => 'Отменить',
        'Тақырып' => 'Заголовок ',
        'Кескін' => 'Изображение',
        'Created_at' => 'Дата создания'
    ],

    'graph_blocks' => [
        'add_new' => 'Добавить',
        'graph_blocks' => 'Графические блоки',
        'Негізгі мәтін' => 'Главный текст',
        'Сипаттамасы' => 'Описание',
        'Түсі' => 'Цвет',
        'order' => 'Порядок',
        'actions_table' => 'Действия',
        'delete_confirm' => 'Да, удалить!',
        'cancel' => 'Отменить',
        'Реті' => 'Порядок',
        'count' => 'Count',
        'Count' => 'Count',
        'graph_blocks_edit' => 'Редактировать Графические блоки',
        'graph_blocks_add' => 'добавлять Графические блоки',
    ],

    'footer_widgets' => [
        'footer_widgets' => 'Нижний колонтитул',
        'title' => 'Заголовок',
        'content' => 'Контент',
        'order' => 'Порядок',
        'slug'=>'URL-путь',
        'add_new' => 'Добавить',
        'actions_table' => 'Действия',
        'delete_confirm' => 'Да, удалить!',
        'cancel' => 'Отменить',
        'Created At' => 'Дата создания',
        'footer_widgets_add' => 'добавлять Нижний колонтитул',
        'footer_widgets_edit' => 'Редактировать Нижний колонтитул'
    ],

    'users' => [
        'users' => 'Пользователи',
        'add_new' => 'Добавить',
        'Аты' => 'Имя',
        'Электронная почта' => 'Эл. адрес',
        'Тегі' => 'фамилия',
        'Created At' => 'Дата создания',
        'Primary Role' => 'основная роль',
        'actions' => 'Действия',
        'active' => 'Активный',
        'inActive' => 'Неактивный',
        'edit_users' => 'Редактировать Пользователи',
        'add_users' => 'Добавить Пользователи',
        'name' => 'Имя',
        'email' => 'Электронная почта',
        'last_name' => 'Фамилия',
        'contact' => 'Контакты',
        'Status' => 'Статус',
        'status' => 'Статус',
//        'active' => 'Активный',
//        'inActive' => 'Неактивный',
        'Электрондық пошта' => 'Электронная почта'
    ],

    'roles' => [
        'roles' => 'Роли',
        'add_new' => 'Добавить роль',
        'name' => 'Имя',
        'display_name' => 'Отображаемое имя',
        'slug' => 'URL-путь',
        'permissions' => 'Разрешения',
        'submit' => 'Принять',
        'actions_table' => 'Действия',
        'Атауы' => 'Имя',
        'Көрсетілетін атауы' => 'Отображаемое имя',
        'URL-жол' => 'URL-путь',
        'roles_add' => 'добавлять Роли',
        'roles_edit' => 'Редактировать Роли',

        'add' =>[
            'select_all' => 'Выбрать все',
            'deselect_all' => 'Отменить выбор',
            'section' => 'Секция',
            'page' => 'Страница',
            'browse' => 'Просмотривать',
            'read' => 'Читать',
            'add' => 'Добавлять',
            'edit' => 'Редактировать',
            'delete' => 'Удалять',
        ]
    ],

    'product' => [
        'products' => 'Продукты',
        'add_new' => 'Добавить продукт',
        'actions_table' => 'Действия',
        'edit_product' => 'Редактировать Продукты',
        'add_product' => 'Добавить Продукты',
        'manager'=> 'Менеджер',
        'save' => 'Сохранить',
        'Атауы' => 'Наименование',
        'БП коды' => 'Код БП',
        'Логотип' => 'Логотип',
        'Еншілес ұйымдар' => 'Дочерние организации',
        'Өнім категориясы' => 'Категория продукта',
        'all' => 'Все',
        'Сипаттамасы' => 'Описание',
        'Тапсырыс бере алады' => 'Могут подавать',
        'Created At' => 'Дата создания'
    ],

    'product_category' => [
        'product_categories' => 'Категории продуктов',
        'add_new' => 'Добавить',
        'actions_table' => 'Действия',
        'created_at' => 'Дата создания',
        'Атауы' => 'Наименование',
        'Сипаттамасы' => 'Описание',
        'Логотип' => 'Логотип',
        'add' => 'Добавить Категории продуктов',
        'edit' => 'Редактировать Категории продуктов',
        'save' => 'Сохранить',

        'Created At' => 'Created At',
        'add_product_category' => 'Добавить Категории продуктов',
        'edit_product_category' => 'Редактировать Категории продуктов',

    ],

    'media' => [
        'media' => 'Медиа',
        'upload' => 'Загрузить',
        'add folder' => 'Добавить папку',
        'move' => 'Переместить',
        'delete' => 'Удалить',
        'crop' => 'Обрезать',
        'media library' => 'Медиа библиотека ',
        'title' => 'Наименование',
        'type' => 'Тип',
        'size' => 'Размер',
        'public url' => 'Публичная ссылка',
        'last modified' => 'Последнее изменение',
        'add new folder' => 'Добавить новую папку',
        'new folder name' => 'Название новой папки',
        'cancel' => 'Отменить',
        'create new folder' => 'Создать новую папку',
        'move file/foler' => 'Переместить файл/папку',
        'destination folder' => 'Папка назначения',
        'cancel' => 'Отменить',
        'move' => 'Переместить',
        'are you sure you want to delete the following file(s)?' => 'Вы уверены, что хотите удалить следующие файл(ы)?',
        'deleting a folder will remove all files and folders contained inside' => 'Удаление папки удалит все файлы и папки, содержащиеся внутри',
        'Cancel' => 'Отменить',
        'Yes, Delete it!' => 'Да, удалить!',
        'Menu Builder' => 'Конструктор меню',
        'Add new' => 'Добавить',
        'builder' => 'Конструктор',
        'new menu item' => 'Новый элемент меню ',
        'edit menu item' => 'Редактировать элемент меню',
        'title of the menu item' => 'Наименование элемента меню',
        'link type' => 'Тип сслыки',
        'static url' => 'Статистический URL-путь',
        'dynamic route' => 'Динамический маршрут',
        'url for the menu item' => 'URL для элемента меню',
        'font icon class for the menu item' => 'Класс значка шрифта для элемента меню',
        'color in rgb or hex (optional)' => 'цвет в rgb или hex (опционально)',
        'open in' => 'Открыть в',
        'same tab/window' => 'том же окне',
        'new tab/window' => 'новом окне',
        'update' => 'Обновить',
        'edit' => 'Редактировать',
        'delete' => 'Удалить',
        'search' => 'Поиск',
    ],

    'menu' => [
        'edit_page' => 'Редактировать элемент меню',
        'add_page' => 'Добавить',
        'menus'=> 'Конструктор меню',
        'actions' => 'Действия',
        'menu_builder' => 'Конструктор меню',
        'add_new' => 'Добавить',
        'builder' => 'Конструктор',
        'new_menu_item' => 'Новый элемент меню ',
        'edit_menu_item' => 'Редактировать элемент меню',
        'title_of_the_menu_item' => 'Наименование элемента меню',
        'link_type' => 'Тип сслыки',
        'static_url' => 'Статистический URL-путь',
        'dynamic_route' => 'Динамический маршрут',
        'url_for_the_menu_item' => 'URL для элемента меню',
        'font_icon_class_for_the_menu_item' => 'Класс значка шрифта для элемента меню',
        'color_in_rgb_or_hex_(optional)' => 'цвет в rgb или hex (опционально)',
        'open_in' => 'Открыть в',
        'same_tab/window' => 'том же окне',
        'new_tab/window' => 'новом окне',
        'update' => 'Обновить',
        'edit' => 'Редактировать',
        'delete' => 'Удалить',
        'search' => 'Поиск',
        'Конструктор' => 'Конструктор',
        'name' => 'Атауы'
    ],

    'only_formats_are_allowed' => 'Разрешены только форматы',

    'front_end' =>[
        'service_documents' => 'Документы по услуге',
        'application_success_message' => 'Вы подали заявку на эту услугу. Наши сотрудники рассмотрят вашу заявку и вскрое свяжутся с вами.'
    ],

    'service_inquiry' => [
        'return_to_list' => 'Вернуться к списку',
        'viewing_service_inquiry' => 'Просмотр заявки на услугу',
        'registration_number' => 'Номер регистрации'
    ],

    'product_manager' => [
        'product managers' => 'Менеджеры продукта',
        'add new' => 'Добавить',
        'first name' => 'Имя',
        'last name' => 'Фамилия',
        'gender' => 'Пол',
        'email' => 'Электронная почта',
        'contact' => 'Контакты',
        'actions' => 'Действия',
        'view' => 'Посмотреть',
        'edit' => 'Редактировать',
        'delete' => 'Удалить',
    ],

    'subsidiaries' => [
        'Еншілес_ұйымдар' => 'Дочерние',
        'subsidiaries_add' => 'Добавить Дочерние организации',
        'subsidiaries_edit' => 'Редактировать Дочерние организации',
        'Атауы' => 'заглавие',
        'Сипаттамасы' => 'Описание',
        'Жарыққа шығару уақыты' => 'Время выхода',
        'actions_table' => 'действия',
    ],

    'product-categories' => [
        'Өнім_категориялары' => 'Өнім_категориялары',
        'subsidiaries_add' => 'Add категория продукта',
        'subsidiaries_edit' => 'Edit категория продукта',
        'Атауы' => 'имя объекта',
        'Сипаттамасы' => 'Описание',
        'Логотип' => 'Логотип',
        'Жарыққа шығару уақыты' => 'Created At',
        'действия' => 'действия',
        'actions_table' => 'действия',
        'add_new' => 'Добавить новое',
        'view' => 'View',
        'edit' => 'Edit',
        'delete' => 'Delete',
    ],

    'consulting_service_product' => [
        'Консалтинг Сервис Продукты' => 'Консалтинг Сервис Продукт',
        'consulting_service_product_add' => 'добавлять Консалтинг Сервис Продукт',
        'consulting_service_product_edit' => 'редактировать Консалтинг Сервис Продукт',
        'Өнім' => 'Продукт',
        'Жарыққа шығару уақыты' => 'Created At',
        'действия ' => 'действия',
        'view' => 'Посмотреть',
        'edit' => 'Редактировать',
        'delete' => 'Удалить',
    ],

    'product-managers' => [
        'product-managers_add' => 'Добавить Менеджеры продукта',
        'product-managers_edit' => 'Редактировать Менеджеры продукта'
    ]
];
