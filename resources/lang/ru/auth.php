<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login' => 'Авторизоваться',
    'logout' => 'Выйти',
    'failed' => 'Эти учетные данные не соответствуют нашим записям.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'giveItTry' => 'Попробуйте!',
    'welcome' => 'Добро пожаловать',
    'sign in to account' => 'Пожалуйста, войдите в ваш аккаунт',
    'no account' => 'Нет аккаунта?',
    'sign up now' => 'Зарегистрироваться',
    'email' => 'Электронная почта',
    'email here' => 'Ваша эл. почта...',
    'password' => 'Пароль',
    'password here' => 'Ваш пароль...',
    'keep logged in' => 'Запомнить меня',
    'forgot password?' => 'Забыли пароль?',

    'sign up' => 'Зарегистрироваться',
    'takes few minutes' => 'Это займет всего несколько минут, чтобы зарегистрироваться',
    'first name' => 'Имя',
    'last name' => 'Фамилия',
    'patronymic name' => "Отчество",
    'confirm email' => 'Подтвердить почту',
    'gender' => 'Пол',
    'date of birth' => 'Дата рождения',
    'address' => 'Адрес проживания',
    'phone number' => 'Номер телефона',
    'region' => 'Область',
    'town/city' => 'Город проживания',
    'post code' => 'Почтовый индекс',
    'terms' => 'Принять наши',
    'condition' => 'условия и положения',
    'already an account' => 'Уже есть аккаунт?',
    'sign' => 'Войти',
    'sign_page' => 'Войти',
    'VATno' => 'НДС нет',
    'sign_page' => 'Войти',
    'forgot password' => 'Забыли пароль?',
    'forgot password instructions' => 'Если вы забыли пароль, введите свой адрес электронной почты и следуйте инструкциям',
    'login here' => 'Войти здесь',
    'send link' => 'Отправить ссылку',
];
