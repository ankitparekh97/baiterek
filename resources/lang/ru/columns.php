<?php

return [
    'Name' => 'Имя',
    'Last name' => 'Фамилия',
    'Applicant type' => 'Тип заявителя',
    'Number of request' => 'Номерзаявления',
    'Name of service' => 'Наименование услуги',
    'Name of responsible person' => 'ФИО ответственного лица',
    'Status of request' => 'Статус заявления',
    'Created At' => 'Создан в',
];
