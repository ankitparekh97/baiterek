<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    "sEmptyTable" => "No data available in table",
	"sInfo" => "Показаны записи с _START_ до _END_ из _TOTAL_",
	"sInfoEmpty" => "Показаны записи с 0 до 0 из 0",
	"sInfoFiltered" => "(filtered from _MAX_ total entries)",
	"sInfoPostFix" => "",
	"sInfoThousands" => ",",
	"sLengthMenu" => "Показать _MENU_ записей",
	"sLoadingRecords" => "Loading...",
	"sProcessing" => "Processing...",
	"sSearch" => "Поиск:",
	"sZeroRecords" => "No matching records found",
	"sFirst" => "First",
	"sLast" => "Last",
	"sNext" => "Следующий",
	"sPrevious" => "Предыдущий",
	"sSortAscending" => ": activate to sort column ascending",
	"sSortDescending" => ": activate to sort column descending"
];