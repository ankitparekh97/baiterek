<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Fields Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'First Name' => 'Имя',
    'Last Name' => 'Фамилия',
    'Contact' => 'Номера телефона',
    'Patronymic name' => "Отчество",
    'E-mail' => 'Электронная почта',
    'Email of recipient' => 'Электронная почта получателя',
    'Gender' => 'Пол',
    'Marital Status' => 'Семейный статус',
    'Address of Residence' => 'Адрес проживания',
    'Message' => 'Сообщение',
    'Choose' => 'Выбрать',
    'Male' => 'мужчина',
    'Female' => 'женский',
    'Married' => 'замужем',
    'Single' => 'не замужем',
    'Separated' => 'отделенный',
    'Divorced' => 'Разведенный',
    'Apply Now' => 'Подать сейчас',
    'Application Status' => 'Статус приложения',
    'Application Type' => 'Тип заявителя',
    'Physical Entity' => 'Физическое лицо',
    'Legal entity' => 'Юридическое лицо',
    'Organization' => 'организация',
    'Applicant Documents' => 'Документы заявителя',
    'IIN/BIN' => 'ИИН/БИН',
    'View All' => 'Просмотреть все',
    'Search' => 'Поиск',
    'Apply' => 'Подать заявку',
    'More' => 'Подробнее',
    'News' => 'Новости',
    'Read More' => 'Читать дальше',
    'Latest News' => 'Последние новости',
    'Browse Services' => 'Просмотрите наши услуги',
    'Top Products' => 'Лучший продукт по категории',
    'ProductDetails' => 'информация о продукте',
    'ApplicantDetails' => 'Сведения о заявителе',
    'ApplicationForm' => 'Форма заявки',
    'Step1' => 'Шаг 1',
    'Step2' => 'Шаг 2',
    'Step3' => 'Шаг 3',
    'Step4' => 'Шаг 5',
    'Step5' => 'Шаг 5',
    'checkpoints' => 'ропускной пункт',
    'addMoreBtn' => 'Добавить больше',
    'documentPool' => 'пул документов',
    'Milestone' => 'веха',
    'Coming soon'  => 'Скоро',
    'Save Draft'  => 'Сохранить черновик',
    'selectMilestone'  => 'Выберите этап (дней)',
    'externalVerification'  => 'Требуется внешняя проверка',
    'comment'  => 'Комментарий',
    'externalDoc'  => 'Добавить документ',
    'documentError'  => 'Оба поля обязательны для заполнения',
    'ViewProjects' => 'Посмотреть проекты',
    'Projects' => 'проектов',
    'NoProjectsAvailable' => 'Нет доступных проектов',
];
