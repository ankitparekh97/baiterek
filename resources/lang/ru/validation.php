<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => ':attribute должен быть принят.',
    'default_accept' => 'Разрешен только файл такого типа, как jpg / png / jpeg / pdf.',
    'active_url' => ':attribute не является допустимым URL-адресом.',
    'after' => ':attribute должна быть датой после :date.',
    'after_or_equal' => ':attribute должна быть датой после или равна :date.',
    'alpha' => ':attribute может содержать только буквы.',
    'alpha_dash' => ':attribute может содержать только буквы, цифры, тире и подчеркивания.',
    'alpha_num' => ':attribute может содержать только буквы и цифры.',
    'array' => ':attribute должен быть массивом.',
    'before' => ':attribute должна быть датой перед :date.',
    'before_or_equal' => ':attribute должна быть датой до или равна :date.',
    'between' => [
        'numeric' => ':attribute должен быть между :min и :max.',
        'file' => ':attribute должен быть между :min и :max килобайтами.',
        'string' => ':attribute должен быть между :min и :max символами.',
        'array' => ':attribute должен иметь между :min и :max элементами.',
    ],
    'boolean' => 'Поле :attribute должно быть истинным или ложным.',
    'confirmed' => 'Подтверждение :attribute не совпадает.',
    'date' => ':attribute является недопустимой датой.',
    'date_equals' => ':attribute должна быть датой, равной :date.',
    'date_format' => ':attribute не соответствует формату :format.',
    'different' => ':attribute и :other должны быть разными.',
    'digits' => ':attribute должен быть :digits цифрами.',
    'digits_between' => ':attribute должен быть между :min и :max цифрами.',
    'dimensions' => ':attribute имеет недопустимые размеры изображения.',
    'distinct' => 'Поле :attribute имеет повторяющееся значение.',
    'email' => ':attribute должен быть действующим адресом электронной почты.',
    'ends_with' => ':attribute должен заканчиваться одним из следующих : :values',
    'exists' => 'Выбранный :attribute недействителен.',
    'file' => ':attribute должен быть файлом.',
    'filled' => 'Поле :attribute должно иметь значение.',
    'gt' => [
        'numeric' => ':attribute должно быть больше :value.',
        'file' => ':attribute должно быть больше :value килобайт.',
        'string' => ':attribute должно быть больше :value символов.',
        'array' => ':attribute должно содержать более :value элементов.',
    ],
    'gte' => [
        'numeric' => ':attribute должно быть больше или равно :value.',
        'file' => ':attribute должен быть больше или равен :value килобайтам.',
        'string' => ':attributeдолжно быть больше или равно :value символам.',
        'array' => 'У :attribute должно быть :value элементов или больше.',
    ],
    'image' => ':attribute должно быть изображением.',
    'in' => 'Выбранный :attribute недействителен.',
    'in_array' => 'Поле :attribute не существует в :other.',
    'integer' => ':attribute должно быть целым числом.',
    'ip' => ':attribute должен быть действующим IP-адресом.',
    'ipv4' => ':attribute должен быть действующим IPv4-адресом.',
    'ipv6' => ':attribute должен быть действующим IPv6-адресом.',
    'json' => ':attribute должна быть допустимой строкой JSON.',
    'lt' => [
        'numeric' => ':attribute должно быть меньше :value.',
        'file' => ':attribute должен быть меньше :value килобайт.',
        'string' => ':attribute должно быть меньше :value символов.',
        'array' => 'У :attribute должно быть меньше :value элементов.',
    ],
    'lte' => [
        'numeric' => ':attribute должно быть меньше или равно :value.',
        'file' => ':attribute должен быть меньше или равно :value килобайтам.',
        'string' => ':attribute должно быть меньше или равно :value символам.',
        'array' => ':attribute не должно содержать более :value элементов.',
    ],
    'max' => [
        'numeric' => ':attribute не может быть больше :max.',
        'file' => ':attribute не может быть больше :max килобайт.',
        'string' => ':attribute не может быть больше :max символов.',
        'array' => ':attribute не может содержать более :max элементов.',
    ],
    'mimes' => ':attribute должен быть файлом типа : :values.',
    'mimetypes' => ':attribute должен быть файлом типа : :values.',
    'min' => [
        'numeric' => ':attribute должно быть как минимум :min.',
        'file' => ':attribute должен быть не менее :min килобайт.',
        'string' => ':attribute должно быть не менее :min символов.',
        'array' => ':attribute должно содержать как минимум :min элементов.',
    ],
    'not_in' => 'Выбранный :attribute недействителен.',
    'not_regex' => ':attribute формат недействителен.',
    'numeric' => ':attribute должен быть числом.',
    'password' => 'Пароль неверен.',
    'present' => 'Поле :attribute должно присутствовать.',
    'regex' => ':attribute формат недействителен.',
    'default_required' => 'Это поле обязательно к заполнению.',
    'required' => 'Поле :attribute является обязательной.',
    'required_if' => 'Поле :attribute является обязательным, если :other :value.',
    'required_unless' => 'Поле :attribute является обязательным, если :other не находится в :values.',
    'required_with' => 'Поле :attribute обязательно при наличии :values.',
    'required_with_all' => 'Поле :attribute обязательно, если присутствует :values.',
    'required_without' => 'Поле :attribute является обязательным, когда :values отсутствует.',
    'required_without_all' => 'Поле :attribute является обязательным, если все из :values отсутствует.',
    'same' => ':attribute и :other должны совпадать.',
    'size' => [
        'numeric' => ':attribute должно быть :size.',
        'file' => ':attribute должно быть :size килобайт.',
        'string' => ':attribute должно быть :size символами.',
        'array' => ':attribute должен содержать :size элементов.',
    ],
    'starts_with' => ':attribute должен начинаться с одного из следующих: :values',
    'string' => ':attribute должна быть строкой.',
    'timezone' => ':attribute должна быть допустимой зоной.',
    'unique' => ':attribute уже занято.',
    'uploaded' => ':attribute не удалось загрузить.',
    'url' => ':attribute формат недействителен.',
    'uuid' => ':attribute должен быть действительным UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'Сообщение',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

    'invalid password' => 'Неправильный пароль. Пожалуйста, попробуйте еще раз.',
    'email not registered' => 'Идентификатор электронной почты не зарегистрирован.',
    'terms' => 'Пожалуйста, примите наши условия и положения.',
    'equalTo' => 'Пожалуйста, введите тот же адрес электронной почты.',
    'valid contact' => 'Пожалуйста, введите правильную контактную информацию.',

    'registration not complete' => 'Ваша регистрация не может быть завершена. Пожалуйста, свяжитесь с администратором сайта.',
    'registration complete email error' => 'Ваша регистрация прошла успешно. Пожалуйста, проверьте электронную почту, чтобы завершить процесс регистрации. Если вы не получили регистрационное письмо, свяжитесь с администратором сайта.',
    'registration complete' => 'Ваша регистрация прошла успешно. Пожалуйста, проверьте электронную почту, чтобы завершить процесс регистрации.',

    'email not found' => 'Электронная почта не совпадает в наших записях.',
    'reset password link send' => 'Ссылка для сброса пароля была отправлена на вашу электронную почту.',
    'link expire' => 'Эта ссылка больше не доступна',

    'userIsInactive' => 'Ваша учетная запись заблокирована, пожалуйста, свяжитесь с администратором',
];
