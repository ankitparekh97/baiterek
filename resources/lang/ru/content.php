<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Content Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'download' => 'Download',
    'documents' => 'Documents',
    'No Match' => 'Совпадение не найдено',

    'cabinet' => 'Кабинет',
    'copy_rights' => 'Авторское право',
    'all_rights' => 'Байтерек, Все права защищены.',

    'General Information' => 'Сәйкестік табылған жоқ',

    'dueInDay' => 'Due in :day day',
    'overdueInDay' => 'Overdue by :day day',
    'overdueToday' => 'Overdue by today',
    'appSendSince' => 'Application send since :day day',
    'appSendToday' => 'Application send today',

    'Draft' => 'Черновик',
    'Applied' => 'Заявка отправлена',
    'Application received' => 'Заявка получена',
    'On control' => 'На контроле',
    'Control' => 'Контроль',
    'Data/Documents are missing' => 'Информация/Документы недостают',
    'In process' => 'В процессе',
    'Accepted' => 'Принято',
    'Rejected' => 'Отказано',

    'noActivity' => 'Нет активности',
    'moveThisReq' => ' осы сұранысты қозғалу :before в :after',
    'cancel' => 'Отмена',
    'activityList' => 'Список активности',
    'save' => 'Сохранить',
    'write_a_message' => 'Напишите сообщение…',
    'ServiceInquiryDetail' => 'Подробная информация об услуге',
    'ExportToPDF' => 'Экспорт в PDF',
    'ExportToCSV' => 'Экспорт в XLS',
    'ticket' => 'проездной билет',
    'edit' => 'редактировать',
    'markedAs' => ' отмеченный :label :checked',
    'checked' => 'проверено',
    'unchecked' => 'не проверено',
    'completeCheckpointFirst' => 'Пожалуйста, сначала заполните все контрольные точки.',
    'checkpointComment' => 'set comment ":checkPointComment" to "checkPointLabel" checkpoint',
    'any_comment' => 'Type....',
    'title_statement' => '«Национальный управляющий холдинг «Байтерек»',
    'verificationIsPending' => 'Third party verification is pending',
    'apply_for_more' => 'Подать ещё',
    'see_application_status' => 'Посмотреть статус заявки',
    'title' => 'Наименование',
    'customer' => 'Клиент',
    'ticket_number' => 'Номер регистрации',
    'language' => 'язык',
    'due' => 'Время ожидания (дни)',
    'details' => 'Детали',
    'action' => 'Действия',
    'status' => 'Статус',
    'invite_users_for_verification' => 'пригласить пользователей для проверки',
    'invite' => 'приглашать',
    'verification_data' => 'данные проверки',
    'data' => 'данные',
    'moveThisApplication' => 'Ваша заявка :after',
    'verificationMessage' => 'имеет :status этот запрос',
    'invitedUsers' => 'пригласил ":email" для :document подтверждения',
    'andAddedComment' => ' и добавил комментарий :comment',
    'upload' => 'Upload',
    'Unauthorized' => 'Вы не авторизованы для просмотра этой страницы',

    'migrated_files' => 'Приложенные файлы',
    'userName' => 'Имя пользователя',
    'fileName' => 'Наименование файла',
    'fileDate' => 'Дата миграции',
    'product_name' => 'Наименование услуги',
    'documents_migrated' => 'Приложенные файлы',
    'download_migrated' => 'Скачать',
    'go to old dashboard' => 'Перейти на предыдущую версию портала',
];
