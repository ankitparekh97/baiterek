<?php

return [

    /*
    |--------------------------------------------------------------------------
    | content language lines
    |--------------------------------------------------------------------------
    |
    | the following language lines are used during authentication for various
    | messages that we need to display to the user. you are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'status change successfully' => 'Статус успешно обновлен',
    'successfully add new product and invitation sent to product manager' => 'Новый продукт успешно добавлен и приглашение менеджеру продукта было отправлено',
    'successfully updated product' => 'Продукт успешно обновлен',
    'successfully deleted this product' => 'Данный продукт успешно удален',
    'oops something went wrong' => 'Что-то пошло не так',
    'invited successfully' => 'Приглашение успешно отправлено',
    'third party verification is pending' => 'Проверка третьей стороны еще не завершена',
    'your application has been drafted successfully and notification sent on your registered email address' => 'Ваша заявка успешно сохранена в черновиках и и на ваш зарегистрированный адрес электронной почты отправлено уведомление.',
    'your profile has been updated successfully' => 'Ваш профиль успешно обновлен',
    'admin cms' => 'Администратор CMS',
    'successfully updated page' => 'Страница успешно обновлена',
    'successfully deleted page' => 'Страница успешно удалена',
    'successfully added new page' => 'Новая страница успешно добавлена',
    'successfully updated banner category' => 'Категория баннеров успешно обновлена',
    'successfully deleted banner category' => 'Категория баннеров успешно удалена',
    'successfully added new banner category' => 'Новая категория баннеров успешно добавлена',
    'successfully updated news' => 'Новости успешно обновлены',
    'successfully deleted this news' => 'Новости успешно удалены',
    'successfully added new news' => 'Новости успешно добавлены',
    'successfully updated graph block' => 'Графический блок успешно обновлен',
    'successfully deleted graph block' => 'Графический блок успешно удален',
    'successfully added new graph block' => 'Новый графический блок успешно добавлен',
    'successfully updated footer widget' => 'Нижний колонтитул успешно обновлен',
    'successfully added new footer widget' => 'Новый нижний колонтитул успешно добавлен',
    'successfully deleted footer widget' => 'Нижний колонтитул успешно удален',
    'successfully updated menu' => 'Меню успешно обновлен',
    'successfully added new menu' => 'Новое меню успешно добавлено',
    'successfully deleted this menu' => 'Меню успешно удален',
    'successfully added new user activation link has been sent to' => 'Новый пользователь успешно добавлен. Ссылка с активацией отправлена ":email"',
    'successfully updated new user' => 'Новый пользователь успешно обновлен',
    'successfully deleted this user' => 'Пользователь успешно удален',
    'successfully added new role' => 'Новая роль успешно добавлена',
    'successfully updated role' => 'Роль успешно обновлена',
    'successfully deleted this role' => 'Роль успешно удалена',
    'successfully added new product category' => 'Новая категория продуктов успешно добавлена',
    'successfully updated product category' => 'Категория продуктов успешно обновлена',
    'successfully deleted product category' => 'Категория продуктов успешно удалена',
    'successfully added new product manager activation link has been sent to' => 'Новый менеджер продукта успешно добавлен. Ссылка с активацией отправлена :email',
];