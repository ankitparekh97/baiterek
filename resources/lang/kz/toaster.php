<?php

return [

    /*
    |--------------------------------------------------------------------------
    | content language lines
    |--------------------------------------------------------------------------
    |
    | the following language lines are used during authentication for various
    | messages that we need to display to the user. you are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'status change successfully' => 'Статус сәтті түрде жаңартылды',
    'successfully add new product and invitation sent to product manager' => 'Жаңа өнім сәтті түрде қосылды және өнім менеджеріне шақыру жөнелтілді',
    'successfully updated product' => 'Өнім сәтті түрде жаңартылды',
    'successfully deleted this product' => 'Бұл өнім сәтті түрде жойылды',
    'oops something went wrong' => 'Бірдеңе дұрыс болмады',
    'invited successfully' => 'Шақыру сәтті түрде жөнелтілді',
    'third party verification is pending' => 'Үшінші тараптың тексеруі аяқталған жоқ',
    'your application has been drafted successfully and notification sent on your registered email address' => 'Сіздің өтінішіңіз қолжазбаның алғашқы түрінде сәтті сақталды және тіркелген электрондық поштаңызға хабарлама жіберілді.',
    'your profile has been updated successfully' => 'Сіздің профиліңіз сәтті түрде жаңартылды',
    'admin cms' => 'CMS администраторы',
    'successfully updated page' => 'Бет сәтті түрде жаңартылды',
    'successfully deleted page' => 'Бет сәтті түрде жойылды',
    'successfully added new page' => 'Жаңа бет сәтті түрде қосылды',
    'successfully updated banner category' => 'Баннерлер категориясы сәтті түрде жаңартылды',
    'successfully deleted banner category' => 'Баннерлер категориясы сәтті түрде жойылды',
    'successfully added new banner category' => 'Жаңа баннерлер категориясы сәтті түрде қосылды',
    'successfully updated news' => 'Жаңалықтар сәтті түрде жаңартылды',
    'successfully deleted this news' => 'Жаңалықтар сәтті түрде жойылды',
    'successfully added new news' => 'Жаңалықтар сәтті түрде қосылды',
    'successfully updated graph block' => 'Графикалық блок сәтті түрде жаңартылды',
    'successfully deleted graph block' => 'Графикалық блок сәтті түрде жойылды',
    'successfully added new graph block' => 'Жаңа графикалық блок сәтті түрде қосылды',
    'successfully updated footer widget' => 'Төменгі деректеме сәтті түрде жаңартылды',
    'successfully added new footer widget' => 'Жаңа төменгі деректеме сәтті түрде қосылды',
    'successfully deleted footer widget' => 'Төменгі деректеме сәтті түрде жаңартылды',
    'successfully updated menu' => 'Мәзір сәтті түрде жаңартылды',
    'successfully added new menu' => 'Жаңа мәзір сәтті түрде қосылды',
    'successfully deleted this menu' => 'Мәзір сәтті түрде жойылды',
    'successfully added new user activation link has been sent to' => 'Жаңа қолданушы сәтті түрде қосылды. Белсендіруі бар сілтеме ":email" жіберілді.',
    'successfully updated new user' => 'Жаңа қолданушы сәтті түрде жаңартылды',
    'successfully deleted this user' => 'Қолданушы сәтті түрде жойылды',
    'successfully added new role' => 'Жаңа рөл сәтті түрде қосылды',
    'successfully updated role' => 'Рөл сәтті түрде жаңартылды',
    'successfully deleted this role' => 'Рөл сәтті түрде жойылды',
    'successfully added new product category' => 'Жаңа өнімдер категориясы сәтті түрде қосылды',
    'successfully updated product category' => 'Өнімдер категориясы сәтті түрде жаңартылды',
    'successfully deleted product category' => 'Өнімдер категориясы сәтті түрде жойылды',
    'successfully added new product manager activation link has been sent to' => 'Жаңа өнім менеджері сәтті түрде қосылды. Белсендіруі бар сілтеме :email жіберілді.',
];