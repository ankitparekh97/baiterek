<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Fields Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'First Name' => 'Аты',
    'Last Name' => 'Тегі',
    'Contact' => 'Телефон нөмірлері',
    'Patronymic name' => "Әкесінің аты",
    'E-mail' => 'Электрондық пошта',
    'Email of recipient' => 'Алушының электрондық поштасы',
    'Gender' => 'Жыныс',
    'Marital Status' => 'Отбасы жағдайы',
    'Address of Residence' => 'Тұрғылықты мекен-жайы',
    'Message' => 'Хабар',
    'Choose' => 'Таңдау',
    'Male' => 'мужчина',
    'Female' => 'женский',
    'Married' => 'замужем',
    'Single' => 'не замужем',
    'Separated' => 'отделенный',
    'Divorced' => 'Разведенный',
    'Apply Now' => 'Қазір қолданыңыз',
    'Application Status' => 'Өтінім күйі',
    'Application Type' => 'Өтініш берушінің түрі',
    'Physical Entity' => 'Жеке тұлға',
    'Legal entity' => 'Заңды тұлға',
    'Organization' => 'Ұйымдастыру',
    'Applicant Documents' => 'Өтініш берушінің құжаттары',
    'IIN/BIN' => 'ЖСН/БСН',
    'View All' => 'Барлығын қарау',
    'Search' => 'Іздеу',
    'Apply' => 'Өтінім беру',
    'More' => 'Толығырақ ',
    'News' => 'Жаңалықтар',
    'Read More' => 'Толығырақ',
    'Latest News' => 'Соңғы жаңалықтар',
    'Browse Services' => 'Біздің қызметтерімізді шолыңыз',
    'Top Products' => 'Санат бойынша үздік өнім',
    'ProductDetails' => 'Өнім туралы мәліметтер',
    'ApplicantDetails' => 'Өтініш беруші туралы мәліметтер',
    'ApplicationForm' => 'Өтініш формасы',
    'Step1' => '1-қадам',
    'Step2' => '2-қадам',
    'Step3' => '3-қадам',
    'Step4' => '4-қадам',
    'Step5' => '5-қадам',
    'checkpoints' => 'Бақылау-өткізу пункті',
    'addMoreBtn' => 'Қосымша қосыңыз',
    'documentPool' => 'құжаттар пулы',
    'Milestone' => 'Белес',
    'Coming soon'  => 'Жақында шығады',
    'Save Draft'  => 'Жобаны сақтау',
    'selectMilestone'  => 'Уақытты таңдау (күндер)',
    'externalVerification'  => 'Сыртқы тексеру қажет',
    'comment'  => 'Түсініктеме',
    'externalDoc'  => 'Құжатты қосыңыз',
    'documentError'  => 'Екі өріс міндетті',
    'ViewProjects' => 'Жобаларды қарау',
    'Projects' => 'Жобалар',
    'NoProjectsAvailable' => 'Жобалар қол жетімді емес',
];
