<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    "sEmptyTable" => "No data available in table",
	"sInfo" => "_TOTAL_ жазбаның _START_-_END_ көрсетілуде",
	"sInfoEmpty" => "0 жазбаның 0-0 көрсетілуде",
	"sInfoFiltered" => "(filtered from _MAX_ total entries)",
	"sInfoPostFix" => "",
	"sInfoThousands" => ",",
	"sLengthMenu" => "_MENU_ жазбаны көрсету",
	"sLoadingRecords" => "Loading...",
	"sProcessing" => "Processing...",
	"sSearch" => "Іздеу:",
	"sZeroRecords" => "No matching records found",
	"sFirst" => "First",
	"sLast" => "Last",
	"sNext" => "Келесі",
	"sPrevious" => "Алдыңғы",
	"sSortAscending" => ": activate to sort column ascending",
	"sSortDescending" => ": activate to sort column descending"
];