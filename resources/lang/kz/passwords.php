<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Your password has been reset!',
    'sent' => 'We have e-mailed your password reset link!',
    'token' => 'This password reset token is invalid.',
    'user' => "We can't find a user with that e-mail address.",
    'throttled' => 'Please wait before retrying.',

    'resetPassword' => 'Жасырын сөзді қалпына келтіру',
    'please change your password to activate your account' => 'тіркелгіні іске қосу үшін құпия сөзіңізді өзгертіңіз.',
    'currentPassword' => 'Ағымдағы Құпия сөз',
    'newPassword' => 'Жаңа жасырын сөз',
    'confirmNewPassword' => 'Жасырын сөзді растаңыз',
    'changePassword' => 'құпия сөзді өзгерту',
    'New password set successfully' => 'Жаңа жасырын сөз сәтті орнатылды',
    'Please reset your password to login your account' => 'Жеке кабинетіңізге кіру үшін жасырын сөзіңізді қалпына келтіріңіз.',
    'resetPassword' => 'Жасырын сөзді қалпына келтіру'

];
