<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Content Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'Documents' => 'Documents',
    'close' => 'жабық',
    'delete_question' => 'Мұны шынымен жойғыңыз келе ме?',
    'delete_confirm' => 'Иә, жой!',
    'cancel' => 'Болдырмау',

    'are_you_sure_delete' => 'Шынымен жойғыңыз келе ме?',
    'bulk_delete_confirm' => 'Ия, бұларды жойыңыз',
    'bulk_delete_nothing' => 'Сіз жою үшін ештеңе таңдамадыңыз',

    'delete_selected' => 'Таңдалғандарды жою',
    'view' => 'Көрініс',
    'edit' => 'Өңдеу',
    'delete' => 'Жою',
    'active' => 'Актив',
    'inactive' => 'Бейактив',

    'Service Inquiries' => 'Қызмет өтінімі',

    'banner_masters' => [
        'add_new' => 'Жаңадан қосу',
        'Атауы' => 'Атауы',
        'Жарыққа шығару уақыты' => 'Жарыққа шығару уақыты',
        'Ені' => 'Ені',
        'Биіктігі' => 'Биіктігі',
        'Папка' => 'Папка',
        'URL-жол' => 'URL-жол',
        'actions' => 'Әрекеттер',
        'banner_masters_edit' => 'Өңдеу Баннерлер категориясы',
        'banner_masters_add' => 'қосу Баннерлер категориясы ',
        'banner_masters' => 'Баннерлер категориясы '
    ],

    'banner_images' => [
        'title' => 'Атауы',
        'add_new' => 'Жаңадан қосу',
        'banner_images' => 'Баннерлер кескіні',
        'image' => 'Кескін',
        'created_at' => 'Жарыққа шығару уақыты',
        'banner_category' => 'Баннерлер категориясы ',
        'actions' => 'Әрекеттер',
        'primary_text' => 'Негізгі мәтін',
//        'button_url' => 'Түйменің URL',
        'order' => 'Реті',
        'Реті' => 'Реті',
        'all' => 'Барлығы',
        'secondary_text' => 'Екінші реттік мәтін',

        'Кескін' => 'Кескін',
        'Атауы' => 'Атауы',
        'Негізгі мәтін' => 'Негізгі мәтін',
        'Түйме мәтінінің түсі' => 'Түйме мәтінінің түсі',
        'Түйменің түсі' => 'Түйменің түсі',
        'Реті' => 'Реті',
        'Жарыққа шығару уақыты' => 'Жарыққа шығару уақыты',
        'Екінші реттік мәтін' => 'Екінші реттік мәтін',

        'Banner Category' => 'Баннерлер категориясы ',
        'Түйме URL' => 'Түйменің URL',
        'banner_images_edit' => 'Өңдеу Баннерлер кескіні',
        'banner_images_add' => 'қосу Баннерлер кескіні'
    ],

    'pages' => [
        'asdasd'=>'2222222',
        'add_new' => 'Жаңадан қосу',
        'title' => 'Атауы',
        'status' => 'Статус',
        'actions_table' => 'Әрекеттер',
        'pages' => 'Парақшалар',
        'delete_confirm' => 'Иә, жой!',
        'cancel' => 'Болдырмау',
        'return_to_list' => 'Тізімге қайта оралу',
        'viewing_page' => 'Парақшаны қарау',
        'edit_page' => 'Парақшаны өңдеу',
        'edit' => [
            'edit' => 'Парақшаны өңдеу',
            'add' => 'қосу Парақшалар',
            'Title' => 'Атауы',
            'Excerpt' => 'Үзінді',
            'Body' => 'Негізгі бөлім',
            'Slug' => 'URL-жол',
            'Meta Description' => 'Meta Description',
            'Meta Keywords' => 'Meta Keywords',
            'Status' => 'Статус',
            'save' => 'Сақтау'
        ],
        'view' => 'Парақшаны қарау',
        'edit_page_detail' => 'Өңдеу',
        'delete' => 'Жою',

        'Атауы' => 'Атауы',
        'Статус' => 'Статус',
        'delete_conformation' => 'Сіз парақшаларды жою керектігіне сенімдімісіз бе?'
    ],

    'news' => [
        'news' => 'Жаңалықтар',
        'add_new' => 'Жаңадан қосу',
        'actions_table' => 'Әрекеттер',
        'delete_confirm' => 'Иә, жой!',
        'cancel' => 'Болдырмау',
        'Тақырып' => 'Тақырып',
        'Кескін' => 'Кескін',
        'Created At' => 'Жарыққа шығару уақыты',
        'news_add' => 'қосу Жаңалықтар',
        'news_edit' => 'Өңдеу Жаңалықтар'
    ],

    'projects' => [
        'Жобалар' => 'Жобалар',
        'Атауы' => 'Атауы',
        'Description' => 'Сипаттамасы',
        'Күй' => 'Күй',
        'add_new' => 'Жаңадан қосу',
        'actions_table' => 'Әрекеттер',
        'delete_confirm' => 'Иә, жой!',
        'cancel' => 'Болдырмау',
        'Тақырып' => 'Тақырып',
        'Кескін' => 'Кескін',
        'Created_at' => 'Жарыққа шығару уақыты'
    ],

    'graph_blocks' => [
        'add_new' => 'Жаңадан қосу',
        'graph_blocks' => 'Графикалық блоктар',
        'Негізгі мәтін' => 'Негізгі мәтін',
        'Сипаттамасы' => 'Сипаттамасы',
        'Түсі' => 'Түсі',
        'order' => 'Реті',
        'actions_table' => 'Әрекеттер',
        'delete_confirm' => 'Иә, жой!',
        'cancel' => 'Болдырмау',
        'Реті' => 'Реті',
        'count' => 'Count',
        'Count' => 'Count',
        'graph_blocks_edit' => 'Өңдеу Графикалық блоктар',
        'graph_blocks_add' => 'қосу Графикалық блоктар',
        'Электрондық пошта' => 'Электрондық пошта',
    ],

    'footer_widgets' => [
        'footer_widgets' => 'Төменгі деректеме',
        'Атауы' => 'Атауы',
        'Мазмұны' => 'Мазмұны',
        'Реті' => 'Реті',
        'URL-жол'=>'URL-жол',
        'add_new' => 'Жаңадан қосу',
        'actions_table' => 'Әрекеттер',
        'delete_confirm' => 'Иә, жой!',
        'cancel' => 'Болдырмау',
        'Created At' => 'Жарыққа шығару уақыты',
        'footer_widgets_add' => 'қосу Төменгі деректеме',
        'footer_widgets_edit' => 'Өңдеу Төменгі деректеме'
    ],

    'users' => [
        'users' => 'Қолданушылар',
        'add_new' => 'Жаңадан қосу',
        'Аты' => 'Аты',
        'Электрондық пошта' => 'Электрондық пошта',
        'Тегі' => 'Тегі',
        'Created At' => 'Жарыққа шығару уақыты',
        'Primary Role' => 'негізгі рөл',
        'actions' => 'Әрекеттер',
        'status' => 'Статус',
        'active' => 'Актив',
        'inActive' => 'Бейактив',
        'edit_users' => 'Өңдеу Қолданушылар',
        'add_users' => 'Қолданушыны қосу',
        'name' => 'Аты',
        'email' => 'Электрондық пошта',
        'last_name' => 'Тегі',
        'contact' => 'Байланыс',
        'Status' => 'Статус',
//        'active' => 'Актив',
//        'inActive' => 'Бейактив',
    ],

    'roles' => [
        'roles' => 'Рөлдер',
        'add_new' => 'Рөл қосу',
        'name' => 'Атауы',
        'display_name' => 'Көрсетілетін атауы',
        'slug' => 'URL-жол',
        'permissions' => 'Рұқсат',
        'submit' => 'Қабылдау',
        'actions_table' => 'Әрекеттер',
        'Атауы' => 'Атауы',
        'Көрсетілетін атауы' => 'Көрсетілетін атауы',
        'URL-жол' => 'URL-жол',
        'roles_add' => 'қосу Рөлдер',
        'roles_edit' => 'Өңдеу Рөлдер',

        'add' =>[
            'select_all' => 'Барлығын таңдау',
            'deselect_all' => 'Таңдауды болдырмау',
            'section' => 'Секция',
            'page' => 'Бет',
            'browse' => 'Шолу',
            'read' => 'Оқу',
            'add' => 'Қосу',
            'edit' => 'Өңдеу',
            'delete' => 'Жою',
        ]
    ],

    'product' => [
        'products' => 'Өнімдер',
        'add_new' => 'Өнім қосу',
        'actions_table' => 'Әрекеттер',
        'edit_product' => 'Өңдеу Өнімдер',
        'add_product' => 'Өнім қосу',
        'manager'=> 'Менеджер',
        'save' => 'Сақтау',
        'Атауы' => 'Атауы',
        'БП коды' => 'БП коды',
        'Логотип' => 'Логотип',
        'Еншілес ұйымдар' => 'Еншілес ұйымдар',
        'Өнім категориясы' => 'Өнім категориясы',
        'all' => 'Барлығы',
        'Сипаттамасы' => 'Сипаттамасы',
        'Тапсырыс бере алады' => 'Тапсырыс бере алады',
        'Created At' => 'Жарыққа шығару уақыты'
    ],

    'product_category' => [
        'Атауы' => 'Атауы',
        'Сипаттамасы' => 'Сипаттамасы',
        'Логотип' => 'Логотип',
        'product_categories' => 'Өнім категориялары',
        'add_new' => 'Жаңадан қосу',
        'actions_table' => 'Әрекеттер',
        'created_at' => 'Жарыққа шығару уақыты',
        'add' => 'Өнім категориясын қосу',
        'edit' => 'өнім санатын өзгерту',
        'save' => 'Сақтау',

        'Created At' => 'Created At',
        'add_product_category' => 'Өнім категориясын қосу',
        'edit_product_category' => 'Өңдеу Product Category',

    ],

    'product-categories' => [
        'Өнім_категориялары' => 'Өнім категориясы',
        'product_categories_add' => 'Add Product Category',
        'product_categories_edit' => 'Edit Product Category',
        'Атауы' => 'Атауы',
        'Сипаттамасы' => 'Сипаттамасы',
        'Логотип' => 'Логотип',
        'Жарыққа шығару уақыты' => 'Created At',
        'Әрекеттер' => 'Әрекеттер',
        'actions_table' => 'Әрекеттер',
        'add_new' => 'Жаңасын қосыңыз',
        'view' => 'View',
        'edit' => 'Edit',
        'delete' => 'Delete',
    ],

    'media' => [
        'media' => 'Медиа',
        'upload' => 'Жүктеу',
        'add folder' => 'Папканы қосу',
        'move' => 'Көшіру',
        'delete' => 'Жою',
        'crop' => 'Кесу',
        'media library' => 'Медиа кітапханасы',
        'title' => 'Атауы',
        'type' => 'Түрі',
        'size' => 'Өлшем',
        'public url' => 'Ашық сілтеме',
        'last modified' => 'Соңғы өзгеріс',
        'add new folder' => 'Жаңа папканы қосу',
        'new folder name' => 'Жаңа папка атауы',
        'cancel' => 'Болдырмау',
        'create new folder' => 'Жаңа папканы құру',
        'move file/foler' => 'Файл/папканы көшіру',
        'destination folder' => 'Тағайындалған папка',
        'cancel' => 'Болдырмау',
        'move' => 'Көшіру',
        'are you sure you want to delete the following file(s)?' => 'Сіз файлды(дарды) жою керектігіне сенімдімісіз бе?',
        'deleting a folder will remove all files and folders contained inside' => 'Папканы жою папкамен бірге ішіндегі бүкіл файлдар мен папкаларды жояды',
        'Cancel' => 'Болдырмау',
        'Yes, Delete it!' => 'Иә, жою!',
        'Menu Builder' => 'Мәзір конструкторы',
        'Add new' => 'Қосу',
        'builder' => 'Конструктор',
        'new menu item' => 'Жаңа мәзір элементі',
        'edit menu item' => 'asdasd Мәзір элементін өңдеу',
        'title of the menu item' => 'Мәзір элементінің атауы',
        'link type' => 'Сілтеме түрі',
        'static url' => 'Статикалық URL-жол',
        'dynamic route' => 'Динамикалық бағыттама',
        'url for the menu item' => 'Мәзір элементіне URL',
        'font icon class for the menu item' => 'Мәзір элементіне арналған қаріп белгісінің тобы',
        'color in rgb or hex (optional)' => 'Түстің rgb немесе hex коды (міндетті емес)',
        'open in' => 'Ашу тәсілі',
        'same tab/window' => 'Дәл сол терезеде',
        'new tab/window' => 'Жаңа терезеде',
        'update' => 'Жаңарту',
        'edit' => 'Өңдеу',
        'delete' => 'Жою',
        'search' => 'Іздеу',
    ],

    'menu' => [
        'edit_page' => 'Мәзір элементін өңдеу',
        'add_page' => 'Қосу Мәзір конструкторы',
        'menus'=> 'Мәзір конструкторы',
        'actions' => 'Әрекеттер',
        'menu_builder' => 'Мәзір конструкторы',
        'add_new' => 'Қосу',
        'builder' => 'Конструктор',
        'new_menu_item' => 'Жаңа мәзір элементі',
        'edit_menu_item' => 'Мәзір элементін өңдеу',
        'title_of_the_menu_item' => 'Мәзір элементінің атауы',
        'link_type' => 'Сілтеме түрі',
        'static_url' => 'Статикалық URL-жол',
        'dynamic_route' => 'Динамикалық бағыттама',
        'url_for_the_menu_item' => 'Мәзір элементіне URL',
        'font_icon_class_for_the_menu_item' => 'Мәзір элементіне арналған қаріп белгісінің тобы',
        'color_in_rgb_or_hex_(optional)' => 'Түстің rgb немесе hex коды (міндетті емес)',
        'open_in' => 'Ашу тәсілі',
        'same_tab/window' => 'Дәл сол терезеде',
        'new_tab/window' => 'Жаңа терезеде',
        'update' => 'Жаңарту',
        'edit' => 'Өңдеу',
        'delete' => 'Жою',
        'search' => 'Іздеу',
        'Конструктор' => 'Конструктор',
        'name' => 'Имя'
    ],

    'only_formats_are_allowed' => 'Тек форматқа рұқсат етіледі',
    'front_end' =>[
        'service_documents' => 'Қызмет бойынша құжаттама',
        'application_success_message' => 'Сіз бұл қызметке тапсырыс жібердіңіз. Біздің қызметкерлеріміз сіздің тарсырысыңызды қарап, жақын арада сізбен байланысады.'
    ],

    'service_inquiry' => [
        'return_to_list' => 'Тізімге қайта оралу',
        'viewing_service_inquiry' => 'Қызмет өтінімін қарау',
        'registration_number' => 'Тіркеу нөмірі'
    ],

    'product_manager' => [
        'product managers' => 'Өнім менеджерлері',
        'add new' => 'Жаңадан қосу',
        'first name' => 'Аты',
        'last name' => 'Тегі',
        'gender' => 'Жынысы',
        'email' => 'Электрондық пошта',
        'contact' => 'байланыс',
        'actions' => 'Әрекеттер',
        'view' => 'Көру',
        'edit' => 'Өңдеу',
        'delete' => 'Жою',
    ],

    'subsidiaries' => [
        'Еншілес_ұйымдар' => 'Еншілес_ұйымдар',
        'subsidiaries_add' => 'Өнім Еншілес ұйымдар',
        'subsidiaries_edit' => 'Өңдеу Еншілес ұйымдар',
        'Атауы' => 'Атауы',
        'Сипаттамасы' => 'Сипаттамасы',
        'Жарыққа шығару уақыты' => 'Жарыққа шығару уақыты',
        'actions_table' => 'Әрекеттер',
    ],

    'consulting_service_product' => [
        'Сервистік_өнімдер' => 'Сервистік өнімдер',
        'consulting_service_product_add' => 'Қосу Кеңес беру қызметі өнімі',
        'consulting_service_product_edit' => 'Өңдеу Кеңес беру қызметі өнімі',
        'Өнім' => 'Өнім',
        'Жарыққа шығару уақыты' => 'Created At',
        'actions_table' => 'Әрекеттер',
        'add_new' => 'Қосу',
        'view' => 'Көру',
        'edit' => 'Өңдеу',
        'delete' => 'Жою',
    ],

    'product-managers' => [
        'product-managers_add' => 'Өнім Өнім менеджерлері',
        'product-managers_edit' => 'Өңдеу Өнім менеджерлері'
    ]
];
