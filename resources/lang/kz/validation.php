<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => ':attribute қабылдануы керек.',
    'default_accept' => 'Тек jpg / png / jpeg / pdf файлына рұқсат етіледі.',
    'active_url' => ':attribute дұрыс URL-мекенжайы емес.',
    'after' => ':attribute күні :date-дан кейінгі күн болуы керек.',
    'after_or_equal' => ':attribute күні :date күніне тең немесе одан кейінгі күн болуы керек.',
    'alpha' => ':attribute тек әріптерден ғана кұрала алады.',
    'alpha_dash' => ':attribute құрамында тек әріптер, сандар, сызықшалар мен астынғы сызықтар ғана болуы мүмкін.',
    'alpha_num' => ':attribute тек әріптер мен сандардан ғана құрала алады.',
    'array' => ':attribute массив болуы керек.',
    'before' => ':attribute күні :date-не дейінгі күн болуы керек.',
    'before_or_equal' => ':attribute күні :date күніне тең немесе оған дейінгі күн болуы керек.',
    'between' => [
        'numeric' => ':attribute мәні :min мен :max аралығында болуы керек.',
        'file' => ':attribute өлшемі :min және :max килобайт арасында болуы керек.',
        'string' => ':attribute мәні :min мен :max арасындағы таңбалар болуы керек.',
        'array' => ':attribute мәні :min мен :max элементтер арасында болуы керек.',
    ],
    'boolean' => ':attribute өрісі шын немесе жалған болуы керек.',
    'confirmed' => ':attribute растауы сәйкес келмейді.',
    'date' => ':attribute жарамсыз күн.',
    'date_equals' => ':attribute күні :date күніне тең болуы керек.',
    'date_format' => ':attribute :format форматына сәйкес келмейді.',
    'different' => ':attribute мен :other әр түрлі болуы керек.',
    'digits' => ':attribute :digits цифрлардан тұруы керек.',
    'digits_between' => ':attribute :min мен :max сандар арасында болуы керек.',
    'dimensions' => ':attribute кескін өлшемдері дұрыс емес.',
    'distinct' => ':attribute өрісінің құрамында қайталанатын мәндер бар.',
    'email' => ':attribute жарамды электрондық пошта мекенжайы болуы керек.',
    'ends_with' => ':attribute келесілердің бірімен аяқталуы керек : :values.',
    'exists' => 'Таңдалған :attribute жарамсыз.',
    'file' => ':attribute файл болуы керек.',
    'filled' => ':attribute өрісінің мәні болуы керек.',
    'gt' => [
        'numeric' => ':attribute :value-дан үлкен болуы керек.',
        'file' => ':attribute :value килобайттан үлкен болуы керек.',
        'string' => ':attribute :value таңбалардан көп болуы керек.',
        'array' => ':attribute :value артық элементтерден құралуы керек.',
    ],
    'gte' => [
        'numeric' => ':attribute :value үлкен немесе тең болуы керек.',
        'file' => ':attribute :value килобайттан үлкен немесе оған тең болуы керек.',
        'string' => ':attribute :value таңбалардан үлкен немесе оған тең болуы керек.',
        'array' => ':attribute элементтері :value элементтеріне тең немесе одан да көп болуы керек.',
    ],
    'image' => ':attribute кескін болуы керек.',
    'in' => 'Таңдалған :attribute жарамсыз.',
    'in_array' => ':attribute өрісі :other ішінде жоқ.',
    'integer' => ':attribute бүтін сан болуы керек.',
    'ip' => ':attribute жарамды IP-мекенжайы болуы керек.',
    'ipv4' => ':attribute жарамды IPv4-мекенжайы болуы керек.',
    'ipv6' => ':attribute жарамды IPv6-мекен-жайы болуы керек.',
    'json' => ':attribute жарамды JSON жолы болуы керек.',
    'lt' => [
        'numeric' => ':attribute мәні :value-дан аз болуы керек.',
        'file' => ':attribute өлшемі :value килобайттан аз болуы керек.',
        'string' => ':attribute таңбалардан :value аз болуы керек.',
        'array' => ':attribute элементтері :value элементтерінен аз болуы керек.',
    ],
    'lte' => [
        'numeric' => ':attribute мәні :value-ден кіші немесе оған тең болуы керек.',
        'file' => ':attribute өлшемі :value килобайттан кем немесе тең болуы керек.',
        'string' => ':attribute :value таңбалардан кем немесе оған тең болуы керек.',
        'array' => ':attribute элементтері :value элементтерден артық болмауы керек.',
    ],
    'max' => [
        'numeric' => ':attribute :max-дан артық болмауы керек.',
        'file' => ':attribute :max килобайттан аспауы керек.',
        'string' => ':attribute :max таңбалардан аспауы керек.',
        'array' => ':attribute элементтері :maxэлементтерден артық болмауы керек.',
    ],
    'mimes' => ':attribute : :values типті файл болуы керек.',
    'mimetypes' => ':attribute : :values типті файл болуы керек.',
    'min' => [
        'numeric' => ':attribute кемінде :min болуы керек .',
        'file' => ':attribute өлшемі кем дегенде :min килобайттан тұруы керек.',
        'string' => ':attribute мәні кемінде :min таңбадан тұруы керек.',
        'array' => ':attribute құрамы кемінде :min элементтерден тұруы керек.',
    ],
    'not_in' => 'Таңдалған :attribute жарамсыз.',
    'not_regex' => ':attribute форматы жарамсыз.',
    'numeric' => ':attribute сан болуы керек.',
    'password' => 'Жасыран сөз дұрыс емес.',
    'present' => ':attribute өрісі болуы керек.',
    'regex' => ':attribute форматы жарамсыз.',
    'default_required' => 'Бұл керек аймақ.',
    'required' => ':attribute өрісі міндетті.',
    'required_if' => ':attribute өрісі қажет, егер :other :value болса.',
    'required_unless' => ':attribute өрісі, егер :other :values ішінде болмаса, міндетті болып табылады.',
    'required_with' => ':attribute өрісі :values болған кезде толтырылуы міндетті.',
    'required_with_all' => ':attribute өрісі, егер :values болса міндетті.',
    'required_without' => ':attribute өрісі :values болмаған кезде қажет.',
    'required_without_all' => ':attribute өрісі, егер :values ешбірі жоқ болса, міндетті.',
    'same' => ':attribute мен :other сәйкес келуі керек.',
    'size' => [
        'numeric' => ':attribute :size болу керек.',
        'file' => ':attribute :size килобайт болуы керек.',
        'string' => ':attribute :size таңбадан тұруы керек.',
        'array' => ':attribute құрамында :size элементтер болуы керек.',
    ],
    'starts_with' => ':attribute келесілердің бірінен басталуы керек: :values',
    'string' => ':attribute жол болуы керек.',
    'timezone' => ':attribute жарамды аймақ болуы керек.',
    'unique' => ':attribute бос емес.',
    'uploaded' => ':attribute жүктелуі сәтсіз.',
    'url' => ':attribute форматы жарамсыз.',
    'uuid' => ':attribute жарамды UUID болуы керек.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'Хабарлама',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

    'invalid password' => 'Жасырын сөз дұрыс емес. Әрекетті қайталаңыз.',
    'email not registered' => 'Электрондық пошта идентификаторы тіркелмеген.',
    'terms' => 'Біздің ережелер мен шарттарымызды қабылдалыңыз.',
    'equalTo' => 'Ээлектрондық пошта мекенжайын қайтадан енгізіңіз.',
    'valid contact' => 'Дұрыс байланыс ақпараттарыңызды енгізіңіз.',

    'registration not complete' => 'Тіркелуіңізді аяқтау мүмкін емес. Сайт әкімшісіне хабарласыңыз.',
    'registration complete email error' => 'Сіздің тіркелуіңіз сәтті өтті. Тіркелу процессін аяқтау үшін электрондық поштаңызды тексеріңіз. Егер сізге тіркелу туралы хаттама келмеген жағдайда, сайт әкімшісіне хабарласыңыз.',
    'registration complete' => 'Сіздің тіркелуіңіз сәтті өтті. Тіркелу процессін аяқтау үшін электрондық поштаңызды тексеріңіз.',

    'email not found' => 'Электрондық пошта біздің жазбаларымызға сәйкес келмейді.',
    'reset password link send' => 'Жасырын сөзді қалпына келтіру сілтемесі электрондық поштаңызға жіберілді.',
    'link expire' => 'Бұл сілтеме бұдан былай қол жетімді емес.',

    'userIsInactive' => 'Сіздің жеке есептік жазбаңыз бұғатталған, әкімшімен байланысыңыз',
];
