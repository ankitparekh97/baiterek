<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login' => 'Кіру',
    'logout' => 'Шығу',
    'failed' => 'Бұл тіркелгі деректері біздің жазбаларымызға сәйкес келмейді.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'giveItTry' => 'Көріп көріңіз!',
    'welcome' => 'Қош келдіңіз',
    'sign in to account' => 'Жеке кабинетіңізге кіріңіз',
    'no account' => 'Жеке кабинетіңіз жоқ па?',
    'sign up now' => 'Жүйеге тіркелу',
    'email' => 'Электрондық пошта',
    'email here' => 'Сіздің эл. поштаңыз...',
    'password' => 'Жасырын сөз',
    'password here' => 'Сіздің жасырын сөзіңіз…',
    'keep logged in' => 'Мені жүйеде ұста',
    'forgot password?' => 'Жасырын сөзді ұмыттыңыз ба?',

    'sign up' => 'Жүйеге тіркелу',
    'takes few minutes' => 'Жүйеге тіркелу көп уақытыңызды алмайды',
    'first name' => 'Аты',
    'last name' => 'Тегі',
    'patronymic name' => "Әкесінің аты",
    'confirm email' => 'Электрондық поштаны растау',
    'gender' => 'Жыныcы',
    'date of birth' => 'Туған күні',
    'address' => 'Тұрғылықты мекен-жайы',
    'phone number' => 'Телефон нөмірі',
    'region' => 'Облыс',
    'town/city' => 'Тұрғылықты қала',
    'post code' => 'Пошта индексі',
    'terms' => 'қабылдаңыз',
    'condition' => 'Біздің шарттарымызды',
    'already an account' => 'Жүйеде барсыз ба?',
    'sign' => 'Кіруs',
    'VATno' => 'ҚҚС №',

    'forgot password' => 'Жасырын сөзді ұмыттыңыз ба?',
    'forgot password instructions' => 'Егер жасырын сөзді ұмытып қалсаңыз, электрондық поштаны енгізіп, нұсқауларды орындаңыз',
    'login here' => 'Жүйеге кіру',
    'send link' => 'Сілтемені жіберу',
    'sign_page' => 'Кіру',
    'condition_first' => 'Біздің',
    'condition_second' => 'шарттарымызды',
];
