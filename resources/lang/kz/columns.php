<?php

return [
    'Name' => 'Аты',
    'Last name' => 'Тегі',
    'Applicant type' => 'Өтініш берушінің түрі',
    'Number of request' => 'Өтінім нөмірі',
    'Name of service' => 'Қызмет атауы',
    'Name of responsible person' => 'Жауапты тұлғаның аты-жөні',
    'Status of request' => 'Өтінім статусы',
    'Created At' => 'Жасалды At',
];
