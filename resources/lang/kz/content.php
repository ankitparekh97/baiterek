<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Content Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'download' => 'Download',
    'documents' => 'Documents',
    'No Match' => 'Сәйкестік табылған жоқ',

    'cabinet' => 'Кабинет',
    'copy_rights' => 'Авторлық құқық',
    'all_rights' => 'Бәйтерек, Барлық құқықтар қорғалған',

    'General Information' => 'Сәйкестік табылған жоқ',

    'dueInDay' => 'Due in :day day',
    'overdueInDay' => 'Overdue by :day day',
    'overdueToday' => 'Overdue by today',
    'appSendSince' => 'Application send since :day day',
    'appSendToday' => 'Application send today',

    'Draft' => 'Қолжазбаның алғашқы түрі',
    'Applied' => 'Өтінім жіберілді',
    'Application received' => 'Өтінім алынды',
    'On control' => 'Бақылауда',
    'Control' => 'Бақылау',
    'Data/Documents are missing' => 'Ақпарат/Құжаттар жеткіліксіз',
    'In process' => 'Жұмыс барысында',
    'Accepted' => 'Қабылданды',
    'Rejected' => 'Қабылданбады',

    'noActivity' => 'Әрекет жоқ',
    'moveThisReq' => ' перенес этот запрос из :before дейін :after',
    'cancel' => 'Болдырмау',
    'activityList' => 'Әрекеттер тізімі',
    'save' => 'Сақтау',
    'write_a_message' => 'Хабар жазу ...',
    'ServiceInquiryDetail' => 'Қызметтік анықтама',
    'ExportToPDF' => 'PDF-ке экспорттау',
    'ExportToCSV' => 'XLS-ге экспорттау',
    'ticket' => 'билет',
    'edit' => 'өңдеңіз',
    'markedAs' => ' белгіленген :label :checked',
    'checked' => 'тексерілді',
    'unchecked' => 'тексерілмеген',
    'completeCheckpointFirst' => 'Алдымен барлық өткізу пункттерін толтырыңыз',
    'checkpointComment' => 'set comment ":checkPointComment" to "checkPointLabel" checkpoint',
    'any_comment' => 'Type....',
    'title_statement' => '«Бәйтерек» Ұлттық басқарушы холдингі»',
    'verificationIsPending' => 'Third party verification is pending',
    'apply_for_more' => 'Тағыда тапсырыс жіберу',
    'see_application_status' => 'Тапсырыстың статусын қарау',
    'title' => 'Атауы',
    'customer' => 'Клиент',
    'ticket_number' => 'Тіркеу нөмірі',
    'language' => 'Тіл',
    'due' => 'Күту уақыты (күндер)',
    'details' => 'Толығырақ',
    'action' => 'Әрекеттер',
    'status' => 'Статус',
    'invite_users_for_verification' => 'пайдаланушыларды тексеруге шақыру',
    'invite' => 'шақыру',
    'verification_data' => 'тексеру деректері',
    'data' => 'деректер',
    'moveThisApplication' => 'Сіздің өтінішіңіз - бұл :after',
    'verificationMessage' => 'имеет :status этот запрос',
    'invitedUsers' => 'шақырды ":email" тексеру :document үшін',
    'andAddedComment' => ' және түсініктеме қосты :comment',
    'upload' => 'Upload',
    'Unauthorized' => 'Бұл бетті қарауға рұқсатыңыз жоқ',

    'migrated_files' => 'Қоса тіркелген файлдар',
    'userName' => 'Қолданушының аты',
    'fileName' => 'Файл атауы',
    'fileDate' => 'Көші-қон күні',
    'product_name' => 'Қызмет атауы',
    'documents_migrated' => 'Қоса тіркелген файлдар',
    'download_migrated' => 'Жүктеу',
    'go to old dashboard' => 'Порталдың алдыңғы нұсқасына өту'
];
