<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    "sEmptyTable" => "No data available in table",
	"sInfo" => "Showing _START_ to _END_ of _TOTAL_ entries",
	"sInfoEmpty" => "Showing 0 to 0 of 0 entries",
	"sInfoFiltered" => "(filtered from _MAX_ total entries)",
	"sInfoPostFix" => "",
	"sInfoThousands" => ",",
	"sLengthMenu" => "Show _MENU_ entries",
	"sLoadingRecords" => "Loading...",
	"sProcessing" => "Processing...",
	"sSearch" => "Search:",
	"sZeroRecords" => "No matching records found",
	"sFirst" => "First",
	"sLast" => "Last",
	"sNext" => "Next",
	"sPrevious" => "Previous",
	"sSortAscending" => ": activate to sort column ascending",
	"sSortDescending" => ": activate to sort column descending"
];