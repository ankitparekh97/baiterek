<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Fields Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'First Name' => 'First Name',
    'Last Name' => 'Last Name',
    'Contact' => 'Phone numbers',
    'Patronymic name' => "Patronymic name",
    'E-mail' => 'E-mail address',
    'Email of recipient' => 'Email of recipient',
    'Gender' => 'Gender',
    'Marital Status' => 'Marital Status',
    'Address of Residence' => 'Address of Residence',
    'Message' => 'Message',
    'Choose' => 'Choose',
    'Male' => 'Male',
    'Female' => 'Female',
    'Married' => 'Married',
    'Single' => 'Single',
    'Separated' => 'Separated',
    'Divorced' => 'Divorced',
    'Apply Now' => 'Apply Now',
    'Application Type' => 'Application Type',
    'Application Status' => 'Application Status',
    'Physical Entity' => 'Physical Entity',
    'Legal entity' => 'Legal entity',
    'Organization' => 'Organization',
    'Applicant Documents' => 'Applicant Documents (Multiple Upload)',
    'IIN/BIN' => 'IIN/BIN',
    'View All' => 'View All',
    'Search' => 'Search',
    'Apply' => 'Apply',
    'More' => 'More',
    'Read More' => 'Read More',
    'News' => 'News',
    'Latest News' => 'Latest News',
    'Browse Services' => 'Browse Our Services',
    'Top Products' => 'Top Product By Category',
    'Feedback' => 'Feedback',
    'ProductDetails' => 'Product/Service Information',
    'ApplicantDetails' => 'Applicant General Information fields for Application',
    'ApplicationForm' => 'Application Fields specific to product',
    'Step1' => 'Step 1',
    'Step2' => 'Step 2',
    'Step3' => 'Step 3',
    'Step4' => 'Step 5',
    'Step5' => 'Step 5',
    'checkpoints' => 'Checkpoints',
    'addMoreBtn' => 'Add More',
    'documentPool' => 'Document pool',
    'Milestone' => 'Milestone',
    'Coming soon'  => 'Coming soon',
    'Save Draft'  => 'Save Draft',
    'selectMilestone'  => 'Select Milestone (Days)',
    'externalVerification'  => 'External Verification required',
    'comment'  => 'Comment',
    'externalDoc'  => 'Add Document',
    'documentError'  => 'Both fields are required',
    'ViewProjects' => 'View Projects',
    'NoProjectsAvailable' => 'No Projects Available',
    'Projects' => 'Projects',
];
