<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Your password has been reset!',
    'sent' => 'We have e-mailed your password reset link!',
    'token' => 'This password reset token is invalid.',
    'user' => "We can't find a user with that e-mail address.",
    'throttled' => 'Please wait before retrying.',

    'resetPassword' => 'Reset Password',
    'please change your password to activate your account' => 'please change your password to activate your account.',
    'currentPassword' => 'Current Password',
    'newPassword' => 'New Password',
    'confirmNewPassword' => 'Confirm New Password',
    'changePassword' => 'Change Password',
    'New password set successfully' => 'New password set successfully',
    'Please reset your password to login your account' => 'Please reset your password to login your account.',
    'resetPassword' => 'Reset Password',
];
