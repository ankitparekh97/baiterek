<?php

return [
    'Name' => 'Name',
    'Last name' => 'Last name',
    'Applicant type' => 'Applicant type',
    'Number of request' => 'Number of request',
    'Name of service' => 'Name of service',
    'Name of responsible person' => 'Name of responsible person',
    'Status of request' => 'Status of request',
    'Created At' => 'Created At',
];
