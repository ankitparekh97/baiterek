<?php

return [

    /*
    |--------------------------------------------------------------------------
    | content language lines
    |--------------------------------------------------------------------------
    |
    | the following language lines are used during authentication for various
    | messages that we need to display to the user. you are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'status change successfully' => 'Status Change Successfully',
    'successfully add new product and invitation sent to product manager' => 'Successfully Add New Product and invitation sent to product manager',
    'successfully updated product' => 'Successfully Updated product',
    'successfully deleted this product' => 'Successfully Deleted This Product',
    'oops something went wrong' => 'Oops Something went wrong',
    'invited successfully' => 'Invited Successfully',
    'third party verification is pending' => 'Third party verification is pending',
    'your application has been drafted successfully and notification sent on your registered email address' => 'Your application has been drafted successfully and notification sent on your registered email address.',
    'your profile has been updated successfully' => 'Your profile has been updated successfully.',
    'admin cms' => 'Admin CMS',
    'successfully updated page' => 'Successfully updated page.',
    'successfully deleted page' => 'Successfully deleted page',
    'successfully added new page' => 'Successfully Added new page',
    'successfully updated banner category' => 'Successfully updated banner category',
    'successfully deleted banner category' => 'Successfully deleted banner category',
    'successfully added new banner category' => 'Successfully added new banner category',
    'successfully updated news' => 'Successfully updated news',
    'successfully deleted this news' => 'Successfully deleted this news',
    'successfully added new news' => 'Successfully added new news',
    'successfully updated graph block' => 'Successfully updated graph block',
    'successfully deleted graph block' => 'Successfully deleted graph block',
    'successfully added new graph block' => 'Successfully added new graph block',
    'successfully updated footer widget' => 'Successfully updated footer widget',
    'successfully added new footer widget' => 'Successfully added new footer widget',
    'successfully deleted footer widget' => 'Successfully deleted footer widget',
    'successfully updated menu' => 'Successfully updated menu',
    'successfully added new menu' => 'Successfully added new menu',
    'successfully deleted this menu' => 'Successfully deleted this menu',
    'successfully added new user activation link has been sent to' => 'Successfully added new user. Activation link has been sent to :email',
    'successfully updated new user' => 'Successfully updated new user',
    'successfully deleted this user' => 'Successfully deleted this user',
    'successfully added new role' => 'Successfully added new role',
    'successfully updated role' => 'Successfully Updated Role',
    'successfully deleted this role' => 'Successfully deleted this role',
    'successfully added new product category' => 'Successfully added new product category',
    'successfully updated product category' => 'Successfully updated product category',
    'successfully deleted product category' => 'Successfully Deleted product category',
    'successfully added new product manager activation link has been sent to' => 'Successfully added new product manager. Activation link has been sent to :email',
];