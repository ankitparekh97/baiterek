<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Content Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'download' => 'Download',
    'documents' => 'Documents',
    'No Match' => 'No Match Found',

    'cabinet' => 'Cabinet',
    'copy_rights' => 'Copyright',
    'all_rights' => 'Baiterek, All rights reserved.',

    'General Information' => 'General Applicant Information',

    'dueInDay' => 'Due in :day day',
    'overdueInDay' => 'Overdue by :day day',
    'overdueToday' => 'Overdue by today',
    'appSendSince' => 'Application send since :day day',
    'appSendToday' => 'Application send today',

    'Draft' => 'Draft',
    'Applied' => 'Applied',
    'Application received' => 'Application received',
    'On control' => 'On control',
    'Control' => 'Control',
    'Data/Documents are missing' => 'Data/Documents are missing',
    'In process' => 'In process',
    'Accepted' => 'Accepted',
    'Rejected' => 'Rejected',

    'noActivity' => 'No activity',
    'moveThisReq' => ' moved this inquiry from :before to :after',
    'cancel' => 'Cancel',
    'activityList' => 'Activity List',
    'save' => 'Save',
    'write_a_message' => 'Write a message…',
    'ServiceInquiryDetail' => 'Service Inquiry Detail',
    'ExportToPDF' => 'Export to PDF',
    'ExportToCSV' => 'Export to Excel',
    'ticket' => 'Ticket',
    'edit' => 'Edit',
    'markedAs' => ' marked :label :checked',
    'checked' => 'checked',
    'unchecked' => 'unchecked',
    'completeCheckpointFirst' => 'Please complete all the checkpoints first',
    'checkpointComment' => 'set comment ":checkPointComment" to ":checkPointLabel" checkpoint',
    'any_comment' => 'Type....',
    'title_statement' => '“Baiterek” National Managing Holding',
    'verificationIsPending' => 'Third party verification is pending',
    'apply_for_more' => 'Apply for more',
    'see_application_status' => 'See application status',
    'title' => 'Title',
    'customer' => 'Customer',
    'ticket_number' => 'Registration number',
    'language' => 'Language',
    'due' => 'Waiting time (days)',
    'details' => 'Details',
    'action' => 'Action',
    'status' => 'status',
    'invite_users_for_verification' => 'Invite Users For Verification',
    'invite' => 'Invite',
    'verification_data' => 'Verification Data',
    'data' => 'Data',
    'moveThisApplication' => 'Your application is :after',
    'verificationMessage' => 'has :status this inquiry',
    'invitedUsers' => 'has invited ":email" for :document verification',
    'andAddedComment' => ' and added a comment ":comment"',
    'upload' => 'Upload',
    'Unauthorized' => 'You are not authorized to view this page',

    'migrated_files' => 'Attachments',
    'userName' => 'User name',
    'fileName' => 'File name',
    'fileDate' => 'Migrated date',
    'product_name' => 'Service name',
    'documents_migrated' => 'Attachments',
    'download_migrated' => 'Download',
    'go to old dashboard' => 'Go to previous version of the portal'
];
