<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login' => 'Login',
    'logout' => 'Log out',
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'giveItTry' => 'Give it a try!',
    'welcome' => 'Welcome',
    'sign in to account' => 'Please sign in to your account',
    'no account' => 'No account?',
    'sign up now' => 'Sign up now',
    'email' => 'E-mail',
    'email here' => 'Email here...',
    'password' => 'Password',
    'password here' => 'Password here...',
    'keep logged in' => 'Keep me logged in',
    'forgot password?' => 'Forgot password?',

    'sign up' => 'Sign Up',
    'takes few minutes' => 'It only takes few minutes to create an account',
    'first name' => 'First Name',
    'last name' => 'Last Name',
    'patronymic name' => "Patronymic name",
    'confirm email' => 'Confirm E-mail',
    'gender' => 'Gender',
    'date of birth' => 'Date of birth',
    'address' => 'Address',
    'phone number' => 'Phone numbers',
    'region' => 'Region',
    'town/city' => 'Town/City',
    'post code' => 'Post Code',
    'terms' => 'Accept our',
    'condition' => 'Terms and Conditions',
    'already an account' => 'Already have an account?',
    'sign' => 'Sign In',
    'sign_page' => 'Sign In',
    'VATno' => 'VAT no',
    'sign_page' => 'Sign In',
    'forgot password' => 'Forgot password',
    'forgot password instructions' => 'If you have forgotten your password and need a reminder, please input your email address and follow the on-screen instructions',
    'login here' => 'Login here',
    'send link' => 'Send link',
];
