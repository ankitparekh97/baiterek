<?php

return [
    'Sent' => 'Sent',
    'Received' => 'Received',
    'In process' => 'In process',
    'Data/Documents are missing' => 'Data/Documents are missing',
    'Accepted' => 'Accepted',
    'Rejected ' => 'Rejected ',
];
