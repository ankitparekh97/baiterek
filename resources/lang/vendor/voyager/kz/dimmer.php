<?php

return [
    'page'           => 'парақша|парақша',
    'page_link_text' => 'Барлық парақшаларды қарау',
    'page_text'      => 'Tiene :count :string en su base de datos. Haga clic en el botón de abajo para ver todas las páginas.',
    'page_text'      => 'Сіздің деректер базаңызда :count :string бар. Барлық парақшаларды көру үшін төмендегі батырманы басыңыз.',
    'post'           => 'Post|Posts',
    'post_link_text' => 'Ver todos los posts',
    'post_text'      => 'Tiene :count :string en su base de datos. Haga clic en el botón de abajo para ver todos los posts.',
    'user'           => 'Usuario|Usuarios',
    'user_link_text' => 'Ver todos los usuarios',
    'user_text'      => 'Tiene :count :string en su base de datos. Haga clic en el botón de abajo para ver todos los usuarios.',
];