<?php

return [
    'new_menu_item'        => 'Жаңа мәзір элементі',
    'edit_item'            => 'Мәзір элементін өңдеу',
    'item_title'           => 'Мәзір элементінің атауы',
    'link_type'            => 'Сілтеме түрі',
    'static_url'           => 'Статикалық URL-жол',
    'dynamic_route'        => 'Динамикалық бағыттама',
    'url'                  => 'Мәзір элементіне URL',
    'icon_class'           => 'Мәзір элементіне арналған қаріп белгісінің тобы',
    'color'                => 'Түстің rgb немесе hex коды (міндетті емес)',
    'open_in'              => 'Ашу тәсілі',
    'open_same'            => 'Дәл сол терезеде',
    'open_new'             => 'Жаңа терезеде',


    'color_ph'             => 'Color (p. ej. #ffffff o rgb (255, 255, 255)',
    'create_new_item'      => 'Crear una opción de menú',
    'delete_item_confirm'  => 'Sí, eliminar esta opción de menú',
    'delete_item_question' => '¿Está seguro de que desea eliminar esta opción del menú?',
    'drag_drop_info'       => 'Arraste y suelte las opciones de menú para reorganizarlas',
    'icon_class2'          => 'Voyager Font Class</a>)',
    'icon_class_ph'        => 'Ícono (opcional)',
    'item_route'           => 'Ruta para la opción de menú',
    'route_parameter'      => 'Parámetros de ruta (si existen)',
    'successfully_created' => 'Se creó una opción de menú.',
    'successfully_deleted' => 'Opción de menú eliminada exitosamente.',
    'successfully_updated' => 'Opción de menú actualizada exitosamente.',
    'updated_order'        => 'Orden actualizado exitosamente.',
    'usage_hint'           => 'Puede imprimir un menú en cualquier lugar de su sitio llamando a ',
];
