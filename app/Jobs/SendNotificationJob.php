<?php

namespace App\Jobs;

use App\Mail\StatusChangeMail;
use App\Models\ServiceInquiryActivity;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $id;
    public $userLang;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id, $userLang)
    {
        $this->id = $id;
        $this->userLang = $userLang;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $id = $this->id;
            $serviceInquiryActivity = ServiceInquiryActivity::where('id', $id)->with('serviceInquiry.user')->first();
            $email = $serviceInquiryActivity->serviceInquiry->user->email;
            $name = $serviceInquiryActivity->serviceInquiry->user->name;
            $statusChangeMail = new StatusChangeMail($serviceInquiryActivity, $name, $this->userLang);
//            Mail::to('savan@avdevs.com')->send($statusChangeMail);
            Mail::to($email)->send($statusChangeMail);
        } catch (\Exception $e){
            Log::notice('SEND STATUS CHANGE EMAIL EXCEPTIONS LOG => '. $e->getMessage());
        }
    }
}
