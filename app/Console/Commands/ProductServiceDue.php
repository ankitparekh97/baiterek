<?php

namespace App\Console\Commands;

use App\Models\ServiceInquiries;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ProductServiceDue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'productService:due';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Email notification to Supervisor and PM';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $serviceInquiries = ServiceInquiries::all();
        foreach ($serviceInquiries as $serviceInquiry){
            if($serviceInquiry->product->milestone){
                if(Carbon::now()->diffInDays($serviceInquiry->due_date,false) == -1){
                    $user = User::find($serviceInquiry->product->created_by);
                    try{
                        Mail::to($user->email)
                            ->send(new \App\Mail\ProductServiceDue($user,$serviceInquiry));
                    }catch (\Exception $e){
                        Log::info('Supervisor email not sent in Product Service Due');
                    }
                }

                if(Carbon::parse($serviceInquiry->due_date)->diffInDays(Carbon::now()) == env('PRODUCT_SERVICE_DUE_PM')){
                    $user = User::find($serviceInquiry->product->user_id);
                    try{
                        Mail::to($user->email)
                            ->send(new \App\Mail\ProductServiceDue($user,$serviceInquiry));
                    }catch (\Exception $e){
                        Log::info('PM email not sent in Product Service Due');
                    }

                }
            }
        }
    }
}
