<?php

namespace App\Console\Commands;

use App\Helper\Helper;
use App\Models\MigratedDocument;
use App\Models\ServiceInquiries;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Filesystem;
use League\Flysystem\Sftp\SftpAdapter;

class MigrateFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:files';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download Files from other domain';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
//            $arr = [];
//            dd(count(array_unique($arr)));
            $serviceInquiries = ServiceInquiries::with('user', 'product')->get();
            foreach ($serviceInquiries as $serviceInquiry) {
                try {
                    $jsonData = json_decode($serviceInquiry->old_json_data);
                    $oldUserId = is_null($serviceInquiry->user->userID) ? $serviceInquiry->user_id : $serviceInquiry->user->userID;
                    $userID = $serviceInquiry->user_id;
                    $s = collect($jsonData)->where('type', 'appendable_table')->map(function ($item) {
                        if (isset($item->data)) {
                            if (collect($item->data)->where('type', 'link')->count() > 0) {
                                return collect($item->data)->where('type', 'link')->toArray();
                            }
                        }
                    })->filter(function ($item) {
                        return !is_null($item);
                    })->toArray();
                    $datas = Helper::mergeArrayofArrays($s);
                    if (count($datas)) {
                        foreach ($datas as $k => $data) {
                            try {
                                $url = 'https://digital.baiterek.gov.kz/Synergy/rest/api/storage/file/get?identifier=';
                                $identifier = str_replace('https://digital.baiterek.gov.kz/baiterek/file?identifier=', '', substr($data->value, 0, strpos($data->value, '&token')));
                                $identifier = str_replace('http://digital.baiterek.gov.kz/baiterek/file?identifier=', '', substr($identifier, 0, strpos($data->value, '&token')));
                                $url .= $identifier;
                                Log::channel('migrating_files')->notice('AT IDENTIFIER => ' . $identifier);
                                $this->comment($identifier);
                                $migratedDocument = MigratedDocument::where('identifier', $identifier)->where('user_id', $userID)->first();
                                if ($migratedDocument) {
                                    continue;
                                }
                                $exceptoinMessage = null;
                                $storageUrl = null;
                                $fileName = null;
                                try {
                                    /* GET FILE DETAILS */
                                    $fileDetails = $this->getFileDetails($identifier);
                                    $fileName = $fileDetails->name;
                                    if (!empty($fileDetails->errorMessage)) {
                                        $exceptoinMessage = $fileDetails->errorMessage;
                                    }
                                    /* END */
                                    $userPath = $serviceInquiry->user->name . '_' . $serviceInquiry->user->id;
                                    $path = $userPath . '/' . $serviceInquiry->id . '/' . $serviceInquiry->request_id . '_' . Carbon::now()->getTimestamp() . '_' . $fileName;
                                    $client = new \GuzzleHttp\Client();
                                    $res = $client->request('GET', $url, [
                                        'auth' => ['superadmin_admin', 'Heinkb1737!']
                                    ]);
                                    $content = $res->getBody()->getContents();
                                    Helper::storeSynergyFile('MigratedDocuments/' . $path, $content);
                                    $storageUrl = $path;
                                } catch (\Exception $e) {
                                    $exceptoinMessage = $e->getMessage();
                                    $this->error('Something went wrong! => $identifier |||| ' . $identifier);
                                    Log::channel('migrating_files')->notice('Something went wrong! => $identifier |||| ' . $identifier);
                                    $this->error($e->getMessage());
                                    Log::channel('migrating_files')->notice($e->getMessage());
                                }
                                $migratedDocument = new MigratedDocument;
                                $migratedDocument->old_userID = $oldUserId;
                                $migratedDocument->user_id = $userID;
                                $migratedDocument->identifier = $identifier;
                                $migratedDocument->product_id = $serviceInquiry->product_id;
                                $migratedDocument->service_inquiry_id = $serviceInquiry->id;
                                $migratedDocument->name = $fileName;
                                $migratedDocument->path = $storageUrl;
                                $migratedDocument->exception = $exceptoinMessage;
                                $migratedDocument->save();
                            } catch (\Exception $e) {
                                $this->error('Something went wrong! => $datas |||| ' . $data->key);
                                Log::channel('migrating_files')->notice('Something went wrong! => $datas |||| ' . $data->key);
                                $this->error($e->getMessage());
                                Log::channel('migrating_files')->notice($e->getMessage());
                                continue;
                            }
                        }

                    }
                } catch (\Exception $e) {
                    $this->error($e->getMessage());
                    Log::channel('migrating_files')->notice('Something went wrong! => ' . $serviceInquiry->id);
                    Log::channel('migrating_files')->notice($e->getMessage());
                    continue;
                }
//        $this->comment($serviceInq->user_id);
            }
            Mail::raw('All files are migrated successfully.....', function ($message) {
                $message->to(['ronny.stark.2019@gmail.com', 'savansolanki22@gmail.com', 'savan@avdevs.com', 'sandeep@avdevs.com'])->subject('FILES MIGRATION');
            });
            $this->info('..............................Process Finished..............................');
            /*****************************************    REGISTRIES    *********************************************************/ 

        } catch (\Exception $e) {
            $this->error('Something went wrong! Main Try Catch');
            Log::channel('migrating_files')->notice('Something went wrong! Main Try Catch');
            Log::channel('migrating_files')->notice($e->getMessage());
            $this->error($e->getMessage());
        }
    }

    public function getFileDetails($identifier)
    {
        $client = new \GuzzleHttp\Client();
        $registryResponse = $client->request('GET', 'https://digital.baiterek.gov.kz/Synergy/rest/api/storage/description', [
            'timeout' => 300,
            'auth' => ['superadmin_admin', 'Heinkb1737!'],
            'query' => ['elementID' => $identifier]
        ]);
        $registryResponse = $registryResponse->getBody()->getContents();
        $file = json_decode($registryResponse);
        $errorMessage = '';
        if (!empty($file->errorMessage)) {
            $errorMessage = $file->errorMessage;
        }
//        dd($file);
        $data = (Object)[
            'name' => $file->name ?? null,
            'errorMessage' => $errorMessage
        ];
//        $old_userID = $asformsResponse->oldUuid;
        return $data;
    }
}