<?php
/**
 * Created by PhpStorm.
 * User: Jiten Patel
 * Date: 4/9/2019
 * Time: 2:09 PM
 */

namespace App\Traits;
use Yajra\Datatables\DataTables;
use App\Helper\Helper;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;

trait BrowseDataTables {
    protected function browse(Request $request){
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            $query = $model::select('*');
            $slug = str_replace('-', '_', $slug);

            return Datatables::of($query)
                ->editColumn('id', function($query) use ($slug) {
                    if(auth()->user()->hasPermission('delete_'.$slug)) {
                        return '<input type="checkbox" name="row_id" id="checkbox_'.$query->id.'" value="'.$query->id.'">';
                    } else {
                        return '';
                    }
                })
                ->editColumn('action', function($query) use ($dataType, $slug) {
                    return Helper::getGeneralActions($dataType, $query, $slug);
                })
                ->editColumn('created_at', function($query) use ($dataType) {
                    return Helper::getFormattedDate($query->created_at);
                })
                ->rawColumns(['id','action'])
                ->make(true);
        } else {
            abort(404);
        }
    }
}