<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Subsidiaries extends Model
{
    use Translatable;

    protected $translatable = ['title', 'description'];

    public function subsidiary() {
        return $this->hasMany(SubsidiaryUsers::class,'subsidiary_id');
    }
}
