<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubsidiaryUsers extends Model
{
    protected $fillable = [
        'user_id',
        'subsidiary_id',
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function subsidiary() {
        return $this->belongsTo(Subsidiaries::class);
    }
}
