<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceInquiryActivity extends Model
{
    const CHANGE_STATUS = 'changeStatus', TEXT_MESSAGE = 'textMessage', CHECKPOINT_MARK = 'checkpointMark', CHECKPOINT_COMMENT = 'checkpointComment', ACTIVITY_VERIFICATION = 'activityVerification', INVITE_FOR_VERIFICATION = 'inviteForVerification';
    const CUSTOMER_FILES = 'customerFiles';
    protected $table = 'service_inquiry_activities';
    protected $guarded = ['id'];

    public function serviceInquiry()
    {
        return $this->belongsTo(ServiceInquiries::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
