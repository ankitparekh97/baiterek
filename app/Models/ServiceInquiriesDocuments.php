<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceInquiriesDocuments extends Model
{
    protected $fillable = [
        'document',
        'service_inquiries_id',
    ];
}
