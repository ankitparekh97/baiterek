<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceInquiryCheckPoint extends Model
{
    const TRUE = 1, FALSE = 0;
    protected $table = 'service_inquiry_check_points';
    protected $guarded = ['id'];

    public function serviceInquiry()
    {
        return $this->belongsTo(ServiceInquiries::class,'service_inquiry_id');
    }
}
