<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCheckPoint extends Model
{
    const TEXT = 0, DOCUMENT = 1;

    protected $fillable = [
        'product_id',
        'checkpoint',
        'type',
        'document_name',
        'status',
    ];

    public function scopeDocument($query) {
        return $query->where('product_check_points.type',self::DOCUMENT);
    }
    public function scopeText($query) {
        return $query->where('product_check_points.type',self::TEXT);
    }
}
