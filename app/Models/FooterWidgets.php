<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class FooterWidgets extends Model
{
    use Translatable;

    protected $translatable = ['title', 'content'];
}
