<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Models\Role;
use TCG\Voyager\Traits\Translatable;

class Products extends Model
{
    use Translatable;
    const VERIFICATION_YES = 1, VERIFICATION_NO = 0;
    protected $translatable = ['title', 'code', 'description'];

    protected $fillable = [
        'id',
        'title',
        'code',
        'productCategory_id',
        'subsidiary_id',
        'user_id',
//        'organization',
        'description',
        'can_apply',
        'logo',
        'is_verification_required'
    ];

    public static function rules() {
        return [
            'title' => 'required',
            'code' => 'required',
            'productCategory_id' => 'required',
            'subsidiary_id' => 'required',
            'user_id' => 'required',
            'logo' => 'required|image|mimes:jpeg,png,jpg|max:20000',
        ];
    }

    public static function rulesMessages(){
        return [
            'title.required'    => 'Please enter Title',
            'code.required'    => 'Please enter Code',
            'productCategory_id.required'    => 'Please enter Product Category',
            'subsidiary_id.required'    => 'Please enter Subsidiary',
            'logo.required'    => 'Please upload Product logo',
        ];
    }

    public function subsidiaryId() {
        return $this->belongsTo(Subsidiaries::class,'subsidiary_id','id');
    }

    public function productCategoryId() {
        return $this->belongsTo(ProductCategories::class,'productCategory_id','id');
    }

    public function serviceInquiry() {
        return $this->belongsTo(ServiceInquiries::class,'id','product_id');
    }

    public function userId() {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function productCheckPoints() {
        return $this->hasMany(ProductCheckPoint::class,'product_id');
    }

    public function applicationForm()
    {
        return $this->belongsTo(ApplicationForms::class,'id','product_id');
    }

}
