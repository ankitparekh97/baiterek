<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class ApplicationForms extends Model
{
    use Translatable;
    protected $translatable = ['fields'];

    protected $fillable = [
        'id',
        'product_id',
        'fields',
        'milestone',
    ];
}
