<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class ConsultingServiceProducts extends Model
{
    use Translatable;

    protected $translatable = [''];

    public function product(){
        return $this->belongsTo(Products::class,'product_id','id');
    }
}
