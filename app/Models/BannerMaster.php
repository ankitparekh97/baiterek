<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class BannerMaster extends Model
{
    use Translatable;
    protected $translatable = ['title'];

    public static function rules() {
        return [
            'title' => 'required',
            'width' => 'required|numeric',
            'height' => 'required|numeric',
            'folder' => 'required',
        ];
    }

    public function bannerImages()
    {
        return $this->hasMany(BannerImages::class,'banner_masters_id','id');
    }
}
