<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Projects extends Model
{
    use Translatable;
    protected $translatable = ['title', 'description'];

    public function product(){
        return $this->belongsTo(Products::class,'product_id','id');
    }
}
