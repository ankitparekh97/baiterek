<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceInquiryVerification extends Model
{
    const VERIFICATION_YES = 1, VERIFICATION_NO = 0;
    protected $table = 'service_inquiry_verifications';
    protected $guarded = ['id'];

    protected $fillable = [
        'service_inquiry_id',
        'email',
        'link',
        'is_verified',
        'inquiry_status',
        'action_taken_at',
        'comment',
        'path',
        'documents',
    ];
}
