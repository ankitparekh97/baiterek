<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class ServiceInquiries extends Model
{
    const APPLIED ='Applied',
        DRAFT = 'Draft',
        ACCEPTED = 'Accepted',
        REJECTED = 'Rejected',
        IN_PROCESS = 'In process',
        ON_CONTROL = 'On control',
        APPLICATION_RECEIVED = 'Application received',
        CONTROL = 'Control',
        DATA_DOCUMENTS_ARE_MISSING = 'Data/Documents are missing';
    use Translatable;
    protected $translatable = [
        'name',
        'last_name',
        'email',
        // 'marital_status',
        // 'gender',
        'address',
        'application_type',
        'contact',
        'patronymiv_name',
        // 'email_of_recipient',
        'message',
        // 'product_id',
        // 'user_id',
        'status_of_request'
    ];

    protected $fillable = [
        'fields',
        'locale',
        'due_date',
        'application_form_id',
        'name',
        'last_name',
        'email',
        'marital_status',
        'gender',
        'address',
        'application_type',
        'contact',
        'patronymiv_name',
        'email_of_recipient',
        'application_type',
        'application_status',
        'message',
        'product_id',
        'user_id',
        'status_of_request',
        'pm_feedback',
        'request_id',
        'dataUUID',
        'old_json_data',
        'is_migrated'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Products::class);
    }

    public function activities()
    {
        return $this->hasMany(ServiceInquiryActivity::class,'service_inquiry_id','id');
    }

    public function inquiryCheckPoints()
    {
     return $this->hasMany(ServiceInquiryCheckPoint::class,'service_inquiry_id','id');
    }

    public function verifications()
    {
        return $this->hasMany(ServiceInquiryVerification::class,'service_inquiry_id','id');
    }
}
