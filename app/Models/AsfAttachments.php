<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AsfAttachments extends Model
{
    protected $table = 'asf_attachments';
    protected $guarded = ['id'];
}
