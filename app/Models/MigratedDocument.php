<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 */
class MigratedDocument extends Model
{
    protected $table = 'migrated_documents';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function serviceInquiry()
    {
        return $this->belongsTo(ServiceInquiries::class,'service_inquiry_id');
    }
}
