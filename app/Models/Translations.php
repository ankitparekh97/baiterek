<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Translations extends Model
{
    use Translatable;
    protected $translatable = ['value'];

    protected $table = 'translations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'table_name',
        'column_name',
        'foreign_key',
        'locale',
        'value',
    ];
}
