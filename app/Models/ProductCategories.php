<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class ProductCategories extends Model
{
    use Translatable;

    protected $translatable = ['title', 'description'];

    public static function rules() {
        return [
            'title' => 'required',
        ];
    }

    public function products(){
        return $this->hasMany(Products::class,'productCategory_id','id');
    }
}
