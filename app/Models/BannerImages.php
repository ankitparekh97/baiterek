<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class BannerImages extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'title',
        'title_color',
        'image',
        'banner_masters_id',
        'primary_text',
        'secondary_text',
        'url',
        'url_text',
        'url_text_color',
        'url_text',
        'order',
        'slug'
    ];

    use Translatable;

    protected $translatable = ['title', 'primary_text', 'secondary_text', 'url_text'];

    public static function rules() {
        $regex = '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';

        return [
            'title' => 'required',
            'banner_masters_id' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:20000',
//            'order' => 'required|unique:banner_images|numeric',
//            'url' => 'regex:' . $regex,
        ];
    }

    public static function rulesMessages(){
        return [
            'banner_masters_id.required'    => 'Please select Banner category.',
            'image.required'    => 'Please upload banner.',
//            'order.required'    => 'Please enter order number.',
//            'url.regex' => 'Please enter url in format [http://www.abc.com]',
        ];
    }

    public function bannerMasterId()
    {
        return $this->belongsTo(BannerMaster::class);
    }
}
