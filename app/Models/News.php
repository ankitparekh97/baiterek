<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use TCG\Voyager\Traits\Translatable;

class News extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'author_id',
        'image',
        'short_description',
        'long_description',
//        'order',
    ];

    use Translatable;

    protected $translatable = ['short_description', 'long_description'];

    public static function rules() {
        return [
            'image' => 'required|image|mimes:jpeg,png,jpg|max:20000',
//            'order' => 'required',
            'short_description' => 'required',
            'long_description' => 'required',
        ];
    }

    public function user(){
        return $this->belongsTo(User::class,'author_id');
    }
}
