<?php

namespace App\Models;

use App\Helper\Helper;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;
use TCG\Voyager\Models\MenuItem;
use TCG\Voyager\Models\Role;
use TCG\Voyager\Traits\Translatable;

//use Lab404\Impersonate\Models\Impersonate;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable, HasApiTokens;
//    use Translatable;
//
//    protected $translatable = ['name'];
    const IN_ACTIVE = 0, ACTIVE = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_name',
        'email',
        'password',
        'role_id',
        'dob',
        'gender',
        'address',
        'contact',
        'city',
        'city_id',
        'region',
        'region_id',
        'postcode',
        'patronymic',
        'is_active',
        'VAT_no',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public static function generatePassword(){
        return substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@!#$%&*') , 0 , 10 );
    }

    public static function rules() {
        return [
            'email' => 'required|unique:users',
            'role_id' => 'required',
            'name' => 'required',
            'last_name' => 'required',
//            'gender' => 'required',
//            'address' => 'required',
            'contact' => 'required|digits_between:0,11',
//            'city' => 'required',
//            'postcode' => 'required',
//            'VAT_no' => 'required|unique:users',
        ];
    }

    public static function accessRule($id) {
        return [
            'name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
//            'gender' => 'required',
//            'address' => 'required',
            'contact' => 'required|digits_between:0,11',
//            'city' =>   'required',
//            'postcode' => 'required',
//            'VAT_no' => 'required|unique:users,VAT_no,'.$id,
        ];
    }

    public static function passwordRule() {
        return [
            'password' => [
                'required',
                'string',
                'required_with:confirm_password',
                'same:confirm_password',
                'min:8',             // must be at least 8 characters in length
                'regex:/[a-z]/',      // must contain at least one lowercase letter
                'regex:/[A-Z]/',      // must contain at least one uppercase letter
                'regex:/[0-9]/',      // must contain at least one digit
                'regex:/[@$!%*#?&]/', // must contain a special character
            ],
            'confirm_password' => 'required|min:8',
        ];
    }

    public static function passwordRuleMessages(){
        return [
            'password.required'    => 'Password should be minimum 8 characters and must contain one upper case, one lowercase character, one digit and a special character',
            'confirm_password.required'    => 'Confirm Password should be same as password',
        ];
    }

    public function role() {
        return $this->belongsTo(Role::class);
    }

    public function news() {
        return $this->belongsTo(News::class);
    }

    public function subsidiary_user() {
        return $this->hasMany(SubsidiaryUsers::class,'user_id');
    }
}
