<?php

namespace App\Mail;

use App\Helper\Helper;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProductMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data;
    public $productManager;
    public $actionUrl;

    public function __construct($data,$productManager,$actionUrl) {

        $this->data = $data;
        $this->productManager = $productManager;
        $this->actionUrl = $actionUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->from(Helper::getSenderEmail(), Helper::getSenderName())->subject('Invitation from Baiterek to create new product')->markdown('vendor.voyager.emails.productMail')->with([
            'data' => $this->data,
            'productManager' => $this->productManager,
            'actionUrl' => $this->actionUrl
        ]);
    }
}
