<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ServiceRequest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;
    public $product;
    public $actionLink;
    public $token;

    public function __construct($user,$product,$actionLink)
    {
        $this->user  = $user;
        $this->product  = $product;
        $this->actionLink  = $actionLink;
//        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(\App\Helper\Helper::getSenderEmail(), \App\Helper\Helper::getSenderName())->subject(trans('email.newApplication'))->markdown('vendor.voyager.emails.serviceRequest')->with([
            'user' => $this->user,
            'product' => $this->product,
            'actionLink' => $this->actionLink,
            'userLang' => 'ru'
//            'token'     => $this->token
        ]);
    }
}
