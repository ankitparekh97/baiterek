<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Useremail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;
    public $password;
    public $actionLink;
    public $userLang;

    public $token;

    public function __construct($user, $password, $actionLink, $userLang)
    {
        $this->user  = $user;
        $this->password = $password;
        $this->actionLink = $actionLink;
        $this->userLang = $userLang;
//        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        app()->setLocale($this->userLang);
        $subject = trans('email.userEmailSubject');
        return $this->from(\App\Helper\Helper::getSenderEmail(), \App\Helper\Helper::getSenderName())->subject($subject)->markdown('vendor.voyager.emails.user')->with([
            'user'      => $this->user,
            'password'  => $this->password,
            'btnLink' => $this->actionLink,
            'userLang' => $this->userLang
//            'token'     => $this->token
        ]);
    }
}
