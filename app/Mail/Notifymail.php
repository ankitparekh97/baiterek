<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Notifymail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $name;
    public $oldStatus;
    public $newStatus;

    public function __construct($name, $oldStatus, $newStatus)
    {
        $this->name  = $name;
        $this->oldStatus = $oldStatus;
        $this->newStatus = $newStatus;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(\App\Helper\Helper::getSenderEmail(), \App\Helper\Helper::getSenderName())->subject('Application Status Changed')->markdown('vendor.voyager.emails.notify')->with([
            'name'      => $this->name,
            'oldStatus'  => $this->oldStatus,
            'newStatus' => $this->newStatus
        ]);
    }
}
