<?php

namespace App\Mail;

use App\Helper\Helper;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Forgotpassword extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $name;
    public $token;
    public $actionUrl;
    public $userLang;

    public function __construct($name,$token,$actionUrl,$userLang) {

        $this->name = $name;
        $this->token = $token;
        $this->actionUrl = $actionUrl;
        $this->userLang = $userLang;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        app()->setLocale($this->userLang);
        $subject = trans('email.forgorPasswordSubject');
        return $this->from(Helper::getSenderEmail(), Helper::getSenderName())->subject($subject)->markdown('vendor.voyager.emails.passwordReset')->with([
            'name' => $this->name,
            'token' => $this->token,
            'actionUrl' => $this->actionUrl,
            'userLang' => $this->userLang
        ]);
    }
}
