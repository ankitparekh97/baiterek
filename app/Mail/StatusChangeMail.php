<?php

namespace App\Mail;

use App\Helper\Helper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class StatusChangeMail extends Mailable
{
    use Queueable, SerializesModels;

    public $userLang;
    public $name;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $serviceInquiryActivity;
    public function __construct($serviceInquiryActivity, $name, $userLang)
    {
        $this->serviceInquiryActivity = $serviceInquiryActivity;
        $this->userLang = $userLang;
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        app()->setLocale($this->userLang);
//        app()->setLocale('ru');
        $actionUrl = url('admin/service-inquiries/').'/'.$this->serviceInquiryActivity->service_inquiry_id;
//        $subject = 'Изменение статуса заявки в Digital Baiterek';
        $subject = trans('email.statusSubject');
        return $this->from(Helper::getSenderEmail(), Helper::getSenderName())->subject($subject)
            ->markdown('vendor.voyager.emails.statusUpdate')
            ->with([
                'serviceInquiryActivity' => $this->serviceInquiryActivity,
                'actionUrl' => $actionUrl,
                'name' => $this->name,
                'userLang' => 'ru'
            ]);
    }
}
