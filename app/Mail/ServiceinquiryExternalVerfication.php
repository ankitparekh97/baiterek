<?php

namespace App\Mail;

use App\Helper\Helper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ServiceinquiryExternalVerfication extends Mailable
{
    use Queueable, SerializesModels;
    public $serviceInquiry,$serviceInquiryVerification;
    public $userLang;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($serviceInquiry,$serviceInquiryVerification, $userLang)
    {
        $this->serviceInquiryVerification = $serviceInquiryVerification;
        $this->serviceInquiry = $serviceInquiry;
        $this->userLang = $userLang;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        app()->setLocale($this->userLang);
        return $this->from(Helper::getSenderEmail(), Helper::getSenderName())->subject('Согласование заявки в Digital Baiterek')
            ->markdown('vendor.voyager.emails.externalVerification')
            ->with([
                'serviceInquiryVerification' => $this->serviceInquiryVerification,
                'serviceInquiry' => $this->serviceInquiry,
                'userLang' => $this->userLang
            ]);
    }
}
