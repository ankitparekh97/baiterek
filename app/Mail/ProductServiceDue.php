<?php

namespace App\Mail;

use App\Helper\Helper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProductServiceDue extends Mailable
{
    use Queueable, SerializesModels;
    public $user,$serviceInquiry;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$serviceInquiry)
    {
        $this->user = $user;
        $this->serviceInquiry = $serviceInquiry;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
//        $this->from(env('MAIL_FROM'),'Baiterek');
        return $this->from(Helper::getSenderEmail(), Helper::getSenderName())->subject('Product Service Due')
            ->markdown('vendor.voyager.emails.productServiceDue')
            ->with([
                'user' => $this->user,
                'serviceInquiry' => $this->serviceInquiry,
            ]);
//        return $this->view('vendor.voyager.emails.productServiceDue',[
//            'user' => $this->user,
//            'serviceInquiry' => $this->serviceInquiry,
//        ]);
    }
}
