<?php
namespace App\Export;
use App\Http\Controllers\Voyager\ServiceInquiryController;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class Export extends ServiceInquiryController implements FromView, ShouldAutoSize
{
    private $data;
    public function __construct($data){
        $this->data = $data;
    }

    public function view(): View
    {
        return view('vendor.voyager.service-inquiries.csv', [
            'serviceInquiry' => $this->data
        ]);
    }
}