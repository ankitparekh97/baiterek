<?php
namespace App\Helper;

use App\Models\BannerImages;
use App\Models\BannerMaster;
use App\Models\FooterWidgets;
use App\Models\GraphBlocks;
use App\Models\News;
use App\Models\ServiceInquiries;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use TCG\Voyager\Models\Menu;
use League\Flysystem\Filesystem;
use League\Flysystem\Sftp\SftpAdapter;

class Helper
{
    private $slug;
    private $data;
    private $dataType;
    private $date;
    private $type;
    private $appendTime;
    private $Id;

    public function __construct($slug,$data,$dataType,$date,$type,$appendTime,$Id){
        $this->slug = $slug;
        $this->data = $data;
        $this->dataType = $dataType;
        $this->date = $date;
        $this->type = $type;
        $this->appendTime = $appendTime;
        $this->Id = $Id;
    }

    public static function updateSlugForRole(){
        $roles = \TCG\Voyager\Models\Role::all();
        if($roles->count() > 0) {
            foreach ($roles as $role) {
                $role->slug = self::generateSlug($role->name);
                $role->save();
            }
        }
    }

    public static function updateSlugForFooterWidgets(){
        $footerWidgets = FooterWidgets::all();
        if($footerWidgets->count() > 0) {
            foreach ($footerWidgets as $footerWidget) {
                $footerWidget->slug = self::generateSlug($footerWidget->title);
                $footerWidget->save();
            }
        }
    }

    public static function generateSlug($name){
        return strtolower(str_replace(' ','-', $name));
    }

    public static function getFilteredName($name){
        return str_replace(' ', '-', strtolower($name));
    }

    public static function getBrowseColumns($dataType) {
        $browseColumns = [];
        array_push($browseColumns, ['data' => 'id', 'name' => 'id', 'searchable' => false]);

        foreach($dataType->browseRows as $row) {
            if($row->type == 'relationship'){
                array_push($browseColumns, [
                    'data' => $row->details->column,
                    'name' => $row->details->column,
                    'searchable' => true
                ]);
            } else {
                array_push($browseColumns, [
                    'data' => $row->field,
                    'name' => $row->field,
                    'searchable' => true
                ]);
            }
        }
        array_push($browseColumns, ['data' => 'action', 'name' => 'action', 'searchable' => false]);
        return json_encode($browseColumns);
    }

    public static function getBrowseColumnsServiceInquiry($dataType) {
        $browseColumns = [];
        array_push($browseColumns, ['data' => 'id', 'name' => 'id', 'searchable' => false]);
        if (\auth()->user()->role->slug == 'customer') {
            $role_customer = [
                ['data' => 'request_id', 'name' => 'request_id', 'searchable' => true],
                ['data' => 'name_of_service', 'name' => 'name_of_service', 'searchable' => false],
                ['data' => 'responsible_person', 'name' => 'responsible_person', 'searchable' => false],
                ['data' => 'status_of_request', 'name' => 'status_of_request', 'searchable' => false],
                ['data' => 'created_at', 'name' => 'created_at', 'searchable' => false],
            ];
            $browseColumns = array_merge($browseColumns, $role_customer);
        } else if (\auth()->user()->role->slug == 'product-manager') {
            $role_product_manager = [
                ['data' => 'request_id', 'name' => 'request_id', 'searchable' => true],
                ['data' => 'name', 'name' => 'name', 'searchable' => true],
                ['data' => 'last_name', 'name' => 'last_name', 'searchable' => true],
                ['data' => 'applicant_type', 'name' => 'applicant_type', 'searchable' => false],
                ['data' => 'status_of_request', 'name' => 'status_of_request', 'searchable' => false],
                ['data' => 'created_at', 'name' => 'created_at', 'searchable' => false],
            ];
            $browseColumns = array_merge($browseColumns, $role_product_manager);
        } else {
            foreach($dataType->browseRows as $row) {
                if($row->type == 'relationship'){
                    array_push($browseColumns, [
                        'data' => $row->details->column,
                        'name' => $row->details->column,
                        'searchable' => true
                    ]);
                } else {
                    array_push($browseColumns, [
                        'data' => $row->field,
                        'name' => $row->field,
                        'searchable' => true
                    ]);
                }
            }
        }
        array_push($browseColumns, ['data' => 'action', 'name' => 'action', 'searchable' => false]);
        return json_encode($browseColumns);
    }

    public static function getProductManagers() {

        $browseColumns = [];
        array_push($browseColumns, ['data' => 'id', 'name' => 'id', 'searchable' => false]);
        $role_product_manager = [
            ['data' => 'name', 'name' => 'name', 'searchable' => true],
            ['data' => 'last_name', 'name' => 'last_name', 'searchable' => true],
            ['data' => 'email', 'name' => 'email', 'searchable' => false],
            ['data' => 'gender', 'name' => 'gender', 'searchable' => false],
            ['data' => 'created_at', 'name' => 'created_at', 'searchable' => false],
        ];
        $browseColumns = array_merge($browseColumns, $role_product_manager);

        array_push($browseColumns, ['data' => 'action', 'name' => 'action', 'searchable' => false]);
        return json_encode($browseColumns);
    }

    public static function getGeneralActions($dataType, $data, $slug = ''){
        app()->setLocale(\Session::get('locale'));
        $strActions = '';
        foreach (\TCG\Voyager\Facades\Voyager::actions() as $action) {
            $action = new $action($dataType, $data);

            $slug = str_replace('-', '_', $slug);
            $permissionName = strtolower(str_replace('View', 'Read', $action->getTitle())).'_'.$slug;
            if($action->shouldActionDisplayOnDataType()) {
                if($action->getTitle() == 'View') {
                    if(auth()->user()->hasPermission($permissionName)) {
                            $strActions .= '<a href="'.$action->getRoute($dataType->name).'?filter=equals&s='.$data->id.'&n='.$data->title.'" title="'.$action->getTitle().'" '.$action->convertAttributesToHtml().'>
                                            <i class="'.$action->getIcon().'"></i><span class="hidden">'.__('portal.'.strtolower($action->getTitle())).'</span> '.__('portal.'.strtolower($action->getTitle())).'
                                            </a>';

                    }
                } else {
                    if(auth()->user()->hasPermission($permissionName)) {
                        $strActions .= '<a href="'.$action->getRoute($dataType->name).'" title="'.$action->getTitle().'" '.$action->convertAttributesToHtml().'>
                                        <i class="'.$action->getIcon().'"></i><span class="hidden">'.__('portal.'.strtolower($action->getTitle())).'</span> '.__('portal.'.strtolower($action->getTitle())).'
                                        </a>';
                    }
                }
            }
        }
//        dd($strActions);
        return $strActions;
    }

    public static function getFormattedDate($date, $appendTime = false){
        return \Carbon\Carbon::parse($date)->format(self::getDateFormat('php', $appendTime));
    }

    public static function getDateFormat($type = 'js', $appendTime = false){
        if($type == 'js') {
            return 'YYYY-MM-DD';
        } else if($type == 'php') {
            return ($appendTime == true) ? 'Y-m-d H:i:s' : 'Y-m-d';
        }
    }

    public static function getDataTypeBySlug($slug){
        $dataType = \TCG\Voyager\Facades\Voyager::model('DataType')->where('slug', '=', $slug)->first();
        $model = app($dataType->model_name);

        $query = $model::select('*')->get();
        return $query;
    }

    public static function getBannerCategoryBySlug($slug){
        $bannerMaster = BannerMaster::where('slug', $slug)->first();
        if(!empty($bannerMaster)) {
            return $bannerMaster;
        } else {
            return '';
        }
    }

    public static function getBannersImages($slug){
        $bannerMaster = self::getBannerCategoryBySlug($slug);

        if(!empty($bannerMaster)){
            $query = BannerImages::where('banner_masters_id', $bannerMaster->id)->orderBy('order')->get()->translate('locale', app()->getLocale());
        } else {
            $query = '';
        }

        return $query;
    }

    public static function getGraphBlocks(){
        $query = GraphBlocks::orderBy('order')->get()->translate('locale', app()->getLocale());
        return $query;
    }

    public static function getNews(){
        if(url('/') != \Illuminate\Support\Facades\Request::url()){
            $query = News::with('user')
                ->orderBy('id','desc')
                ->get()
                ->translate('locale', app()->getLocale())
                ->paginate(24);
        } else {
            $query = News::with('user')
                ->orderBy('id','desc')
                ->limit(4)
                ->get()
                ->translate('locale', app()->getLocale())
                ->paginate(24);
        }

        return $query;
    }

    public static function getNewsDetail($id){
        if(!empty($id)){
//            $query = FooterWidgets::where('slug', $slug)->orderBy('order')->first()->translate('locale', app()->getLocale());
            $query = News::where('id', $id)->orderBy('order')->get()->translate('locale', app()->getLocale())->first();
        } else {
            $query = array();
        }
        return $query;
    }

    public static function getFooterWidget($slug){
        if(!empty($slug)){
//            $query = FooterWidgets::where('slug', $slug)->orderBy('order')->first()->translate('locale', app()->getLocale());
            $query = FooterWidgets::where('slug', $slug)->orderBy('order')->get()->translate('locale', app()->getLocale())->first();
        } else {
            $query = array();
        }
        return $query;
    }

    public static function getUrlSegment(){
        $current_uri = url()->current();
        $segment = last(explode('/', str_replace(''.url('').'', '', $current_uri)));
        return $segment;
    }

    public static function getSenderEmail(){
        if(\TCG\Voyager\Facades\Voyager::setting('admin.sender_email')) {
            return \TCG\Voyager\Facades\Voyager::setting('admin.sender_email');
        } else {
            return config('mail.from.address');
        }
    }

    public static function getSenderName(){
        if(\TCG\Voyager\Facades\Voyager::setting('admin.sender_name')) {
            return \TCG\Voyager\Facades\Voyager::setting('admin.sender_name');
        } else {
            return config('mail.from.name');
        }
    }

    public static function generateRememberToken($Id){
        return md5(str_replace('-', '', $Id).floor(microtime(true)));
    }

    public static function getSectionName($action, $typeValue, $type){
        if($type == 'url') {
            return $action.'_'.str_replace('admin/', '',
                    str_replace('-', '_',
                        str_replace(' ', '_', $typeValue)));
        } else {
            return $action.'_'.str_replace(' ', '_',
                    str_replace('-', '_', $typeValue));
        }
    }

    public static function isSinglePermission($section){
        $singleSections = [
            'browse_media',
            'browse_hooks',
            'browse_bread',
            'browse_compass',
            'browse_database',
        ];
        return in_array($section, $singleSections);
    }

    public static function getAvailableActions(){
        $actions = [
            'browse',
            'read',
            'add',
            'edit',
            'delete'
        ];
        return $actions;
    }

    public static function getPermissionKeys($menuItem){
        $permissionKeys = [];
	    if(!is_null($menuItem->url) && $menuItem->url != '') {
            foreach (self::getAvailableActions() as $action) {
                $sectionName = self::getSectionName($action, $menuItem->url, 'url');
                array_push($permissionKeys, $sectionName);
                if(self::isSinglePermission($sectionName)) break;
            }
        } elseif (!is_null($menuItem->route) && $menuItem->route != '') {
            foreach (self::getAvailableActions() as $action) {
                $routeItems = explode('.', $menuItem->route);
                $sectionName = self::getSectionName($action, $routeItems[1], 'route');
                array_push($permissionKeys, $sectionName);
                if(self::isSinglePermission($sectionName)) break;
            }
        }
	    return $permissionKeys;
    }

    public static function getPermissionTree(){
        $menu = Menu::where('name', 'admin')->first();
        $parentItems = $menu->parent_items->whereNotIn('title', ['Home', 'Logout'])->sortBy('order');

        $permissionTree = [];
        foreach ($parentItems as $parent) {
            $parentInfo = [
                'id' => $parent->id,
                'title' => $parent->title,
                'permission_keys' => Helper::getPermissionKeys($parent),
            ];

            $childItems = $parent->children()->orderBy('order')->get();
            $childList = [];
            foreach ($childItems as $child) {
                array_push($childList, [
                    'id' => $child->id,
                    'title' => $child->title,
                    'permission_keys' => Helper::getPermissionKeys($child),
                ]);
            }
            $parentInfo['children'] = $childList;
            array_push($permissionTree, $parentInfo);
        }
        return $permissionTree;
    }

    public static function getCustomers()
    {
        $users = User::whereHas('role', function($query){
            return $query->where('slug', 'customer');
        })->get();
        return $users;
    }

    public static function getCustomerStatus($status)
    {
        if($status == ServiceInquiries::APPLICATION_RECEIVED){
            return ServiceInquiries::APPLIED;
        }else if($status == ServiceInquiries::CONTROL){
            return ServiceInquiries::ON_CONTROL;
        } else {
            return $status;
        }
    }

    public static function mergeArrayofArrays($array, $property = null)
    {
        return array_reduce(
            (array) $array, // make sure this is an array too, or array_reduce is mad.
            function($carry, $item) use ($property) {

                $mergeOnProperty = (!$property) ?
                    $item :
                    (is_array($item) ? $item[$property] : $item->$property);

                return is_array($mergeOnProperty)
                    ? array_merge($carry, $mergeOnProperty)
                    : $carry;
            }, array()); // start the carry with empty array
    }

    public static function storeSynergyFile($path,$content)
    {
        // FIX PATH = public/MigratedDocuments
        Storage::disk('sftp')->put($path, $content);
        /*$systemPath = storage_path('app/public/MigratedDocuments');
        $files = \File::allFiles( $systemPath );

        $path2 = '/usr/requestdocs/';
//            $path = '/var/www/html/MigratedDocuments/';
        $filesystem = new Filesystem(new SftpAdapter([
            'host' => '10.7.7.4',
            'port' => 22,
            'username' => 'epp',
            'password' => '123456',
            'root' => $path2,
            'timeout' => 30,
        ]));
        if(!$filesystem){
            return null;
        }
        $write = array();
        foreach ($files as $node) {
            $content = \File::get($node);
            $write[] = $filesystem->put($node->getRelativePathname(),$content); // upload file
        }
        Storage::disk('local')->delete($file);*/
        return $path;
    }
}
