<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class FrontendAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if(Auth::user()) {
            if(Auth::user()->is_active == 0){
                if($request->route()->uri == 'resetpassword' ||
                    $request->route()->uri == 'updatepassword') {
                    return $next($request);
                } else {
                    return redirect(route('frontend.resetpassword'));
                }
            }
        }
        return $next($request);
    }
}
