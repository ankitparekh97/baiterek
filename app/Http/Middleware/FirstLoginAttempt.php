<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class FirstLoginAttempt
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()) {
            if(Auth::user()->is_active == 0){
                if($request->route()->uri == 'admin/change-pass' ||
                    $request->route()->uri == 'admin/update-pass') {
                    return $next($request);
                } else {
                    return redirect(route('voyager.change-pass'));
                }
            }



        }
        return $next($request);
    }
}
