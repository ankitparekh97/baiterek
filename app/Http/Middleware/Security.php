<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Security
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()) {
//            $exp = "/(['1'='1' --])\w+/";
//            if (count($_POST) > 0) {
//                dd(preg_match($exp, $_POST['formFields'], $match),$_POST['formFields']);
//                if(preg_match($exp, json_encode($_POST), $match)) {
//                    return view('errors.501');
//                }
//            }

            if(count($_GET)>0){
                if(json_encode($_GET) != strip_tags(json_encode($_GET))) {
                    return view('errors.501');
                }
            }

            $request->header('X-Frame-Options', 'SAMEORIGIN');
            $request->header('X-Content-Type-Options', 'nosniff');
            $request->header('X-XSS-Protection', '1; mode=block');
            $request->header('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
            $request->header('Pragma', 'no-cache');
            $request->header('Expires', 'Sat, 01 Jan 1990 00:00:00 GMT');
        }

        return $next($request);
    }
}
