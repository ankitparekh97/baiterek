<?php

namespace App\Http\Controllers\Voyager;

use App\Helper\Helper;
use App\Http\Controllers\Controller;
use App\Mail\Useremail;
use App\Models\User;
use App\Traits\BrowseDataTables;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use Illuminate\Support\Facades\Validator;
use TCG\Voyager\Models\Role;
use TCG\Voyager\Traits\Translatable;
use Yajra\DataTables\DataTables;

class ProductManagerController extends \TCG\Voyager\Http\Controllers\Controller
{
    use BreadRelationshipParser;

    public function index() {
        if(!auth()->user()->hasPermission('browse_product_managers')) {
            return view('vendor.voyager.unauthorized');
        }
        $menuView = 'ProductManager';
        return view('vendor.voyager.users.product-managers',compact('menuView'));
    }

    public function create(Request $request)
    {
        if(!auth()->user()->hasPermission('browse_product_managers')) {
            return view('vendor.voyager.unauthorized');
        }

        $slug = 'users';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? new $dataType->model_name()
            : false;

        foreach ($dataType->addRows as $key => $row) {
            $dataType->addRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $productManagerRole = Role::where('slug','product-manager')->first();
        $menuView = 'ProductManager';
        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'productManagerRole','menuView'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), User::rules());

        if ($validator->fails()) {
            if ($validator->fails()) {
                return back()->withErrors($validator);
            }
        }
        $slug = 'users';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        if (!$request->has('_validate')) {

            $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
            $data->remember_token = sha1(time());
            $password = User::generatePassword();
            $data->setPasswordAttribute($password);
            $data->is_active = 1;
            $data->save();

            if ($request->ajax()) {
                return response()->json(['success' => true, 'data' => '']);
            }

            try{
                $actionLink = url('admin/login');
                $userLang = app()->getLocale();
                Mail::to($request->email)->send(new Useremail($data, $password,$actionLink,$userLang));

                return redirect()
                    ->route("product-managers")
                    ->with([
                        'message'    => trans('toaster.successfully added new product manager activation link has been sent to', ['email' => $request->email]),
//                        'message'    => __('voyager::generic.successfully_added_new')." The activation link has been sent to ".$request->email.".",
                        'alert-type' => 'success',
                    ]);

            } catch (\Exception $ex) {
                Log::error($ex->getMessage());
                return redirect()
                    ->route("product-managers")
                    ->with([
                        'message'    => __('voyager::generic.successfully_added_new')." User",
                        'alert-type' => 'success',
                    ]);
            }

        }
    }

    public function edit(Request $request, $id)
    {
        if(!auth()->user()->hasPermission('browse_product_managers')) {
            return view('vendor.voyager.unauthorized');
        }
        $slug = 'users';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? app($dataType->model_name)->findOrFail($id)
            : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');


        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);
        $menuView = 'ProductManager';
        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable','menuView'));
    }

    public function show(Request $request,$id)
    {
        $slug = 'users';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);
        $menuView = 'ProductManager';
        $view = 'voyager::bread.read';

        if (view()->exists("voyager::$slug.read")) {
            $view = "voyager::$slug.read";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable','menuView'));
    }

    public function destroy(Request $request, $id)
    {
        if(!auth()->user()->hasPermission('browse_product_managers')) {
            abort('403');
        }

        $slug = 'users';
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Init array of IDs
        $ids = [];
        $relatedUserDataIds = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }

        foreach ($ids as $id) {
            $user = User::where('id', '=',$id)->first();
            if ($user != null) {
                $userIds[] = $user->id;
            }
        }

        $displayName = count($ids) > 1 ? "These ".$dataType->display_name_plural : "This ".$dataType->display_name_singular;

        if (count($userIds) == 0) {
            $userNames = [];

            foreach ($userIds as $id) {
                $data = User::where('id', $id)->first();
                $userNames[] = $data->title;
            }

            $stringRelatedUserDataNames = implode(',', $userNames);
            $displayPucntuation = count($userIds) > 1 ? 'are' : 'is';

            $data = [
                'message'    => "You cannot Delete "."{$displayName}"." as "."{$stringRelatedUserDataNames}"." {$displayPucntuation} ",
                'alert-type' => 'error',
            ];
        }
        else {
            foreach ($ids as $id) {
                $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
            }

            $res = $data->destroy($ids);
            $data = $res
                ? [
                    'message'    => __('voyager::generic.successfully_deleted')." {$displayName}",
                    'alert-type' => 'success',
                ]
                : [
                    'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                    'alert-type' => 'error',
                ];
        }

        if(isset($request->PM)){
            return redirect()->route("product-managers")->with($data);
        } else {
            return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), User::accessRule($id));

        if ($validator->fails()) {
            if ($validator->fails()) {
                return back()->withErrors($validator);
            }
        }

        $slug = 'users';
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $data = call_user_func([$dataType->model_name, 'findOrFail'], $request->id);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $request->id);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->has('_validate'))
        {
            $update = $this->insertUpdateData($request, $slug, $dataType->editRows, $data);
            $update->is_active = 1;
            $update->save();

            return redirect()
                ->route("product-managers")
                ->with([
                    'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }
    }

    public function browse(Request $request){

        if(!auth()->user()->hasPermission('browse_product_managers')) {
            abort('403');
        }

        $productManagerRole = Role::where('slug','product-manager')->first();
        $query = User::where('role_id', $productManagerRole->id)->orderBy('id','desc');

        return DataTables::of($query)
            ->editColumn('id', function($query) {
                return '<input type="checkbox" name="row_id" id="checkbox_'.$query->id.'" value="'.$query->id.'">';
            })
            ->editColumn('action', function($query) {
                $delete = '<a href="javascript:;" title="Delete" class="btn btn-sm btn-danger pull-right delete" data-id="'.$query->id.'" id="delete-'.$query->id.'">
											<i class="voyager-trash"></i><span class="hidden">Delete</span> '.trans('portal.product_manager.delete').'
                                        </a>';

                $edit = '<a href="'.url('/').'/admin/product-managers/edit/'.$query->id.'" title="Edit" class="btn btn-sm btn-primary pull-right edit">
											<i class="voyager-edit"></i><span class="hidden">Edit</span> '.trans('portal.product_manager.edit').'
                                        </a>';

                $view = '<a href="'.url('/').'/admin/product-managers/read/'.$query->id.'" title="View" class="btn btn-sm btn-warning pull-right view">
                                            <i class="voyager-eye"></i><span class="hidden">View</span> '.trans('portal.product_manager.view').'
                                        </a>';

                return $delete . $edit . $view;
            })
            ->rawColumns(['action', 'id'])
            ->make(true);
    }
}
