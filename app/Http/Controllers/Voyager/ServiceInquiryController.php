<?php

namespace App\Http\Controllers\Voyager;

use App\Export\Export;
use App\Helper\Helper;
use App\Jobs\SendNotificationJob;
use App\Mail\ServiceinquiryExternalVerfication;
use App\Mail\StatusChangeMail;
use App\Models\ApplicationForms;
use App\Models\ProductCheckPoint;
use App\Models\ServiceInquiries;
use App\Models\ServiceInquiriesDocuments;
use App\Models\ServiceInquiryActivity;
use App\Models\ServiceInquiryCheckPoint;
use App\Models\ServiceInquiryVerification;
use App\Models\Translations;
use App\Models\User;
use App\Traits\BrowseDataTables;
//use Barryvdh\DomPDF\PDF;
use Carbon\Carbon;
use Dompdf\Dompdf;
use Dompdf\Options;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Excel;
use stdClass;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\File;
use App\Mail\Notifymail;
use App\Models\Products;
use App\Http\Controllers\Services;
use App\Mail\ServiceRequest;

class ServiceInquiryController extends \TCG\Voyager\Http\Controllers\Controller
{
    use BreadRelationshipParser;
    //use BrowseDataTables;

    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        $browseRows = null;

        if (\auth()->user()->role->slug == 'customer') {
            //$browseRows = [ trans('columns.Number of request'), trans('columns.Name of service'), trans('columns.Name of responsible person'), trans('columns.Status of request'), trans('columns.Created At')];
            $browseRows = ['Number of request', 'Name of service', 'Name of responsible person', 'Status of request', 'Created At'];
        } else if (\auth()->user()->role->slug == 'product-manager') {
            $browseRows = ['Number of request', 'Name', 'Last Name', 'Applicant type', 'Status of request', 'Created At'];
        } else {
            $browseRows = $dataType->browseRows->pluck('display_name');
        }
        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];

        $searchNames = [];
        if ($dataType->server_side) {
            $searchable = SchemaManager::describeTable(app($dataType->model_name)->getTable())->pluck('name')->toArray();
            $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->get();
            foreach ($searchable as $key => $value) {
                $displayName = $dataRow->where('field', $value)->first()->getTranslatedAttribute('display_name');
                $searchNames[$value] = $displayName ?: ucwords(str_replace('_', ' ', $value));
            }
        }

        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', null);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $query = $model->{$dataType->scope}();
            } else {
                $query = $model::select('*');
            }

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model)) && Auth::user()->can('delete', app($dataType->model_name))) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query = $query->withTrashed();
                }
            }

            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value != '' && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        if (($isModelTranslatable = is_bread_translatable($model))) {
            $dataTypeContent->load('translations');
        }

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->default_search_key ?? null;

        // Actions
        $actions = [];
        if (!empty($dataTypeContent->first())) {
            foreach (Voyager::actions() as $action) {
                $action = new $action($dataType, $dataTypeContent->first());

                if ($action->shouldActionDisplayOnDataType()) {
                    $actions[] = $action;
                }
            }
        }

        // Define showCheckboxColumn
        $showCheckboxColumn = false;
        if (Auth::user()->can('delete', app($dataType->model_name))) {
            $showCheckboxColumn = true;
        } else {
            foreach ($actions as $action) {
                if (method_exists($action, 'massAction')) {
                    $showCheckboxColumn = true;
                }
            }
        }

        // Define orderColumn
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + ($showCheckboxColumn ? 1 : 0);
            $orderColumn = [[$index, 'desc']];
            if (!$sortOrder && isset($dataType->order_direction)) {
                $sortOrder = $dataType->order_direction;
                $orderColumn = [[$index, $dataType->order_direction]];
            } else {
                $orderColumn = [[$index, 'desc']];
            }
        }

        $role = \auth()->user()->role->slug;
        $productLists = Products::when($role, function ($productLists, $role) {
                                        if($role == 'supervisor'){
                                            $productLists->where('created_by', \auth()->id());
                                        }
                                        if($role == 'customer'){
                                            $productLists->whereHas('serviceInquiry', function ($productLists){
                                                $productLists->where('user_id', \auth()->id());
                                            });
                                        }
                                        if($role == 'product-manager'){
                                            $productLists->where('user_id', \auth()->id());
                                        }
                                  })->get()->translate('locale', app()->getLocale());

        $status_list = config('applicationStatus.serviceInquiries');

        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::$slug.browse")) {
            $view = "voyager::$slug.browse";
        }
        return Voyager::view($view, compact(
            'actions',
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchNames',
            'isServerSide',
            'defaultSearchKey',
            'usesSoftDeletes',
            'showSoftDeleted',
            'showCheckboxColumn',
            'productLists',
            'status_list',
            'browseRows'
        ));
    }

    public function create(Request $request)
    {
        $slug = $this->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? new $dataType->model_name()
            : false;

        foreach ($dataType->addRows as $key => $row) {
            $dataType->addRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    public function store(Request $request)
    {
        /*$validator = Validator::make($request->all(), \App\Models\ServiceInquiries::rules());

        if ($validator->fails()) {
            if ($validator->fails()) {
                return back()->withErrors($validator);
            }
        }*/
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->has('_validate'))
        {
            $destinationPath = public_path('storage/');
            $dir = 'news';

            if( is_dir($destinationPath . $dir) == false ){
                mkdir($destinationPath .$dir,0777);
            }

//            $filesystem = config('voyager.storage.disk');
//            $realPath = Storage::disk($filesystem)->getDriver()->getAdapter()->getPathPrefix();
//
//            $file = $request->image->store('/news', $filesystem);
//            $img = \Intervention\Image\Facades\Image::make($realPath.$file)->save($realPath.$file);
//            $img->resize(355, 265, function ($constraint) {
//                $constraint->aspectRatio();
//            })->save($realPath.$file);

            $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

//            $image = $request->file('image');
//            $input['imagename'] = time().'.'.$image->extension();


//            $img = \Intervention\Image\Facades\Image::make($image->path());
//            $img->resize(355, 265, function ($constraint) {
//                $constraint->aspectRatio();
//            })->save($destinationPath.'/'.$input['imagename']);
//
//            $image->move($destinationPath . $dir, $input['imagename']);

            // save author id
            $data->author_id = Auth::user()->id;
            $data->save();

            if ($request->ajax()) {
                return response()->json(['success' => true, 'data' => '']);
            }

            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                    'message'    => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }
    }

    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? app($dataType->model_name)->findOrFail($id)
            : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'id'));
    }

    public function update(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $request->id instanceof Model ? $request->id->{$request->id->getKeyName()} : $request->id;

        $data = call_user_func([$dataType->model_name, 'findOrFail'], $request->id);

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $request->id);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->has('_validate'))
        {
            if ($data->application_status != $request->application_status) {
                Mail::to($request->email)->send(new Notifymail($request->name, $data->application_status, $request->application_status));
            }

            $updateData = $this->insertUpdateData($request, $slug, $dataType->editRows, $data);
            if(isset($request->application_status)){
                $updateData->application_status = $request->application_status;
                $updateData->save();
            }

            $destinationPath = public_path('storage/');
            $dir = 'documents';
            if( is_dir($destinationPath . $dir) == false ){
                mkdir($destinationPath .$dir,0777);
            }

            if($request->hasfile('documents'))
            {
                $allowedFileExtension=['pdf','doc','docx'];

                try {

                    foreach($request->file('documents') as $image)
                    {
                        $extension = $image->getClientOriginalExtension();
                        $check = in_array($extension,$allowedFileExtension);
                        if($check)
                        {
                            $file = rand() . $image->getClientOriginalName();
                            $image->move(public_path('storage/'.$dir), $file);

                            $data = array('document' => $file, 'service_inquiries_id' => $updateData->id);
                            $serviceInquiriesDocumentsModel = new ServiceInquiriesDocuments();
                            $serviceInquiriesDocumentsModel->create([
                                'document' => $data['document'],
                                'service_inquiries_id' => $data['service_inquiries_id'],
                            ]);
                        }
                        else
                        {
                            return back()->with('error','Only PDF or MS WORD documents are accepted');
                        }
                    }

                } catch (\Exception $exception) {
                    Log::error('Document Issue backend' .$exception->getMessage());
                }
            }

            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                    'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }
    }

    public function show(Request $request,$id)
    {
        app()->setLocale(Session::get('locale'));
        $slug = $this->getSlug($request);
        $role = \auth()->user()->role->slug;

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check permission
        $this->authorize('read', app($dataType->model_name));

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.read';

        if (view()->exists("voyager::$slug.read")) {
            $view = "voyager::$slug.read";
        }

        try {
            $details = ServiceInquiries::where('id', $id)->with('product','inquiryCheckPoints','verifications')->first();
            if($role == 'customer'){
                 if($details->user_id != \auth()->user()->id){
                     return redirect()->route('voyager.service-inquiries.index')->with([
                         'message'    => trans('content.Unauthorized'),
                         'alert-type' => 'error',
                     ]);
                 }
            }

            if($role == 'product-manager'){
                if($details->product->user_id != Auth::user()->id){
                    return redirect()->route('voyager.service-inquiries.index')->with([
                        'message'    => trans('content.Unauthorized'),
                        'alert-type' => 'error',
                    ]);
                }
            }
            $activities = ServiceInquiryActivity::with('user')->where('service_inquiry_id',$id)->latest()->get()->map(function($item){
                $item['dateTime'] = $item->created_at->toDayDateTimeString();
                return $item;
            });

            $formData = json_decode($details->fields) ?? [];
            //ApplicationForms::where('id',1)->first()->translate(app()->getLocale());

            try{
                $productTitle = $details->product->translate('locale',app()->getLocale())->title;
            }catch (\Exception $e){
                $productTitle = $details->product->title;
            }

            $formHtml = '';
            $formHtml .= '<div id="formBuilderData">';
            foreach ($formData as $key => $data) {
//                dump($data);
                $formHtml .= '<div class="form-group">';
                if ($data->type == 'file') {
                    $fileName = ServiceInquiriesDocuments::where('service_inquiries_id', $details->id)->where('document', 'LIKE', $data->name.'%')->first();
                    if(isset($fileName->document)){
                        $customerFolder = $details->user->name .'_'. $details->user->id;
                        $serviceInquiryVerificationId = $details->id;
                        $docPath = 'Documents/' . $customerFolder . '/' . $serviceInquiryVerificationId . '/' . $fileName->document;

                        $formHtml .= '<div class="form-inline">';
                        $formHtml .= '<h4 class="float-left">' . $data->label . ' : <a target="_blank" href="'.url('customerAttachmentDownload'). '?path='.$docPath.'">' . $data->name .'</a> | <a download="" href="'.url('customerAttachmentDownload'). '?path='.$docPath.'"><i class="voyager-download" aria-hidden="true"></i></a></h4>';
                        $formHtml .= '</div>';
//                        $formHtml .= '<div class="form-inline">';
//                        $formHtml .=  '<input type="checkbox" name="document_send" id="document_send_'.$key.'">';
//                        $formHtml .=  '<label for="document_send_'.$key.'" class="cursor">'.$data->label.'</label>';
//                        $formHtml .= '</div>';
                    }
                } else if( $data->type == 'header' ) {
                    try{
                        $formHtml .= '<'.$data->subtype.'>' . $data->label .'</'.$data->subtype.'>';
                    } catch (\Exception $s){
                        continue;
                    }
                } else if( $data->type == 'paragraph' ) {
                    try{
                        $formHtml .= '<'.$data->subtype.'>' . $data->label .'</'.$data->subtype.'>';
                    } catch (\Exception $s){
                        continue;
                    }
                } else if( $data->type == 'hidden' || $data->type == 'button') {
                }else {
                    try{
                        $label = isset($data->label) ? $data->label . ' : ' : null ;
                        $formHtml .= '<h4>' . $label . implode(', ', $data->userData) . '</h4>';
                    } catch (\Exception $s){
                        continue;
                    }
                }
                //$formHtml .= '<h4>'..'</h4>';
//                dump($formHtml);
                $formHtml .= '</div>';
            }
//            dd();

            if(\auth()->user()->role->slug != 'customer') {
                //$details->status_of_request
                $formHtml .= '<div id="checkPoints">';
                $disabled = auth()->user()->role->slug == 'customer' ? 'disabled="true"' : null;
                foreach ($details->inquiryCheckPoints as $inquiryCheckPoint) {
                    if($inquiryCheckPoint->application_status == $details->status_of_request){
                        $checked = $inquiryCheckPoint->status ? 'checked' : null;
                        if($checked){
                            $formHtml .= '<div class="checkbox form-inline">
                                  <label><input type="checkbox" value="" checked '.$disabled.' class="checkPoint" data-id="'.$inquiryCheckPoint->id.'"><div class="lineThrough">'.$inquiryCheckPoint->check_point_name.'</div></label>
                                  <input type="text" class="checkPointComment form-control" placeholder="'.trans('content.any_comment').'" value="'.$inquiryCheckPoint->comment.'" data-id="'.$inquiryCheckPoint->id.'">
                                </div>';
                        } else{
                            $formHtml .= '<div class="checkbox form-inline">
                                  <label><input type="checkbox" value="" '.$checked.' '.$disabled.' class="checkPoint" data-id="'.$inquiryCheckPoint->id.'"><div>'.$inquiryCheckPoint->check_point_name.'</div></label>
                                  <input type="text" class="checkPointComment form-control" placeholder="'.trans('content.any_comment').'" value="'.$inquiryCheckPoint->comment.'" data-id="'.$inquiryCheckPoint->id.'">
                                </div>';
                        }
                    }
                }
                $formHtml .= '</div>';
            }
            $formHtml .= '</div>';

            $activityHtml = '<div class="form-group">';
            if ($details->product->is_verification_required) {
                if(\auth()->user()->role->slug != 'customer') {
                    $activityHtml .= '<div class="form-inline"><label>Invite Users For Verification </label> <br> ';
                    foreach ($formData as $key => $data) {
                        if ($data->type == 'file') {
                            $fileName = ServiceInquiriesDocuments::where('service_inquiries_id', $details->id)->where('document', 'LIKE', $data->name.'%')->first();
                            if(isset($fileName->document)){
                                $activityHtml .= '<div class="col-md-12">';
                                    $activityHtml .=  '<input type="checkbox" name="document_send[]" id="document_send_'.$key.'" value="'.$fileName->document.','.urlencode( $data->label).'"> ';
                                $activityHtml .=  '<label for="document_send_'.$key.'" class="cursor">'.$data->label.'</label>';
                                $activityHtml .= '</div>';
                            }
                        }
                    }
                    $activityHtml .= '<input type="email" class="form-control inviteUsers" value="" style="width : 95%"> <button class="btn btn-success inviteButton" data-id="' . $details->id . '"> Invite</button></div>';
                }
            }

            $activityHtml .= '<br><b style="font-weight: bold;">' . trans('content.activityList') . '</b>';
            $activityHtml .= '<div id="activityList">';
            if ($activities->count() == 0) {
                $activityHtml .= '<br><div style="text-align: center"><p>' . trans('content.noActivity') . '</p></div>';
            } else {
                foreach ($activities as $activity) {
                    if ($activity->activity_type == ServiceInquiryActivity::CHANGE_STATUS) {
                        if(\auth()->user()->role->slug != 'customer') {
                            $details = json_decode($activity->message);
                            $msg = '<b style="font-weight: bold;">' . $activity->user->name . ' - </b>' . trans('content.moveThisReq', ['before' => trans('content.' . $details->beforeStatus), 'after' => trans('content.' . $details->afterStatus)]);
                            $activityHtml .= '<div class="inquirySend col-md-12" style="cursor: default;"><p class="float-left text_align_m">' . $msg . '</p><br><p class="float-right">' . $activity->dateTime . '</p></div>';
                        }
                    } else if ($activity->activity_type == ServiceInquiryActivity::CHECKPOINT_COMMENT) {
                        if(\auth()->user()->role->slug != 'customer'){
                            $details = (Array)json_decode($activity->message);
                            //dd($details);
                            $msg = '<b style="font-weight: bold;">' . $activity->user->name . ' - </b>' . trans('content.checkpointComment', $details);
                            $activityHtml .= '<div class="inquirySend col-md-12" style="cursor: default;"><p class="float-left text_align_m">' . $msg . '</p><br><p class="float-right">' . $activity->created_at->toDayDateTimeString() . '</p></div>';
                        }
                    } else if ($activity->activity_type == ServiceInquiryActivity::ACTIVITY_VERIFICATION) {
                        if(\auth()->user()->role->slug != 'customer'){
                            $details = json_decode($activity->message);
                            $status = $details->is_verified ? trans('content.Accepted') : trans('content.Rejected') ;
                            $statusColour = $details->is_verified ? 'statusAccept' : 'statusReject';
                            //dd($details);
                            $msg = '<b style="font-weight: bold;">' . $details->email . ' - </b>' . trans('content.verificationMessage', ['status' => $status]);
                            if(!empty($details->comment)){
                                $msg .= trans('content.andAddedComment',['comment' => $details->comment]);
                            }

                            $activityHtml .= '<div class="inquirySend col-md-12 '.$statusColour.'" style="cursor: default;"><p class="float-left text_align_m">' . $msg . '</p>';
                            if(isset($details->documents)){
                                foreach (json_decode($details->documents) as $item) {
                                    $activityHtml .= '<a class="float-left" style="margin-left: 10px;" href="'.asset('storage/'.$item).'" target="_blank"><img style="width: 32px; background: white; padding: 6px; border-radius: 6px;" src="'.asset('images/doc-thumbnail.svg').'"></a>';
                                }
                            }
                            $activityHtml .= '<br><p class="float-right">' . Carbon::parse($details->action_taken_at)->toDayDateTimeString() . '</p></div>';
                        }
                    }  else if ($activity->activity_type == ServiceInquiryActivity::INVITE_FOR_VERIFICATION) {
                        if(\auth()->user()->role->slug != 'customer'){
                            $details = json_decode($activity->message);
                            $serviceInquiryVerification = ServiceInquiryVerification::where('id', $details->verificationId)->first();
                            $documents = implode(', ',array_keys((Array)json_decode($serviceInquiryVerification->path)));
                            $s = '<b style="font-weight: bold;"> ' . urldecode($documents) . ' </b>';
                            $msg = '<b style="font-weight: bold;">' . $activity->user->name . ' - </b>' . trans('content.invitedUsers', ['email' => $serviceInquiryVerification->email, 'document' => $s]);
                            $activityHtml .= '<div class="inquirySend col-md-12" style="cursor: default;"><p class="float-left text_align_m">' . $msg . '</p><br><p class="float-right">' . $activity->created_at->toDayDateTimeString() . '</p></div>';
                        }
                    } else if ($activity->activity_type == ServiceInquiryActivity::CHECKPOINT_MARK) {
                        if(\auth()->user()->role->slug != 'customer'){
                            $details = json_decode($activity->message);
                            $msg = '<b style="font-weight: bold;">' . $activity->user->name . ' - </b>' .trans('content.markedAs', ['label' => $details->checkPointLabel, 'checked' => $details->checkPointStatus ? trans('content.checked') : trans('content.unchecked')]);
                            $activityHtml .= '<div class="inquirySend col-md-12" style="cursor: default;"><p class="float-left text_align_m">' . $msg . '</p><br><p class="float-right">' . $activity->dateTime . '</p></div>';
                        }
                    } else if ($activity->activity_type == ServiceInquiryActivity::CUSTOMER_FILES) {
                            try{
                                $DocFiles = @unserialize($activity->requested_param);
                                if($DocFiles != false){
                                    $msg = '<b style="font-weight: bold;">' .$activity->user->name . '-'.' </b> '.$activity->message;

                                    $activityHtml .= '<div class="inquirySend col-md-12 " style="cursor: default;"><p class="float-left text_align_m">' . $msg . '</p>';
                                    if(isset($activity->requested_param)){
                                        foreach ($DocFiles as $DocFile){
                                            $activityHtml .= '<a class="float-left" style="margin-left: 10px;" href="'.asset('storage/'.$DocFile).'" target="_blank"><img style="width: 32px; background: white; padding: 6px; border-radius: 6px;" src="'.asset('images/doc-thumbnail.svg').'"></a>';
                                        }
                                    }
                                    $activityHtml .= '<br><p class="float-right">' . Carbon::parse($activity->created_at)->toDayDateTimeString() . '</p></div>';
                                }
                            }catch (\Exception $e){
                            }
                    } else {
                        $activityHtml .= '<div class="inquirySend col-md-12" style="cursor: default; background-color: wheat;"><p class="float-left text_align_m"><b style="font-weight: bold;">' . $activity->user->name . ' - </b> ' . $activity->message . '</p><br><p class="float-right">' . $activity->dateTime . '</p></div>';
                    }
                }
            }
            $activityHtml .= '</div>';
            $activityHtml .= '</div>';
            return Voyager::view($view, compact(
                'dataType', 'dataTypeContent', 'isModelTranslatable', 'activityHtml', 'productTitle', 'formHtml', 'id'
            ));

//            return response(['status' => true, 'message' => 'Done', 'activityHtml' => $activityHtml, 'productTitle' => $productTitle, 'formHtml' => $formHtml], 200);
        } catch (\Exception $e) {
//            return response(['status' => false, 'message' => $e->getMessage()], 203);
            return Voyager::view($view, compact(
                'dataType', 'dataTypeContent', 'isModelTranslatable'
            ));
        }
//        return Voyager::view($view, compact(
//            'dataType', 'dataTypeContent', 'isModelTranslatable'
//        ));
    }

    public function destroy(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        // Init array of IDs
        $ids = [];
        $relatednewsIds = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }

        foreach ($ids as $id) {
            $relatedNewsData = ServiceInquiries::where('id', '=',$id)->first();
            if ($relatedNewsData != null) {
                $relatednewsIds[] = $relatedNewsData->id;
            }
        }

        $displayName = count($ids) > 1 ? "These ".$dataType->display_name_plural : "This ".$dataType->display_name_singular;

        if (count($relatednewsIds) == 0) {
            $relatedNews = [];

            foreach ($relatednewsIds as $id) {
                $data = ServiceInquiries::where('id', $id)->first();
                $relatedNews[] = $data->name;
            }

            $data = [
                'message'    => "You cannot Delete "."{$displayName}",
                'alert-type' => 'error',
            ];
        }
        else {
            foreach ($ids as $id) {
                $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
                $this->cleanup($dataType, $data);
            }

            $res = $data->destroy($ids);
            $data = $res
                ? [
                    'message'    => __('voyager::generic.successfully_deleted')." {$displayName}",
                    'alert-type' => 'success',
                ]
                : [
                    'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                    'alert-type' => 'error',
                ];


        }

        return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
    }

    protected function cleanup($dataType, $data)
    {
        // Delete Translations, if present
        if (is_bread_translatable($data)) {
            $data->deleteAttributeTranslations($data->getTranslatableAttributes());
        }

        // Delete Images
        $this->deleteBreadImages($data, $dataType->deleteRows->where('type', 'image'));

        // Delete Files
        foreach ($dataType->deleteRows->where('type', 'file') as $row) {
            if (isset($data->{$row->field})) {
                foreach (json_decode($data->{$row->field}) as $file) {
                    $this->deleteFileIfExists($file->download_link);
                }
            }
        }
    }

    public function deleteBreadImages($data, $rows)
    {
        foreach ($rows as $row) {
            if ($data->{$row->field} != config('voyager.user.default_avatar')) {
                $this->deleteFileIfExists($data->{$row->field});
            }

            if (isset($row->details->thumbnails)) {
                foreach ($row->details->thumbnails as $thumbnail) {
                    $ext = explode('.', $data->{$row->field});
                    $extension = '.'.$ext[count($ext) - 1];

                    $path = str_replace($extension, '', $data->{$row->field});

                    $thumb_name = $thumbnail->name;

                    $this->deleteFileIfExists($path.'-'.$thumb_name.$extension);
                }
            }
        }

        if ($rows->count() > 0) {
            event(new BreadImagesDeleted($data, $rows));
        }
    }

    public function browse(Request $request){
        app()->setLocale($request->lang);
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            $role = \auth()->user()->role->slug;
            $customer_id = $request->customer_id;
            $product_id = $request->product_id;
            $user_id = $request->user_id;
            $request_id = $request->request_id;
            $status_of_request = $request->status_of_request;

            //$query = $model::select('id','locale','due_date','application_form_id','user_id','product_id','created_at','updated_at','request_id','status_of_request')

            $query = $model::select('*')
                ->when($product_id, function ($query) use($product_id) {
                    if($product_id != ''){
                        $query->where('product_id',$product_id);
                    }
                })
                ->when($request_id, function ($query) use($request_id) {
                    if($request_id != ''){
                        $query->where('request_id','LIKE','%'.$request_id.'%');
                    }
                })
                ->when($user_id, function ($query) use($user_id){
                    $query->whereHas('user',function($query) use($user_id){
                        return $query->where('name','LIKE','%'.$user_id.'%')
                                     ->orWhere('last_name','LIKE','%'.$user_id.'%');
                    });
                })
                ->when($status_of_request, function ($query) use($status_of_request) {
                    if($status_of_request != ''){
                        $query->where('status_of_request',$status_of_request);
                    }
                })
                ->when($customer_id, function ($query) use($customer_id) {
                    if($customer_id != ''){
                        $query->where('user_id',$customer_id);
                    }
                })
                ->when($role, function ($query, $role) {
                    if($role == 'customer'){
                        $query->where('user_id',\auth()->user()->id);
                    }
                    if($role == 'product-manager'){
                        $query->whereHas('product',function($query){
                            return $query->where('user_id', \auth()->id());
                        });
                    }
//                    if($role == 'supervisor'){
//                        $query->whereHas('product',function($query){
//                            return $query->where('created_by', \auth()->id());
//                        });
//                    }
                })
                ->with('product','user')->orderBy('id','desc');

            $s = [];

            $statusList = \auth()->user()->role->slug == 'customer' ? config('applicationStatus.serviceInquiriesCustomer') : config('applicationStatus.serviceInquiries');
            foreach ($statusList as $item) {
                $s[$item] = [
                    "id" => $item,
                    "title" => trans("content.".$item),
                    "class" => $this->getKanbanColour($item),
                    "item" => []
                ];
            }

            if(session()->get('view_inquiries_view') != 1 || auth()->user()->role->slug == 'customer') {

                $status_list = config('applicationStatus.serviceInquiries');
//                $status_list = $query->get()->groupBy('status_of_request')->map(function ($item , $key){
//                    return $key;
//                });

                $table = Datatables::of($query->orderBy('id','desc'));

                $table = $table
                    ->addColumn('title', function($query){
                        try {
                            $title = $query->product->translate('locale',app()->getLocale())->title;
                            return $title;
                        }catch (\Exception $e){
                            Log::error('Product ID'. $query->product->id .'  '.$e->getMessage());
                            return $query->product->title;
                        }
                    })
                    ->addColumn('name', function($query){
                        return $query->user->name .' '. $query->user->last_name;
                    })
                    ->addColumn('ticket_number', function($query){
                        return strtoupper($query->request_id);
                    })
                    ->addColumn('language', function($query){
                        return strtoupper($query->locale);
                    })
                    ->addColumn('due', function($query){
                        $diff = $query->due_date != null ? Carbon::parse($query->due_date)->diffInDays(Carbon::now(),false) : 0;
                        return $diff;
//                        $html = '';
//                        if (\auth()->user()->role->slug == 'customer') {
//                            $html .= '<p style="float: left;color: blue"><b style="padding: 3px 6px 3px 6px;border-radius: 10px;" class="'.$this->getKanbanColour($query->status_of_request).'">' . trans('content.appSendSince', ['day' => $query->created_at->diffInDays(Carbon::now(),false)]) . '</b></p>';
//                        } else {
//                            if ($diff > 0) {
//                                if ($diff == 0) {
//                                    $html .= '<p style="float: left;color: red"><b style="padding: 3px 6px 3px 6px;border-radius: 10px;" class="'.$this->getKanbanColour($query->status_of_request).'">' .trans('content.overdueToday', ['day' => $diff]) . '</b></p>';
//                                } else {
//                                    $html .= '<p style="float: left;color: red"><b style="padding: 3px 6px 3px 6px;border-radius: 10px;" class="'.$this->getKanbanColour($query->status_of_request).'">' .trans('content.overdueInDay', ['day' => $diff]) . '</b></p>';
//                                }
//                            } else {
//                                $html .= '<p style="float: left;color: blue"><b style="padding: 3px 6px 3px 6px;border-radius: 10px;" class="'.$this->getKanbanColour($query->status_of_request).'">' .trans('content.dueInDay', ['day' => abs($diff)]) . '</b></p>';
//                            }
//                        }
//                        return $html;
                    })
                    ->addColumn('action', function($query) use ($status_list){
                        $html = '';
                        $html .= '<select class="changeStatus" data-id="'.$query->id.'">';
                        $html .= '<option value="">select</option>';
                        foreach ($status_list as $status_lis){
                            if($status_lis != ''){
                                if($query->status_of_request == $status_lis){
                                    $html .= '<option value="'.$status_lis.'" selected>'.trans('content.'.$status_lis).'</option>';
                                }else{
                                    $html .= '<option value="'.$status_lis.'">'.trans('content.'.$status_lis).'</option>';
                                }
                            }
                        }
                        $html .= '</select>';
                        return $html;
                    })
                    ->addColumn('model_detail', function($query){
                        $html = '<a href="'.url('admin/service-inquiries/').'/'.$query->id.'" style="cursor: pointer" data-id="' . $query->id . '"> '.trans('content.details').' </a>';
                        if(auth()->user()->role->slug == 'customer' && $query->status_of_request == ServiceInquiries::DRAFT) {
                            $html .= '<a onclick=window.location.href="'.route('voyager.service-inquiries.draftApply',['id' => $query->id]).'" style="font-weight: 900;cursor: pointer;margin-left: 10px;">'.trans('content.edit').'</a>';
                        }
                        return $html;
                    })
                    ->addColumn('current_status', function($query){
                        $status = Helper::getCustomerStatus($query->status_of_request);
                        return trans('content.'.$status);
                    })
                    ->rawColumns(['title', 'name', 'language', 'due', 'model_detail', 'action', 'current_status']);
                return $table->make(true);
            }


            $browse = $query->get()->groupBy('status_of_request')->map(function ($item , $key) use ($role) {
                $data = $item->map(function ($value) {
                    $html = '';
                    try {
                        $html .= '<p style="font-weight: bold;font-size: 11px;">' . $value->product->translate('locale', app()->getLocale())->title . '</p>';
                    } catch (\Exception $e) {
                    }
                    $html .= '<br>';
                    $diff = $value->due_date != null ? Carbon::parse($value->due_date)->diffInDays(Carbon::now(),false) : 0;
                    $html .= '<div class="form-group" style="margin-top: -1em; padding: 0">';
                    if (\auth()->user()->role->slug == 'customer') {
                        $html .= '<p style="float: right;color: blue"><b style="padding: 3px 6px 3px 6px;border-radius: 10px;" class="'.$this->getKanbanColour($value->status_of_request).'">' .$value->created_at->diffInDays(Carbon::now(),false) /* trans('content.appSendSince', ['day' => $value->created_at->diffInDays(Carbon::now(),false)])*/ . '</b></p>';
                    } else {
                        if ($diff > 0) {
                            if ($diff == 0) {
                                $html .= '<p style="float: right;color: red"><b style="padding: 3px 6px 3px 6px;border-radius: 10px;" class="'.$this->getKanbanColour($value->status_of_request).'">' .$diff /*trans('content.overdueToday', ['day' => $diff])*/ . '</b></p>';
                            } else {
                                $html .= '<p style="float: right;color: red"><b style="padding: 3px 6px 3px 6px;border-radius: 10px;" class="'.$this->getKanbanColour($value->status_of_request).'">' .$diff /*trans('content.overdueInDay', ['day' => $diff])*/ . '</b></p>';
                            }
                        } else {
                            $html .= '<p style="float: right;color: blue"><b style="padding: 3px 6px 3px 6px;border-radius: 10px;" class="'.$this->getKanbanColour($value->status_of_request).'">' .$diff  /*trans('content.dueInDay', ['day' => abs($diff)])*/ . '</b></p>';
                        }
                    }
                    $html .= '<p style=""><img style="border-radius: 50%;float: left;" src="'.asset('/storage/'.$value->user->avatar).'" width="25"></p>';
                    $html .= '<p style="float:left;margin-left: 8px;">'.$value->user->name.' '.$value->user->last_name.'</p>';
                    $html .= '</div>';
                    $html .= '<div class="form-group" style="margin: -1em 0 -1em 0; padding: 0">';

                    if($value->status_of_request === ServiceInquiries::DRAFT){
                        $html .= '<p style="float: left;color: #8f9192"><span style="font-style: italic;">'.trans('content.ticket').' #'.strtoupper($value->request_id).'</span></p>';
                        $html .= '<p style="color: #FF5722;float: right;"><b style="padding: 3px 6px 3px 6px;border-radius: 10px;background: #f4f5f7;font-weight: bold;font-size: 12px;color: #172b4d;">'.strtoupper($value->locale).'</b>';
                        if(auth()->user()->role->slug == 'customer'){
                            $html .= '<a style="font-weight: 900;cursor: pointer;margin-left: 5px;">|</a><a onclick=window.location.href="'.route('voyager.service-inquiries.draftApply',['id' => $value->id]).'" style="font-weight: 900;cursor: pointer;margin-left: 10px;">'.trans('content.edit').'</a>';
                        }
                        $html .= '</p>';
                    }else{
                        $html .= '<p style="float: left;color: #8f9192"><span style="font-style: italic;">'.trans('content.ticket').' #'.strtoupper($value->request_id).'</span></p>';
                        $html .= '<p style="color: #FF5722;float: right;"><b style="border-radius: 50%;background: #f4f5f7;font-weight:bold;font-size: 12px;padding: 5px;color: #172b4d;">'.strtoupper($value->locale).'</b></p>';
                    }


                    $html .= '</div>';
                    $html .= '';
                    return [
                        'id' => $value->id,
                        "title" => $html
                    ];
                })->toArray();

                if($role == 'product-manager'){
                    return [
                        //"id" => !empty($key) ? str_slug($key) : 'default',
                        "id" => !empty($key) ? $key : 'Default',
                        "title" => !empty($key) ? trans("content.".$key) : 'Default',
                        "class" => $this->getKanbanColour($key),
                        //"dragTo" => [],
                        "item" => $data,
                    ];
                } else {
                    return [
                        //"id" => !empty($key) ? str_slug($key) : 'default',
                        "id" => !empty($key) ? $key : 'Default',
                        "title" => !empty($key) ? trans("content.".$key) : 'Default',
                        "class" => $this->getKanbanColour($key),
                        "dragTo" => [],
                        "item" => $data,
                    ];
                }
            })->toArray();

            $final = [];
            foreach ($s as $key => $item) {
                if ($key == ServiceInquiries::ON_CONTROL) {
                    if(\auth()->user()->role->slug == 'customer'){
                        if(isset($browse[ServiceInquiries::CONTROL])){
                            $s[$key]['item'] = array_merge($s[$key]['item'], $browse[ServiceInquiries::CONTROL]['item']);
                        }
                        $final[$key] = $s[$key];
                    }else{
                        $final[$key] = isset($browse[$key]) ? $browse[$key] : $s[$key];
                    }
                } else if ($key == 'Applied') {
                    if(\auth()->user()->role->slug == 'customer'){
                        if(isset($browse[ServiceInquiries::APPLICATION_RECEIVED])){
                            $s[$key]['item'] = array_merge($s[$key]['item'], $browse[ServiceInquiries::APPLICATION_RECEIVED]['item']);
                        }
                        $final[$key] = $s[$key];
                    }else{
                        $final[$key] = isset($browse[$key]) ? $browse[$key] : $s[$key];
                    }
                } else {
                    $final[$key] = isset($browse[$key]) ? $browse[$key] : $s[$key];
                }
            }
            $browse = array_values($final);
            return response(['data' => $browse],200);
        } else {
            abort(404);
        }
    }

    public function getKanbanColour($status_of_request)
    {
        $color = null;
        switch ($status_of_request) {
            case ServiceInquiries::APPLICATION_RECEIVED:
            case ServiceInquiries::APPLIED:
            case ServiceInquiries::IN_PROCESS:
                $color = 'info';
                break;
            case ServiceInquiries::ON_CONTROL:
            case ServiceInquiries::CONTROL:
                $color = 'primary';
                break;
            case ServiceInquiries::DATA_DOCUMENTS_ARE_MISSING:
                $color = 'warning';
                break;
            case ServiceInquiries::ACCEPTED:
                $color = 'success';
                break;
            case ServiceInquiries::REJECTED:
                $color = 'error';
                break;
            default:
                $color = 'all_lable';
        }
        return $color;
    }

    public function ajaxStatusUpdate(Request $request)
    {
        //$html = null;
        app()->setLocale($request->lang);
        try {
            $query = ServiceInquiries::where('id', $request->data_id)->with('inquiryCheckPoints','user')->first();
            $verificationStatus = ServiceInquiryVerification::where('service_inquiry_id',$query->id)->where('inquiry_status',$query->status_of_request)->get();
            if($verificationStatus->count() != $verificationStatus->sum('is_verified')){
                return response(['status' => false, 'message' => trans('toaster.third party verification is pending') ], 203);
            }
            $serviceInquiryCheckPoint = ServiceInquiryCheckPoint::where('service_inquiry_id',$request->data_id)->where('application_status',$query->status_of_request)->get();
            $count = $serviceInquiryCheckPoint->count();
            $sum = $serviceInquiryCheckPoint->sum('status');
            if($count == $sum){
                $beforeStatus = $query->status_of_request;
                $query->status_of_request = $request->status;
                $status = $query->save();
                if ($status) {
                    $serviceInquiryActivity = ServiceInquiryActivity::create([
                        'service_inquiry_id' => $request->data_id,
                        'user_id' => \auth()->id(),
                        'activity_type' => ServiceInquiryActivity::CHANGE_STATUS,
                        'message' => json_encode(['beforeStatus' => $beforeStatus, 'afterStatus' => $request->status]),
                        'requested_param' => serialize($request->all()),
                    ]);
                    SendNotificationJob::dispatch($serviceInquiryActivity->id, $query->locale);
//                    SendNotificationJob::dispatch($serviceInquiryActivity->id, $request->lang);
//                    Mail::to($query->user->email)->send(new StatusChangeMail($serviceInquiryActivity));
                }
                return response(['status' => true, 'message' => 'Done'], 200);
            } else {
                return response(['status' => false, 'message' => trans('content.completeCheckpointFirst') ], 203);
            }
        } catch (\Exception $e) {
            return response(['status' => false, 'message' => $e->getMessage()], 203);
        }
        //$html = '<span class="label '.$this->getLabelColour($query->status_of_request).' getDropDown" data-id="'.$query->id.'" style="cursor:pointer">'.$query->status_of_request.'</span><div id="dro-id-'.$query->id.'"></div>';
//        return response(['status' => true, 'labels' => $html], 200);
    }

    public function ajaxDetailedRequest(Request $request)
    {
        try {
            app()->setLocale($request->lang);
//            dd(app()->getLocale());
            $details = ServiceInquiries::where('id', $request->data_id)->with('product','inquiryCheckPoints','verifications')->first();
            $activities = ServiceInquiryActivity::with('user')->where('service_inquiry_id',$request->data_id)->latest()->get()->map(function($item){
                $item['dateTime'] = $item->created_at->toDayDateTimeString();
                return $item;
            });

            $formData = json_decode($details->fields) ?? [];
            //ApplicationForms::where('id',1)->first()->translate(app()->getLocale());

            $productTitle = $details->product->title;

            $formHtml = '';
            $formHtml .= '<div id="formBuilderData">';
            foreach ($formData as $data) {
//                dump($data);
                $formHtml .= '<div class="form-group">';
                if ($data->type == 'file') {
                    $fileName = ServiceInquiriesDocuments::where('service_inquiries_id', $details->id)->where('document', 'LIKE', $data->name.'%')->first();
                    if(isset($fileName->document)){
                        $formHtml .= '<h4 style="font-size: 14px;">' . $data->label . ' : <a target="_blank" href="'.asset('storage/documents/'.$fileName->document).'">' . $data->name .'</a> | <a download="" href="'.asset('storage/documents/'.$fileName->document).'"><i class="voyager-download" aria-hidden="true"></i></a></h4>';
                    }
                } else if( $data->type == 'paragraph' || $data->type == 'header' || $data->type == 'hidden' || $data->type == 'button') {
                }else {
                    try{
                        $formHtml .= '<h4 style="font-size: 14px;">' . $data->label . ' : ' . implode(', ', $data->userData) . '</h4>';
                    } catch (\Exception $s){
                        continue;
                    }
                }
                //$formHtml .= '<h4>'..'</h4>';
//                dump($formHtml);
                $formHtml .= '</div>';
            }
//            dd();

            if(\auth()->user()->role->slug != 'customer') {
                //$details->status_of_request
                $formHtml .= '<div id="checkPoints">';
                $disabled = auth()->user()->role->slug == 'customer' ? 'disabled="true"' : null;
                foreach ($details->inquiryCheckPoints as $inquiryCheckPoint) {
                    if($inquiryCheckPoint->application_status == $details->status_of_request){
                        $checked = $inquiryCheckPoint->status ? 'checked' : null;
                        if($checked){
                            $formHtml .= '<div class="checkbox form-inline">
                                  <label><input type="checkbox" value="" checked '.$disabled.' class="checkPoint" data-id="'.$inquiryCheckPoint->id.'"><div class="lineThrough">'.$inquiryCheckPoint->check_point_name.'</div></label>
                                  <input type="text" class="checkPointComment form-control" placeholder="'.trans('content.any_comment').'" value="'.$inquiryCheckPoint->comment.'" data-id="'.$inquiryCheckPoint->id.'">
                                </div>';
                        } else{
                            $formHtml .= '<div class="checkbox form-inline">
                                  <label><input type="checkbox" value="" '.$checked.' '.$disabled.' class="checkPoint" data-id="'.$inquiryCheckPoint->id.'"><div>'.$inquiryCheckPoint->check_point_name.'</div></label>
                                  <input type="text" class="checkPointComment form-control" placeholder="'.trans('content.any_comment').'" value="'.$inquiryCheckPoint->comment.'" data-id="'.$inquiryCheckPoint->id.'">
                                </div>';
                        }
                    }
                }
                $formHtml .= '</div>';
            }
            $formHtml .= '</div>';

            $activityHtml = '<div class="form-group">';
            if ($details->product->is_verification_required) {
                if(\auth()->user()->role->slug != 'customer') {
                    $activityHtml .= '<div class="form-inline"><label>Invite Users For Verification </label> : <input type="email" class="form-control inviteUsers" id="tokenfield" value="" style="width : 55%"> <button class="btn btn-success inviteButton" data-id="' . $details->id . '"> Invite</button></div>';
                    //dd($details->verifications);
                    //$activityHtml .= '<p></p>';
                }
            }

            $activityHtml .= '<br><b style="font-weight: bold;">' . trans('content.activityList') . '</b>';
            $activityHtml .= '<div id="activityList">';
            if ($activities->count() == 0) {
                $activityHtml .= '<br><div style="text-align: center"><p>' . trans('content.noActivity') . '</p></div>';
            } else {
                foreach ($activities as $activity) {
                    if ($activity->activity_type == ServiceInquiryActivity::CHANGE_STATUS) {
                        if(\auth()->user()->role->slug != 'customer') {
                            $details = json_decode($activity->message);
                            $msg = '<b style="font-weight: bold;">' . $activity->user->name . ' - </b>' . trans('content.moveThisReq', ['before' => trans('content.' . $details->beforeStatus), 'after' => trans('content.' . $details->afterStatus)]);
                            $activityHtml .= '<div class="inquirySend col-md-12" style="cursor: default;"><p class="float-left text_align_m">' . $msg . '</p><br><p class="float-right">' . $activity->dateTime . '</p></div>';
                        }
                    } else if ($activity->activity_type == ServiceInquiryActivity::CHECKPOINT_COMMENT) {
                        if(\auth()->user()->role->slug != 'customer'){
                            $details = (Array)json_decode($activity->message);
                            //dd($details);
                            $msg = '<b style="font-weight: bold;">' . $activity->user->name . ' - </b>' . trans('content.checkpointComment', $details);
                            $activityHtml .= '<div class="inquirySend col-md-12" style="cursor: default;"><p class="float-left text_align_m">' . $msg . '</p><br><p class="float-right">' . $activity->created_at->toDayDateTimeString() . '</p></div>';
                        }
                    } else if ($activity->activity_type == ServiceInquiryActivity::CHECKPOINT_MARK) {
                        if(\auth()->user()->role->slug != 'customer'){
                            $details = json_decode($activity->message);
                            $msg = '<b style="font-weight: bold;">' . $activity->user->name . ' - </b>' .trans('content.markedAs', ['label' => $details->checkPointLabel, 'checked' => $details->checkPointStatus ? trans('content.checked') : trans('content.unchecked')]);
                            $activityHtml .= '<div class="inquirySend col-md-12" style="cursor: default;"><p class="float-left text_align_m">' . $msg . '</p><br><p class="float-right">' . $activity->dateTime . '</p></div>';
                        }
                    } else {
                        $activityHtml .= '<div class="inquirySend col-md-12" style="cursor: default; background-color: wheat;"><p class="float-left text_align_m"><b style="font-weight: bold;">' . $activity->user->name . ' - </b> ' . $activity->message . '</p><br><p class="float-right">' . $activity->dateTime . '</p></div>';
                    }
                }
            }
            $activityHtml .= '</div>';
            $activityHtml .= '</div>';
            return response(['status' => true, 'message' => 'Done', 'activityHtml' => $activityHtml, 'productTitle' => $productTitle, 'formHtml' => $formHtml], 200);
        } catch (\Exception $e) {
            return response(['status' => false, 'message' => $e->getMessage()], 203);
        }
    }

    public function ajaxMessageActivity(Request $request)
    {
        try {
            if(empty($request->message)){
                return response(['status' => false, 'message' => 'Message field is null'], 200);
            }
            $activity = ServiceInquiryActivity::create([
                'service_inquiry_id' => $request->data_id,
                'user_id' => \auth()->id(),
                'activity_type' => ServiceInquiryActivity::TEXT_MESSAGE,
                'message' => $request->message,
                'requested_param' => serialize($request->all()),
            ]);
            $html = '<div class="comment col-md-12"><p class="float-left text_align_m"><b style="font-weight: bold;">'.$activity->user->name.' - </b> '.$activity->message.'</p><br><p class="float-right">'.$activity->created_at->toDayDateTimeString().'</p></div>';
            return response(['status' => true, 'message' => 'Done', 'html' => $html], 200);
        } catch (\Exception $e) {
            return response(['status' => false, 'message' => $e->getMessage()], 203);
        }
    }

    public function draftApply($id)
    {
        app()->setLocale(\Illuminate\Support\Facades\Session::get('locale'));
        $serviceInquiryDetails = ServiceInquiries::find($id)->translate('locale', app()->getLocale());
        $product = Products::find($serviceInquiryDetails->product_id)->translate('locale', app()->getLocale());
        return view('frontend.serviceRequest.draftApply', ['product' => $product, 'serviceInquiryDetails' => $serviceInquiryDetails]);
    }

    public function exportToPdf(Request $request)
    {
        if(!$serviceInquiry = ServiceInquiries::find($request->id)){
            return response()->json([
                'status' => false
            ]);
        }

        if($request->type == "csv"){
            return \Excel::download(new Export($serviceInquiry), 'service-request-'.$serviceInquiry->id.'.xlsx');
        }

        if($request->type == "pdf"){
            \PDF::setOptions(['dpi' => 150, 'defaultFont' => 'ru']);
            $pdf = \PDF::loadView('vendor.voyager.service-inquiries.pdf', ['serviceInquiry' => $serviceInquiry]);
            return $pdf->download('service-request-'.$serviceInquiry->id.'.pdf');
        }
        return true;
    }

    public function checkPointStatus(Request $request)
    {
        try{
            app()->setLocale($request->lang);
            $serviceCheckpoint = ServiceInquiryCheckPoint::where('id',$request->data_id)->first();
            $serviceCheckpoint->status = $request->status;
            $serviceCheckpoint->save();
            $activity = ServiceInquiryActivity::create([
                'service_inquiry_id' => $serviceCheckpoint->service_inquiry_id,
                'user_id' => \auth()->id(),
                'activity_type' => ServiceInquiryActivity::CHECKPOINT_MARK,
                'message' => json_encode(['checkPointLabel' => $serviceCheckpoint->check_point_name, 'checkPointId' => $serviceCheckpoint->id, 'checkPointStatus' => $serviceCheckpoint->status]),
                'requested_param' => serialize($request->all()),
            ]);
            $activityHtml = '';
            $details = json_decode($activity->message);
            $msg = '<b style="font-weight: bold;">' . $activity->user->name . ' - </b>' .trans('content.markedAs', ['label' => $details->checkPointLabel, 'checked' => $details->checkPointStatus ? trans('content.checked') : trans('content.unchecked')]);
            $activityHtml .= '<div class="inquirySend col-md-12" style="cursor: default;"><p class="float-left text_align_m">' . $msg . '</p><br><p class="float-right">' . $activity->created_at->toDayDateTimeString() . '</p></div>';
            return response(['status' => true, 'message' => 'Done', 'html' => $activityHtml], 200);
        } catch (\Exception $e) {
            return response(['status' => false, 'message' => $e->getMessage()], 203);
        }
    }

    public function checkPointComment(Request $request)
    {
        try {
            app()->setLocale($request->lang);
            $serviceCheckpoint = ServiceInquiryCheckPoint::where('id',$request->data_id)->first();
            $serviceCheckpoint->comment = $request->comment;
            $serviceCheckpoint->save();

            $activity = ServiceInquiryActivity::create([
                'service_inquiry_id' => $serviceCheckpoint->service_inquiry_id,
                'user_id' => \auth()->id(),
                'activity_type' => ServiceInquiryActivity::CHECKPOINT_COMMENT,
                'message' => json_encode(['checkPointLabel' => $serviceCheckpoint->check_point_name, 'checkPointId' => $serviceCheckpoint->id, 'checkPointComment' => $serviceCheckpoint->comment]),
                'requested_param' => serialize($request->all()),
            ]);
            $activityHtml = '';
            $details = (Array) json_decode($activity->message);
            $msg = '<b style="font-weight: bold;">' . $activity->user->name . ' - </b>' .trans('content.checkpointComment', $details);
            $activityHtml .= '<div class="inquirySend col-md-12" style="cursor: default;"><p class="float-left text_align_m">' . $msg . '</p><br><p class="float-right">' . $activity->created_at->toDayDateTimeString() . '</p></div>';
            return response(['status' => true, 'message' => 'Done', 'html' => $activityHtml], 200);
        } catch (\Exception $e) {
            return response(['status' => false, 'message' => $e->getMessage()], 203);
        }
    }

    public function ajaxInviteUsers(Request $request)
    {
        try {
            app()->setLocale($request->lang);
            $inquiry = ServiceInquiries::with('product.subsidiaryId')->where('id',$request->data_id)->first();
            foreach (explode(', ', $request->emails) as $key => $email) {
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $timestamp = Carbon::now()->timestamp.$key;
                    if(isset($request->documentsSend)){
                        //$documentsSend = json_encode($request->documentsSend);
                        $documentsSend = [];
                        foreach ($request->documentsSend as $k => $item){
                            $data = explode(',',$item);
                            $key = array_last($data);
                            $value = array_first($data);
                            $documentsSend[++$k.') '.$key] = $value;
                        }
//                        foreach ($request->documentsSend as $documentSend){
                            $serviceInquiryVerification = ServiceInquiryVerification::create([
                                'email' => $email,
                                'service_inquiry_id' => $request->data_id,
                                'link' => $timestamp,
                                'path' => json_encode($documentsSend),
                                'inquiry_status' => $inquiry->status_of_request,
                            ]);
//                        }
                    }
                    else{
                        $serviceInquiryVerification = ServiceInquiryVerification::create([
                            'email' => $email,
                            'service_inquiry_id' => $request->data_id,
                            'link' => $timestamp,
                            'inquiry_status' => $inquiry->status_of_request,
                        ]);
                    }
                    $activity = ServiceInquiryActivity::create([
                        'service_inquiry_id' => $request->data_id,
                        'user_id' => \auth()->id(),
                        'activity_type' => ServiceInquiryActivity::INVITE_FOR_VERIFICATION,
                        'message' => json_encode(['verificationId' => $serviceInquiryVerification->id]),
                        'requested_param' => serialize($request->all()),
                    ]);
                    $activityHtml = '';
                    $documents = implode(', ',array_keys((Array)json_decode($serviceInquiryVerification->path)));
                    $s = '<b style="font-weight: bold;"> ' . urldecode($documents) . ' </b>';
                    $msg = '<b style="font-weight: bold;">' . $activity->user->name . ' - </b>' . trans('content.invitedUsers', ['email' => $serviceInquiryVerification->email, 'document' => $s]);
                    $activityHtml .= '<div class="inquirySend col-md-12" style="cursor: default;"><p class="float-left text_align_m">' . $msg . '</p><br><p class="float-right">' . $activity->created_at->toDayDateTimeString() . '</p></div>';
                    try{
                        $s = Mail::to($email)->send(new ServiceinquiryExternalVerfication($inquiry,$serviceInquiryVerification, $request->lang));
                    }catch (\Exception $e){
                        Log::info('email not sent in External verification');
                    }
                }
            }
//            return response(['status' => true, 'message' => 'Invited successful.'], 200);
            return response(['status' => true, 'message' => __('toaster.invited successfully'), 'html' => $activityHtml], 200);
        } catch (\Exception $e) {
            return response(['status' => false, 'message' => $e->getMessage()], 203);
        }
    }
    public function customerFile(Request $request)
    {
        try {
            if($request->document_customer) {
                $docName = [];
                foreach ($request->document_customer as $key => $extDoc) {
                    $image = $extDoc;
                    $path = 'public/serviceInquiry/customerDocument/';
                    $fileName = Carbon::now()->timestamp.$key. '.' . $image->getClientOriginalName();
                    $storeFileName = 'serviceInquiry/customerDocument/' . $fileName;
                    Storage::disk('local')->put($path . $fileName, file_get_contents($image));
                    array_push($docName, $storeFileName);
                }
            }

            $activity = ServiceInquiryActivity::create([
                'service_inquiry_id' => $request->data_id,
                'user_id' => \auth()->id(),
                'activity_type' => ServiceInquiryActivity::CUSTOMER_FILES,
                'message' => ServiceInquiryActivity::CUSTOMER_FILES,
                'requested_param' => serialize($docName),
            ]);

            return redirect()->back();
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }
}
