<?php

namespace App\Http\Controllers\Voyager;

use App\Helper\Helper;
use App\Http\Controllers\Controller;
//use App\Traits\BrowseDataTables;
use App\Models\BannerMaster;
use App\Traits\BrowseDataTables;
use Faker\Provider\File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Image;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use Illuminate\Support\Facades\Validator;

use Yajra\DataTables\DataTables;


class BannerImagesController extends \TCG\Voyager\Http\Controllers\Controller
{
    use BreadRelationshipParser;
    //use BrowseDataTables;

    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];
        $searchable = $dataType->server_side ? array_keys(SchemaManager::describeTable(app($dataType->model_name)->getTable())->toArray()) : '';
        $dataType->order_column ? $dataType->order_column : $dataType->order_column="title";
        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', null);
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + 1;
            $orderColumn = [[$index, 'desc']];
            if (!$sortOrder && isset($dataType->order_direction)) {
                $sortOrder = $dataType->order_direction;
                $orderColumn = [[$index, $dataType->order_direction]];
            } else {
                $orderColumn = [[$index, 'desc']];
            }
        }

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            $query = $model::select('*');

            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        if (($isModelTranslatable = is_bread_translatable($model))) {
            $dataTypeContent->load('translations');
        }

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = isset($dataType->default_search_key) ? $dataType->default_search_key : null;

        $bannerCategory = BannerMaster::all();

        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::$slug.browse")) {
            $view = "voyager::$slug.browse";
        }

        return Voyager::view($view, compact(
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchable',
            'isServerSide',
            'defaultSearchKey',
            'bannerCategory'
        ));
    }

    public function browse(Request $request){
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            $banner_category = $request->banner_category;
            $query = $model::select('*')
                            ->when($banner_category, function ($query) use($banner_category) {
                                if($banner_category!=''){
                                    $query->where('banner_masters_id',$banner_category);
                                }
                            })
                            ->orderBy('order');

            $slug = str_replace('-', '_', $slug);

            return Datatables::of($query)
                ->editColumn('id', function($query) use ($slug) {
                    if(auth()->user()->hasPermission('delete_'.$slug)) {
                        return '<input type="checkbox" name="row_id" id="checkbox_'.$query->id.'" value="'.$query->id.'">';
                    } else {
                        return '';
                    }
                })
                ->editColumn('banner_masters_id', function($query) use ($slug) {
                    $bannerCategory = BannerMaster::where('id',$query->banner_masters_id)->first();
                    return $bannerCategory->title;
                })
                ->editColumn('image', function($query) use ($dataType) {
                    $path = asset('storage/'.$query->image);
                    return '<img src="'.$path.'" width="50">';
                })
                ->editColumn('action', function($query) use ($dataType, $slug) {
                    return Helper::getGeneralActions($dataType, $query, $slug);
                })
                ->editColumn('created_at', function($query) use ($dataType) {
                    return Helper::getFormattedDate($query->created_at);
                })
                ->rawColumns(['id','banner_masters_id','image','action'])
                ->make(true);

        } else {
            abort('404');
        }
    }

    public function create(Request $request)
    {
        $slug = $this->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? new $dataType->model_name()
            : false;

        foreach ($dataType->addRows as $key => $row) {
            $dataType->addRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $bannerCategory = BannerMaster::all();

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable','bannerCategory'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), \App\Models\BannerImages::rules(), \App\Models\BannerImages::rulesMessages());
        if ($validator->fails()) {
            if ($validator->fails()) {
                return back()->withErrors($validator);
            }
        }
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->has('_validate'))
        {
            $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

            $bannerMaster = BannerMaster::find($request->banner_masters_id);

            $filesystem = config('voyager.storage.disk');
            $realPath = Storage::disk($filesystem)->getDriver()->getAdapter()->getPathPrefix();

            // $file = $request->image->store('/banner-images'.'/'.date('FY', strtotime(date("Y/m/d"))), $filesystem);
            $img = Image::make($realPath.$data->image);
            $img->resize($bannerMaster->width, $bannerMaster->height, function ($constraint) {              
                $constraint->aspectRatio();
            })->save($realPath.$data->image);

            if ($request->ajax()) {
                return response()->json(['success' => true, 'data' => '']);
            }

            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                    'message'    => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }
    }

    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? app($dataType->model_name)->findOrFail($id)
            : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        $bannerMaster = BannerMaster::where('id',$dataTypeContent->banner_masters_id)->first();

         // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $bannerCategory = BannerMaster::all();

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'id', 'bannerMaster','bannerCategory'));
    }

    public function update(Request $request)
    {
        $regex = '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
        $validator = Validator::make($request->all(), [
            'title'  => 'required',
            'banner_masters_id' => 'required',
//            'order' => 'required|numeric',
//            'url' => 'regex:' . $regex,
        ],[
            'banner_masters_id.required' => 'Please select Banner category.',
//            'order.required'    => 'Please enter order number.',
//            'url.regex' => 'Please enter url in format [http://www.abc.com]',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        // Compatibility with Model binding.
        $id = $request->id instanceof Model ? $request->id->{$request->id->getKeyName()} : $request->id;

        $data = call_user_func([$dataType->model_name, 'findOrFail'], $request->id);
        
        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $request->id);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->has('_validate'))
        {
            $result = $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

            $bannerMaster = BannerMaster::find($request->banner_masters_id);

            $filesystem = config('voyager.storage.disk');
            $realPath = Storage::disk($filesystem)->getDriver()->getAdapter()->getPathPrefix();
            // $file = $request->image->store('/banner-images'.'/'.date('FY', strtotime(date("Y/m/d"))), $filesystem);
            $img = Image::make($realPath.$result->image);
            $img->resize($bannerMaster->width, $bannerMaster->height, function ($constraint) {              
                $constraint->aspectRatio();
            })->save($realPath.$result->image);

            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                    'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }
    }

    public function show(Request $request,$id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        $bannerMaster = BannerMaster::where('id',$dataTypeContent->banner_masters_id)->first();

        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check permission
        $this->authorize('read', app($dataType->model_name));

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.read';

        if (view()->exists("voyager::$slug.read")) {
            $view = "voyager::$slug.read";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable','bannerMaster'));
    }

    public function destroy(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        // Init array of IDs
        $ids = [];
        $relatedBannerImagesIds = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }

        foreach ($ids as $id) {
            $relatedBannerImagesData = \App\Models\BannerImages::where('id', '=',$id)->first();
            if ($relatedBannerImagesData != null) {
                $relatedBannerImagesIds[] = $relatedBannerImagesData->id;
            }
        }

        $displayName = count($ids) > 1 ? "These ".$dataType->display_name_plural : "This ".$dataType->display_name_singular;

        if (count($relatedBannerImagesIds) == 0) {
            $relatedBannerImagesDataNames = [];

            foreach ($relatedBannerImagesIds as $id) {
                $data = \App\Models\BannerImages::where('id', $id)->first();
                $relatedBannerImagesDataNames[] = $data->title;
            }

            $data = [
                'message'    => "You cannot Delete "."{$displayName}",
                'alert-type' => 'error',
            ];
        }
        else {
            foreach ($ids as $id) {
                $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
                $this->cleanup($dataType, $data);

                $bannerImageMaster = \App\Models\BannerImages::find($id)->first();
                $banner_master_id = $bannerImageMaster->banner_masters_id;
                $bannerMaster = BannerMaster::where('id',$banner_master_id)->first();

                $destinationPath = public_path('storage/'.$bannerMaster->folder);
                \Illuminate\Support\Facades\File::delete($destinationPath . '/'. $bannerMaster->image);
            }

            $res = $data->destroy($ids);
            $data = $res
                ? [
                    'message'    => __('voyager::generic.successfully_deleted')." {$displayName}",
                    'alert-type' => 'success',
                ]
                : [
                    'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                    'alert-type' => 'error',
                ];


        }

        return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
    }

    /**
     * Remove translations, images and files related to a BREAD item.
     *
     * @param \Illuminate\Database\Eloquent\Model $dataType
     * @param \Illuminate\Database\Eloquent\Model $data
     *
     * @return void
     */
    protected function cleanup($dataType, $data)
    {
        // Delete Translations, if present
        if (is_bread_translatable($data)) {
            $data->deleteAttributeTranslations($data->getTranslatableAttributes());
        }

        // Delete Images
        $this->deleteBreadImages($data, $dataType->deleteRows->where('type', 'image'));

        // Delete Files
        foreach ($dataType->deleteRows->where('type', 'file') as $row) {
            if (isset($data->{$row->field})) {
                foreach (json_decode($data->{$row->field}) as $file) {
                    $this->deleteFileIfExists($file->download_link);
                }
            }
        }
    }

    /**
     * Delete all images related to a BREAD item.
     *
     * @param \Illuminate\Database\Eloquent\Model $data
     * @param \Illuminate\Database\Eloquent\Model $rows
     *
     * @return void
     */
    public function deleteBreadImages($data, $rows)
    {
        foreach ($rows as $row) {
            if ($data->{$row->field} != config('voyager.user.default_avatar')) {
                $this->deleteFileIfExists($data->{$row->field});
            }

            if (isset($row->details->thumbnails)) {
                foreach ($row->details->thumbnails as $thumbnail) {
                    $ext = explode('.', $data->{$row->field});
                    $extension = '.'.$ext[count($ext) - 1];

                    $path = str_replace($extension, '', $data->{$row->field});

                    $thumb_name = $thumbnail->name;

                    $this->deleteFileIfExists($path.'-'.$thumb_name.$extension);
                }
            }
        }

        if ($rows->count() > 0) {
            event(new BreadImagesDeleted($data, $rows));
        }
    }
}
