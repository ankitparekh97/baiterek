<?php

namespace App\Http\Controllers\Voyager;

use App\Helper\Helper;
use App\Traits\BrowseDataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use App\Models\User;
use App\Models\UserRole;
use TCG\Voyager\Models\Role;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Events\BreadDataDeleted;
use Yajra\Datatables\Datatables;

class VoyagerRoleController extends \TCG\Voyager\Http\Controllers\Controller
{
    use BreadRelationshipParser;

    //***************************************
    //               ____
    //              |  _ \
    //              | |_) |
    //              |  _ <
    //              | |_) |
    //              |____/
    //
    //      Browse our Data Type (B)READ
    //
    //****************************************

        public function index(Request $request)
         {
             // GET THE SLUG, ex. 'posts', 'pages', etc.
             // $slug = $this->getSlug($request);
             $slug = "roles";

             // GET THE DataType based on the slug
             $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
             // dd($dataType);
             // Check permission
             $this->authorize('browse', app($dataType->model_name));

             $getter = $dataType->server_side ? 'paginate' : 'get';

             $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];
             $searchable = $dataType->server_side ? array_keys(SchemaManager::describeTable(app($dataType->model_name)->getTable())->toArray()) : '';
             $orderBy = $request->get('order_by', $dataType->order_column);
             $sortOrder = $request->get('sort_order', null);
             $orderColumn = [];
             if ($orderBy) {
                 $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + 1;
                 $orderColumn = [[$index, 'desc']];
                 if (!$sortOrder && isset($dataType->order_direction)) {
                     $sortOrder = $dataType->order_direction;
                     $orderColumn = [[$index, $dataType->order_direction]];
                 } else {
                     $orderColumn = [[$index, 'desc']];
                 }
             }

             // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
             if (strlen($dataType->model_name) != 0) {
                 $model = app($dataType->model_name);
                 $query = $model::select('*');

                 // If a column has a relationship associated with it, we do not want to show that field
                 $this->removeRelationshipField($dataType, 'browse');

                 if ($search->value && $search->key && $search->filter) {
                     $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                     $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                     $query->where($search->key, $search_filter, $search_value);
                 }

                 if ($orderBy && in_array($orderBy, $dataType->fields())) {
                     $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                     $dataTypeContent = call_user_func([
                         $query->orderBy($orderBy, $querySortOrder),
                         $getter,
                     ]);
                 } elseif ($model->timestamps) {
                     $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
                 } else {
                     $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
                 }

                 // Replace relationships' keys for labels and create READ links if a slug is provided.
                 $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
             } else {
                 // If Model doesn't exist, get data from table name
                 $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
                 $model = false;
             }

             // Check if BREAD is Translatable
             if (($isModelTranslatable = is_bread_translatable($model))) {
                 $dataTypeContent->load('translations');
             }

             // Check if server side pagination is enabled
             $isServerSide = isset($dataType->server_side) && $dataType->server_side;

             // Check if a default search key is set
             $defaultSearchKey = isset($dataType->default_search_key) ? $dataType->default_search_key : null;

             $view = 'voyager::bread.browse';

             if (view()->exists("voyager::$slug.browse")) {
                 $view = "voyager::$slug.browse";
             }

             return Voyager::view($view, compact(
                 'dataType',
                 'dataTypeContent',
                 'isModelTranslatable',
                 'search',
                 'orderBy',
                 'orderColumn',
                 'sortOrder',
                 'searchable',
                 'isServerSide',
                 'defaultSearchKey'
             ));
         }

    //***************************************
    //                _____
    //               |  __ \
    //               | |__) |
    //               |  _  /
    //               | | \ \
    //               |_|  \_\
    //
    //  Read an item of our Data Type B(R)EAD
    //
    //****************************************

    public function show(Request $request, $id)
    {
        // $slug = $this->getSlug($request);
        $slug = "roles";

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check permission
        $this->authorize('read', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.read';

        $rolePermissions = $dataTypeContent->permissions->pluck('key')->toArray();
        $permissionTree = Helper::getPermissionTree();

        if (view()->exists("voyager::$slug.read")) {
            $view = "voyager::$slug.read";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'rolePermissions', 'permissionTree'));
    }

    //***************************************
    //                ______
    //               |  ____|
    //               | |__
    //               |  __|
    //               | |____
    //               |______|
    //
    //  Edit an item of our Data Type BR(E)AD
    //
    //****************************************

    public function edit(Request $request, $id)
    {
        // $slug = $this->getSlug($request);
        $slug = "roles";

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? app($dataType->model_name)->findOrFail($id)
            : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.edit-add';

        $rolePermissions = $dataTypeContent->permissions->pluck('key')->toArray();
        $permissionTree = Helper::getPermissionTree();

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'rolePermissions', 'permissionTree'));
    }

    // POST BR(E)AD
    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        //Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->ajax()) {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

            // event(new SectionDataUpdated($dataType, $data, $request));

            $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

            $permissions = $request->input('permissions', []);
            array_push($permissions, 'browse_admin');
            DB::table('permission_role')->where('role_id', '=', $id)->delete();
            $grantedPermissions = [];
            foreach ($permissions as $permission) {
                $permissionData = Permission::firstOrCreate(['key'=> $permission]);
                array_push($grantedPermissions, $permissionData->id);
            }

            $data->permissions()->sync($grantedPermissions);

            return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message'    => __('toaster.successfully updated role')." {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
        }
    }

    //***************************************
    //
    //                   /\
    //                  /  \
    //                 / /\ \
    //                / ____ \
    //               /_/    \_\
    //
    //
    // Add a new item of our Data Type BRE(A)D
    //
    //****************************************

    public function create(Request $request)
    {
        // $slug = $this->getSlug($request);
        $slug = "roles";

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? new $dataType->model_name()
            : false;

        foreach ($dataType->addRows as $key => $row) {
            $dataType->addRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }
        $permissionTree = Helper::getPermissionTree();
        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'permissionTree'));
    }

    // POST BRE(A)D
    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        //Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->ajax()) {
            $data = new $dataType->model_name();

            $this->insertUpdateData($request, $slug, $dataType->addRows, $data);

//            event(new SectionDataAdded($dataType, $data, $request));

            $permissions = $request->input('permissions', []);
            array_push($permissions, 'browse_admin');
            $grantedPermissions = [];
            foreach ($permissions as $permission) {
                $permissionData = Permission::firstOrCreate(['key'=> $permission]);
                array_push($grantedPermissions, $permissionData->id);
            }
            $data->permissions()->sync($grantedPermissions);

            return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message'    => __('toaster.successfully added new role')." {$dataType->display_name_singular}",
                'alert-type' => 'success',
                ]);
        }
    }

    //***************************************
    //                _____
    //               |  __ \
    //               | |  | |
    //               | |  | |
    //               | |__| |
    //               |_____/
    //
    //         Delete an item BREA(D)
    //
    //****************************************

    public function destroy(Request $request, $id)
    {
        // $slug = $this->getSlug($request);
        $slug = "roles";

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        // Init array of IDs
        $ids = [];
        $primaryRoleIds = [];
        $secondaryRoleids = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }
        
        foreach ($ids as $id) {
            $primaryRole = User::select('role_id')->where('role_id', $id)->first();
            if ($primaryRole != null) {
                $primaryRoleIds[] = $primaryRole->role_id;
            }
            $secondaryRole = UserRole::select('role_id')->where('role_id', $id)->first();
            if ($secondaryRole != null) {
                $secondaryRoleids[] = $secondaryRole->role_id;
            }
        }
        
        $displayName = count($ids) > 1 ? "These ".$dataType->display_name_plural : "This ".$dataType->display_name_singular;

        if (count($primaryRoleIds) != 0) {
            $primaryRoleNames = [];
            
            foreach ($primaryRoleIds as $id) {
                $data = Role::where('id', $id)->first();
                $primaryRoleNames [] = $data->display_name;
            }

            $stringPrimaryRoleNames = implode(',', $primaryRoleNames);
            $displayPucntuation = count($primaryRoleIds) > 1 ? 'are' : 'is';

            $data = [
                    'message'    => "You cannot Delete "."{$displayName}"." as "."{$stringPrimaryRoleNames}"." {$displayPucntuation} "."already Assigned as Primary Role to User.",
                    'alert-type' => 'error',
                ];
        }
        elseif (count($secondaryRoleids) != 0) {
            $secondaryRoleNames = [];
            
            foreach ($secondaryRoleids as $id) {
                $data = Role::where('id', $id)->first();
                $secondaryRoleNames [] = $data->display_name;
            }

            $stringSecondaryRoleNames = implode(',', $secondaryRoleNames);
            $displayPucntuation = count($secondaryRoleids) > 1 ? 'are' : 'is';

            $data = [
                    'message'    => "You cannot Delete "."{$displayName}"." as "."{$stringSecondaryRoleNames}"." {$displayPucntuation} "."already Assigned as Secondary Role to User.",
                    'alert-type' => 'error',
                ];
        }
        else {
            foreach ($ids as $id) {
                $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
                $this->cleanup($dataType, $data);
//                event(new SectionDataDeleted($dataType, $data, $request));
            }

            $res = $data->destroy($ids);
            $data = $res
                ? [
                    'message'    => __('toaster.successfully deleted this role')." {$displayName}",
                    'alert-type' => 'success',
                ]
                : [
                    'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                    'alert-type' => 'error',
                ];

            if ($res) {
                event(new BreadDataDeleted($dataType, $data));
            }
        }
        return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
    }

    /**
     * Remove translations, images and files related to a BREAD item.
     *
     * @param \Illuminate\Database\Eloquent\Model $dataType
     * @param \Illuminate\Database\Eloquent\Model $data
     *
     * @return void
     */
    protected function cleanup($dataType, $data)
    {
        // Delete Translations, if present
        if (is_bread_translatable($data)) {
            $data->deleteAttributeTranslations($data->getTranslatableAttributes());
        }

        // Delete Images
        $this->deleteBreadImages($data, $dataType->deleteRows->where('type', 'image'));

        // Delete Files
        foreach ($dataType->deleteRows->where('type', 'file') as $row) {
            if (isset($data->{$row->field})) {
                foreach (json_decode($data->{$row->field}) as $file) {
                    $this->deleteFileIfExists($file->download_link);
                }
            }
        }
    }

    /**
     * Delete all images related to a BREAD item.
     *
     * @param \Illuminate\Database\Eloquent\Model $data
     * @param \Illuminate\Database\Eloquent\Model $rows
     *
     * @return void
     */
    public function deleteBreadImages($data, $rows)
    {
        foreach ($rows as $row) {
            if ($data->{$row->field} != config('voyager.user.default_avatar')) {
                $this->deleteFileIfExists($data->{$row->field});
            }

            if (isset($row->details->thumbnails)) {
                foreach ($row->details->thumbnails as $thumbnail) {
                    $ext = explode('.', $data->{$row->field});
                    $extension = '.'.$ext[count($ext) - 1];

                    $path = str_replace($extension, '', $data->{$row->field});

                    $thumb_name = $thumbnail->name;

                    $this->deleteFileIfExists($path.'-'.$thumb_name.$extension);
                }
            }
        }

        if ($rows->count() > 0) {
            event(new BreadImagesDeleted($data, $rows));
        }
    }

     public function browse(Request $request){
         // GET THE SLUG, ex. 'posts', 'pages', etc.
         $slug = $this->getSlug($request);

         // GET THE DataType based on the slug
         $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

         // Check permission
         $this->authorize('browse', app($dataType->model_name));

         if (strlen($dataType->model_name) != 0) {
             $model = app($dataType->model_name);
             $query = $model::select('*');
             $slug = str_replace('-', '_', $slug);
             return Datatables::of($query)
                 ->editColumn('id', function($query) use ($slug) {
                     if(auth()->user()->hasPermission('delete_'.$slug)) {
                         return '<input type="checkbox" name="row_id" id="checkbox_'.$query->id.'" value="'.$query->id.'">';
                     } else {
                         return '';
                     }
                 })
                 ->editColumn('action', function($query) use ($dataType, $slug) {
                     if(strtolower($query->name) != 'super admin') {
                         return Helper::getGeneralActions($dataType, $query, $slug);
                     } else {
                         return '<a href="'.url('admin/roles/'.$query->id).'" title="View" class="btn btn-sm btn-warning pull-right view">
                             <i class="voyager-eye"></i><span class="hidden">View</span></a>';
                     }

                 })
                 ->editColumn('created_at', function($query) use ($dataType) {
                     return Helper::getFormattedDate($query->created_at);
                 })
                 ->rawColumns(['action', 'id'])
                 ->make(true);
         } else {
             abort(404);
         }
     }
}