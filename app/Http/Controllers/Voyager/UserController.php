<?php

namespace App\Http\Controllers\Voyager;

use App\Helper\Helper;
use App\Http\Controllers\Controller;
use App\Mail\Useremail;
use App\Models\Subsidiaries;
use App\Models\SubsidiaryUsers;
use App\Models\User;
use App\Traits\BrowseDataTables;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use Illuminate\Support\Facades\Validator;
use TCG\Voyager\Models\Role;
use TCG\Voyager\Traits\Translatable;
use Yajra\DataTables\DataTables;

class UserController extends \TCG\Voyager\Http\Controllers\Controller
{
    use BreadRelationshipParser;
    use BrowseDataTables;

    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];
        $searchable = $dataType->server_side ? array_keys(SchemaManager::describeTable(app($dataType->model_name)->getTable())->toArray()) : '';
        $dataType->order_column ? $dataType->order_column : $dataType->order_column="title";
        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', null);
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + 1;
            $orderColumn = [[$index, 'desc']];
            if (!$sortOrder && isset($dataType->order_direction)) {
                $sortOrder = $dataType->order_direction;
                $orderColumn = [[$index, $dataType->order_direction]];
            } else {
                $orderColumn = [[$index, 'desc']];
            }
        }

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            $query = $model::select('*');

            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        if (($isModelTranslatable = is_bread_translatable($model))) {
            $dataTypeContent->load('translations');
        }

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = isset($dataType->default_search_key) ? $dataType->default_search_key : null;
        $menuView = '';

        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::$slug.browse")) {
            $view = "voyager::$slug.browse";
        }

        return Voyager::view($view, compact(
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchable',
            'isServerSide',
            'menuView',
            'defaultSearchKey'
        ));
    }

    public function browse(Request $request){
        app()->setLocale($request->lang);
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            $query = $model::select('*')->latest();

            $slug = str_replace('-', '_', $slug);

            return Datatables::of($query)
                ->editColumn('id', function($query) use ($slug) {
                    if(auth()->user()->hasPermission('delete_'.$slug)) {
                        return '<input type="checkbox" name="row_id" id="checkbox_'.$query->id.'" value="'.$query->id.'">';
                    } else {
                        return '';
                    }
                })
                ->editColumn('role_id', function($query) use ($dataType) {
                    return $query->role->display_name;
                })
                ->editColumn('action', function($query) use ($dataType, $slug) {
                    return Helper::getGeneralActions($dataType, $query, $slug);
                })
                ->editColumn('created_at', function($query) use ($dataType) {
                    return Helper::getFormattedDate($query->created_at);
                })
                ->addColumn('status', function($query) use ($dataType) {
//                    $action = '';
//                    if($query->id != \auth()->id()){
//                        $action = 'getDropdown';
//                    }
                    //$html = '<div class="'.$action.'" data-id="'.$query->id.'">';
                    $html = '<div class="getDropdown" data-id="'.$query->id.'">';
                    if ($query->status == User::ACTIVE) {
                        $html .= '<span style="cursor: pointer;" class="badge badge-success">'.trans('portal.users.active').'</span>';
                    } else {
                        $html .= '<span style="cursor: pointer;" class="badge badge-danger">'.trans('portal.users.inActive').'</span>';
                    }
                    $html .= '</div>';
                    if($query->id == \auth()->id()){
                        $html = null;
                    }
                    return $html;
                })
                ->rawColumns(['id','role_id','status','action'])
                ->make(true);

        } else {
            abort('404');
        }
    }

    public function create(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? new $dataType->model_name()
            : false;

        foreach ($dataType->addRows as $key => $row) {
            $dataType->addRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $subsidiaries = Subsidiaries::all()->translate('locale', app()->getLocale());

        $productManagerRole = Role::where('slug','product-manager')->first();
        $menuView = '';
        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'productManagerRole','menuView','subsidiaries'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), User::rules());

        if ($validator->fails()) {
            if ($validator->fails()) {
                return back()->withErrors($validator);
            }
        }
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows);

        /*
        * Prepare Translations and Transform data
        */
        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->has('_validate')) {

            $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
            $data->remember_token = sha1(time());
            $password = User::generatePassword();
            $data->setPasswordAttribute($password);
            $data->is_active = 1;
            $data->save();

            $role = Role::where('id',$request->role_id)->first();
            if($role->slug == 'supervisor'){
                $subsidiaryUsers = New SubsidiaryUsers();
                $subsidiaryUsers->user_id = $data->id;
                $subsidiaryUsers->subsidiary_id = $request->subsidiary_id;
                $subsidiaryUsers->save();
            }

            if ($request->ajax()) {
                return response()->json(['success' => true, 'data' => '']);
            }

            try{
                $actionLink = url('admin/login');
                $userLang = app()->getLocale();
                Mail::to($request->email)->send(new Useremail($data, $password,$actionLink,$userLang));

                return redirect()
                    ->route("voyager.{$dataType->slug}.index")
                    ->with([
                        'message'    => trans('toaster.successfully added new user activation link has been sent to',['email' => $request->email]) ,
//                        'message'    => __('voyager::generic.successfully_added_new')." The activation link has been sent to ".$request->email.".",
                        'alert-type' => 'success',
                    ]);

            } catch (\Exception $ex) {
                Log::error($ex->getMessage());
                return redirect()
                    ->route("voyager.{$dataType->slug}.index")
                    ->with([
                        'message'    => __('voyager::generic.successfully_added_new')." User",
                        'alert-type' => 'success',
                    ]);
            }

        }
    }

    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? app($dataType->model_name)->findOrFail($id)
            : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        $subsidiaries = Subsidiaries::all()->translate('locale', app()->getLocale());
        $subsidiaryUser = SubsidiaryUsers::where('user_id',$dataTypeContent->id)->first();

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);
        $menuView = '';
        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable','menuView','subsidiaries','subsidiaryUser'));
    }

    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), User::accessRule($id));

        if ($validator->fails()) {
            if ($validator->fails()) {
                return back()->withErrors($validator);
            }
        }

        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
//        $id = $request->id instanceof Model ? $request->id->{$request->id->getKeyName()} : $request->id;
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $data = call_user_func([$dataType->model_name, 'findOrFail'], $request->id);

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        $role = Role::where('id',$request->role_id)->first();
        $subsidiaryUser = SubsidiaryUsers::where('user_id',$id)->first();
        if($role->slug == 'supervisor'){
            if(!empty($subsidiaryUser)){
                $subsidiaryUser->delete();
            }

            $subsidiaryUsers = SubsidiaryUsers::firstOrCreate([
                'user_id' => $data->id,
                'subsidiary_id' => $request->subsidiary_id,
            ]);

        } else {
            if($data->role->slug == 'supervisor'){
                if(!empty($subsidiaryUser)){
                    $subsidiaryUser->delete();
                }
            }
        }

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $request->id);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->has('_validate'))
        {
            $update = $this->insertUpdateData($request, $slug, $dataType->editRows, $data);
            $update->is_active = 1;
            $update->save();


            return redirect()
                ->route("voyager.{$dataType->slug}")
                ->with([
                    'message'    => __('toaster.successfully updated new user'),
                    'alert-type' => 'success',
                ]);
        }
    }

    public function show(Request $request,$id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check permission
        $this->authorize('read', app($dataType->model_name));

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);
        $menuView = '';
        $view = 'voyager::bread.read';

        if (view()->exists("voyager::$slug.read")) {
            $view = "voyager::$slug.read";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable','menuView'));
    }

    public function destroy(Request $request, $id)
    {
        $slug = 'users';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
//        $this->authorize('delete', app($dataType->model_name));

        // Init array of IDs
        $ids = [];
        $relatedUserDataIds = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }

        foreach ($ids as $id) {
            $user = User::where('id', '=',$id)->first();
            if ($user != null) {
                $userIds[] = $user->id;
            }
        }

        $displayName = count($ids) > 1 ? "These ".$dataType->display_name_plural : "This ".$dataType->display_name_singular;

        if (count($userIds) == 0) {
            $userNames = [];

            foreach ($userIds as $id) {
                $data = User::where('id', $id)->first();
                $userNames[] = $data->title;
            }

            $stringRelatedUserDataNames = implode(',', $userNames);
            $displayPucntuation = count($userIds) > 1 ? 'are' : 'is';

            $data = [
                'message'    => "You cannot Delete "."{$displayName}"." as "."{$stringRelatedUserDataNames}"." {$displayPucntuation} ",
                'alert-type' => 'error',
            ];
        }
        else {
            foreach ($ids as $id) {
                $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
                $this->cleanup($dataType, $data);
            }

            $res = $data->destroy($ids);
            $data = $res
                ? [
                    'message'    => __('toaster.successfully deleted this user'),
                    'alert-type' => 'success',
                ]
                : [
                    'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                    'alert-type' => 'error',
                ];
        }

        if(isset($request->PM)){
            return redirect()->route("product-managers")->with($data);
        } else {
            return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
        }

    }

    protected function cleanup($dataType, $data)
    {
        // Delete Translations, if present
        if (is_bread_translatable($data)) {
            $data->deleteAttributeTranslations($data->getTranslatableAttributes());
        }

        // Delete Images
        $this->deleteBreadImages($data, $dataType->deleteRows->where('type', 'image'));

        // Delete Files
        foreach ($dataType->deleteRows->where('type', 'file') as $row) {
            if (isset($data->{$row->field})) {
                foreach (json_decode($data->{$row->field}) as $file) {
                    $this->deleteFileIfExists($file->download_link);
                }
            }
        }
    }

    public function deleteBreadImages($data, $rows)
    {
        foreach ($rows as $row) {
            if ($data->{$row->field} != config('voyager.user.default_avatar')) {
                $this->deleteFileIfExists($data->{$row->field});
            }

            if (isset($row->details->thumbnails)) {
                foreach ($row->details->thumbnails as $thumbnail) {
                    $ext = explode('.', $data->{$row->field});
                    $extension = '.'.$ext[count($ext) - 1];

                    $path = str_replace($extension, '', $data->{$row->field});

                    $thumb_name = $thumbnail->name;

                    $this->deleteFileIfExists($path.'-'.$thumb_name.$extension);
                }
            }
        }

        if ($rows->count() > 0) {
            event(new BreadImagesDeleted($data, $rows));
        }
    }

    public function productManagerGrid(Request $request)
    {  
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = 'users';

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];
        $searchable = $dataType->server_side ? array_keys(SchemaManager::describeTable(app($dataType->model_name)->getTable())->toArray()) : '';
        $dataType->order_column ? $dataType->order_column : $dataType->order_column="title";
        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', null);
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + 1;
            $orderColumn = [[$index, 'desc']];
            if (!$sortOrder && isset($dataType->order_direction)) {
                $sortOrder = $dataType->order_direction;
                $orderColumn = [[$index, $dataType->order_direction]];
            } else {
                $orderColumn = [[$index, 'desc']];
            }
        }

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            $query = $model::select('*');

            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        if (($isModelTranslatable = is_bread_translatable($model))) {
            $dataTypeContent->load('translations');
        }

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = isset($dataType->default_search_key) ? $dataType->default_search_key : null;
        $menuView = 'ProductManager';

        $view = "voyager::$slug.product-managers";

        return Voyager::view($view, compact(
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchable',
            'isServerSide',
            'defaultSearchKey',
            'menuView'
        ));
    }

    public function productManagerBrowse(Request $request){
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = 'users';

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            $productManagerRole = Role::where('slug','product-manager')->first();
            $query = $model::select('id','name','last_name','email','gender','role_id')->where('role_id', $productManagerRole->id)->orderBy('id','desc');

            $slug = str_replace('-', '_', $slug);

            return Datatables::of($query)
                ->editColumn('id', function($query) use ($slug) {
                    if(auth()->user()->hasPermission('delete_'.$slug)) {
                        return '<input type="checkbox" name="row_id" id="checkbox_'.$query->id.'" value="'.$query->id.'">';
                    } else {
                        return '';
                    }
                })
                ->editColumn('action', function($query) use ($dataType, $slug) {
                    $delete = '<a href="javascript:;" title="Delete" class="btn btn-sm btn-danger pull-right delete" data-id="'.$query->id.'" id="delete-'.$query->id.'">
											<i class="voyager-trash"></i><span class="hidden">Delete</span> Delete
                                        </a>';

                    $edit = '<a href="'.url('/').'/admin/product-managers/edit/'.$query->id.'" title="Edit" class="btn btn-sm btn-primary pull-right edit">
											<i class="voyager-edit"></i><span class="hidden">Edit</span> Edit
                                        </a>';

                    $view = '<a href="'.url('/').'/admin/product-managers/read/'.$query->id.'" title="View" class="btn btn-sm btn-warning pull-right view">
                                            <i class="voyager-eye"></i><span class="hidden">View</span> View
                                        </a>';

                    return $delete . $edit . $view;
                })
                ->editColumn('role_id', function($query) use ($dataType) {
                    return $query->role->name;
                })
                ->rawColumns(['id','action','created_at'])
                ->make(true);

        } else {
            abort('404');
        }
    }

    public function productManagersCreate(Request $request)
    {
        $slug = 'users';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? new $dataType->model_name()
            : false;

        foreach ($dataType->addRows as $key => $row) {
            $dataType->addRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $productManagerRole = Role::where('slug','product-manager')->first();
        $menuView = 'ProductManager';

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'productManagerRole','menuView'));
    }

    public function productManagersStore(Request $request)
    {
        $validator = Validator::make($request->all(), User::rules());

        if ($validator->fails()) {
            if ($validator->fails()) {
                return back()->withErrors($validator);
            }
        }
        $slug = 'users';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows);

        /*
        * Prepare Translations and Transform data
        */
        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->has('_validate')) {

            $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
            $data->remember_token = sha1(time());
            $password = User::generatePassword();
            $data->setPasswordAttribute($password);
            $data->is_active = 1;
            $data->save();

            if ($request->ajax()) {
                return response()->json(['success' => true, 'data' => '']);
            }

            try{
                $actionLink = url('admin/login');
                $userLang = app()->getLocale();
                Mail::to($request->email)->send(new Useremail($data, $password,$actionLink,$userLang));

                return redirect()
                    ->route("voyager.product-managers")
                    ->with([
                        'message'    => __('voyager::generic.successfully_added_new')." The activation link has been sent to ".$request->email.".",
                        'alert-type' => 'success',
                    ]);

            } catch (\Exception $ex) {
                Log::error($ex->getMessage());
                return redirect()
                    ->route("voyager.product-managers.index")
                    ->with([
                        'message'    => __('voyager::generic.successfully_added_new')." Product Manager",
                        'alert-type' => 'success',
                    ]);
            }

        }
    }

    public function productManagersEdit(Request $request, $id)
    {
        $slug = 'users';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? app($dataType->model_name)->findOrFail($id)
            : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $menuView = 'ProductManager';
        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable','menuView'));
    }

    public function productManagersUpdate(Request $request, $id)
    {
        $validator = Validator::make($request->all(), User::accessRule($id));

        if ($validator->fails()) {
            if ($validator->fails()) {
                return back()->withErrors($validator);
            }
        }

        $slug = 'users';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
//        $id = $request->id instanceof Model ? $request->id->{$request->id->getKeyName()} : $request->id;
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $data = call_user_func([$dataType->model_name, 'findOrFail'], $request->id);

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $request->id);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->has('_validate'))
        {
            $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

            return redirect()
                ->route("voyager.product-managers.index")
                ->with([
                    'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }
    }

    public function productManagersShow(Request $request,$id)
    {
        $slug = 'users';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check permission
        $this->authorize('read', app($dataType->model_name));

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);
        $menuView = 'ProductManager';

        $view = 'voyager::bread.read';

        if (view()->exists("voyager::$slug.read")) {
            $view = "voyager::$slug.read";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable','menuView'));
    }

    public function changePassword(){
        return view('auth.changepassword');
    }

    public function updatePassword(Request $request)
    {
        if (!(Hash::check($request->current_password, Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error", "Your current password does not matches with the password you provided.");
        }

        if (strcmp($request->current_password, $request->password) == 0) {
            //Current password and new password are same
            return redirect()->back()->with("error", "New Password cannot be same as your current password.");
        }

        $this->validate($request, [
            'current_password' => 'required',
            'password' => 'min:6|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'min:6'
        ]);

        $user = Auth::user();
        $user->is_active = true;

        $password = $request->get('password');
        $user->setPasswordAttribute($password);
        $user->save();

        return Redirect::to('/admin')->with([
            'message'    => "Your Account Activated Successfully!!",
            'alert-type' => 'success',
        ]);
    }

    public function userStatus(Request $request)
    {
        $user = User::where('id', $request->userId)->first();
        if (!$user) {
            return response(['status' => false],203);
        }
        $user->status = $request->status == User::ACTIVE ? User::ACTIVE : User::IN_ACTIVE;
        $user->save();
        return response(['status' => true],203);
    }
}
