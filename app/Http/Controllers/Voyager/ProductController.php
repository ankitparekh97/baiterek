<?php

namespace App\Http\Controllers\Voyager;

use App\Helper\Helper;
use App\Http\Controllers\Controller;
//use App\Traits\BrowseDataTables;
use App\Mail\ProductMail;
use App\Models\ApplicationForms;
use App\Models\ProductCheckPoint;
use App\Models\Products;
use App\Models\SubsidiaryUsers;
use App\Models\Translations;
use App\Models\User;
use App\Traits\BrowseDataTables;
use Faker\Provider\File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Image;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use Illuminate\Support\Facades\Validator;
use TCG\Voyager\Models\Role;
use TCG\Voyager\Tests\TranslatableModel;
use Yajra\DataTables\DataTables;

class ProductController extends \TCG\Voyager\Http\Controllers\Controller
{
    use BreadRelationshipParser;
//    use BrowseDataTables;

    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];

        $searchNames = [];
        if ($dataType->server_side) {
            $searchable = SchemaManager::describeTable(app($dataType->model_name)->getTable())->pluck('name')->toArray();
            $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->get();
            foreach ($searchable as $key => $value) {
                $displayName = $dataRow->where('field', $value)->first()->getTranslatedAttribute('display_name');
                $searchNames[$value] = $displayName ?: ucwords(str_replace('_', ' ', $value));
            }
        }

        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', null);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $query = $model->{$dataType->scope}();
            } else {
                $query = $model::select('*');
            }

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model)) && Auth::user()->can('delete', app($dataType->model_name))) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query = $query->withTrashed();
                }
            }

            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value != '' && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        if (($isModelTranslatable = is_bread_translatable($model))) {
            $dataTypeContent->load('translations');
        }

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->default_search_key ?? null;

        // Actions
        $actions = [];
        if (!empty($dataTypeContent->first())) {
            foreach (Voyager::actions() as $action) {
                $action = new $action($dataType, $dataTypeContent->first());

                if ($action->shouldActionDisplayOnDataType()) {
                    $actions[] = $action;
                }
            }
        }

        // Define showCheckboxColumn
        $showCheckboxColumn = false;
        if (Auth::user()->can('delete', app($dataType->model_name))) {
            $showCheckboxColumn = true;
        } else {
            foreach ($actions as $action) {
                if (method_exists($action, 'massAction')) {
                    $showCheckboxColumn = true;
                }
            }
        }

        // Define orderColumn
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + ($showCheckboxColumn ? 1 : 0);
            $orderColumn = [[$index, 'desc']];
            if (!$sortOrder && isset($dataType->order_direction)) {
                $sortOrder = $dataType->order_direction;
                $orderColumn = [[$index, $dataType->order_direction]];
            } else {
                $orderColumn = [[$index, 'desc']];
            }
        }

        $file = 'generalInfo.json';

        if(file_exists(base_path($file))) {
            $generalInfo = file_get_contents(base_path($file));
        } else {
            $generalInfo = '';
        }

        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::$slug.browse")) {
            $view = "voyager::$slug.browse";
        }

        return Voyager::view($view, compact(
            'actions',
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchNames',
            'isServerSide',
            'defaultSearchKey',
            'usesSoftDeletes',
            'showSoftDeleted',
            'showCheckboxColumn',
            'generalInfo'
        ));
    }

    public function create(Request $request)
    {
        $slug = $this->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? new $dataType->model_name()
            : false;

        foreach ($dataType->addRows as $key => $row) {
            $dataType->addRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $role = Role::where('slug','supervisor')->first();
        $users = User::where('role_id', $role->id)->get();

        $application_form_kz = ApplicationForms::where('product_id', $dataTypeContent->id)->first();
        if(!empty($application_form_kz)){
            $application_form_en = Translations::where('foreign_key',$application_form_kz->id)
                ->where('table_name','application_forms')
                ->where('column_name','fields')
                ->where('locale','en')
                ->first();

            $application_form_ru = Translations::where('foreign_key',$application_form_kz->id)
                ->where('table_name','application_forms')
                ->where('column_name','fields')
                ->where('locale','ru')
                ->first();
        } else {
            $application_form_kz = '';
            $application_form_ru = '';
            $application_form_en = '';
        }

        $formDatakZ = ((!empty($application_form_kz->fields)) ? $application_form_kz->fields : '[{"type":"header","subtype":"h1","label":"Өтініш беруші туралы мәліметтер","access":false},{"type":"text","required":false,"label":"Аты","className":"form-control","name":"text-1581664657530","access":false,"subtype":"text"},{"type":"text","required":false,"label":"Тек","className":"form-control","name":"text-1581664658755","access":false,"subtype":"text"},{"type":"text","required":false,"label":"Әкесінің аты","className":"form-control","name":"text-1581664659530","access":false,"subtype":"text"},{"type":"text","subtype":"email","required":false,"label":"Электрондық пошта","className":"form-control","name":"text-1581664793640","access":false},{"type":"textarea","required":false,"label":"Мәтін аймағы","className":"form-control","name":"textarea-1581664892261","access":false,"subtype":"textarea"},{"type":"select","required":false,"label":"Жыныс","className":"form-control","name":"select-1581664822014","access":false,"multiple":false,"values":[{"label":"Еркек","value":"Еркек","selected":true},{"label":"Әйел","value":"Әйел"}]},{"type":"select","required":false,"label":"Отбасы жағдайы","className":"form-control","name":"select-1581664843572","access":false,"multiple":false,"values":[{"label":"Жалғыз","value":"Жалғыз","selected":true},{"label":"Үйленген","value":"Үйленген"},{"label":"Бөлінген","value":"Бөлінген"},{"label":"Ажырасқан","value":"Ажырасқан"}]},{"type":"text","required":false,"label":"Мекен-жайы","className":"form-control","name":"text-1581664871971","access":false,"subtype":"text"},{"type":"textarea","subtype":"textarea","required":false,"label":"Хабар","className":"form-control","name":"textarea-1581664904725","access":false}]');
        $formDatarU = ((!empty($application_form_ru->value)) ? $application_form_ru->fields : '[{"type":"header","subtype":"h1","label":"Данные заявителя","access":true},{"type":"text","required":false,"label":"Имя","className":"form-control","name":"text-1581418022702","access":false,"subtype":"text"},{"type":"text","required":false,"label":"Фамилия","className":"form-control","name":"text-1581418023518","access":false,"subtype":"text"},{"type":"text","required":false,"label":"Имя отчество","className":"form-control","name":"text-1581418024268","access":false,"subtype":"text"},{"type":"text","subtype":"email","required":false,"label":"Эл. адрес","className":"form-control","name":"text-1581418025571","access":false},{"type":"select","required":false,"label":"Пол","className":"form-control","name":"select-1581418066682","access":false,"multiple":false,"values":[{"label":"мужчина","value":"мужчина","selected":true},{"label":"женский","value":"женский"}]},{"type":"select","required":false,"label":"Семейный статус","className":"form-control","name":"select-1581418085445","access":false,"multiple":false,"values":[{"label":"не замужем","value":"не замужем","selected":true},{"label":"замужем","value":"замужем"},{"label":"отделено","value":"отделено"},{"label":"Разведенный","value":"Разведенный"}]},{"type":"text","required":false,"label":"контакт","className":"form-control","name":"text-1581418120439","access":false,"subtype":"text"},{"type":"textarea","required":false,"label":"Сообщение","className":"form-control","name":"textarea-1581418155985","access":false,"subtype":"textarea"},{"type":"textarea","required":false,"label":"Адрес резидента","className":"form-control","name":"textarea-1581418134913","access":false,"subtype":"textarea"}]');
        $formDataeN = ((!empty($application_form_en->value)) ? $application_form_en->fields : '[{"type":"header","subtype":"h3","label":"Applicant Data","access":true},{"type":"text","required":false,"label":"First Name","className":"form-control","name":"text-1581418022702","access":false,"subtype":"text"},{"type":"text","required":false,"label":"Last Name","className":"form-control","name":"text-1581418023518","access":false,"subtype":"text"},{"type":"text","required":false,"label":"Patronymiv name","className":"form-control","name":"text-1581418024268","access":false,"subtype":"text"},{"type":"text","subtype":"email","required":false,"label":"Email","className":"form-control","name":"text-1581418025571","access":false},{"type":"select","required":false,"label":"Gender","className":"form-control","name":"select-1581418066682","access":false,"multiple":false,"values":[{"label":"Male","value":"Male","selected":true},{"label":"Female","value":"Female"}]},{"type":"select","required":false,"label":"Marital Status","className":"form-control","name":"select-1581418085445","access":false,"multiple":false,"values":[{"label":"Single","value":"Single","selected":true},{"label":"Married","value":"Married"},{"label":"Seperated","value":"Seperated"},{"label":"Divorced","value":"Divorced"}]},{"type":"text","required":false,"label":"Contact","className":"form-control","name":"text-1581418120439","access":false,"subtype":"text"},{"type":"textarea","required":false,"label":"Message","className":"form-control","name":"textarea-1581418155985","access":false,"subtype":"textarea"},{"type":"textarea","required":false,"label":"Address of Resident","className":"form-control","name":"textarea-1581418134913","access":false,"subtype":"textarea"}]');

        $view = 'voyager::bread.edit-add';
        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'users','formDatakZ', 'formDataeN', 'formDatarU','application_form_kz'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Products::rules(), Products::rulesMessages());

        if ($validator->fails()) {
            if ($validator->fails()) {
                return back()->withErrors($validator);
            }
        }
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->has('_validate'))
        {
            if($request->hasFile('photos')) {
                $destinationPath = public_path('storage/');
                $dir = 'products';

                if (is_dir($destinationPath . $dir) == false) {
                    mkdir($destinationPath . $dir, 0777);
                }

                $filesystem = config('voyager.storage.disk');
                $realPath = Storage::disk($filesystem)->getDriver()->getAdapter()->getPathPrefix();

                $file = $request->logo->store('/products', $filesystem);
                \Intervention\Image\Facades\Image::make($realPath . $file)->save($realPath . $file);
            }


            $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

            $product = Products::find($data['id']);
            $product->milestone = $request->milestone;
            if(auth()->user()->role->slug == 'product-manager'){
                $product->is_verification_required = $request->is_verification_required;
            }

            $product->created_by = auth()->user()->id;
            $product->save();

            if(\auth()->user()->role->slug == 'product-manager') {

                $formDatakZ = (($request->kazakhFields != '[]') ? $request->kazakhFields : '[{"type":"header","subtype":"h1","label":"Өтініш беруші туралы мәліметтер","access":false},{"type":"text","required":false,"label":"Аты","className":"form-control","name":"text-1581664657530","access":false,"subtype":"text"},{"type":"text","required":false,"label":"Тек","className":"form-control","name":"text-1581664658755","access":false,"subtype":"text"},{"type":"text","required":false,"label":"Әкесінің аты","className":"form-control","name":"text-1581664659530","access":false,"subtype":"text"},{"type":"text","subtype":"email","required":false,"label":"Электрондық пошта","className":"form-control","name":"text-1581664793640","access":false},{"type":"textarea","required":false,"label":"Мәтін аймағы","className":"form-control","name":"textarea-1581664892261","access":false,"subtype":"textarea"},{"type":"select","required":false,"label":"Жыныс","className":"form-control","name":"select-1581664822014","access":false,"multiple":false,"values":[{"label":"Еркек","value":"Еркек","selected":true},{"label":"Әйел","value":"Әйел"}]},{"type":"select","required":false,"label":"Отбасы жағдайы","className":"form-control","name":"select-1581664843572","access":false,"multiple":false,"values":[{"label":"Жалғыз","value":"Жалғыз","selected":true},{"label":"Үйленген","value":"Үйленген"},{"label":"Бөлінген","value":"Бөлінген"},{"label":"Ажырасқан","value":"Ажырасқан"}]},{"type":"text","required":false,"label":"Мекен-жайы","className":"form-control","name":"text-1581664871971","access":false,"subtype":"text"},{"type":"textarea","subtype":"textarea","required":false,"label":"Хабар","className":"form-control","name":"textarea-1581664904725","access":false}]');
                $formDatarU = (($request->russianFields != '[]') ? $request->russianFields : '[{"type":"header","subtype":"h1","label":"Данные заявителя","access":true},{"type":"text","required":false,"label":"Имя","className":"form-control","name":"text-1581418022702","access":false,"subtype":"text"},{"type":"text","required":false,"label":"Фамилия","className":"form-control","name":"text-1581418023518","access":false,"subtype":"text"},{"type":"text","required":false,"label":"Имя отчество","className":"form-control","name":"text-1581418024268","access":false,"subtype":"text"},{"type":"text","subtype":"email","required":false,"label":"Эл. адрес","className":"form-control","name":"text-1581418025571","access":false},{"type":"select","required":false,"label":"Пол","className":"form-control","name":"select-1581418066682","access":false,"multiple":false,"values":[{"label":"мужчина","value":"мужчина","selected":true},{"label":"женский","value":"женский"}]},{"type":"select","required":false,"label":"Семейный статус","className":"form-control","name":"select-1581418085445","access":false,"multiple":false,"values":[{"label":"не замужем","value":"не замужем","selected":true},{"label":"замужем","value":"замужем"},{"label":"отделено","value":"отделено"},{"label":"Разведенный","value":"Разведенный"}]},{"type":"text","required":false,"label":"контакт","className":"form-control","name":"text-1581418120439","access":false,"subtype":"text"},{"type":"textarea","required":false,"label":"Сообщение","className":"form-control","name":"textarea-1581418155985","access":false,"subtype":"textarea"},{"type":"textarea","required":false,"label":"Адрес резидента","className":"form-control","name":"textarea-1581418134913","access":false,"subtype":"textarea"}]');
                $formDataeN = (($request->englishFields != '[]') ? $request->englishFields : '[{"type":"header","subtype":"h3","label":"Applicant Data","access":true},{"type":"text","required":false,"label":"First Name","className":"form-control","name":"text-1581418022702","access":false,"subtype":"text"},{"type":"text","required":false,"label":"Last Name","className":"form-control","name":"text-1581418023518","access":false,"subtype":"text"},{"type":"text","required":false,"label":"Patronymiv name","className":"form-control","name":"text-1581418024268","access":false,"subtype":"text"},{"type":"text","subtype":"email","required":false,"label":"Email","className":"form-control","name":"text-1581418025571","access":false},{"type":"select","required":false,"label":"Gender","className":"form-control","name":"select-1581418066682","access":false,"multiple":false,"values":[{"label":"Male","value":"Male","selected":true},{"label":"Female","value":"Female"}]},{"type":"select","required":false,"label":"Marital Status","className":"form-control","name":"select-1581418085445","access":false,"multiple":false,"values":[{"label":"Single","value":"Single","selected":true},{"label":"Married","value":"Married"},{"label":"Seperated","value":"Seperated"},{"label":"Divorced","value":"Divorced"}]},{"type":"text","required":false,"label":"Contact","className":"form-control","name":"text-1581418120439","access":false,"subtype":"text"},{"type":"textarea","required":false,"label":"Message","className":"form-control","name":"textarea-1581418155985","access":false,"subtype":"textarea"},{"type":"textarea","required":false,"label":"Address of Resident","className":"form-control","name":"textarea-1581418134913","access":false,"subtype":"textarea"}]');

                $ru = array();
                $rUData = json_decode($formDatarU,true);

                foreach($rUData as $rkey => $rVal){
                    if(isset($rVal['multiple']) && $rVal['multiple'] == false){
                        unset($rVal['multiple']);
                    }
                    $ru[] = $rVal;
                }
                $ruFields = json_encode($ru);

                $kz = array();
                $kzData = json_decode($formDatakZ,true);
                foreach($kzData as $kKey => $kVal){
                    if(isset($kVal['multiple']) && $kVal['multiple'] == false){
                        unset($kVal['multiple']);
                    }
                    $kz[] = $kVal;
                }
                $kzFields = json_encode($kz);

                $en = array();
                $enData = json_decode($formDataeN,true);
                foreach($enData as $key => $val){
                    if(isset($val['multiple']) && $val['multiple'] == false){
                        unset($val['multiple']);
                    }
                    $en[] = $val;
                }
                $enFields = json_encode($en);

                $applicationformModel = new ApplicationForms();
                $applicationForm = $applicationformModel->create([
                    'product_id' => $data['id'],
                    'fields' => $kzFields,
                ]);

                $il8en = Translations::create([
                    'foreign_key' => $applicationForm->id,
                    'table_name' => 'application_forms',
                    'column_name' => 'fields',
                    'value' => $enFields,
                    'locale' => 'en',
                ]);


                $il8ru = Translations::create([
                    'foreign_key' => $applicationForm->id,
                    'table_name' => 'application_forms',
                    'column_name' => 'fields',
                    'value' => $ruFields,
                    'locale' => 'ru',
                ]);

                if (isset($request->inputs)) {
                    foreach ($request->inputs as $key => $input) {
                        if (!empty($input) && !empty($request->checkpoint_step[$key])) {
                            $productCheckPoint = new ProductCheckPoint();
                            $productCheckPoint->product_id = $data['id'];
                            $productCheckPoint->checkpoint = $input;
                            $productCheckPoint->status = $request->checkpoint_step[$key];
                            $productCheckPoint->type = ProductCheckPoint::TEXT;
                            $productCheckPoint->save();
                        }
                    }
                }

                if (isset($request->documents)) {
                    foreach ($request->documents as $key => $document) {
                        if (!empty($document) && !empty($request->document_name[$key])) {
                            $image = $document;
                            $path = 'public/products/checkpoint/';
                            $fileName = time() . '.' . $image->getClientOriginalExtension();
                            $storeFileName = 'products/checkpoint/' . $fileName;
                            Storage::disk('local')->put($path . $fileName, file_get_contents($image));

                            $productCheckPoint = new ProductCheckPoint();
                            $productCheckPoint->product_id = $data['id'];
                            $productCheckPoint->checkpoint = $storeFileName;
                            $productCheckPoint->document_name = $request->document_name[$key];
                            $productCheckPoint->type = ProductCheckPoint::DOCUMENT;
                            $productCheckPoint->save();

                        }
                    }
                }
            }
            if ($request->ajax()) {
                return response()->json(['success' => true, 'data' => '']);
            }

            try{
                $actionLink = url('admin/login');
                $productManager = User::find($request->user_id);
                Mail::to($productManager->email)->send(new ProductMail($data,$productManager,$actionLink));

                return redirect()
                    ->route("voyager.{$dataType->slug}.index")
                    ->with([
                        'message'    => __('toaster.successfully add new product and invitation sent to product manager'),
                        'alert-type' => 'success',
                    ]);

            } catch (\Exception $ex) {
                Log::error($ex->getMessage());
                return redirect()
                    ->route("voyager.{$dataType->slug}.index")
                    ->with([
                        'message'    => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                        'alert-type' => 'success',
                    ]);
            }


        }
    }

    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? app($dataType->model_name)->findOrFail($id)
            : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $application_form_kz = ApplicationForms::where('product_id', $dataTypeContent->id)->first();

        if(!empty($application_form_kz)) {
            $application_form_en = Translations::where('foreign_key', $application_form_kz->id)
                ->where('table_name', 'application_forms')
                ->where('column_name', 'fields')
                ->where('locale', 'en')
                ->first();

            $application_form_ru = Translations::where('foreign_key', $application_form_kz->id)
                ->where('table_name', 'application_forms')
                ->where('column_name', 'fields')
                ->where('locale', 'ru')
                ->first();

        }
        else
        {
            $application_form_kz = '';
            $application_form_ru = '';
            $application_form_en = '';
        }

        $formDatakZ = ((!empty($application_form_kz->fields)) ? $application_form_kz->fields : '[{"type":"header","subtype":"h1","label":"Өтініш беруші туралы мәліметтер","access":false},{"type":"text","required":false,"label":"Аты","className":"form-control","name":"text-1581664657530","access":false,"subtype":"text"},{"type":"text","required":false,"label":"Тек","className":"form-control","name":"text-1581664658755","access":false,"subtype":"text"},{"type":"text","required":false,"label":"Әкесінің аты","className":"form-control","name":"text-1581664659530","access":false,"subtype":"text"},{"type":"text","subtype":"email","required":false,"label":"Электрондық пошта","className":"form-control","name":"text-1581664793640","access":false},{"type":"textarea","required":false,"label":"Мәтін аймағы","className":"form-control","name":"textarea-1581664892261","access":false,"subtype":"textarea"},{"type":"select","required":false,"label":"Жыныс","className":"form-control","name":"select-1581664822014","access":false,"multiple":false,"values":[{"label":"Еркек","value":"Еркек","selected":true},{"label":"Әйел","value":"Әйел"}]},{"type":"select","required":false,"label":"Отбасы жағдайы","className":"form-control","name":"select-1581664843572","access":false,"multiple":false,"values":[{"label":"Жалғыз","value":"Жалғыз","selected":true},{"label":"Үйленген","value":"Үйленген"},{"label":"Бөлінген","value":"Бөлінген"},{"label":"Ажырасқан","value":"Ажырасқан"}]},{"type":"text","required":false,"label":"Мекен-жайы","className":"form-control","name":"text-1581664871971","access":false,"subtype":"text"},{"type":"textarea","subtype":"textarea","required":false,"label":"Хабар","className":"form-control","name":"textarea-1581664904725","access":false}]');
        $formDatarU = ((!empty($application_form_ru->value)) ? $application_form_ru->value : '[{"type":"header","subtype":"h1","label":"Данные заявителя","access":true},{"type":"text","required":false,"label":"Имя","className":"form-control","name":"text-1581418022702","access":false,"subtype":"text"},{"type":"text","required":false,"label":"Фамилия","className":"form-control","name":"text-1581418023518","access":false,"subtype":"text"},{"type":"text","required":false,"label":"Имя отчество","className":"form-control","name":"text-1581418024268","access":false,"subtype":"text"},{"type":"text","subtype":"email","required":false,"label":"Эл. адрес","className":"form-control","name":"text-1581418025571","access":false},{"type":"select","required":false,"label":"Пол","className":"form-control","name":"select-1581418066682","access":false,"multiple":false,"values":[{"label":"мужчина","value":"мужчина","selected":true},{"label":"женский","value":"женский"}]},{"type":"select","required":false,"label":"Семейный статус","className":"form-control","name":"select-1581418085445","access":false,"multiple":false,"values":[{"label":"не замужем","value":"не замужем","selected":true},{"label":"замужем","value":"замужем"},{"label":"отделено","value":"отделено"},{"label":"Разведенный","value":"Разведенный"}]},{"type":"text","required":false,"label":"контакт","className":"form-control","name":"text-1581418120439","access":false,"subtype":"text"},{"type":"textarea","required":false,"label":"Сообщение","className":"form-control","name":"textarea-1581418155985","access":false,"subtype":"textarea"},{"type":"textarea","required":false,"label":"Адрес резидента","className":"form-control","name":"textarea-1581418134913","access":false,"subtype":"textarea"}]');
        $formDataeN = ((!empty($application_form_en->value)) ? $application_form_en->value : '[{"type":"header","subtype":"h3","label":"Applicant Data","access":true},{"type":"text","required":false,"label":"First Name","className":"form-control","name":"text-1581418022702","access":false,"subtype":"text"},{"type":"text","required":false,"label":"Last Name","className":"form-control","name":"text-1581418023518","access":false,"subtype":"text"},{"type":"text","required":false,"label":"Patronymiv name","className":"form-control","name":"text-1581418024268","access":false,"subtype":"text"},{"type":"text","subtype":"email","required":false,"label":"Email","className":"form-control","name":"text-1581418025571","access":false},{"type":"select","required":false,"label":"Gender","className":"form-control","name":"select-1581418066682","access":false,"multiple":false,"values":[{"label":"Male","value":"Male","selected":true},{"label":"Female","value":"Female"}]},{"type":"select","required":false,"label":"Marital Status","className":"form-control","name":"select-1581418085445","access":false,"multiple":false,"values":[{"label":"Single","value":"Single","selected":true},{"label":"Married","value":"Married"},{"label":"Seperated","value":"Seperated"},{"label":"Divorced","value":"Divorced"}]},{"type":"text","required":false,"label":"Contact","className":"form-control","name":"text-1581418120439","access":false,"subtype":"text"},{"type":"textarea","required":false,"label":"Message","className":"form-control","name":"textarea-1581418155985","access":false,"subtype":"textarea"},{"type":"textarea","required":false,"label":"Address of Resident","className":"form-control","name":"textarea-1581418134913","access":false,"subtype":"textarea"}]');

        $view = 'voyager::bread.edit-add';
        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'formDatakZ', 'formDataeN', 'formDatarU', 'application_form_kz','id'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'productCategory_id' => 'required',
            'subsidiary_id' => 'required',
        ],[
            'email.required' => 'Please enter Title',
            'productCategory_id.required' => 'Please enter Product Category',
            'subsidiary_id.required' => 'Please enter Subsidiary',
        ]);

        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $request->id instanceof Model ? $request->id->{$request->id->getKeyName()} : $request->id;

        $data = call_user_func([$dataType->model_name, 'findOrFail'], $request->id);

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $request->id);

        $product = Products::find($data['id']);
        $product->milestone = $request->milestone;
        if(auth()->user()->role->slug == 'product-manager'){
            $product->is_verification_required = $request->is_verification_required;
        }

        $product->save();

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->has('_validate'))
        {
            if(isset($request->logo))
            {
                $filesystem = config('voyager.storage.disk');
                $realPath = Storage::disk($filesystem)->getDriver()->getAdapter()->getPathPrefix();

                $file = $request->logo->store('/products', $filesystem);
                \Intervention\Image\Facades\Image::make($realPath.$file)->save($realPath.$file);
            }

            $data = $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

            if(\auth()->user()->role->slug == 'product-manager'){

                $formDatakZ = (($request->kazakhFields != '[]') ? $request->kazakhFields : '[{"type":"header","subtype":"h1","label":"Өтініш беруші туралы мәліметтер","access":false},{"type":"text","required":false,"label":"Аты","className":"form-control","name":"text-1581664657530","access":false,"subtype":"text"},{"type":"text","required":false,"label":"Тек","className":"form-control","name":"text-1581664658755","access":false,"subtype":"text"},{"type":"text","required":false,"label":"Әкесінің аты","className":"form-control","name":"text-1581664659530","access":false,"subtype":"text"},{"type":"text","subtype":"email","required":false,"label":"Электрондық пошта","className":"form-control","name":"text-1581664793640","access":false},{"type":"textarea","required":false,"label":"Мәтін аймағы","className":"form-control","name":"textarea-1581664892261","access":false,"subtype":"textarea"},{"type":"select","required":false,"label":"Жыныс","className":"form-control","name":"select-1581664822014","access":false,"multiple":false,"values":[{"label":"Еркек","value":"Еркек","selected":true},{"label":"Әйел","value":"Әйел"}]},{"type":"select","required":false,"label":"Отбасы жағдайы","className":"form-control","name":"select-1581664843572","access":false,"multiple":false,"values":[{"label":"Жалғыз","value":"Жалғыз","selected":true},{"label":"Үйленген","value":"Үйленген"},{"label":"Бөлінген","value":"Бөлінген"},{"label":"Ажырасқан","value":"Ажырасқан"}]},{"type":"text","required":false,"label":"Мекен-жайы","className":"form-control","name":"text-1581664871971","access":false,"subtype":"text"},{"type":"textarea","subtype":"textarea","required":false,"label":"Хабар","className":"form-control","name":"textarea-1581664904725","access":false}]');
                $formDatarU = (($request->russianFields != '[]') ? $request->russianFields : '[{"type":"header","subtype":"h1","label":"Данные заявителя","access":true},{"type":"text","required":false,"label":"Имя","className":"form-control","name":"text-1581418022702","access":false,"subtype":"text"},{"type":"text","required":false,"label":"Фамилия","className":"form-control","name":"text-1581418023518","access":false,"subtype":"text"},{"type":"text","required":false,"label":"Имя отчество","className":"form-control","name":"text-1581418024268","access":false,"subtype":"text"},{"type":"text","subtype":"email","required":false,"label":"Эл. адрес","className":"form-control","name":"text-1581418025571","access":false},{"type":"select","required":false,"label":"Пол","className":"form-control","name":"select-1581418066682","access":false,"multiple":false,"values":[{"label":"мужчина","value":"мужчина","selected":true},{"label":"женский","value":"женский"}]},{"type":"select","required":false,"label":"Семейный статус","className":"form-control","name":"select-1581418085445","access":false,"multiple":false,"values":[{"label":"не замужем","value":"не замужем","selected":true},{"label":"замужем","value":"замужем"},{"label":"отделено","value":"отделено"},{"label":"Разведенный","value":"Разведенный"}]},{"type":"text","required":false,"label":"контакт","className":"form-control","name":"text-1581418120439","access":false,"subtype":"text"},{"type":"textarea","required":false,"label":"Сообщение","className":"form-control","name":"textarea-1581418155985","access":false,"subtype":"textarea"},{"type":"textarea","required":false,"label":"Адрес резидента","className":"form-control","name":"textarea-1581418134913","access":false,"subtype":"textarea"}]');
                $formDataeN = (($request->englishFields != '[]') ? $request->englishFields : '[{"type":"header","subtype":"h3","label":"Applicant Data","access":true},{"type":"text","required":false,"label":"First Name","className":"form-control","name":"text-1581418022702","access":false,"subtype":"text"},{"type":"text","required":false,"label":"Last Name","className":"form-control","name":"text-1581418023518","access":false,"subtype":"text"},{"type":"text","required":false,"label":"Patronymiv name","className":"form-control","name":"text-1581418024268","access":false,"subtype":"text"},{"type":"text","subtype":"email","required":false,"label":"Email","className":"form-control","name":"text-1581418025571","access":false},{"type":"select","required":false,"label":"Gender","className":"form-control","name":"select-1581418066682","access":false,"multiple":false,"values":[{"label":"Male","value":"Male","selected":true},{"label":"Female","value":"Female"}]},{"type":"select","required":false,"label":"Marital Status","className":"form-control","name":"select-1581418085445","access":false,"multiple":false,"values":[{"label":"Single","value":"Single","selected":true},{"label":"Married","value":"Married"},{"label":"Seperated","value":"Seperated"},{"label":"Divorced","value":"Divorced"}]},{"type":"text","required":false,"label":"Contact","className":"form-control","name":"text-1581418120439","access":false,"subtype":"text"},{"type":"textarea","required":false,"label":"Message","className":"form-control","name":"textarea-1581418155985","access":false,"subtype":"textarea"},{"type":"textarea","required":false,"label":"Address of Resident","className":"form-control","name":"textarea-1581418134913","access":false,"subtype":"textarea"}]');

                $ru = array();
                $rUData = json_decode($formDatarU,true);

                foreach($rUData as $rkey => $rVal){
                    if(isset($rVal['multiple']) && $rVal['multiple'] == false){
                        unset($rVal['multiple']);
                    }
                    $ru[] = $rVal;
                }
                $ruFields = json_encode($ru);

                $kz = array();
                $kzData = json_decode($formDatakZ,true);
                foreach($kzData as $kKey => $kVal){
                    if(isset($kVal['multiple']) && $kVal['multiple'] == false){
                        unset($kVal['multiple']);
                    }
                    $kz[] = $kVal;
                }
                $kzFields = json_encode($kz);

                $en = array();
                $enData = json_decode($formDataeN,true);
                foreach($enData as $key => $val){
                    if(isset($val['multiple']) && $val['multiple'] == false){
                        unset($val['multiple']);
                    }
                    $en[] = $val;
                }
                $enFields = json_encode($en);

                $applicationForms = ApplicationForms::updateOrCreate([
                    'product_id' => $data['id'],
                ],[
                    'fields' => $kzFields,
                ]);

                $il8en =  Translations::updateOrCreate([
                    'table_name' => 'application_forms',
                    'foreign_key' => $applicationForms->id,
                    'column_name' => 'fields',
                    'locale' => 'en',
                ],[
                    'value' => $enFields,
                ]);

                $il8ru =  Translations::updateOrCreate([
                    'table_name' => 'application_forms',
                    'foreign_key' => $applicationForms->id,
                    'column_name' => 'fields',
                    'locale' => 'ru',
                ],[
                    'value' => $ruFields,
                ]);

                $productCheckPoints = ProductCheckPoint::where('product_id',$data['id'])->get();
                $productCheckPoints->map(function ($productCheckPoint) {
                    if($productCheckPoint->type == ProductCheckPoint::TEXT){
                        $productCheckPoint->delete();
                    }
                });

                if(isset($request->inputs)) {
                    foreach ($request->inputs as $key => $input) {
                        if (!empty($input) && !empty($request->checkpoint_step[$key])) {
                            $productCheckPoint = new ProductCheckPoint();
                            $productCheckPoint->product_id = $data['id'];
                            $productCheckPoint->checkpoint = $input;
                            $productCheckPoint->status = $request->checkpoint_step[$key];
                            $productCheckPoint->type = ProductCheckPoint::TEXT;
                            $productCheckPoint->save();
                        }
                    }
                }

                if(isset($request->documents)) {
                    foreach ($request->documents as $key => $document) {
                        if (!empty($document) && !empty($request->document_name[$key])) {
                            $image = $document;
                            $path = 'public/products/checkpoint/';
                            $fileName = time() . '.' . $image->getClientOriginalExtension();
                            $storeFileName = 'products/checkpoint/'.$fileName;
                            Storage::disk('local')->put($path . $fileName, file_get_contents($image));

                            $productCheckPoint = new ProductCheckPoint();
                            $productCheckPoint->product_id = $data['id'];
                            $productCheckPoint->checkpoint = $storeFileName;
                            $productCheckPoint->document_name = $request->document_name[$key];
                            $productCheckPoint->type = ProductCheckPoint::DOCUMENT;
                            $productCheckPoint->save();
                        }
                    }
                }
            }

            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                    'message'    => __('toaster.successfully updated product'),
                    'alert-type' => 'success',
                ]);
        }
    }

    public function show(Request $request,$id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }


        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check permission
        $this->authorize('read', app($dataType->model_name));

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.read';

        if (view()->exists("voyager::$slug.read")) {
            $view = "voyager::$slug.read";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    public function destroy(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        // Init array of IDs
        $ids = [];
        $relatedProductIds = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }

        foreach ($ids as $id) {
            $relatedProductData = Products::where('id', '=',$id)->first();
            if ($relatedProductData != null) {
                $relatedProductIds[] = $relatedProductData->id;
            }
        }

        $displayName = count($ids) > 1 ? "These ".$dataType->display_name_plural : "This ".$dataType->display_name_singular;

        if (count($relatedProductIds) == 0) {
            $relatedProducts = [];

            foreach ($relatedProductIds as $id) {
                $data = Products::where('id', $id)->first();
                $relatedProducts[] = $data->title;
            }

            $data = [
                'message'    => "You cannot Delete "."{$displayName}",
                'alert-type' => 'error',
            ];
        }
        else {
            foreach ($ids as $id) {
                $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
                $this->cleanup($dataType, $data);

                $deleteProductFromApplication = ApplicationForms::where('product_id',$id)->first();
                $deleteProduct = (!empty($deleteProductFromApplication) ? ApplicationForms::where('product_id',$id)->delete() : '');

                $product = Products::where('id',$id)->first();

                $destinationPath = public_path('storage/products');
                \Illuminate\Support\Facades\File::delete($destinationPath . '/'. $product->image);
            }

            $res = $data->destroy($ids);
            $data = $res
                ? [
                    'message'    => __('toaster.successfully deleted this product'),
                    'alert-type' => 'success',
                ]
                : [
                    'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                    'alert-type' => 'error',
                ];


        }

        return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
    }

    /**
     * Remove translations, images and files related to a BREAD item.
     *
     * @param \Illuminate\Database\Eloquent\Model $dataType
     * @param \Illuminate\Database\Eloquent\Model $data
     *
     * @return void
     */
    protected function cleanup($dataType, $data)
    {
        // Delete Translations, if present
        if (is_bread_translatable($data)) {
            $data->deleteAttributeTranslations($data->getTranslatableAttributes());
        }

        // Delete Images
        $this->deleteBreadImages($data, $dataType->deleteRows->where('type', 'image'));

        // Delete Files
        foreach ($dataType->deleteRows->where('type', 'file') as $row) {
            if (isset($data->{$row->field})) {
                foreach (json_decode($data->{$row->field}) as $file) {
                    $this->deleteFileIfExists($file->download_link);
                }
            }
        }
    }

    /**
     * Delete all images related to a BREAD item.
     *
     * @param \Illuminate\Database\Eloquent\Model $data
     * @param \Illuminate\Database\Eloquent\Model $rows
     *
     * @return void
     */
    public function deleteBreadImages($data, $rows)
    {
        foreach ($rows as $row) {
            if ($data->{$row->field} != config('voyager.user.default_avatar')) {
                $this->deleteFileIfExists($data->{$row->field});
            }

            if (isset($row->details->thumbnails)) {
                foreach ($row->details->thumbnails as $thumbnail) {
                    $ext = explode('.', $data->{$row->field});
                    $extension = '.'.$ext[count($ext) - 1];

                    $path = str_replace($extension, '', $data->{$row->field});

                    $thumb_name = $thumbnail->name;

                    $this->deleteFileIfExists($path.'-'.$thumb_name.$extension);
                }
            }
        }

        if ($rows->count() > 0) {
            event(new BreadImagesDeleted($data, $rows));
        }
    }

    public function saveGeneralInfo(Request $request)
    {
        if ($request->ajax())
        {
            $file = 'generalInfo.json';

            \Illuminate\Support\Facades\File::delete(base_path($file));

            $contents = $request['form_fields'];
            $response = \Illuminate\Support\Facades\File::put(base_path($file),$contents);

            return response()->json(['success' => true, 'response' => $response]);
        }
    }

    public function getLabelTranslations(Request $request)
    {
        if ($request->ajax())
        {
            Lang::setLocale($request->lang);
            $content = Lang::get('fields');

            return response()->json(['content' => $content]);
        }
    }

    public function getLangTranslations(Request $request,$lang)
    {
        $langSub = substr($lang,0,2);
        if(file_exists(base_path('resources/lang/'.$langSub.'/'.$lang))) {
            $contents = file_get_contents(base_path('resources/lang/'.$langSub.'/'.$lang));
        } else {
            $contents = '';
        }

        return $contents;
    }

    public function removeCheckpointDocument(Request $request)
    {
        if ($request->ajax())
        {
           if(!$productCheckpoint = ProductCheckPoint::find($request->id)){
               return response()->json([
                  'status' => false
               ]);
           }

           $destinationPath = public_path('storage/'.$productCheckpoint->checkpoint);

           if(file_exists($destinationPath)){
               \Illuminate\Support\Facades\File::delete($destinationPath);
           }
            $productCheckpoint->delete();
            return response()->json([
                'status' => true
            ]);
        }
    }
    public function browse(Request $request)
    {
        $role = \auth()->user()->role->slug;

        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $subsidiaryUser = SubsidiaryUsers::select('subsidiary_id')->where('user_id',\auth()->user()->id)->first();

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

//            $query = $model::select('products.*','product_categories.title as productCategory_id','subsidiaries.title as subsidiary_id')
//                ->join('product_categories', 'products.productCategory_id', '=', 'product_categories.id','left')
//                ->join('subsidiaries', 'products.subsidiary_id', '=', 'subsidiaries.id','left')
//                ->when($role, function ($query, $role) use($subsidiaryUser){
//                    if($role == 'product-manager'){
//                        $query->where('user_id',\auth()->user()->id);
//                    }
////                    if($role == 'supervisor'){
////                        $query->where('subsidiary_id',$subsidiaryUser);
////                    }
//                });
            $query = $model::with('subsidiaryId','productCategoryId','userId')
                            ->when($role, function ($query, $role) use($subsidiaryUser){
                                if($role == 'product-manager'){
                                    $query->where('user_id',\auth()->user()->id);
                                }
            //                    if($role == 'supervisor'){
            //                        $query->where('subsidiary_id',$subsidiaryUser);
            //                    }
                            })->orderBy('id','desc');

//            $query = $model::select('*')->orderBy('id','desc');

            $slug = str_replace('-', '_', $slug);

//            dd($query->get());
            return DataTables::of($query)
                ->editColumn('id', function($query) use ($slug) {
                    if(auth()->user()->hasPermission('delete_'.$slug)) {
                        return '<input type="checkbox" name="row_id" id="checkbox_'.$query->id.'" value="'.$query->id.'">';
                    } else {
                        return '';
                    }
                })->editColumn('subsidiary_id', function($query) use ($slug) {
                    return $query->subsidiaryId->title;
                })
                ->editColumn('productCategory_id', function($query) use ($slug) {
                    return $query->productCategoryId->title;
                })
                ->editColumn('user_id', function($query) use ($slug) {
                    return $query->userId->title;
                })
                ->editColumn('logo', function($query) use ($slug) {
                    return '<img src="'.asset('storage/'.$query->logo).'" style="width:100px">';
                })
                ->editColumn('productCategory_id', function($query){
                    return $query->productCategoryId->title;
                })
                ->editColumn('subsidiary_id', function($query){
                    return $query->subsidiaryId->title;
                })
                ->editColumn('action', function($query) use ($dataType, $slug) {
                    return Helper::getGeneralActions($dataType, $query, $slug);
                })
                ->editColumn('created_at', function($query) use ($dataType) {
                    return Helper::getFormattedDate($query->created_at);
                })
                ->rawColumns(['logo', 'id', 'subsidiary_id',  'productCategory_id', 'user_id', 'created_at','action'])
                ->make(true);

        } else {
            abort('404');
        }
    }
}
