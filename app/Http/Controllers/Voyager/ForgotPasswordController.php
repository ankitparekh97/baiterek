<?php

namespace App\Http\Controllers\Voyager;

use App\Helper\Helper;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class ForgotPasswordController extends \TCG\Voyager\Http\Controllers\Controller
{
    public function index(){

    }

    public function forgotPassword(){
        return view('auth.forgotpassword');
    }

    public function emailResetPasswordLink(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
        ]);

        if(!$user = User::where('email',$request->email)->first()){
            return redirect(route('forgotpassword'))->with('error', trans('validation.email not found'));
        } else {

            if(is_null($user->remember_token)) {
                $user->remember_token = Helper::generateRememberToken($user->email);
                $user->save();
            }

            $passwordResetModel = new \App\Models\PasswordReset();
            $passwordResetModel->email = $user->email;
            $passwordResetModel->token = $user->remember_token;
            $passwordResetModel->save();
        }

        $email = $user->email;
        $resetPasswordLink = url('admin/reset-password-form',['token' => $user->remember_token]);

        if(!empty($email)){
            Mail::to($email)->send(new \App\Mail\Forgotpassword($user->name,$user->remember_token,$resetPasswordLink));
            if( count(Mail::failures()) == 0 ) {
                return redirect(route('forgotpassword'))->with('success', trans('validation.reset password link send'));
            }
        }
    }

    public function resetPasswordForm($token)
    {
        $user = \App\Models\PasswordReset::where('token',$token)->first();
        if(!$user){
            return redirect(url('/login'))->with('error',trans('validation.link expire'));
        }
        return view('auth.passwords.reset',compact('token'));
    }

    public function savePassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'min:6|required',
            'confirm_password' => 'min:6|required_with:password|same:password'
        ]);

        $user = \App\Models\PasswordReset::where('token',$request->token)->first();
        if ($user) {
            // Update password
            $password = $request->password;

            $users = User::where('email',$user->email)->first();
            $users->setPasswordAttribute($password);
            $users->save();

            $passwordRestModel = \App\Models\PasswordReset::where('email',$user->email)->first();

            // Delete the row from password reset table
            $passwordRestModel->where('email',$user->email)->forceDelete();
            return redirect(url('/admin/login'))->with('success','New password set successfully. You can now login here to access system.');

        } else {
            // Redirect forgot password page with error message
            return redirect(url('/admin/login'))->with('error','This link is no longer available.');
        }

    }

}
