<?php

namespace App\Http\Controllers\Voyager;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Image;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use TCG\Voyager\Voyager;
use Yajra\DataTables\Facades\DataTables;

class ProfileController extends \TCG\Voyager\Http\Controllers\Controller {

    use BreadRelationshipParser;

    public function index(Request $request) {
        $dataType = \TCG\Voyager\Facades\Voyager::model('DataType')->where('slug', '=', "users")->first();

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? app($dataType->model_name)->findOrFail(Auth::user()->id)
            : DB::table($dataType->name)->where('id', Auth::user()->id)->first(); // If Model doest exist, get data from table name

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        return \TCG\Voyager\Facades\Voyager::view('vendor.voyager.user-profile.profile', compact('dataType', 'dataTypeContent'));
    }

    public function editProfile(Request $request, $id = 0)
    {
        if($id == 0){
            $id = \auth()->id();
        }
        $dataType = \TCG\Voyager\Facades\Voyager::model('DataType')->where('slug', '=', "users")->first();

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? app($dataType->model_name)->findOrFail(Auth::user()->id)
            : DB::table($dataType->name)->where('id', Auth::user()->id)->first(); // If Model doest exist, get data from table name

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        return \TCG\Voyager\Facades\Voyager::view('vendor.voyager.user-profile.profile', compact('dataType', 'dataTypeContent'));
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), User::accessRule(Auth::user()->id));
        if(!empty($request->get('password')) || !empty($request->get('confirm_password'))) {
            $validator = Validator::make($request->all(), User::passwordRule(), User::passwordRuleMessages());
        }

        if ($validator->fails()) {
            if ($validator->fails()) {
                return back()->withErrors($validator);
            }
        }

//        if(!empty($request->get('password')) || !empty($request->get('confirm_password')))
//        {
//            if (empty($request->get('confirm_password'))) {
//                // The passwords matches
//                return back()->with("error", "CONFIRM PASSWORD is required.");
//            }
//
//            if ((Hash::check($request->get('password'), Auth::user()->password))) {
//                // The passwords matches
//                return back()->with("error", "Your new password should not be same as your existing password.");
//            }
//
//            if (strcmp($request->get('confirm_password'), $request->get('password')) != 0) {
//                //Current password and new password are same
//                return back()->with("error", "PASSWORD and CONFIRM PASSWORD should be same.");
//            }
//        }

        $slug = "users";
        $dataType = \TCG\Voyager\Facades\Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $request->user_id instanceof Model ? $request->user_id->{$request->user_id->getKeyName()} : $request->user_id;

        $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->ajax()) {
            $update = $this->insertUpdateData($request, $slug, $dataType->editRows, $data);
            $update->is_active = 1;
            if (isset($request->password)) {
                $update->setPasswordAttribute($request->password);
            }
            $update->save();

            $dataTypeContent = (strlen($dataType->model_name) != 0)
                ? app($dataType->model_name)->findOrFail(Auth::user()->id)
                : DB::table($dataType->name)->where('id', Auth::user()->id)->first(); // If Model doest exist, get data from table name

            return redirect()
                ->route("voyager.profiles", compact('dataType', 'dataTypeContent'))
                ->with([
                    'message'    => __('toaster.your profile has been updated successfully'),
                    'alert-type' => 'success',
                ]);
        }
    }

    public function imageUpload(Request $request) {
        try {
            $x = (int)$request->get('x');
            $y = (int)$request->get('y');
            $height = (int)$request->get('height');
            $width = (int)$request->get('width');

            $filesystem = config('voyager.storage.disk');

            $realPath = Storage::disk($filesystem)->getDriver()->getAdapter()->getPathPrefix();

            $file = $request->avatar->store('/users'.'/'.date('FY'), $filesystem);

            \Intervention\Image\Facades\Image::make($realPath.$file)->crop($width, $height, $x, $y)->save($realPath.$file);

            $user = User::find($request->user_id);
            $user->avatar = $file;
            $user->save();

            $success = true;
            $message = "Profile picture updated successfully";
            $path = \TCG\Voyager\Facades\Voyager::image(preg_replace('/^public\//', '', $file));
        }
        catch (\Exception $e) {
            $success = false;
            $message = $e->getMessage();
            $path = '';
        }

        return response()->json(compact('success', 'message', 'path'));
    }

    public function migratedDocuments()
    {
        $dataType = (Object) [
            'icon' => 'voyager-file-text',
            'display_name_plural' => 'Documents',
            'description' => '',
            'model_name' => 'App\Models\MigratedDocument',
            'slug' => 'my-documents',
            'order_column' => '',
            'order_display_column' => '',
        ];
        return \TCG\Voyager\Facades\Voyager::view('vendor.voyager.user-profile.documents', compact('dataType'));
    }

    public function migratedDocumentsbrowse(Request $request){
        app()->setLocale($request->lang);
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);
        $model = 'App\Models\MigratedDocument';
        $query = $model::select('*');
        if(\auth()->user()->role->slug != 'admin'){
            $query = $query->where('user_id',\auth()->user()->id);
        }
        $query = $query->with('user','serviceInquiry')->orderBy('id','desc');
        $table = Datatables::of($query->orderBy('id','desc'));

        $table = $table
            ->editColumn('user_id', function($query){
                $name = $query->old_userID;
                if(!empty($query->user)){
                    $name = $query->user->name .' '. $query->user->last_name;
                }
                return $name;
            })
            ->editColumn('service_inquiry_id', function($query){
                $html = $query->serviceInquiry->product->title ?? null;
                return $html;
            })
            ->editColumn('path', function($query){
                $path = 'MigratedDocuments/' .$query->path;
                $html = '<a style="text-decoration: none;" class="btn btn-success" href="' . route('document.download') . '?path='.$path.'"><i class="voyager-download"></i></a>';
                return $html;
            })
            ->editColumn('created_at', function($query){
                return $query->created_at->toDayDateTimeString();
            })
            ->addColumn('action', function($query) {
                $html = '';
                return $html;
            })
            ->rawColumns(['user_id', 'path', 'action']);
        return $table->make(true);
    }
}
