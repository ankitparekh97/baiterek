<?php

namespace App\Http\Controllers;
use App\Models\ProductCategories;
use App\Models\ProductCheckPoint;
use App\Models\Products;
use App\Models\Projects;
use App\Models\ServiceInquiries;
use App\Models\ServiceInquiriesDocuments;
use App\Models\ServiceInquiryCheckPoint;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

abstract class Services extends \App\Http\Controllers\Controller
{
    public static function getAllProductCategories($offset=null,$term=null){
        $productCategories = ProductCategories::orderBy('id', 'desc')
            ->when($offset, function ($query, $offset) {
                $query->limit($offset);
            })
            ->when($term, function ($query, $term) {
                $query->whereTranslation('title','LIKE','%'.$term.'%', [app()->getLocale()]);
            })
            ->get()
            ->translate('locale', app()->getLocale());

        if(count($productCategories)>0){
            return $productCategories;
        } else {
            return '';
        }
    }

    public static function getAllProducts($title=null,$category=null){
        $products = Products::when($title, function ($query, $title) {
                                $query->whereTranslation('title','LIKE','%'.$title.'%', [app()->getLocale()]);
                            })
                           ->when($category, function ($query, $category) {
                                $query->where('productCategory_id',$category);
                            })
                            ->orderBy('id', 'desc')
                            ->get()->translate('locale', app()->getLocale());

        if(count($products)>0){
            return $products;
        } else {
            return '';
        }
    }

    public static function getProductsDetail($id){
        $product = Products::with('subsidiaryId')->where('id', $id)->first();
        if(!empty($product)){
            return $product->translate('locale', app()->getLocale());
        } else {
            abort(404);
        }
    }

    public static function getProjectsByProduct($id){
        $product = Projects::with('product')->where('product_id', $id)->where('status','ACTIVE')->get();
        if(count($product)>0){
            return $product->translate('locale', app()->getLocale());
        } else {
            return array();
        }
    }

    public static function getProjectDetail($id){
        $projectDetail = Projects::with('product')
                                ->where('id', $id)
                                ->where('status','ACTIVE')
                                ->first();
        if(!empty($projectDetail)){
            return $projectDetail->translate('locale', app()->getLocale());
        } else {
            return '';
        }
    }

    public static function getCategoryDetail($id){
        $ProductCategory = ProductCategories::where('id', $id)->first()->translate('locale', app()->getLocale());
        if(!empty($ProductCategory)){
            return $ProductCategory;
        } else {
            return '';
        }
    }

    public static function submitInquiry($data)
    {
        try {
            $serviceInquiriesModel = new ServiceInquiries();
            $serviceInquiries = $serviceInquiriesModel->create([
                // 'name' => $data['name'] ,
                // 'last_name' => $data['last_name'],
                // 'email' => $data['email'],
                // 'email_of_recipient' => $data['email_of_recipient'],
                // 'gender' => $data['gender'],
                // 'address' => $data['address'],
                // 'marital_status' => $data['marital_status'],
                // 'application_type' => $data['application_type'],
                // 'message' => $data['message'],
                // 'bin' => $data['bin'],
                // 'contact' => $data['contact'],
                // 'patronymiv_name' => $data['patronymiv_name'],
                'fields' => $data['formFields'],
                'locale' => app()->getLocale(),
                'due_date' => (isset($data['due_date'])) ? $data['due_date'] : null,
                'application_form_id' => $data['application_form_id'],
                'application_status' => config('baiterek.applicationStatus.New'),
                'user_id' => Auth::user()->id,
                'product_id' => $data['product_id'],
                'status_of_request' => $data['status_of_request'],
            ]);
            $request_id = str_pad($serviceInquiries->id, 6, 0, STR_PAD_LEFT) . '-' . Carbon::now()->format('d-m-y');
            $serviceInquiries->request_id = $request_id;
            $serviceInquiries->save();
           return $serviceInquiries->id;

        } catch (\Exception $ex){
            Log::error('Submit Request Issue' .$ex->getMessage());
            return false;
        }
    }

    public static function updateInquiry($id, $data)
    {
        try {
            $serviceInquiries = ServiceInquiries::find($id);
            $serviceInquiries->fields = $data['formFields'];
            $serviceInquiries->locale = app()->getLocale();
            $serviceInquiries->due_date = (isset($data['due_date'])) ? $data['due_date'] : null;
            $serviceInquiries->application_form_id = $data['application_form_id'];
            $serviceInquiries->application_status = config('baiterek.applicationStatus.New');
            $serviceInquiries->user_id = Auth::user()->id;
            $serviceInquiries->product_id = $data['product_id'];
            $serviceInquiries->status_of_request = $data['status_of_request'];
            $serviceInquiries->save();
            return $serviceInquiries->id;

        } catch (\Exception $ex){
            Log::error('Update Request Issue' .$ex->getMessage());
            return false;
        }
    }

    public static function submitInquiryDocuments($data)
    {
        try {
            $serviceInquiriesDocumentsModel = new ServiceInquiriesDocuments();
            $serviceInquiriesDocuments = $serviceInquiriesDocumentsModel->create([
                'document' => $data['document'],
                'service_inquiries_id' => $data['service_inquiries_id'],
            ]);
            return $serviceInquiriesDocuments->id;

        } catch (\Exception $ex){
            Log::error('Document Save Request Issue' .$ex->getMessage());
            return false;
        }
    }

    public static function updateInquiryDocuments($id, $data)
    {
        try {
            $serviceInquiriesDocuments = ServiceInquiriesDocuments::find($id);
            $serviceInquiriesDocuments->document = $data['document'];
            $serviceInquiriesDocuments->service_inquiries_id = $data['service_inquiries_id'];
            $serviceInquiriesDocuments->save();
            return $serviceInquiriesDocuments->id;

        } catch (\Exception $ex){
            Log::error('Document Save Request Issue' .$ex->getMessage());
            return false;
        }
    }

    public static function getProductsByCategory($id=null){
        $products = Products::when($id, function ($query, $id) {
                                $query->where('productCategory_id',$id);
                              })
                            ->orderBy('id', 'desc')
                            ->get()
                            ->translate('locale', app()->getLocale());

        if(count($products)>0){
            return $products;
        } else {
            return '';
        }
    }
}
