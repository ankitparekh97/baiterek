<?php

namespace App\Http\Controllers\Frontend;

use App\Helper\Helper;
use App\Http\Controllers\Controller;
use App\Mail\Forgotpassword;
use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ForgotPasswordController extends Controller
{
    public function index(){

    }

    public function forgotPassword(){
        return view('frontend.auth.forgotpassword');
    }

    public function emailResetPasswordLink(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
        ]);

        if(!$user = User::whereEmail($request->email)->first()){
            return redirect(route('frontend.forgotPassword'))->with('error', trans('validation.email not found'));
        } else {

            if(empty($user->remember_token)) {
                $user->remember_token = Helper::generateRememberToken($user->email);
                $user->save();
            }

            $passwordResetModel = new \App\Models\PasswordReset();
            $passwordResetModel->email = $user->email;
            $passwordResetModel->token = $user->remember_token;
            $passwordResetModel->save();
        }

        $email = $user->email;
        $resetPasswordLink = route('frontend.resetPasswordForm',['token' => $user->remember_token]);

        if(!empty($email)){
            $userLang = app()->getLocale();
            Mail::to($email)->send(new Forgotpassword($user->name,$user->remember_token,$resetPasswordLink,$userLang));
            if( count(Mail::failures()) == 0 ) {
                return redirect(route('frontend.forgotPassword'))->with('success', trans('validation.reset password link send'));
            }
        }
    }

    public function resetPasswordForm($token)
    {
        $user = \App\Models\PasswordReset::where('token',$token)->first();
        if(!$user){
            return redirect(route('frontend.home.index'))->with('error', trans('validation.link expire'));
        }
        return view('frontend.auth.forgotResetPassword',compact('token'));
    }

    public function savePassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'min:6|required',
            'confirm_password' => 'min:6|required_with:password|same:password'
        ]);

        $user = \App\Models\PasswordReset::where('token',$request->token)->first();

        if ($user) {
            // Update password
            $password = $request->password;

            $users = User::where('email',$user->email)->first();
            $users->setPasswordAttribute($password);
            $users->save();

            $passwordRestModel = \App\Models\PasswordReset::where('email',$user->email)->first();

            // Delete the row from password reset table
            $passwordRestModel->where('email',$user->email)->forceDelete();

            return redirect(route('frontend.login'))->with('success',trans('passwords.New password set successfully'));

        } else {
            // Redirect forgot password page with error message
            return redirect(route('frontend.login'))->with('error','This link is no longer available');
        }

    }
}
