<?php

namespace App\Http\Controllers\Frontend;

use App\Helper\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use TCG\Voyager\Models\Page;
use TCG\Voyager\Models\Post;

class SegmentController extends Controller
{
    public function index(){
        $slug = Helper::getUrlSegment();
        switch($slug) {
            case config('baiterek.urlSegments.services'):
                return App::call('ProductController@index');
                break;
            case config('baiterek.urlSegments.categories'):
                return App::call('ProductController@getAllCategories');
                break;
            case config('baiterek.urlSegments.news'):
                return App::call('NewsPasswordController@index');
                break;
            case config('baiterek.urlSegments.login'):
                return App::call('CustomerController@index');
                break;
            case config('baiterek.urlSegments.authCheck'):
                return App::call('CustomerController@login');
                break;
            case config('baiterek.urlSegments.register'):
                return App::call('CustomerController@register');
                break;
            case config('baiterek.urlSegments.checkEmail'):
                return App::call('CustomerController@checkEmail');
                break;
            case config('baiterek.urlSegments.checkVAT_no'):
                return App::call('CustomerController@checkVAT_no');
                break;
            case config('baiterek.urlSegments.save'):
                return App::call('CustomerController@save');
                break;
            case config('baiterek.urlSegments.resetpassword'):
                return App::call('CustomerController@resetpassword');
                break;
            case config('baiterek.urlSegments.updatepassword'):
                return App::call('CustomerController@updatepassword');
                break;
            case config('baiterek.urlSegments.logout'):
                return App::call('CustomerController@logout');
                break;
            case config('baiterek.urlSegments.forgotPassword'):
                return App::call('ForgotPasswordController@forgotPassword');
                break;
            case config('baiterek.urlSegments.emailResetPasswordLink'):
                return App::call('ForgotPasswordController@emailResetPasswordLink');
                break;
            case config('baiterek.urlSegments.resetPasswordForm'):
                return App::call('ForgotPasswordController@resetPasswordForm');
                break;
            case config('baiterek.urlSegments.savePassword'):
                return App::call('ForgotPasswordController@savePassword');
                break;
            case config('baiterek.urlSegments.submitRequest'):
                return App::call('ServiceRequestController@submitRequest');
                break;
            case config('baiterek.urlSegments.applyNotification'):
                return App::call('ServiceRequestController@applyNotification');
                break;
            default:
                $pages = Page::where('slug', $slug)->where('status','ACTIVE')->first();
                if(empty($pages)){
                    abort(404);
                }

                return view('frontend.cms.cms', ['pages'=> $pages]);
                break;
        }
    }
}
