<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Mail\Useremail;
use App\Models\City;
use App\Models\Region;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Validator;
use TCG\Voyager\Models\Role;
use Illuminate\Session\Store as SessionStore;

class CustomerController extends Controller
{
    public function index(){
        if(auth()->check() == true){
            if(Auth::user()->is_active == 0){
                return view('frontend.auth.resetPassword');
            } else {
                return redirect(route('frontend.home.index'));
            }
        } else {
            return view('frontend.auth.login');
        }
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($user = User::where('email', $request->email)->first()) {
            if(Hash::check(trim($request->password), $user->password)){
                if ($user->status == User::IN_ACTIVE) {
                    return redirect(route('frontend.login'))->with('error',trans('validation.userIsInactive'));
                }
                Auth::login($user,$request->remember);
                if($user->is_active == 0){
                    return redirect(route('frontend.resetpassword'));
                } else {
                    $path = $request->session()->pull('url.intended', '/');
                    return redirect()->intended($path);
                    // if(!empty($request->id)){
                    //     return redirect(route('frontend.serviceRequest.apply',['id' => $request->id]));
                    // } else {
                    //     return redirect(route('frontend.home.index'));
                    // }
                }

            } else {
                return redirect(route('frontend.login'))->with('error',trans('validation.invalid password'));
            }
        } else {
            return redirect(route('frontend.login'))->with('error',trans('validation.email not registered'));
        }
    }

    public function register(){
        $regions = Region::get()->pluck('ru_name','id');
        return view('frontend.auth.register',compact('regions'));
    }

    public function ajaxCityList(Request $request)
    {
        $cities = City::where('region_id',$request->dataId)->get()->pluck('ru_name','id');
        return response(['status' => 1, 'data' => $cities],200);
    }

    public function checkEmail(Request $request)
    {
        if ($request->ajax())
        {
            $email = User::where('email', $request->email)->get();

            if (count($email) > 0) {
                return response()->json(false);
            }

            return response()->json(true);
        }
    }

    public function checkVAT_no(Request $request)
    {
        if ($request->ajax())
        {
            $VAT_no = User::where('VAT_no', $request->VAT_no)->get();

            if (count($VAT_no) > 0) {
                return response()->json(false);
            }

            return response()->json(true);
        }
    }

    public function save(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|unique:users,email|email',
            'confirm_email' => 'required|email|same:email',
            'name' => 'required',
            'last_name' => 'required',
//            'patronymic' => 'required',
//            'gender' => 'required',
//            'address' => 'required',
//            'city' => 'required',
//            'region' => 'required',
//            'postcode' => 'required',
//            'VAT_no' => 'required|unique:users',
        ],[
            'email.unique' => 'A user with this email address already exists. Please use an alternative email address or request a password reminder if you have used this address to previously register.',
//            'VAT_no.unique' => 'A user with this VAT no already exists. Please enter other VAT no.',
        ]);

        try {
            $customerRole = config('baiterek.roles');
            $role = Role::where('slug',$customerRole['customer'])->first();
            $password = User::generatePassword();

            $cityName = City::where('id',$request->city)->first()->ru_name ?? 'test';
            $regionName = Region::where('id',$request->region)->first()->ru_name ?? 'test';
            $userModel = new User();
            $user = $userModel->create([
                'email' => $request->email,
                'name' => $request->name ,
                'last_name' => $request->last_name,
                'patronymic' => $request->patronymic,
                'password' => $password,
                'dob' => Carbon::now()->format('Y-m-d'),
                'gender' => 'Male',
                'role_id' => $role->id,
                'address' => 'test',
                'contact' => $request->contact,
                'city' => $cityName,
                'region' => $regionName,
                'city_id' => $request->city,
                'region_id' => $request->region,
                'VAT_no' => '000',
                'postcode' => '000',
            ]);

            $user->setPasswordAttribute($password);

            try{
                $actionLink = route('frontend.login');
                $userLang = app()->getLocale();
                Mail::to($user->email)->send(new Useremail($user, $password,$actionLink, $userLang));
                return redirect(route('frontend.login'))->with('success', trans('validation.registration complete'));
            } catch (\Exception $ex) {
                Log::error($ex->getMessage());
                return redirect(route('frontend.login'))->with('success', trans('validation.registration complete email error'));
            }
        } catch (\Exception $ex){
            Log::notice('CUSTOMER REGISTRATION ERROR => '. $ex->getMessage());
            return redirect(route('frontend.login'))->with('error', trans('validation.registration not complete'));
        }

    }

    public function resetpassword()
    {
        return view('frontend.auth.resetPassword');
    }

    public function updatepassword(Request $request)
    {
        if (!(Hash::check($request->current_password, Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error", "Your current password does not matches with the password you provided.");
        }

        if (strcmp($request->current_password, $request->password) == 0) {
            //Current password and new password are same
            return redirect()->back()->with("error", "New Password cannot be same as your current password.");
        }

        $this->validate($request, [
            'current_password' => 'required',
            'password' => [
                'required',
                'string',
                'required_with:confirm_password',
                'same:confirm_password',
                'min:8',             // must be at least 8 characters in length
                'regex:/[a-z]/',      // must contain at least one lowercase letter
                'regex:/[A-Z]/',      // must contain at least one uppercase letter
                'regex:/[0-9]/',      // must contain at least one digit
                'regex:/[@$!%*#?&]/', // must contain a special character
            ],
            'confirm_password' => 'required|min:8'
        ],[
            'password.regex' => 'Password should be minimum 8 characters and must contain one upper case, one lowercase character, one digit and a special character',
        ]);

        $user = Auth::user();
        $user->is_active = true;

        $password = $request->get('password');
        $user->setPasswordAttribute($password);
        $user->save();

        return redirect(route('frontend.login'))->with('success', trans('passwords.New password set successfully'));
    }

    public function logout(Request $request){
        Auth::logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect(route('frontend.home.index'))->with('success','You logged out.');
    }
}
