<?php

namespace App\Http\Controllers\Frontend;

use App\Helper\Helper;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Services;
use App\Mail\ServiceRequest;
use App\Mail\Useremail;
use App\Models\ConsultingServiceProducts;
use App\Models\ProductCheckPoint;
use App\Models\Products;
use App\Models\ApplicationForms;
use App\Models\ServiceInquiries;
use App\Models\ServiceInquiriesDocuments;
use App\Models\ServiceInquiryCheckPoint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use Symfony\Component\Filesystem\Exception\ExceptionInterface;

class ServiceRequestController extends Services
{
    public function index(){

    }

    public function apply($id)
    {
        if(auth()->check() == true)
        {
            $product = Products::find($id)->translate('locale', app()->getLocale());
            $fields = null;
            $application_form_id = null;
            if(ApplicationForms::where('product_id', $id)->exists()){
                $applicationFormFields = ApplicationForms::where('product_id', $id)->first()->translate('locale', app()->getLocale());
                $fields = json_encode(json_decode($applicationFormFields->fields, true));
                $application_form_id = $applicationFormFields->id;
            }

            $consultingServiceProduct = ConsultingServiceProducts::where('product_id', $id)->first();
            return view('frontend.serviceRequest.apply', [
                    'id' => $id, 'product' => $product,
                    'fields' => $fields,
                    'application_form_id' => $application_form_id,
                    'consultingServiceProduct' => $consultingServiceProduct,
                ]);

        } else {
            session(['url.intended'=>url()->full()]);
            return view('frontend.auth.login');
        }
    }

    public function getDraftData(Request $request)
    {
        try {
            $serviceInquiryDocumentsDetails = ServiceInquiriesDocuments::where('service_inquiries_id',$request->id)->get();
            return response(['status' => true, 'message' => 'Done', 'serviceInquiryDocumentsDetails' => $serviceInquiryDocumentsDetails], 200);
        } catch (\Exception $e) {
            return response(['status' => false, 'message' => $e->getMessage()], 203);
        }
    }

    public function submitRequest(Request $request)
    {
        $destinationPath = public_path('storage/');
        $dir = 'documents';
        if( is_dir($destinationPath . $dir) == false ){
            mkdir($destinationPath .$dir,0777);
        }
        if ($request->ajax()) {
            try {
                if (isset($request->service_inquiry_id) && $request->service_inquiry_id != null) {
                    $services = parent::updateInquiry($request->service_inquiry_id, $request->all());
                }
                else {
                    $services = parent::submitInquiry($request->all());
                }
                try {
                    if (isset($request->service_inquiry_document_ids) && $request->service_inquiry_document_ids != null) {
                        $service_inquiry_document_ids = explode(',', $request->service_inquiry_document_ids);
                        foreach ($service_inquiry_document_ids as $service_inquiry_document_id) {
                            if (ServiceInquiriesDocuments::where('id', $service_inquiry_document_id)->exists()) {
                                $serviceInquiryDocument = ServiceInquiriesDocuments::find($service_inquiry_document_id);
                                $path = public_path('storage/'.$dir).'/'.$serviceInquiryDocument->document;
                                if(file_exists($path)) {
                                    File::delete($path);
                                }
                                $serviceInquiryDocument->delete();
                            }
                        }
                    }
                    $serviceInquiriesDocuments = array();
                    if (count($request->file()) > 0) {
                        foreach($request->file() as $key => $image)
                        {
                            $file = $key . '_' . Auth::user()->id . '_' . $services . '.' . $image->getClientOriginalExtension();
//                            $image->move(public_path('storage/'.$dir), $file);
                            $content = \File::get($image);
//
                            try{
                                $path = 'Documents/';
                                $userFolder = Auth::user()->name . '_' . Auth::user()->id;
                                if(!Storage::disk('sftp')->exists('Documents')){
                                    Storage::disk('sftp')->makeDirectory('Documents');
                                }

                                if(!Storage::disk('sftp')->exists($path . $userFolder)) {
                                    Storage::disk('sftp')->makeDirectory($path . $userFolder);
                                }
                                Storage::disk('sftp')->makeDirectory($path . $userFolder . '/'. $services);

                                // local disk system
                                Storage::disk('sftp')->put($path . $userFolder . '/'. $services . '/'. $file, $content);

                                // remote server upload
                                Storage::disk('sftp')->put($path . $userFolder . '/'. $services . '/'. $file, $content);

                            } catch (ExceptionInterface $exception) {
                                Log::error('Document Error::'. $exception->getMessage());
                            }

                            $data = array('document' => $file, 'service_inquiries_id' => $services);
                            $serviceInquiriesDocuments [] = parent::submitInquiryDocuments($data);
                        }
//                        return response()->json(['success' => true, 'service_inquiry_id' => $services, 'service_inquiry_document_id' => $serviceInquiriesDocuments, 'message' => 'Your Application has been drafted successfully.']);
                    }

                    // email notification to product manager
                    if($services != false) {
                        $product = Products::with('userId')->find($request->product_id);
                        $manager = $product->userId;

                        $actionLink = url('admin/login');
                        try{
                            Mail::to($manager->email)->send(new ServiceRequest($manager,$product,$actionLink));
                            return response()->json(['success' => true, 'service_inquiry_id' => $services, 'service_inquiry_document_id' => $serviceInquiriesDocuments, 'message' => 'Your Application has been drafted successfully and notification sent to your registered email address.']);

                        } catch (\Exception $ex) {
                            Log::error('Could not send email notification to product manager'.$ex->getMessage());
                            return response()->json(['success' => true, 'service_inquiry_id' => $services, 'service_inquiry_document_id' => $serviceInquiriesDocuments, 'message' => 'Your Application has been drafted successfully.']);
                        }

                    } else {
                        return response()->json(['success' => true, 'service_inquiry_id' => $services, 'service_inquiry_document_id' => $serviceInquiriesDocuments, 'message' => 'Your Application could not be drafted. Please contact site administrator.']);
                    }
                } catch (\Exception $exception) {
                    Log::error('Document Issue' .$exception->getMessage());
                }
            }
            catch (\Exception $ex){
                Log::error($ex->getMessage());
                return response()->json(['success' => false, 'error' => 'Your Application could not be drafted. Please try again or contact site administrator.']);
            }
        }
        else {
            try {
                $product_details = Products::find($request->product_id);
                $due_date = Carbon::now()->addDays($product_details->milestone);
                $request->merge(['due_date' => $due_date]);
                if (isset($request->service_inquiry_id) && $request->service_inquiry_id != null) {
                    $services = parent::updateInquiry($request->service_inquiry_id, $request->all());
                    $this->createCheckPoints($services);
                }
                else {
                    $services = parent::submitInquiry($request->all());
                    $this->createCheckPoints($services);
                }
                try {
                    if (isset($request->service_inquiry_document_ids) && $request->service_inquiry_document_ids != null) {
                        $service_inquiry_document_ids = explode(',', $request->service_inquiry_document_ids);
                        foreach ($service_inquiry_document_ids as $service_inquiry_document_id) {
                            if (ServiceInquiriesDocuments::where('id', $service_inquiry_document_id)->exists()) {
                                $serviceInquiryDocument = ServiceInquiriesDocuments::find($service_inquiry_document_id);
                                $path = public_path('storage/'.$dir).'/'.$serviceInquiryDocument->document;
                                if(file_exists($path)) {
                                    File::delete($path);
                                }
                                $serviceInquiryDocument->delete();
                            }
                        }
                    }
                    foreach($request->file() as $key => $image)
                    {
                        if($request->hasFile($key)) {
                            $file = $key . '_' . Auth::user()->id . '_' . $services . '.' . $image->getClientOriginalExtension();
//                            $image->move(public_path('storage/' . $dir), $file);
                            $content = \File::get($image);

                            // file upload
                            try{
                                $path = 'Documents/';
                                $userFolder = Auth::user()->name . '_' . Auth::user()->id;
                                if(!Storage::disk('sftp')->exists('Documents')){
                                    Storage::disk('sftp')->makeDirectory('Documents');
                                }

                                if(!Storage::disk('sftp')->exists($path . $userFolder)) {
                                    Storage::disk('sftp')->makeDirectory($path . $userFolder);
                                }
                                Storage::disk('sftp')->makeDirectory($path . $userFolder . '/'. $services);

                                // local disk system
                                Storage::disk('sftp')->put($path . $userFolder . '/'. $services . '/'. $file, $content);

                                // remote server upload
                                Storage::disk('sftp')->put($path . $userFolder . '/'. $services . '/'. $file, $content);

                            } catch (ExceptionInterface $exception) {
                                Log::error('Document Error::'. $exception->getMessage());
                            }

                            $data = array('document' => $file, 'service_inquiries_id' => $services);
                            $serviceInquiriesDocuments = parent::submitInquiryDocuments($data);
                        }
                    }
                } catch (\Exception $exception) {
                    Log::error('Document Issue' .$exception->getMessage());
                }
                if($services != false) {
                    $product = Products::with('userId')->find($request->product_id)->translate('locale', app()->getLocale());
                    $manager = $product->userId;

                    $actionLink = url('admin/login');
                    try{
                        Mail::to($manager->email)->send(new ServiceRequest($manager,$product,$actionLink));
                        app()->setLocale(\Session::get('locale'));
                        return redirect(route('frontend.applyNotification'))->with('success',@trans('portal.front_end.application_success_message'));
                    } catch (\Exception $ex) {
                        Log::error($ex->getMessage());
                        return redirect(route('frontend.applyNotification'))->with('success','You have applied for this service. Our Baiterek team will adjudicate your application & communicate with you shortly. ');
                    }

                } else {
                    return redirect(route('frontend.applyNotification'))->with('error','Your Application could not be completed. Please contact site administrator.');
                }

            }
            catch (\Exception $ex){
                Log::error($ex->getMessage());
                return redirect(route('frontend.applyNotification'))->with('error','Your Application could not be completed. Please contact site administrator.');
            }
        }
    }

    public function applyService(Request $request) {
        try {
            $product_details = Products::find($request->product_id);
            $due_date = Carbon::now()->addDays($product_details->milestone);
            $request->merge(['due_date' => $due_date]);
            if (isset($request->service_inquiry_id) && $request->service_inquiry_id != null) {
                $services = parent::updateInquiry($request->service_inquiry_id, $request->all());
            }
            else {
                $services = parent::submitInquiry($request->all());
            }
            try {
                $destinationPath = public_path('storage/');
                $dir = 'documents';
                if( is_dir($destinationPath . $dir) == false ){
                    mkdir($destinationPath .$dir,0777);
                }
                if (isset($request->service_inquiry_document_ids) && $request->service_inquiry_document_ids != null && $request->file_count == count($request->file())) {
                    $service_inquiry_document_ids = explode(',', $request->service_inquiry_document_ids);
                    foreach ($service_inquiry_document_ids as $service_inquiry_document_id) {
                        if (ServiceInquiriesDocuments::where('id', $service_inquiry_document_id)->exists()) {
                            $serviceInquiryDocument = ServiceInquiriesDocuments::find($service_inquiry_document_id);
                            $path = public_path('storage/'.$dir).'/'.$serviceInquiryDocument->document;
                            if(file_exists($path)) {
                                File::delete($path);
                            }
                            $serviceInquiryDocument->delete();
                        }
                    }
                }
                foreach($request->file() as $key => $image)
                {
                    $file = $key.'_'.Auth::user()->id.'_'.$services.'.'.$image->getClientOriginalExtension();
                    $image->move(public_path('storage/'.$dir), $file);

                    $data = array('document' => $file, 'service_inquiries_id' => $services);
                    $serviceInquiriesDocuments = parent::submitInquiryDocuments($data);
                }
            } catch (\Exception $exception) {
                Log::error('Document Issue' .$exception->getMessage());
            }
            if($services != false) {
                $product = Products::with('userId')->find($request->product_id);
                $manager = $product->userId;

                $actionLink = url('admin/login');
                try{
                    Mail::to($manager->email)->send(new ServiceRequest($manager,$product,$actionLink));
                    app()->setLocale(\Session::get('locale'));
                    return redirect(route('frontend.applyNotification'))->with('success',@trans('portal.front_end.application_success_message'));
                } catch (\Exception $ex) {
                    Log::error($ex->getMessage());
                    return redirect(route('frontend.applyNotification'))->with('success','You have applied for this service. Our Baiterek team will adjudicate your application & communicate with you shortly. ');
                }

            } else {
                return redirect(route('frontend.applyNotification'))->with('error','Your Application could not be completed. Please contact site administrator.');
            }

        }
        catch (\Exception $ex){
            Log::error($ex->getMessage());
            return redirect(route('frontend.applyNotification'))->with('error','Your Application could not be completed. Please contact site administrator.');
        }
    }

    public function applyNotification(){
        return view('frontend.serviceRequest.applyNotification');
    }

    public function createCheckPoints($services)
    {
        $serviceInquiries = ServiceInquiries::where('id', $services)->first();
        if ($serviceInquiries) {
            $productCheckPoints = ProductCheckPoint::where('product_id', $serviceInquiries->product_id)->where('type', ProductCheckPoint::TEXT)->get();
            foreach ($productCheckPoints as $productCheckPoint) {
                if ($serviceInquiries->status_of_request != 'Draft') {
                    ServiceInquiryCheckPoint::create([
                        'service_inquiry_id' => $serviceInquiries->id,
                        'product_check_point_id' => $productCheckPoint->id,
                        'check_point_name' => $productCheckPoint->checkpoint,
                        'application_status' => $productCheckPoint->status,
//                        'application_status' => $serviceInquiries->status_of_request,
                    ]);
                }
            }
        }
    }

    public function deleteDocument(Request $request)
    {
        try {
            $serviceInquiryDocumentsDetails = ServiceInquiriesDocuments::find($request->id);
            $dir = 'documents';
            $path = public_path('storage/'.$dir).'/'.$serviceInquiryDocumentsDetails->document;
            if(file_exists($path)) {
                File::delete($path);
            }
            $serviceInquiryDocumentsDetails->delete();
            return response(['status' => true, 'message' => 'Done'], 200);
        } catch (\Exception $e) {
            return response(['status' => false, 'message' => $e->getMessage()], 203);
        }
    }
}
