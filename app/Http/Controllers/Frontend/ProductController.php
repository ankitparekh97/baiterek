<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Services;
use Illuminate\Http\Request;
use App\Models\ApplicationForms;
use Illuminate\Support\Facades\Lang;

class ProductController extends Services
{
    public function index(Request $request){
        $title = $request->title;
        $productCategory_id = $request->productCategory_id;

        $products = parent::getAllProducts($title,$productCategory_id);
        $relatedProductIds = ApplicationForms::get()->pluck('product_id')->toArray();
        if ($request->ajax()) {
            return response()->json(['success' => true, 'data' => $products, 'relatedProductIds' => $relatedProductIds, 'ViewProjectsLabel' => Lang::get('fields.ViewProjects')]);
        }

        $productCategories = parent::getAllProductCategories(10);
        return view('frontend.products.products', ['productCategories' => $productCategories, 'products' => $products, 'relatedProductIds' => $relatedProductIds]);
    }

    public function getProduct($id){
        $product = parent::getProductsDetail($id);

        return view('frontend.products.productDetail', ['product' => $product]);
    }

    public function getCategory($id){
        $products = parent::getAllProducts('',$id);
        $productCategory = parent::getCategoryDetail($id);
        $relatedProductIds = ApplicationForms::get()->pluck('product_id')->toArray();

        return view('frontend.products.categoryDetail', ['products' => $products, 'productCategory' => $productCategory, 'relatedProductIds' => $relatedProductIds]);
    }

    public function getAllCategories(Request $request){
        $categories = parent::getAllProductCategories();

        if ($request->ajax()) {
            $pattern = $request->title;

            $categories = parent::getAllProductCategories(4,$pattern);
            return response()->json(['success' => true, 'data' => $categories]);
        }

        return view('frontend.products.categories', ['categories' => $categories]);
    }

}
