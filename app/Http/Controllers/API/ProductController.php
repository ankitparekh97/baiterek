<?php

namespace App\Http\Controllers\API;

use App\Models\ProductCategories;
use App\Models\Products;
use Illuminate\Http\Request;
use \Validator;

class ProductController extends BaseController
{
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors(), 401);
        }
        if (!$productCategory = ProductCategories::find($request->category_id)) {
            $category = [
                'category_id' => 'The selected id is invalid.'
            ];
            return $this->sendError('Product Category Not Found', $category, 401);
        }
        $lang = $request->lang;
        $products = Products::where('productCategory_id', $request->category_id)
            ->when($request->search_param, function ($query, $term) use ($lang) {
                $query->whereTranslation('title', 'LIKE', '%' . $term . '%', [$lang]);
            })
            ->get()->translate('locale', $lang)->paginate($request->num);
        return $this->sendResponse($products, 'Product received successfully', 200);
    }

    public function getProducts(Request $request,$num)
    {
//        $products = Products::withTranslation($request->lang)->paginate($num);
        $lang = $request->lang;
        $products = Products::when($request->search_param, function ($query, $term) use ($lang) {
            $query->whereTranslation('title', 'LIKE', '%' . $term . '%', [$lang]);
        })->get()->translate('locale', $lang)->paginate($num);
        return $this->sendResponse($products, 'Product List',200);
    }
}
