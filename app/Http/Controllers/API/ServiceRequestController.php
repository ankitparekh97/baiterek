<?php

namespace App\Http\Controllers\API;

use App\Models\Products;
use App\Models\ServiceInquiries;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ServiceRequestController extends BaseController
{
    public function getRequestList(Request $request,$num)
    {
        try{
            $lang = $request->lang;
            $role = \auth()->user()->role->slug;
            $requests = ServiceInquiries::with('product.userId')
                ->when($role, function ($query, $role) {
                    if ($role == 'customer') {
                        $query->where('user_id', \auth()->user()->id);
                    }
                })
                ->when($request->search_param, function ($query, $term) use ($lang) {
                    $query->whereHas('product', function (Builder $query2) use ($lang, $term) {
                        $query2->whereTranslation('title', 'LIKE', '%' . $term . '%', [$lang]);
                    });
                })->orderBy('id','desc')->get()
                ->map(function($item){
                    $product = Products::where('id',$item->product_id)->first()->translate('locale', $item->locale);
                    $item['product_title'] = !empty($product) ? $product->title : null;
                    $item['product']['title'] = !empty($product) ? $product->title : null;
                    return $item;
                })
                ->paginate($num);
            return $this->sendResponse($requests, 'Requests List',200);
        } catch (\Exception $e){
            return $this->sendResponse([], $e->getMessage(),401);
        }
    }
}
