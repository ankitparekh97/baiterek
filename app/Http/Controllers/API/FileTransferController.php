<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use FontLib\EOT\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Exception;
use League\Flysystem\Filesystem;
use League\Flysystem\Sftp\SftpAdapter;


class FileTransferController extends BaseController
{
    public function index(){

    }

    public function fileSync($dir)
    {
        $systemPath = storage_path('app/public/'.$dir);
        $files = \File::allFiles( $systemPath );

//        $path = '/var/www/html/baiterek_web/storage/app/public/'.$dir .'/';
//        $privateKey = base_path('alma-private-key.ppk');
//        $filesystem = new Filesystem(new SftpAdapter([
//            'host' => '159.203.116.157',
//            'port' => 22,
//            'username' => 'root',
//            'password' => '',
//            'privateKey' => $privateKey,
//            'root' => $path,
//            'timeout' => 30,
//        ]));

        $path = '/var/www/html/requestdocs/'.$dir .'/';
        $filesystem = new Filesystem(new SftpAdapter([
            'host' => '10.7.7.4',
            'port' => 22,
            'username' => 'epp',
            'password' => '123456',
            'root' => $path,
            'timeout' => 30,
        ]));

        if(!$filesystem){
            dd('Unable to connect');
        }

        // to get all the contents in a specific directory
//       $read = $filesystem->listContents($path,true);
//       dd($path);

        $write = array();
        foreach ($files as $node) {
            $content = \File::get($node);
            $write[] = $filesystem->put($node->getRelativePathname(),$content); // upload file
        }

        return $this->sendResponse($write, 'Files sycned',200);
    }

    public function dirSync($dir)
    {
        $systemPath = storage_path('app/public/'.$dir);
        $dirs = \File::directories( $systemPath );

//        $path = '/var/www/html/baiterek_web/storage/app/public';
//        $privateKey = base_path('alma-private-key.ppk');
//        $filesystem = new Filesystem(new SftpAdapter([
//            'host' => '159.203.116.157',
//            'port' => 22,
//            'username' => 'root',
//            'password' => '',
//            'privateKey' => $privateKey,
//            'root' => $path,
//            'timeout' => 30,
//            'directoryPerm' => 0777
//        ]));

        $path = '/var/www/html/requestdocs';
        $filesystem = new Filesystem(new SftpAdapter([
            'host' => '10.7.7.4',
            'port' => 22,
            'username' => 'epp',
            'password' => '123456',
            'root' => $path,
            'timeout' => 30,
        ]));

        if(!$filesystem){
            dd('Unable to connect');
        }

        // to get all the contents in a specific directory
//      $read = $filesystem->listContents($dir);

        $write = array();
        foreach ($dirs as $node) {
            $basename = basename($node);
            $write[] = $filesystem->createDir($basename);
        }

        return $this->sendResponse($write, 'Directories created successfully',200);
    }

    public function fileSynct()
    {
        $strServer = "10.7.7.4";
        $strServerPort = "22";
        $strServerUsername = "epp";
        $strServerPassword = "123456";
        $source_path = "/var/www/html/baiterek/tmp";
        $destination_path = "/usr/requestdocs";
        $filename = "test.png";

        //connect to server
        $connection = ssh2_connect($strServer, $strServerPort);

        if(ssh2_auth_password($connection, $strServerUsername, $strServerPassword)){
            //Initialize SFTP subsystem
            //echo "connected";
            $resSFTP = ssh2_sftp($connection);

            $resFile = fopen("ssh2.sftp://{$resSFTP}/".$destination_path."/".$filename, 'w');
            $srcFile = fopen($source_path."/".$filename, 'r');
            $writtenBytes = stream_copy_to_stream($srcFile, $resFile);
            fclose($resFile);
            fclose($srcFile);
        }else{
            echo "Unable to authenticate on server";
        }
    }
}
