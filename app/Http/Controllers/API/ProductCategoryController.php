<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ProductCategories;
use Illuminate\Http\Request;

class ProductCategoryController extends BaseController
{
    public function index(Request $request,$num)
    {
        $lang = $request->lang;
        $productCategories = ProductCategories::when($request->search_param, function ($query, $term) use ($lang) {
            $query->whereTranslation('title', 'LIKE', '%' . $term . '%', [$lang]);
        })->get()->translate('locale', $lang)->paginate($num);
        return $this->sendResponse($productCategories, 'Product Categories received successfully',200);
    }
}
