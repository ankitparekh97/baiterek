<?php

namespace App\Http\Controllers\API;

use App\Helper\Helper;
use App\Http\Controllers\Controller;
use App\Http\Resources\RoleResource;
use App\Http\Resources\UserResource;
use App\Models\BannerImages;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
//use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\Mail;
use TCG\Voyager\Models\Role;
use TCG\Voyager\Voyager;
use \Validator;

class UserController extends BaseController
{
    public function login(){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] = $user->createToken('MyApp')->accessToken;
            $success['user'] = new UserResource($user);

            if($user->is_active == 0){
                return $this->sendResponse($success, 'Your account is inactive. Kindly enter activation code to activate your account.',200);
            } else {
                return $this->sendResponse($success, 'User authenticated successfully.',200);
            }
        }
        else{
            return response()->json(['error'=>'Invalid User Name / Password'], 401);
        }
    }

    public function register(Request $request)
    {
        $tempPassword = mt_rand(100000,999999);

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'last_name' => 'required',
            'patronymic' => 'required',
            'email' => 'required|email|unique:users',
            'contact' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors(),401);
        }

        $role = Role::where('slug','customer')->first();

        $userModel = new User();
        $user = $userModel->create([
            'email' => $request->email,
            'name' => $request->name ,
            'last_name' => $request->last_name,
            'patronymic' => $request->patronymic,
//            'gender' => $request->gender,
            'role_id' => $role->id,
            // 'password' => $request->password,
            'password' => $tempPassword,
//            'address' => $request->address,
            'contact' => $request->contact,
//            'city' => $request->city,
//            'postcode' => $request->postcode,
//            'region' => $request->region,
            'VAT_no' => Carbon::now()->getTimestamp()
        ]);

        // $user->setPasswordAttribute($request->password);
        $user->setPasswordAttribute($tempPassword);
        $success['token'] = $user->createToken('MyApp')->accessToken;
        $success['user'] = $user;

        return $this->sendResponse($success, 'User registered successfully.',200);
    }

    public function changePassword(Request $request)
    {
        $user=User::find(auth()->user()->id);
        $user->setPasswordAttribute($request->new_password);
        $user->is_active=1;
        $user->save();

        $success['user'] = new UserResource($user);
        return $this->sendResponse($success, 'Password updated Successfully.',200);
    }

    public function getUserDetails(){
        $user = User::find(auth()->user()->id);
        $success['user'] = new UserResource($user);

        return $this->sendResponse($success, 'User Profile',200);
    }

    public function setUserDetails(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
//            'gender' => 'required',
            'address' => 'required',
            'contact' => 'required',
//            'city' =>   'required',
//            'postcode' => 'required',
//            'region' => 'required',
            //'VAT_no' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors(),401);
        }

        $user = User::find(auth()->user()->id);
        $user->email=$request->email;
        $user->name=$request->name;
        $user->last_name=$request->last_name;
        $user->patronymic=$request->patronymic;
//        $user->dob=$request->dob;
//        $user->gender=$request->gender;
//        $user->role_id=$request->role_id;
        $user->address=$request->address;
        $user->contact=$request->contact;
//        $user->region=$request->region;
//        $user->city=$request->city;
//        $user->postcode=$request->postcode;
        $user->VAT_no=Carbon::now()->getTimestamp();
        $user->save();
        $success['user'] = new UserResource($user);
        return $this->sendResponse($success, 'User details updated successfully.',200);
    }

    public function forgotPassword(Request $request)
    {
        if(!$user = User::whereEmail($request->email)->first()){
            return $this->sendError('Invalid Email',trans('validation.email not found'),203);
        } else {

            if(empty($user->remember_token)) {
                $user->remember_token = Helper::generateRememberToken($user->email);
                $user->save();
            }

            $passwordResetModel = new \App\Models\PasswordReset();
            $passwordResetModel->email = $user->email;
            $passwordResetModel->token = $user->remember_token;
            $passwordResetModel->save();
        }

        $email = $user->email;
        $resetPasswordLink = route('frontend.resetPasswordForm',['token' => $user->remember_token]);
        $userLang = $request->lang ?? 'en';
        if(!empty($email)){
            Mail::to($email)->send(new \App\Mail\Forgotpassword($user->name,$user->remember_token,$resetPasswordLink, $userLang));
            if( count(Mail::failures()) == 0 ) {
                return $this->sendResponse( trans('validation.reset password link send'), 'Password Reset Link Sent',200);
            }
        }
        return $this->sendResponse( trans('validation.reset password link send'), 'Password Reset Link Sent',200);
    }
}
