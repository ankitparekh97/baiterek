<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\RoleResource;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Role;

class RoleController extends BaseController
{
    public function UserRoles(){
        $roles =  RoleResource::collection(Role::all());
        return $this->sendResponse($roles, 'User Roles',200);
    }
}
