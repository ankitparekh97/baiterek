<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends BaseController
{
    public function getNews(Request $request, $num)
    {
        $news = News::with('user')
            ->orderBy('id','desc')
            ->get()
            ->translate('locale', $request->lang)
            ->paginate($num);
        return $this->sendResponse($news, 'News List',200);
    }
}
