<?php $__env->startSection('customer'); ?>

    <div class="col-md-6 col-lg-4 left-banner side-banner green-gradient">
        <div class="side-banner-content">
            <div class="bg-img">
                <img src="<?php echo e(asset('images/leaf-bg.png')); ?>" class="img-fluid">
            </div>
            <div class="slider">
                <div class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="slider-text">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-md-6 col-lg-8 login-sidebar">
        <div class="login-container">
            <a href="<?php echo e(url('/')); ?>"><img src="<?php echo e(asset('images/logo.svg')); ?>" class="img-fluid mb-5" alt="" title=""></a>
            <div>
                <a class="btn-group btn-group-toggle" data-toggle="buttons">
                    <?php $__currentLoopData = config('voyager.multilingual.locales'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <a href="<?php echo e(url('locale/'.$lang)); ?>" class="btn btn-success <?php echo e(($lang === \Illuminate\Support\Facades\Session::get('locale')) ? " active" : ""); ?>"><?php echo e(strtoupper($lang)); ?></a>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </a>
            </div>
            <br>
            <h2><?php echo app('translator')->get('auth.welcome'); ?></h2>
            <p><?php echo app('translator')->get('auth.sign in to account'); ?></p>
            <p><?php echo app('translator')->get('auth.no account'); ?> <span style="color: #2D803D;"><a href="<?php echo e(route('frontend.register')); ?>"><?php echo app('translator')->get('auth.sign up now'); ?></a></span></p>

            <form name="login" id="login" action="<?php echo e(route('frontend.authCheck')); ?>" method="POST" class="mt-3">
                <?php echo e(csrf_field()); ?>


                <?php if(session('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <i class="fa fa-exclamation-triangle fa-1x"></i> <?php echo e(session('error')); ?>

                    </div>
                <?php endif; ?>

                <?php if(session('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <i class="fa fa-check-circle fa-1x"></i> <?php echo e(session('success')); ?>

                    </div>
                <?php endif; ?>

                <div class="form-row border-top pt-5">
                    <div class="form-group col-lg-4 col-md-12">
                        <label for="email"><?php echo app('translator')->get('auth.email'); ?></label>
                        <input type="text" class="form-control" value="<?php echo e(old('email')); ?>" id="email" name="email" placeholder="<?php echo app('translator')->get('auth.email'); ?>" autocomplete="off">
                        <?php if($errors->has('email')): ?>
                            <span class="baiterek-help-block" role="alert">
                        <span><?php echo e($errors->first('email')); ?></span>
                    </span>
                        <?php endif; ?>
                    </div>
                    <div class="form-group col-lg-4 col-md-12">
                        <label for="password"><?php echo app('translator')->get('auth.password'); ?></label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="<?php echo app('translator')->get('auth.password here'); ?>" autocomplete="off">
                        <?php if($errors->has('password')): ?>
                            <span class="baiterek-help-block" role="alert">
                        <span><?php echo e($errors->first('password')); ?></span>
                    </span>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="form-row border-bottom pb-4">
                    <div class="form-group col-md-12 mt-3">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="remember" id="remember" value="1">
                            <label class="custom-control-label" for="remember"><?php echo app('translator')->get('auth.keep logged in'); ?></label>
                        </div>
                    </div>
                </div>

                <div class="form-row  mt-5">
                    <div class="form-group col-md-12 d-flex justify-content-end align-items-center">
                        <a href="<?php echo e(route('frontend.forgotPassword')); ?>" class="forgot-pswd"><?php echo app('translator')->get('auth.forgot password?'); ?></a>
                        <input type="submit" name="login" id="Login" value="<?php echo app('translator')->get('auth.login'); ?>" class="btn btn-block login-button">
                    </div>
                </div>

            </form>

        </div>

    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.usermaster', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/baiterek_web/resources/views/frontend/auth/login.blade.php ENDPATH**/ ?>