<?php $__env->startSection('content'); ?>
	<table border="0" cellpadding="0" cellspacing="0" width="460">

		<!-- Spacing -->
		<tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="50">&nbsp;</td></tr>
		<!-- Spacing -->

		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 600; font-size: 24px;line-height: 34px;">
				Hi <?php echo e($productManager->name ? $productManager->name : "Dear"); ?>

			</td>
		</tr>
		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 600; font-size: 20px;line-height: 32px;">
				You have received an invitation to update a new product from the portal. To view, please log in here <a style="color: #dc4e26; text-align:center;text-decoration: underline;" href="<?php echo e($actionUrl); ?>">click here</a>.
			</td>
		</tr>

		<!-- Spacing -->
		<tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="50">&nbsp;</td></tr>
		<!-- Spacing -->

		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 26px;">
				Here are your details.
			</td>
		</tr>

		<!-- Spacing -->
		<tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="15">&nbsp;</td></tr>
		<!-- Spacing -->

		<tr>
			<td align="center" style="color: #333333; font-family: 'Open Sans'; font-weight: 400; font-size: 16px;line-height: 40px;">
				Product : <?php echo e($data->title); ?>

			</td>
		</tr>

		<tr><td bgcolor="#ffffff" style="background-color:#ffffff;" height="50">&nbsp;</td></tr>
	</table>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('vendor.voyager.emails.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/baiterek_web/resources/views/vendor/voyager/emails/productMail.blade.php ENDPATH**/ ?>