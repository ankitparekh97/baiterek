<?php $__env->startSection('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural); ?>

<?php $__env->startSection('page_header'); ?>
    <?php app()->setLocale(\Session::get('locale')) ?>
    <div class="container-fluid">
        <div class="row cci-page-header">
            
                <h1 class="page-title">
                    <i class="<?php echo e($dataType->icon); ?>"></i> <?php echo e(__('portal.news.news')); ?>

                    <?php if($dataType->description != ''): ?>
                        <a href="#" data-toggle="tooltip" data-placement="bottom" class="cci-tooltip"
                           title="<?php echo e($dataType->description); ?>">
                            <i class="voyager-question"></i>
                        </a>
                    <?php endif; ?>
                </h1>

                
                
                
                
                

                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('add', app($dataType->model_name))): ?>
                    <a href="<?php echo e(route('voyager.'.$dataType->slug.'.create')); ?>" class="btn btn-success btn-add-new">
                        <i class="voyager-plus"></i> <span><?php echo e(__('portal.news.add_new')); ?></span>
                    </a>
                <?php endif; ?>
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete', app($dataType->model_name))): ?>

                    <?php echo $__env->make('vendor.voyager.partials.bulk-delete', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <?php endif; ?>
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', app($dataType->model_name))): ?>
                    <?php if(isset($dataType->order_column) && isset($dataType->order_display_column)): ?>
                        <a href="<?php echo e(route('voyager.'.$dataType->slug.'.order')); ?>" class="btn btn-primary hidden">
                            <i class="voyager-list"></i> <span><?php echo e(__('voyager::bread.order')); ?></span>
                        </a>
                    <?php endif; ?>
                <?php endif; ?>
                <?php echo $__env->make('voyager::multilingual.language-selector', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="page-content browse container-fluid">
        <?php echo $__env->make('voyager::alerts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                <tr>
                                    <th class="nosort">
                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete',app($dataType->model_name))): ?>
                                        <input type="checkbox" class="select_all">
                                    <?php endif; ?>
                                    </th>
                                    <?php $__currentLoopData = $dataType->browseRows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <th>

                                        <?php echo e(__('portal.news.'. $row->display_name)); ?>

                                    </th>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <th class="actions text-right nosort">
                                        <?php echo e(__('portal.news.actions_table')); ?>

                                    </th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr class="cci-filters">
                                    <th>&nbsp;</th>
                                    <?php $__currentLoopData = $dataType->browseRows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <th> <?php echo e($row->display_name); ?> </th>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <th class="actions text-right">&nbsp;</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo e(__('voyager::generic.close')); ?>"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> <?php echo e(__('voyager::generic.delete_question')); ?> <?php echo e(strtolower($dataType->display_name_singular)); ?>?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        <?php echo e(method_field('DELETE')); ?>

                        <?php echo e(csrf_field()); ?>

                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="<?php echo e(__('portal.news.delete_confirm')); ?>">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal"><?php echo e(__('portal.news.cancel')); ?></button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php $__env->stopSection(); ?>


<?php $__env->startSection('javascript'); ?>
    <!-- DataTables -->
    <?php if(!$dataType->server_side && config('dashboard.data_tables.responsive')): ?>
        <script src="<?php echo e(voyager_asset('lib/js/dataTables.responsive.min.js')); ?>"></script>
    <?php endif; ?>
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();

            $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '<?php echo e(route('voyager.'.$dataType->slug.'.browse')); ?>',
                    method: 'POST'
                },
                data: {
                    'lang': '<?php echo e(\Illuminate\Support\Facades\Session::get('locale')); ?>'
                },
                "language": {
                    "sEmptyTable":     "<?php echo e(__('data-table.sEmptyTable')); ?>",
                    "sInfo":           "<?php echo e(__('data-table.sInfo')); ?>",
                    "sInfoEmpty":      "<?php echo e(__('data-table.sInfoEmpty')); ?>",
                    "sInfoFiltered":   "<?php echo e(__('data-table.sInfoFiltered')); ?>",
                    "sInfoPostFix":    "<?php echo e(__('data-table.sInfoPostFix')); ?>",
                    "sInfoThousands":  "<?php echo e(__('data-table.sInfoThousands')); ?>",
                    "sLengthMenu":     "<?php echo e(__('data-table.sLengthMenu')); ?>",
                    "sLoadingRecords": "<?php echo e(__('data-table.sLoadingRecords')); ?>",
                    "sProcessing":     "<?php echo e(__('data-table.sProcessing')); ?>",
                    "sSearch":         "<?php echo e(__('data-table.sSearch')); ?>",
                    "sZeroRecords":    "<?php echo e(__('data-table.sZeroRecords')); ?>",
                    "oPaginate": {
                        "sFirst":    "<?php echo e(__('data-table.sFirst')); ?>",
                        "sLast":     "<?php echo e(__('data-table.sLast')); ?>",
                        "sNext":     "<?php echo e(__('data-table.sNext')); ?>",
                        "sPrevious": "<?php echo e(__('data-table.sPrevious')); ?>"
                    },
                    "oAria": {
                        "sSortAscending":  "<?php echo e(__('data-table.sSortAscending')); ?>",
                        "sSortDescending": "<?php echo e(__('data-table.sSortDescending')); ?>"
                    }
                },
                columnDefs: [ {"targets": 'nosort',"orderable": false}],
                columns: <?php echo \App\Helper\Helper::getBrowseColumns($dataType); ?>,
                order : <?php echo json_encode($orderColumn); ?>,
                pageLength: 25,
                initComplete: function () {
//                    cciInitComplete(this.api().columns());
                },
                fnDrawCallback: function () {
                    $('#dataTable tbody tr').each(function () {
                        $(this).find('td:last').attr('id', 'bread-actions');
                    });
                }
//                dom: '<"top"l<"#cciAction.cci-action text-right">>rt<"bottom"ip<"clear">>'
            });

            <?php if($isModelTranslatable): ?>
            $('.side-body').multilingual();
            //Reinitialise the multilingual features when they change tab
            $('#dataTable').on('draw.dt', function(){
                $('.side-body').data('multilingual').init();
            })
            <?php endif; ?>
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });
        });


        var deleteFormAction;
        $(document).on('click', 'a.delete', function (e) {
            $('#delete_form')[0].action = '<?php echo e(route('voyager.'.$dataType->slug.'.destroy', ['id' => '__id'])); ?>'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('voyager::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/baiterek_web/resources/views/vendor/voyager/news/browse.blade.php ENDPATH**/ ?>