<div class="col-md-12">
    <div class="owl-carousel owl-theme fact-owl">
        <?php if(count(\App\Helper\Helper::getGraphBlocks())>0): ?>
            <?php $__currentLoopData = \App\Helper\Helper::getGraphBlocks(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $graphblocks): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php
                    $grandient_class = substr(str_shuffle('abcdefghijklmnopqrstuvwxyz') , 0 , 5 );
                ?>

                <div class="item">
                    <div class="fact-box <?php echo e($grandient_class); ?>">
                        <div class="number-text">
                            <p class="fact-number"><span class="ft_count"><?php echo e($graphblocks->count); ?></span> <?php echo e($graphblocks->title); ?> </p>
                            <p class="fact-text"><?php echo e($graphblocks->description); ?></p>
                        </div>
                        <img src="<?php echo e(asset('images/fact-img.png')); ?>" class="fact-img">
                    </div>
                    <style>
                        .fact-box.<?php echo e($grandient_class); ?> {
                            background: -webkit-gradient(linear, left top, right top, color-stop(-0.04%, <?php echo e($graphblocks->color); ?>), color-stop(100.04%, <?php echo e($graphblocks->color); ?>));
                            background: linear-gradient(90deg, <?php echo e($graphblocks->color); ?> -0.04%, <?php echo e($graphblocks->color); ?> 100.04%);
                        }
                    </style>
                </div>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
    </div>
</div>

<script type="application/javascript">
    // fact owl
    $(function () {
        $('.fact-owl').owlCarousel({
            loop:true,
            margin:0,
//            nav:true,
            items: 5,
            width:100,
            autoplay:true,
            autoplayTimeout:2000,
            autoplayHoverPause:true,
            responsive: {
                1630:{
                    items:5
                },
                1260:{
                    items:4
                },
                900:{
                    items:3
                },
                700: {
                    items:2
                },
                0: {
                    items: 1
                }
            }
        })
    });

    $(".ft_count").delay(2000).each(function() {
        var mantissa = 0;
        var orig = $(this).text();
        if (orig.indexOf('.')>-1) {
            mantissa = orig.substring(orig.indexOf('.'))
        }
        $(this).prop('Counter', 0).animate({
            Counter: $(this).text()
        }, {
            duration: 2500,
            easing: 'swing',
            step: function(now) {
                $(this).text(Math.floor(now) + mantissa);
            },
        });
    });

</script>
<?php /**PATH /var/www/html/baiterek_web/resources/views/frontend/layouts/graphimages.blade.php ENDPATH**/ ?>