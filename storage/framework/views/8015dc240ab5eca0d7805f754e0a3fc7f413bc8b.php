<div class="profile-language">
    <?php if(\Illuminate\Support\Facades\Auth::user()): ?>

    

        <input type="search" placeholder="Search">
    
    <?php endif; ?>

    <div class="dropdown profile-dropdown">
        <?php if(\Illuminate\Support\Facades\Auth::user()): ?>
            <a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php if(\Illuminate\Support\Facades\Auth::check() == 0): ?>
                    <img src="<?php echo e(asset('storage/user-img.png')); ?>" class="img-fluid" alt="">
                <?php else: ?>
                    <img src="<?php echo e(asset('storage/'.\Illuminate\Support\Facades\Auth::user()->avatar)); ?>" class="img-fluid" alt="">
                <?php endif; ?>
               <?php echo \Illuminate\Support\Facades\Auth::check() ? '<span>'.ucfirst(\Illuminate\Support\Facades\Auth::user()->name).'</span>' : ''; ?>

            </a>
        <?php else: ?>
           <ul class="navbar-nav">
               <li class="nav-item">
                   <a class="btn" href="<?php echo e(route('frontend.login')); ?>">
                       <?php echo app('translator')->get('auth.login'); ?>
                   </a>
               </li>
           </ul>
        <?php endif; ?>

        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <a class="dropdown-item" href="<?php echo e(route('voyager.dashboard')); ?>" target="_blank"><i class="fa fa-id-card" aria-hidden="true"></i> <?php echo app('translator')->get('content.cabinet'); ?></a>
            <a class="dropdown-item" href="<?php echo e(route('frontend.logout')); ?>"><i class="fa fa-lock fa-1x"></i> <?php echo app('translator')->get('auth.logout'); ?></a>
        </div>

    </div>

    <div class="dropdown language-dropdown">
        <a class="btn dropdown-toggle" href="<?php echo e(url('locale/en')); ?>" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           <?php echo e((\Illuminate\Support\Facades\Session::get('locale') ? strtoupper(\Illuminate\Support\Facades\Session::get('locale')) : config('baiterek.locales.KZ'))); ?>

        </a>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <?php $__currentLoopData = config('baiterek.locales'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $langs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if(!empty(\Illuminate\Support\Facades\Session::get('locale'))): ?>
                    <?php if(strtoupper($langs) != strtoupper(\Illuminate\Support\Facades\Session::get('locale'))): ?>
                        <a class="dropdown-item" href="<?php echo e(url('locale/'.$langs)); ?>"><?php echo e(strtoupper($langs)); ?></a>
                    <?php endif; ?>
                <?php else: ?>
                    <?php if(strtoupper($langs)!='KZ'): ?>
                        <a class="dropdown-item" href="<?php echo e(url('locale/'.$langs)); ?>"><?php echo e(strtoupper($langs)); ?></a>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</div>
<?php /**PATH /var/www/html/baiterek_web/resources/views/frontend/layouts/header.blade.php ENDPATH**/ ?>