<?php if(!empty(\App\Helper\Helper::getBannerCategoryBySlug('home-page-middle-block'))): ?>
<div class="business-banner container-fluid" style="background-image:url(<?php echo e(asset('storage/' . \App\Helper\Helper::getBannersImages('home-page-middle-block')[0]->image)); ?>)">
    <div class="row h-100">
        <div class="offset-lg-8 offset-md-6"></div>
        <div class="col-lg-4 col-md-6 black-div">
            <?php $__currentLoopData = \App\Helper\Helper::getBannersImages('home-page-middle-block'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $homepagemiddleblock): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <h3><?php echo $homepagemiddleblock->primary_text; ?></h3>
                <p><?php echo $homepagemiddleblock->secondary_text; ?></p>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</div>
<?php endif; ?>
<?php /**PATH /var/www/html/baiterek_web/resources/views/frontend/layouts/homepagemiddleblock.blade.php ENDPATH**/ ?>