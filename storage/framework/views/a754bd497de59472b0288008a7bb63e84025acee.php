<?php app()->setLocale(\Session::get('locale')) ?>

<?php if(!empty($menuView) || $menuView == 'ProductManager' ): ?>
    <?php $__env->startSection('page_title', __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' Product Manager'); ?>
<?php else: ?>
    <?php $__env->startSection('page_title', __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular')); ?>
<?php endif; ?>

<?php $__env->startSection('css'); ?>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<?php $__env->stopSection(); ?>
<?php
$supervisor_role = Config::get('baiterek.roles.supervisor');
$addEdit = (empty($dataTypeContent->id) ? 'add' : 'edit');
?>
<?php $__env->startSection('page_header'); ?>
    <h1 class="page-title">
        <i class="<?php echo e($dataType->icon); ?>"></i>
        <?php if(!empty($menuView) || $menuView == 'ProductManager' ): ?>
            <?php echo e(__('portal.product-managers.product-managers_'.(isset($dataTypeContent->id) ? 'edit' : 'add'))); ?>

        <?php else: ?>

            <?php echo e(__('portal.users.'.(isset($dataTypeContent->id) ? 'edit' : 'add').'_'.str_replace(' ', '_', strtolower($dataType->display_name_plural)))); ?>

        <?php endif; ?>
    </h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="page-content container-fluid">
            <?php if(!empty($menuView) || $menuView == 'ProductManager' ): ?>
                <form class="form-edit-add" role="form"
                      action="<?php if(!is_null($dataTypeContent->getKey())): ?><?php echo e(route('voyager.product-managers.update', $dataTypeContent->getKey())); ?><?php else: ?><?php echo e(route('voyager.product-managers.store')); ?><?php endif; ?>"
                      method="POST" enctype="multipart/form-data" autocomplete="off">
            <?php else: ?>
                <form class="form-edit-add" role="form"
                      action="<?php if(!is_null($dataTypeContent->getKey())): ?><?php echo e(route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey())); ?><?php else: ?><?php echo e(route('voyager.'.$dataType->slug.'.store')); ?><?php endif; ?>"
                      method="POST" enctype="multipart/form-data" autocomplete="off">
            <?php endif; ?>

            <!-- PUT Method if we are editing -->
            <?php if(isset($dataTypeContent->id)): ?>
                <?php echo e(method_field("PUT")); ?>

            <?php endif; ?>
            <?php echo e(csrf_field()); ?>


                <input type="hidden" name="address" value="Test">
                <input type="hidden" name="gender" value="Male">
                <input type="hidden" name="city" value="Test">
                <input type="hidden" name="VAT_no" value="<?php echo e(uniqid()); ?>">
                <input type="hidden" name="postcode" value="000">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">
                        
                        <?php if(count($errors) > 0): ?>
                            <div class="alert alert-danger">
                                <ul>
                                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><?php echo e($error); ?></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        <?php endif; ?>

                        <div class="panel-body">
                            <div class="form-group">
                                <label for="name"><?php echo e(__('portal.users.name')); ?></label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="<?php echo e(__('portal.users.name')); ?>"
                                       value="<?php echo e(old('name', $dataTypeContent->name ?? '')); ?>">
                            </div>

                            <div class="form-group">
                                <label for="last_name"><?php echo e(__('portal.users.last_name')); ?></label>
                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="<?php echo e(__('portal.users.last_name')); ?>"
                                       value="<?php echo e(old('last_name', $dataTypeContent->last_name ?? '')); ?>">
                            </div>

                            <div class="form-group">
                                <label for="email"><?php echo e(__('portal.users.email')); ?></label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="<?php echo e(__('portal.users.email')); ?>"
                                       value="<?php echo e(old('email', $dataTypeContent->email ?? '')); ?>">
                            </div>

                            <?php if(!empty($menuView) || $menuView == 'ProductManager' ): ?>
                                <input type="hidden" name="role_id" value="<?php echo e((!empty($dataTypeContent->role_id) ? $dataTypeContent->role_id : $productManagerRole->id)); ?>">
                            <?php else: ?>
                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('editRoles', $dataTypeContent)): ?>
                                    <div class="form-group">
                                        <label for="default_role"><?php echo e(__('voyager::profile.role_default')); ?></label>
                                        <?php
                                            $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};
                                            $row     = $dataTypeRows->where('field', 'user_belongsto_role_relationship')->first();
                                            $options = $row->details;
                                        ?>
                                        <?php echo $__env->make('voyager::formfields.relationship', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="additional_roles"><?php echo e(__('voyager::profile.roles_additional')); ?></label>
                                        <?php
                                            $row     = $dataTypeRows->where('field', 'user_belongstomany_role_relationship')->first();
                                            $options = $row->details;
                                            $model = app($options->model);
                                            $query = $model::get();
                                        ?>
                                        <select class="form-control select2" name="user_belongstomany_role_relationship[]" multiple>
                                            <?php $__currentLoopData = $query; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $relationshipData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if(isset($user_roles) && count($user_roles) != 0): ?>
                                                    <?php $__currentLoopData = $user_roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user_role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($relationshipData->{$options->key}); ?>" <?php if(old('user_belongstomany_role_relationship') !== null): ?> <?php if(in_array($relationshipData->{$options->key}, old('user_belongstomany_role_relationship'))): ?><?php echo e('selected="selected"'); ?><?php endif; ?> <?php else: ?> <?php if($user_role->role_id == $relationshipData->{$options->key}): ?><?php echo e('selected="selected"'); ?><?php endif; ?> <?php endif; ?>><?php echo e($relationshipData->{$options->label}); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php else: ?>
                                                    <option value="<?php echo e($relationshipData->{$options->key}); ?>" <?php if(old('user_belongstomany_role_relationship') !== null): ?> <?php if(in_array($relationshipData->{$options->key}, old('user_belongstomany_role_relationship'))): ?><?php echo e('selected="selected"'); ?><?php endif; ?> <?php endif; ?>><?php echo e($relationshipData->{$options->label}); ?></option>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>

                            <div class="form-group subsidiaryList">
                                <label for="report_type"><?php echo e(__('portal.subsidiaries.Еншілес_ұйымдар')); ?>: </label>
                                <select class="form-control" name="subsidiary_id" id="subsidiary_id">
                                    
                                    <?php if(isset($subsidiaries) && count($subsidiaries) != 0): ?>
                                        <?php $__currentLoopData = $subsidiaries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($values->id); ?>" <?php if(isset($subsidiaryUser->subsidiary_id) && $subsidiaryUser->subsidiary_id == $values->id): ?> selected="selected" <?php endif; ?>><?php echo e($values->title); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </select>
                            </div>

                            
                                
                                
                            
                            <div class="form-group">
                                <label for="contact"><?php echo e(__('portal.users.contact')); ?></label>
                                <input type="text" class="form-control" id="contact" name="contact" placeholder="<?php echo e(__('portal.users.contact')); ?>"
                                       value="<?php echo e(old('contact', $dataTypeContent->contact ?? '')); ?>">
                            </div>
                            
                                
                                
                                       
                            
                            
                                
                                
                                       
                            
                            
                                
                                
                                    
                                        
                                        
                                        
                                    
                                    
                                        
                                        
                                        
                                    
                                
                            
                            
                                
                                
                                       
                            
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary save">
                                        <?php if(isset($dataTypeContent->id)): ?> Update <?php else: ?><?php echo e(__('voyager::generic.save')); ?><?php endif; ?>
                                    </button>
                                </div>
                            </div>
                            <?php
                                if (isset($dataTypeContent->locale)) {
                                    $selected_locale = $dataTypeContent->locale;
                                } else {
                                    $selected_locale = config('app.locale', 'kz');
                                }
                            ?>

                        </div>
                    </div>
                </div>
        </form>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="<?php echo e(route('voyager.upload')); ?>" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            <?php echo e(csrf_field()); ?>

            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="<?php echo e($dataType->slug); ?>">
        </form>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script>
        $('document').ready(function () {

            $('.toggleswitch').bootstrapToggle();
            $('input[name=role_id]').select2();
            if('<?php echo e($addEdit); ?>' == 'edit'){
                if($("select[name=role_id] option:selected").text().toLowerCase() == '<?php echo e($supervisor_role); ?>'){
                    $('.subsidiaryList').show();
                    $('#subsidiary_id').select2();
                } else {
                    $('.subsidiaryList').hide();
                }

            } else {
                $('.subsidiaryList').hide();
            }

            var role;
            $('body').on('change','select[name=role_id]',function(e){
                role = $("select[name=role_id] option:selected").text().toLowerCase();
                if(role == '<?php echo e($supervisor_role); ?>'){

                    $('.subsidiaryList').show();
                    $('#subsidiary_id').select2();
                } else {
                    $('.subsidiaryList').hide();
                }
            });

        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('voyager::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/baiterek_web/resources/views/vendor/voyager/users/edit-add.blade.php ENDPATH**/ ?>