<div class="col-md-12">
    <h3 class="section-header"><?php echo app('translator')->get('fields.Browse Services'); ?></h3>
    

    

        <input class="form-control search-here" id="search-category" type="search" placeholder="<?php echo app('translator')->get('fields.Search'); ?>" aria-label="Search">
    
    <div class="services-list categoryList">
        <ul class="owl-carousel myCarousel owl_catagory">
            <?php if(!empty(\App\Http\Controllers\Services::getAllProductCategories('4'))): ?>
                <?php $__currentLoopData = \App\Http\Controllers\Services::getAllProductCategories('4'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $productCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li class="item">
                        <a href="<?php echo e(url('category', ['id' => $productCategory->id])); ?>">
                            <div class="service-box">
                                <div class="serice-icon"> <i class="icon-<?php echo e($productCategory->logo); ?>"></i></div>
                                <h5><?php echo e($productCategory->title); ?></h5>
                                <p><?php echo e($productCategory->description); ?></p>
                                <div class="background-icon"><i class="icon-<?php echo e($productCategory->logo); ?>"></i></div>
                            </div>
                        </a>
                    </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
        </ul>
        <a href="<?php echo e(url('categories')); ?>" class="view-all btn"><?php echo app('translator')->get('fields.View All'); ?></a>
    </div>
</div><?php /**PATH /var/www/html/baiterek_web/resources/views/frontend/layouts/categories.blade.php ENDPATH**/ ?>