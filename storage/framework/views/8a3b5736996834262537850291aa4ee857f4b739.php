<textarea class="form-control richTextBox" name="<?php echo e($row->field); ?>" id="richtext<?php echo e($row->field); ?>">
    <?php if(isset($dataTypeContent->{$row->field})): ?>
        <?php echo e(old($row->field, $dataTypeContent->{$row->field})); ?>

    <?php else: ?>
        <?php echo e(old($row->field)); ?>

    <?php endif; ?>
</textarea>
<?php /**PATH /var/www/html/baiterek_web/resources/views/vendor/voyager/formfields/rich_text_box.blade.php ENDPATH**/ ?>