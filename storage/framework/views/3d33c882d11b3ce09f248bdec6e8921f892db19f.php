<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Digital Baiterek</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <style type="text/css">
        @import  url('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700');
        body { font-family: 'Open Sans' !important; }
    </style>
</head>

<body style="margin: 0; padding: 0;">
<?php app()->setLocale($userLang ?? 'en') ?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;" bgcolor="#ffffff">
                <tr>
                    <td align="center" bgcolor="#1161a8" style="background-color: #2D803D">
                        <table border="0" cellpadding="0" cellspacing="20">
                            <tr><td></td></tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <?php echo $__env->yieldContent('content'); ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#e4f5ff" style="background-color: #6DBD94">
                            <!-- Spacing -->
                            <tr><td bgcolor="#e4f5ff" style="background-color: #6DBD94" height="20">&nbsp;</td></tr>
                            <!-- Spacing -->
                            <tr style="color: #ffffff;">
                                <td align="center" style="color: #333333; background-color: #6DBD94font-family: Open Sans; font-weight: 600; font-size: 24px;line-height: 30px;"><?php echo app('translator')->get('email.thankYou'); ?></td>
                            </tr>
                            <!-- Spacing -->
                            <tr><td bgcolor="#e4f5ff" style="background-color: #6DBD94" height="5">&nbsp;</td></tr>
                            <!-- Spacing -->
                            <tr style="color: #ffffff;">
                                <td align="center" style="color: #333333; background-color: #6DBD94font-family: Open Sans; font-weight: 600; font-size: 18px;line-height: 28px;"><?php echo app('translator')->get('email.digitalBaiterekAdmin'); ?></td>
                            </tr>
                            <!-- Spacing -->
                            <tr><td bgcolor="#e4f5ff" style="background-color: #6DBD94" height="20">&nbsp;</td></tr>
                            <!-- Spacing -->
                        </table>
                    </td>
                </tr>

                
                    
                        
                            
                                
                            
                        
                    
                
            </table>
        </td>
    </tr>
</table>
</body>

</html>

<?php /**PATH /var/www/html/baiterek_web/resources/views/vendor/voyager/emails/default.blade.php ENDPATH**/ ?>