<?php if(!empty(\App\Helper\Helper::getBannerCategoryBySlug('company-logos'))): ?>
<div class="col-md-12">
    <ul>
        <?php $__currentLoopData = \App\Helper\Helper::getBannersImages('company-logos'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $companybanners): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li>
                <img src="<?php echo e(asset('storage/' . $companybanners->image)); ?>" class="img-fluid" alt="">
            </li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
</div>
<?php endif; ?><?php /**PATH /var/www/html/baiterek_web/resources/views/frontend/layouts/companylogos.blade.php ENDPATH**/ ?>