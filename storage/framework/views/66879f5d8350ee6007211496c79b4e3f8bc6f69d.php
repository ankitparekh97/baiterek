<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <meta name="_url" content="<?php echo e(URL('')); ?>">
    <link rel="icon" href="<?php echo e(asset('images/Frame.png')); ?>" type="image/png" >


    <title><?php echo e(__('content.title_statement')); ?></title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo e(asset('css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/bootstrap-datetimepicker.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/toastr.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/owl.carousel.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/owl.theme.default.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/custom.css')); ?>">

    <script src="<?php echo e(asset('js/jquery-3.3.1.min.js')); ?>"></script>
    
    <style>
        #toast-container {
            position: fixed;
            z-index: 999999;
            pointer-events: none;
            left:50% !important;
            transform: translate(-50%, 0px);
        }
        .toast-top-right {
            right: auto !important;
        }
    </style>
</head>

<body>

    <div id="loader" style="display: none;">
        <img src="<?php echo e(asset('images/loader.gif')); ?>" alt="Loader">
    </div>
    <main>
        <section class="login-register">
            <div class="container-fluid">
                <div class="row">
                    <?php echo $__env->yieldContent('customer'); ?>
                </div>
            </div>
        </section>
    </main>


    <script type="application/javascript">
        var baseUrl = $('meta[name="_url"]').attr('content');
        var assetBaseUrl = "<?php echo e(asset('')); ?>";

        <?php if(session('success')): ?>
            toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

            toastr.success("<?php echo e(\Illuminate\Support\Facades\Session::get('success')); ?>");
        <?php endif; ?>

        <?php if(session('error')): ?>
            toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

            toastr.error("<?php echo e(\Illuminate\Support\Facades\Session::get('error')); ?>");
        <?php endif; ?>


        // login register
        $(function () {
            $('.owl-carousel').owlCarousel({
                loop:true,
                margin:0,
                nav:false,
                dots:true,
                items: 1,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    1000:{
                        items:1
                    }
                }
            })
        });

    </script>

</body>
<footer>
    <script src="<?php echo e(asset('js/popper.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/owl.carousel.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/moment.js')); ?>"></script>
    <script src="<?php echo e(asset('js/bootstrap-datetimepicker.js')); ?>"></script>
    <script src="<?php echo e(asset('js/jquery.validate.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/toastr.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/frontend.js')); ?>"></script>
</footer>
</html>
<?php /**PATH /var/www/html/baiterek_web/resources/views/frontend/usermaster.blade.php ENDPATH**/ ?>