<!DOCTYPE html>
<html>
<head>
    <title>Log Activity Lists</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
</head>
<body>


<div class="container">
    <h1>Log Activity Lists</h1>
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Time</th>
            <th>Subject</th>
            <th>URL</th>
            <th>Method</th>
            <th>Ip</th>
            <th width="300px">User Agent</th>
            <th>User Email</th>
            <th>User name</th>
            <th>User Role</th>

        </tr>
        <?php if($logs->count()): ?>
            <?php $__currentLoopData = $logs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $log): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e(++$key); ?></td>
                    <td><?php echo e(\Carbon\Carbon::parse($log->created_at)->format('d/m/Y H:i:s')); ?></td>
                    <td><?php echo e($log->subject); ?></td>
                    <td class="text-success"><?php echo e($log->url); ?></td>
                    <td><label class="label label-info"><?php echo e($log->method); ?></label></td>
                    <td class="text-warning"><?php echo e($log->ip); ?></td>
                    <td class="text-danger"><?php echo e($log->agent); ?></td>
                    <td><?php echo e($log->users->email); ?></td>
                    <td><?php echo e($log->users->name); ?></td>
                    <td><?php echo e($log->users->role->name); ?></td>

                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
    </table>
</div>


</body>
</html>
<?php /**PATH /var/www/html/baiterek_web/resources/views/frontend/logActivity.blade.php ENDPATH**/ ?>