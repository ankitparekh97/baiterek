<?php $__env->startSection('page_title', __('voyager::generic.viewing').' '.$dataType->getTranslatedAttribute('display_name_plural')); ?>

<?php $__env->startSection('page_header'); ?>
    <?php app()->setLocale(\Session::get('locale')) ?>
    <h1 class="page-title">
        <i class="voyager-list-add"></i> <?php echo e(__('portal.menu.'.str_replace(' ', '_', strtolower($dataType->display_name_plural)))); ?>


        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('add',app($dataType->model_name))): ?>
            <a href="<?php echo e(route('voyager.'.$dataType->slug.'.create')); ?>" class="btn btn-success">
                <i class="voyager-plus"></i> <?php echo e(__('portal.menu.add_new')); ?>

            </a>
        <?php endif; ?>
    </h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('voyager::menus.partial.notice', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <div class="page-content container-fluid">
        <?php echo $__env->make('voyager::alerts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <table id="dataTable" class="table table-hover">
                            <thead>
                            <tr>
                                <?php $__currentLoopData = $dataType->browseRows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <th><?php echo e(__('portal.menu.'.str_replace(' ', '_', strtolower( $row->display_name )))); ?></th>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <th class="actions text-right"><?php echo e(__('portal.menu.actions')); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $dataTypeContent; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <?php $__currentLoopData = $dataType->browseRows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <td>
                                        <?php if($row->type == 'image'): ?>
                                            <img src="<?php if( strpos($data->{$row->field}, 'http://') === false && strpos($data->{$row->field}, 'https://') === false): ?><?php echo e(Voyager::image( $data->{$row->field} )); ?><?php else: ?><?php echo e($data->{$row->field}); ?><?php endif; ?>" style="width:100px">
                                        <?php else: ?>
                                            <?php echo e($data->{$row->field}); ?>

                                        <?php endif; ?>
                                    </td>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <td class="no-sort no-click bread-actions">
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete', $data)): ?>
                                            <div class="btn btn-sm btn-danger pull-right delete" data-id="<?php echo e($data->{$data->getKeyName()}); ?>">
                                                <i class="voyager-trash"></i> <?php echo e(__('portal.menu.delete')); ?>

                                            </div>
                                        <?php endif; ?>
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $data)): ?>
                                            <a href="<?php echo e(route('voyager.'.$dataType->slug.'.edit', $data->{$data->getKeyName()})); ?>" class="btn btn-sm btn-primary pull-right edit">
                                                <i class="voyager-edit"></i> <?php echo e(__('portal.menu.edit')); ?>

                                            </a>
                                        <?php endif; ?>
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $data)): ?>
                                            <a href="<?php echo e(route('voyager.'.$dataType->slug.'.builder', $data->{$data->getKeyName()})); ?>" class="btn btn-sm btn-success pull-right">
                                                <i class="voyager-list"></i> <?php echo e(__('portal.menu.builder')); ?>

                                            </a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo e(__('voyager::generic.close')); ?>">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i> <?php echo e(__('voyager::generic.delete_question')); ?> <?php echo e($dataType->getTranslatedAttribute('display_name_singular')); ?>?
                    </h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        <?php echo e(method_field("DELETE")); ?>

                        <?php echo e(csrf_field()); ?>

                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="<?php echo e(__('voyager::generic.delete_this_confirm')); ?> <?php echo e($dataType->getTranslatedAttribute('display_name_singular')); ?>">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal"><?php echo e(__('voyager::generic.cancel')); ?></button>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <!-- DataTables -->
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable({
                "order": [],
                "language": {
                    "sEmptyTable":     "<?php echo e(__('data-table.sEmptyTable')); ?>",
                    "sInfo":           "<?php echo e(__('data-table.sInfo')); ?>",
                    "sInfoEmpty":      "<?php echo e(__('data-table.sInfoEmpty')); ?>",
                    "sInfoFiltered":   "<?php echo e(__('data-table.sInfoFiltered')); ?>",
                    "sInfoPostFix":    "<?php echo e(__('data-table.sInfoPostFix')); ?>",
                    "sInfoThousands":  "<?php echo e(__('data-table.sInfoThousands')); ?>",
                    "sLengthMenu":     "<?php echo e(__('data-table.sLengthMenu')); ?>",
                    "sLoadingRecords": "<?php echo e(__('data-table.sLoadingRecords')); ?>",
                    "sProcessing":     "<?php echo e(__('data-table.sProcessing')); ?>",
                    "sSearch":         "<?php echo e(__('data-table.sSearch')); ?>",
                    "sZeroRecords":    "<?php echo e(__('data-table.sZeroRecords')); ?>",
                    "oPaginate": {
                        "sFirst":    "<?php echo e(__('data-table.sFirst')); ?>",
                        "sLast":     "<?php echo e(__('data-table.sLast')); ?>",
                        "sNext":     "<?php echo e(__('data-table.sNext')); ?>",
                        "sPrevious": "<?php echo e(__('data-table.sPrevious')); ?>"
                    },
                    "oAria": {
                        "sSortAscending":  "<?php echo e(__('data-table.sSortAscending')); ?>",
                        "sSortDescending": "<?php echo e(__('data-table.sSortDescending')); ?>"
                    }
                },
                "columnDefs": [{"targets": -1, "searchable":  false, "orderable": false}]
                <?php if(config('dashboard.data_tables.responsive')): ?>, responsive: true <?php endif; ?>
            });
        });

        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '<?php echo e(route('voyager.'.$dataType->slug.'.destroy', ['id' => '__menu'])); ?>'.replace('__menu', $(this).data('id'));

            $('#delete_modal').modal('show');
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('voyager::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/baiterek_web/resources/views/vendor/voyager/menus/browse.blade.php ENDPATH**/ ?>