<!DOCTYPE html>
<html lang="<?php echo e(config('app.locale')); ?>" dir="<?php echo e(__('voyager::generic.is_rtl') == 'true' ? 'rtl' : 'ltr'); ?>">
<head>
    <title><?php echo $__env->yieldContent('page_title', setting('admin.title') . " - " . setting('admin.description')); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>"/>
    <meta name="assets-path" content="<?php echo e(route('voyager.voyager_assets')); ?>"/>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">

    <!-- Favicon -->
    <?php $admin_favicon = Voyager::setting('admin.icon_image', ''); ?>
    <?php if($admin_favicon == ''): ?>
        <link rel="shortcut icon" href="<?php echo e(voyager_asset('images/logo-icon.png')); ?>" type="image/png">
    <?php else: ?>
        <link rel="shortcut icon" href="<?php echo e(Voyager::image($admin_favicon)); ?>" type="image/png">
    <?php endif; ?>

    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


    <!-- App CSS -->
    <link rel="stylesheet" href="<?php echo e(voyager_asset('css/app.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/smart_wizard.css')); ?>">

    <link href="<?php echo e(asset('css/smart_wizard_theme_circles.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('css/smart_wizard_theme_arrows.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('css/smart_wizard_theme_dots.css')); ?>" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo e(asset('css/bootstrap-datetimepicker.min.css')); ?>">
    <link href="<?php echo e(asset('css/loader.css')); ?>" rel="stylesheet" type="text/css" />

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">

    <?php echo $__env->yieldContent('css'); ?>
    <?php if(__('voyager::generic.is_rtl') == 'true'): ?>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.4.0/css/bootstrap-rtl.css">
        <link rel="stylesheet" href="<?php echo e(voyager_asset('css/rtl.css')); ?>">
    <?php endif; ?>

    <!-- 2 Few Dynamic Styles -->
    <style type="text/css">
        .voyager .side-menu .navbar-header {
            background:<?php echo e(config('voyager.primary_color','#22A7F0')); ?>;
            border-color:<?php echo e(config('voyager.primary_color','#22A7F0')); ?>;
        }
        .widget .btn-primary{
            border-color:<?php echo e(config('voyager.primary_color','#22A7F0')); ?>;
        }
        .widget .btn-primary:focus, .widget .btn-primary:hover, .widget .btn-primary:active, .widget .btn-primary.active, .widget .btn-primary:active:focus{
            background:<?php echo e(config('voyager.primary_color','#22A7F0')); ?>;
        }
        .voyager .breadcrumb a{
            color:<?php echo e(config('voyager.primary_color','#22A7F0')); ?>;
        }
        .hamburger_btn{
            display: none;
        }
        @media  only screen and (max-width: 600px) {
            .hamburger_btn{
                display: block;
            }
        }
        #toast-container {
            position: fixed;
            z-index: 999999;
            pointer-events: none;
            left:50% !important;
            transform: translate(-50%, 0px);
        }
        .toast-top-right {
           right: auto !important;
        }
    </style>

    <?php if(!empty(config('voyager.additional_css'))): ?><!-- Additional CSS -->
        <?php $__currentLoopData = config('voyager.additional_css'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $css): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><link rel="stylesheet" type="text/css" href="<?php echo e(asset($css)); ?>"><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
    <link rel="stylesheet" href="<?php echo e(asset('css/app.css')); ?>">
    <?php echo $__env->yieldContent('head'); ?>
</head>

<body class="voyager <?php if(isset($dataType) && isset($dataType->slug)): ?><?php echo e($dataType->slug); ?><?php endif; ?>">
<div class="loading" style="display: none;z-index: 999999 !important;">Loading&#8230;</div>
<div id="voyager-loader">
    <?php $admin_loader_img = Voyager::setting('admin.loader', ''); ?>
    <?php if($admin_loader_img == ''): ?>
        <img src="<?php echo e(voyager_asset('images/logo-icon.png')); ?>" alt="Voyager Loader">
    <?php else: ?>
        <img src="<?php echo e(Voyager::image($admin_loader_img)); ?>" alt="Voyager Loader">
    <?php endif; ?>
</div>

<?php
if (\Illuminate\Support\Str::startsWith(Auth::user()->avatar, 'http://') || \Illuminate\Support\Str::startsWith(Auth::user()->avatar, 'https://')) {
    $user_avatar = Auth::user()->avatar;
} else {
    $user_avatar = Voyager::image(Auth::user()->avatar);
}
?>

<div class="app-container">
    <div class="fadetoblack visible-xs"></div>
    <div class="row content-container">
        <?php echo $__env->make('voyager::dashboard.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php echo $__env->make('voyager::dashboard.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <script>
            (function(){
                    var appContainer = document.querySelector('.app-container'),
                        sidebar = appContainer.querySelector('.side-menu'),
                        navbar = appContainer.querySelector('nav.navbar.navbar-top'),
                        loader = document.getElementById('voyager-loader'),
                        hamburgerMenu = document.querySelector('.hamburger'),
                        sidebarTransition = sidebar.style.transition,
                        navbarTransition = navbar.style.transition,
                        containerTransition = appContainer.style.transition;

                    sidebar.style.WebkitTransition = sidebar.style.MozTransition = sidebar.style.transition =
                    appContainer.style.WebkitTransition = appContainer.style.MozTransition = appContainer.style.transition =
                    navbar.style.WebkitTransition = navbar.style.MozTransition = navbar.style.transition = 'none';

                    if (window.innerWidth > 768 && window.localStorage && window.localStorage['voyager.stickySidebar'] == 'true') {
                        appContainer.className += ' expanded no-animation';
                        loader.style.left = (sidebar.clientWidth/2)+'px';
                        hamburgerMenu.className += ' is-active no-animation';
                    }

                   navbar.style.WebkitTransition = navbar.style.MozTransition = navbar.style.transition = navbarTransition;
                   sidebar.style.WebkitTransition = sidebar.style.MozTransition = sidebar.style.transition = sidebarTransition;
                   appContainer.style.WebkitTransition = appContainer.style.MozTransition = appContainer.style.transition = containerTransition;
            })();
        </script>
        <!-- Main Content -->
        <div class="container-fluid">
            <div class="side-body padding-top">
                <?php echo $__env->yieldContent('page_header'); ?>
                <div id="voyager-notifications"></div>
                <?php echo $__env->yieldContent('content'); ?>
            </div>
        </div>
    </div>
</div>
<?php echo $__env->make('voyager::partials.app-footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<!-- Javascript Libs -->

<script type="text/javascript" src="<?php echo e(voyager_asset('js/app.js')); ?>"></script>


<script type="text/javascript" src="<?php echo e(asset('js/jquery.smartWizard.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/form-builder-oldercode.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/form-builder-kz-old.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/form-builder-ru-old.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/jquery-ui.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/form-render.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/moment.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/bootstrap-datetimepicker.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.validate.min.js')); ?>"></script>


<script>
    var baseUrl = $('meta[name="_url"]').attr('content');
    var assetBaseUrl = "<?php echo e(asset('')); ?>";

    <?php if(Session::has('alerts')): ?>
        let alerts = <?php echo json_encode(Session::get('alerts')); ?>;
        helpers.displayAlerts(alerts, toastr);
    <?php endif; ?>

    <?php if(Session::has('message')): ?>

    // TODO: change Controllers to use AlertsMessages trait... then remove this
    var alertType = <?php echo json_encode(Session::get('alert-type', 'info')); ?>;
    var alertMessage = <?php echo json_encode(Session::get('message')); ?>;
    var alerter = toastr[alertType];

    if (alerter) {
        alerter(alertMessage);
    } else {
        toastr.error("toastr alert-type " + alertType + " is unknown");
    }
    <?php endif; ?>

    $("span.voyager-tools").parent().parent().hide();
    $( "#sidebar_column" ).hover(function() {
        $('.app-container').addClass('expanded');
    });
    $( "#sidebar_column" ).mouseout(function() {
        $('.app-container').removeClass('expanded');
    });
        /** loading overlay start */
        function showLoading() {
            $('.loading').css('display', 'block');
        }
        function hideLoading() {
            $('.loading').css('display', 'none');
        }
    /** loading overlay end */
    $('textarea').each(function () {
            $(this).val($(this).val().trim());
        }
    );
//    expanded
    <?php if(auth()->user()->role->slug != 'admin' && auth()->user()->role->slug != 'supervisor'): ?>
        $(document).ready(function(){
            $('.product_manager_remove').parent().parent().remove();
        });
    <?php endif; ?>
    <?php if(auth()->user()->role->slug != 'admin'): ?>
        $(document).ready(function(){
            $('.remove_dashboard_manu').parent().parent().remove();
        });
    <?php endif; ?>
</script>
<?php echo $__env->make('voyager::media.manager', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->yieldContent('javascript'); ?>
<?php echo $__env->yieldPushContent('javascript'); ?>
<?php if(!empty(config('voyager.additional_js'))): ?><!-- Additional Javascript -->
    <?php $__currentLoopData = config('voyager.additional_js'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $js): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><script type="text/javascript" src="<?php echo e(asset($js)); ?>"></script><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>

</body>
</html>
<?php /**PATH /var/www/html/baiterek_web/resources/views/vendor/voyager/master.blade.php ENDPATH**/ ?>