<?php $__env->startSection('page_title', __('voyager::generic.view').' '.$dataType->display_name_singular); ?>

<?php $__env->startSection('page_header'); ?>
    <style>
        .cursor{
            cursor: pointer;
        }
        .statusReject{
            background-color: #ffb3b3 !important;
            padding: 20px 25px;
            color: black;
            margin-top: 15px;
            border-radius: 10px;
        }
        .statusAccept{
            background-color: #43d17f !important;
            padding: 20px 25px;
            color: black;
            margin-top: 15px;
            border-radius: 10px;
        }
        .inquirySend{
            background-color: #eaeaea;
            padding: 20px 25px;
            color: black;
            margin-top: 15px;
            border-radius: 10px;
        }
        .comment{
            background-color: wheat;
            padding: 20px 25px;
            color: black;
            margin-top: 15px;
            border-radius: 10px;
        }
    </style>
    <h1 class="page-title">
        <i class="<?php echo e($dataType->icon); ?>"></i> <?php echo e(__('portal.service_inquiry.viewing_service_inquiry')); ?>


        
            
                
                
            
        

            
                
            
        

        <a href="<?php echo e(route('voyager.'.$dataType->slug.'.index')); ?>" class="btn btn-warning">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            <?php echo e(__('portal.service_inquiry.return_to_list')); ?>

        </a>
    </h1>
    <?php app()->setLocale(\Session::get('locale')) ?>
    <div class="language-selector">
        <div class="btn-group btn-group-sm" role="group">
            <?php $__currentLoopData = config('voyager.multilingual.locales'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <a class="btn btn-primary  <?php echo e(( $lang === \Illuminate\Support\Facades\Session::get('locale')) ? " active" : ""); ?>" href="<?php echo e(url('locale/'.$lang)); ?>"> <?php echo e(strtoupper($lang)); ?> </a>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <style>
        .permissions-table {
            width: 75% !important;
        }
        a.exportData, a.exportData:hover {
            color: #49Bc9C;
            font-weight: bold;
        }
        #voyager-loader {
            background: transparent !important;
        }
    </style>
    <div class="page-content read container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered" style="padding-bottom:5px;">
                    <!-- form start -->


                                <div class="modal-header">
                                    <h4 class="modal-title" id="k-m-title"><?php echo $productTitle; ?></h4>
                                    
                                </div>
                                <div class="modal-body">
                                    <div class="dataExport" align="center">
                                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i> <a href="<?php echo e(route('voyager.serviceInquiries.exportServicePdf')); ?>?type=pdf&id=<?php echo e($id); ?>" class="exportData" id="pdfClick" target="_blank"><?php echo e(trans('content.ExportToPDF')); ?></a> |
                                        <i class="fa fa-file-excel-o" aria-hidden="true"></i> <a href="<?php echo e(route('voyager.serviceInquiries.exportServicePdf')); ?>?type=csv&id=<?php echo e($id); ?>" class="exportData" target="_blank" id="csvClick"><?php echo e(trans('content.ExportToCSV')); ?></a>
                                    </div>
                                    <div id="content_detail">
                                        <?php echo $formHtml; ?>

                                    </div>
                                    <?php if(auth()->user()->role->slug != 'customer'): ?>
                                        <hr>
                                        <div class="form-group form-inline">
                                            <div class="col-md-1" style="padding: 0; margin: 0;">
                                                <img src="<?php echo e(asset('storage/users/default.png')); ?>" style="border-radius: 50%;width: 40px;">
                                            </div>
                                            <div class="col-md-11" style="padding: 0; margin: 0;">
                                                <input type="hidden" id="msgBoxId" value="<?php echo e($id); ?>">
                                                <textarea class="form-control" id="messageBox" placeholder="<?php echo e(__('content.write_a_message')); ?>" dir="auto" style="height: 40px;width: 100%; cursor: pointer"></textarea>
                                                <button type="button" id="saveBtn" class="btn btn-success pull-right"><?php echo e(trans('content.save')); ?></button>
                                            </div>
                                        </div>
                                        <div class="col-md-12" style="margin:5px; padding: 2px;border: 1px solid #e4eaec;">
                                            <form enctype="multipart/form-data" method="post" action="<?php echo e(url('CustomerFile')); ?>">
                                                <?php echo csrf_field(); ?>
                                                <input type="hidden" value="<?php echo e($id); ?>" name="data_id">
                                                <div class="col-md-6">
                                                    <span class="form-control" style="border: none;">Document for customer</span>
                                                    <input multiple id="document_customer" name="document_customer[]" type="file" class="form-control" style="border: none;">
                                                </div>
                                                <div class="col-md-6">
                                                    <button type="submit" id="uploadBtn" style="float: right" class="btn btn-success"><?php echo e(trans('content.upload')); ?></button>
                                                </div>
                                            </form>
                                        </div>
                                    <?php endif; ?>
                                    <div id="content_body"><?php echo $activityHtml; ?></div>
                                </div>

                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <?php if($isModelTranslatable): ?>
        <script>
            // $("#messageBox").focusout(function(){
            //     $('#saveBtn').hide();
            // });
            // $('#messageBox').click(function(){
            //     $('#saveBtn').show();
            // });
            $(document).ready(function () {
                $('.side-body').multilingual();
            });
        </script>
        <script src="<?php echo e(voyager_asset('js/multilingual.js')); ?>"></script>
    <?php endif; ?>
    <script>
        <?php if(auth()->user()->role->slug != 'customer'): ?>
        $('#saveBtn').click(function(){
            // $('#saveBtn').hide();
            var message = $('#messageBox').val();
            $.ajax({
                url: "<?php echo e(route('voyager.'.$dataType->slug.'.ajaxMessageActivity')); ?>",
                type: 'POST',
                data: {
                    'message': message,
                    'data_id': $('#msgBoxId').val(),
                    'lang': '<?php echo e(\Illuminate\Support\Facades\Session::get('locale')); ?>',
                },
                success: function (data) {
                    toastr.clear();
                    if (data.status === true) {
                        $('#activityList').prepend(data.html);
                        $('#messageBox').val('');
                    } else {
                        toastr.warning(data.message);
                    }
                },
                error: function () {
                    console.log('request error');
                    toastr.error("Oops something went wrong!");
                }
            });
        });

        $('body').on('change','.checkPoint',function(){
            id = $(this).attr('data-id');
            if($(this).prop( "checked")){
                checkPointStatus("<?php echo e(\App\Models\ServiceInquiryCheckPoint::TRUE); ?>",id);
                $(this).next().addClass('lineThrough')
            }else{
                checkPointStatus("<?php echo e(\App\Models\ServiceInquiryCheckPoint::FALSE); ?>",id);
                $(this).next().removeClass('lineThrough')
            }
        });

        $('body').on('change','.checkPointComment',function(){
            comment = 'null';
            if($(this).val() !== ''){
                comment = $(this).val();
            }
            $.ajax({
                url: "<?php echo e(route('voyager.'.$dataType->slug.'.checkPointComment')); ?>",
                type: 'POST',
                data: {
                    'data_id': $(this).attr('data-id'),
                    'comment' : comment,
                    'lang': '<?php echo e(\Illuminate\Support\Facades\Session::get('locale')); ?>',
                },
                success: function (data) {
                    toastr.clear();
                    if (data.status === true) {
                        $('#activityList').prepend(data.html);
                    } else {
                        console.log(data.message);
                    }
                },
                error: function () {
                    console.log('request error');
                    toastr.error("Oops something went wrong!");
                }
            });
        });
        var documentSendArray = [];
        $('body').on('click','.inviteButton',function(){
            documentSendArray = [];
            $("input[name='document_send[]']:checked").each(function (key, value) {
                documentSendArray.push($(this).val());
            });
            var emails = $('.inviteUsers').val();
            $.ajax({
                url: "<?php echo e(route('voyager.'.$dataType->slug.'.ajaxInviteUsers')); ?>",
                async:false,
                type: 'POST',
                data: {
                    'emails': emails,
                    'data_id': $(this).attr('data-id'),
                    'documentsSend' : documentSendArray,
                    'lang': '<?php echo e(\Illuminate\Support\Facades\Session::get('locale')); ?>',
                },
                success: function (data) {
                    $('#voyager-loader').hide();
                    $("input[name='document_send[]']:checked").each(function (key, value) {
                        $(this).prop('checked',false);
                    });
                    $('.inviteUsers').val('');
                    toastr.clear();
                    if (data.status === true) {
                        $('#activityList').prepend(data.html);
                        toastr.success(data.message);
                    } else {
                        toastr.warning(data.message);
                        console.log(data.message);
                    }
                },
                error: function () {
                    console.log('request error');
                    toastr.error("Oops something went wrong!");
                }
            });
        });
        <?php endif; ?>

        function checkPointStatus(status, id){
            $.ajax({
                url: "<?php echo e(route('voyager.'.$dataType->slug.'.checkPointStatus')); ?>",
                type: 'POST',
                data: {
                    'data_id': id,
                    'status': status,
                    'lang': '<?php echo e(\Illuminate\Support\Facades\Session::get('locale')); ?>',
                },
                success: function (data) {
                    console.log(data);
                    toastr.clear();
                    if (data.status === true) {
                        $('#activityList').prepend(data.html);
                    } else {
                        console.log(data.message);
                    }
                },
                error: function () {
                    console.log('request error');
                    toastr.error("Oops something went wrong!");
                }
            });
        }

        $(document).on('click','#applyNow',function(){
            var form_fields = JSON.stringify(formRender.userData);
            $('#formFields').val(form_fields);
            $('#status_of_request').val("<?php echo e(\App\Models\ServiceInquiries::APPLICATION_RECEIVED); ?>");
            if ($('#apply').valid()) {
                $('form').submit();
            }
            else {
                return false;
            }
        });
        
            

            
            
                
                
            
        
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('voyager::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/baiterek_web/resources/views/vendor/voyager/service-inquiries/read.blade.php ENDPATH**/ ?>