<?php if(count(\App\Helper\Helper::getNews())>0): ?>
<style>
    .long-desc {
        min-height: max-content !important;
        margin-bottom: 10px !important;
    }
    /*.latest_news .news-list ul {*/
        /*flex-wrap: inherit !important;*/
    /*}*/
    /*.owl-nav{*/
        /*display: none;*/
    /*}*/
</style>
<div class="col-md-12">
    <h3 class="section-header"><?php echo app('translator')->get('fields.Latest News'); ?></h3>
    <a href="<?php echo e(route('frontend.news')); ?>" class="news_viewmore" style="float: right"><?php echo app('translator')->get('fields.View All'); ?></a>
    <div class="news-list">
        <ul class="owl-carousel myCarousel owl_catagory">
            <?php $__currentLoopData = \App\Helper\Helper::getNews(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $news): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li class="item">
                    <a href="<?php echo e(url('news', ['id' => $news->id])); ?>">
                        <div class="news-box">
                            <div class="news-img">
                                <img src="<?php echo e(asset('storage/'.$news->image)); ?>" class="img-fluid" alt="">
                            </div>
                            <div class="news-content">
                                <div class="author-date">
                                    <div class="news-author">
                                        
                                    </div>
                                    <div class="news-date">
                                        <p><?php echo e(\Carbon\Carbon::parse($news->created_at)->format('d M, Y')); ?></p>
                                    </div>
                                </div>
                                <div class="news-desc">
                                    <h5><?php echo $news->short_description; ?> </h5>
                                    <p class="long-desc"><?php echo nl2br(str_limit(strip_tags($news->long_description), 150)); ?></p>
                                    <a class="news_viewmore" href="<?php echo e(url('news', ['id' => $news->id])); ?>"><?php echo app('translator')->get('fields.Read More'); ?></a>
                                    
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
</div>
<?php endif; ?>


<script type="application/javascript">
    // fact owl
    $(function () {
        $('.owl_catagory').owlCarousel({
            loop:true,
            margin:0,
            autoplayTimeout:2000,
            autoplayHoverPause:true,
            responsive: {
                1630:{
                    items:4
                },
                1261:{
                    items:3
                },
                903:{
                    items:2
                },
                0: {
                    items: 1
                }
            }
        });
    })
</script>

<?php /**PATH /var/www/html/baiterek_web/resources/views/frontend/layouts/news.blade.php ENDPATH**/ ?>