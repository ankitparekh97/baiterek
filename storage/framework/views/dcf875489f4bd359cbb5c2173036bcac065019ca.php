<?php $__env->startSection('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural); ?>

<?php $__env->startSection('head'); ?>
    <?php app()->setLocale(\Session::get('locale')) ?>
    <link rel="stylesheet" href="<?php echo e(asset('kanban/jkanban.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('typeahade/css/tokenfield-typeahead.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('typeahade/css/bootstrap-tokenfield.css')); ?>">
    <script src="<?php echo e(asset('kanban/jkanban.js')); ?>"></script>
    <style>
        #myKanban {
            overflow-x: auto;
            padding: 20px 0;
        }

        .primary {
            background: #f99fff;
            color: white;
        }

        .success {
            background: #00b961;
            color: white;
        }

        .info {
            background: #2a92bf;
            color: white;
        }

        .warning {
            background: #f4ce46;
            color: white;
        }

        .error {
            background: #fb7d44;
            color: white;
        }
        .all_lable{
            background-color: #2a323a;
            color: #ffffff;
        }
        .kanban-container{
            min-width: 100% !important;
            height: auto;
        }
        .kanban-board{
            width: 418px !important;
            overflow-y: auto;
            height: 40pc;
            background: #ebecf0;
            border: 1px solid #d9d9cd;
        }
        .kanban-board header {
            color: white;
        }
        .text_align_m{
            white-space: normal;
            text-align: left !important;
        }
        .kanban-item {
            border-radius: 15px;
            box-shadow: 0 1px 0 rgba(9,30,66,.25);
        }
        .kanban-item p {
            color: #172b4d;
        }
        a.exportData, a.exportData:hover {
            color: #49Bc9C;
            font-weight: bold;
        }
        .kanban-board header {
            font-size: 15px;
            padding: 10px;
        }

        .lineThrough {
            text-decoration: line-through;
        }

    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_header'); ?>
    <div class="container-fluid">
        <div class="row cci-page-header">
            
            <h1 class="page-title">
                <i class="<?php echo e($dataType->icon); ?>"></i> <?php echo e(__('portal.'.$dataType->display_name_plural)); ?>

                <?php if($dataType->description != ''): ?>
                    <a href="#" data-toggle="tooltip" data-placement="bottom" class="cci-tooltip"
                       title="<?php echo e($dataType->description); ?>">
                        <i class="voyager-question"></i>
                    </a>
                <?php endif; ?>
            </h1>
            <a href="<?php echo e(url('/admin/my-documents')); ?>" class="btn btn-info btn-audit-trial">
            <i class="voyager-archive"></i> <span><?php echo e(__('content.migrated_files')); ?></span>
            </a>

            
            
            
            
            
            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('add', app($dataType->model_name))): ?>
                <a href="<?php echo e(route('voyager.'.$dataType->slug.'.create')); ?>" class="btn btn-success btn-add-new">
                    <i class="voyager-plus"></i> <span><?php echo e(__('voyager::generic.add_new')); ?></span>
                </a>
            <?php endif; ?>
            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete', app($dataType->model_name))): ?>
                <?php echo $__env->make('voyager::partials.bulk-delete', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php endif; ?>
            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', app($dataType->model_name))): ?>
                <?php if(isset($dataType->order_column) && isset($dataType->order_display_column)): ?>
                    <a href="<?php echo e(route('voyager.'.$dataType->slug.'.order')); ?>" class="btn btn-primary hidden">
                        <i class="voyager-list"></i> <span><?php echo e(__('voyager::bread.order')); ?></span>
                    </a>
                <?php endif; ?>
            <?php endif; ?>

            <?php if(auth()->user()->role->slug != 'customer'): ?>
            <a href="<?php echo e(route('view_inquiries', [0])); ?>" class="btn btn-success">
                <span><i class="fa fa-table"></i></span>
            </a>
            <a href="<?php echo e(route('view_inquiries', [1])); ?>" class="btn btn-success">
                <span><i class="fa fa-columns"></i></span>
            </a>
            <?php endif; ?>

            <?php app()->setLocale(\Session::get('locale')) ?>
            <div class="language-selector">
                <div class="btn-group btn-group-sm" role="group">
                    <?php $__currentLoopData = config('voyager.multilingual.locales'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <a class="btn btn-primary  <?php echo e(( $lang === \Illuminate\Support\Facades\Session::get('locale')) ? " active" : ""); ?>" href="<?php echo e(url('locale/'.$lang)); ?>"> <?php echo e(strtoupper($lang)); ?> </a>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="page-content browse container-fluid">
        <?php echo $__env->make('voyager::alerts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <?php if(session()->get('view_inquiries_view') == 0 || session()->get('view_inquiries_view') == null): ?>
                        <div class="row justify-content-md-center">
                            <div class="col col-lg-2">
                            </div>
                            <div class="col-md-auto">
                                <div class="form-group col-md-4">
                                    <label for="report_type"><?php echo e(__('portal.product.products')); ?>:</label>
                                    <select class="form-control productLists" name="productLists" id="productLists">
                                        <option value="" selected="selected"><?php echo e(__('portal.product.all')); ?></option>
                                        <?php if(isset($productLists) && count($productLists) != 0): ?>
                                            <?php $__currentLoopData = $productLists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($values->id); ?>"><?php echo e($values->title); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                                <?php if(auth()->user()->role->slug != 'customer'): ?>
                                <div class="form-group col-md-2">
                                    <label for="report_type"><?php echo e(__('content.customer')); ?>:</label>
                                    <input class="form-control userList" name="userList" id="user_id">
                                </div>
                                <?php endif; ?>
                                <div class="form-group col-md-2">
                                    <label for="report_type"><?php echo e(__('portal.service_inquiry.registration_number')); ?>:</label>
                                    <input class="form-control request_id" name="request_id" id="request_id">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="report_type"><?php echo e(__('portal.pages.status')); ?>:</label>
                                    <select class="form-control statusList" name="statusList" id="statusList">
                                        <option value="" selected="selected"><?php echo e(__('portal.product.all')); ?></option>
                                        <?php if(isset($status_list) && count($status_list) != 0): ?>
                                            <?php $__currentLoopData = $status_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($v); ?>"><?php echo e(__('content.'.$v)); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col col-lg-1">
                            </div>
                        </div>
                        <?php endif; ?>

                        <?php if(session()->get('view_inquiries_view') == 0 || auth()->user()->role->slug == 'customer' || session()->get('view_inquiries_view') == null): ?>
                            <table class="table table-bordered" id="table">
                                <thead>
                                <tr>
                                    <th><?php echo e(__('content.title')); ?></th>
                                    <?php if(auth()->user()->role->slug != 'customer'): ?>
                                        <th><?php echo e(__('content.customer')); ?></th>
                                    <?php endif; ?>
                                    <th><?php echo e(__('content.ticket_number')); ?></th>

                                    <th><?php echo e(__('content.due')); ?></th>
                                    <th><?php echo e(__('content.details')); ?></th>
                                    <?php if(auth()->user()->role->slug != 'customer'): ?>
                                        <th><?php echo e(__('content.action')); ?></th>
                                    <?php else: ?>
                                        <th><?php echo e(__('content.status')); ?></th>
                                    <?php endif; ?>
                                </tr>
                                </thead>
                            </table>
                        <?php else: ?>
                            <div id="myKanban"></div>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
            
            <div class="modal modal-success fade" tabindex="-1" id="detail_modal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo e(__('voyager::generic.close')); ?>">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="k-m-title"><?php echo e(trans('content.ServiceInquiryDetail')); ?></h4>
                        </div>
                        <div class="modal-body">
                            <div class="dataExport" align="center">
                                <i class="fa fa-file-pdf-o" aria-hidden="true"></i> <a href="javascript:void(0)" class="exportData" id="pdfClick" target="_blank"><?php echo e(trans('content.ExportToPDF')); ?></a> |
                                <i class="fa fa-file-excel-o" aria-hidden="true"></i> <a href="javascript:void(0)" class="exportData" target="_blank" id="csvClick"><?php echo e(trans('content.ExportToCSV')); ?></a>
                            </div>
                            <div id="content_detail"></div>
                            <?php if(auth()->user()->role->slug != 'customer'): ?>
                                <hr>
                                <div class="form-group form-inline">
                                    <div class="col-md-1" style="padding: 0; margin: 0;">
                                        <img src="<?php echo e(asset('storage/users/default.png')); ?>" style="border-radius: 50%;width: 40px;">
                                    </div>
                                    <div class="col-md-11" style="padding: 0; margin: 0;">
                                        <input type="hidden" id="msgBoxId" value="36">
                                        <textarea class="form-control" id="messageBox" placeholder="<?php echo e(__('content.write_a_message')); ?>" dir="auto" style="height: 40px;width: 100%; cursor: pointer"></textarea>
                                        <button type="button" id="saveBtn" class="btn btn-success pull-right"><?php echo e(trans('content.save')); ?></button>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div id="content_body"></div>
                        </div>
                        <div class="modal-footer" style="">
                            <button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><?php echo e(trans('content.cancel')); ?></button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('javascript'); ?>
    <script type="text/javascript" src="<?php echo e(asset('typeahade/bootstrap-tokenfield.js')); ?>" charset="UTF-8"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        <?php if(session()->get('view_inquiries_view') != 1 || \auth()->user()->role->slug != 'customer'): ?>
        $(function() {

            $('.productLists').select2();
            $('.statusList').select2();

            var dataTable = $('#table').DataTable({
                processing: true,
                serverSide: true,
                searching: false,
                ajax: {
                    url: '<?php echo e(route('voyager.'.$dataType->slug.'.browse')); ?>',
                    type: 'POST',
                    data: function(data){
                        // Append to data
                        data.lang = '<?php echo e(\Illuminate\Support\Facades\Session::get('locale')); ?>',
                        data.product_id = $('#productLists').val();
                        data.user_id = $('#user_id').val();
                        data.request_id = $('#request_id').val();
                        data.status_of_request = $('#statusList').val();
                    },
                    
                    
                    
                    
                },
                "language": {
                    "sEmptyTable":     "<?php echo e(__('data-table.sEmptyTable')); ?>",
                    "sInfo":           "<?php echo e(__('data-table.sInfo')); ?>",
                    "sInfoEmpty":      "<?php echo e(__('data-table.sInfoEmpty')); ?>",
                    "sInfoFiltered":   "<?php echo e(__('data-table.sInfoFiltered')); ?>",
                    "sInfoPostFix":    "<?php echo e(__('data-table.sInfoPostFix')); ?>",
                    "sInfoThousands":  "<?php echo e(__('data-table.sInfoThousands')); ?>",
                    "sLengthMenu":     "<?php echo e(__('data-table.sLengthMenu')); ?>",
                    "sLoadingRecords": "<?php echo e(__('data-table.sLoadingRecords')); ?>",
                    "sProcessing":     "<?php echo e(__('data-table.sProcessing')); ?>",
                    "sSearch":         "<?php echo e(__('data-table.sSearch')); ?>",
                    "sZeroRecords":    "<?php echo e(__('data-table.sZeroRecords')); ?>",
                    "oPaginate": {
                        "sFirst":    "<?php echo e(__('data-table.sFirst')); ?>",
                        "sLast":     "<?php echo e(__('data-table.sLast')); ?>",
                        "sNext":     "<?php echo e(__('data-table.sNext')); ?>",
                        "sPrevious": "<?php echo e(__('data-table.sPrevious')); ?>"
                    },
                    "oAria": {
                        "sSortAscending":  "<?php echo e(__('data-table.sSortAscending')); ?>",
                        "sSortDescending": "<?php echo e(__('data-table.sSortDescending')); ?>"
                    }
                },
                <?php if(auth()->user()->role->slug != 'customer'): ?>
                "order": [[ 2, "desc" ]],
                <?php else: ?>
                "order": [[ 1, "desc" ]],
                <?php endif; ?>
                columns: [
                    {data: 'title', name: 'title'},
                    // {data: 'product_title', name: 'product_title'},
                    <?php if(auth()->user()->role->slug != 'customer'): ?>
                    // {data: 'user_name', name: 'user_name'},
                    {data: 'name', name: 'name'},
                    <?php endif; ?>
                    // {data: 'ticket_number', name: 'ticket_number'},
                    {data: 'ticket_number', name: 'request_id'},
//                    {data: 'language', name: 'language'},
                    {data: 'due', name: 'due', searchable:false},
                    {data: 'model_detail', name: 'model_detail', searchable:false},
                        <?php if(auth()->user()->role->slug != 'customer'): ?>
                    {data: 'action', name: 'action', searchable:false},
                        <?php else: ?>
                    {data: 'current_status', name: 'current_status', searchable:false}
                    <?php endif; ?>
                ]
            });

            $('body').on('keyup','#user_id, #request_id',function(e){
                dataTable.draw();
            });

            $('body').on('change','.productLists, .statusList',function(e){
                dataTable.draw();
            });
        });
        <?php endif; ?>
        $( document ).ready(function() {

            $('#tokenfield').tokenfield({
                autocomplete: {
                    source: ['red','blue','green','yellow','violet','brown','purple','black','white'],
                    delay: 100
                },
                showAutocompleteOnFocus: true
            });

            <?php if(session()->get('view_inquiries_view') == 1 ): ?>
            var kanban = new jKanban({
                element: "#myKanban",
                gutter: "10px",
                widthBoard: "450px",
                itemHandleOptions:{
                    enabled: false,
                },
                click: function(el) {
                    window.location.href = "<?php echo e(url('admin/service-inquiries/')); ?>/"+$(el).data('eid');
//                    if ($(el).parent().parent().data('id') != 'Draft') {
//                        $('#saveBtn').hide();
//                        toastr.clear();
//                        getDetails($(el).data('eid'));
//                        $('#detail_modal').modal().show();
//                        $msgId = $('#msgBoxId').val();
                        
                        
//                        $('.inviteUsers').tokenfield();
                        // console.log("Trigger on all items click!");
//                    }
                },
                dropEl: function(el, target, source, sibling){
                    status = target.parentElement.getAttribute('data-id');
                    <?php if(auth()->user()->role->slug == 'product-manager'): ?>
                    toastr.clear();
                    var returnStatus = 'test';
                    $.ajax({
                        url: "<?php echo e(route('voyager.'.$dataType->slug.'.ajaxStatusUpdate')); ?>",
                        type: 'POST',
                        async:false,
                        data: {
                            'data_id': $(el).data('eid'),
                            'lang': '<?php echo e(\Illuminate\Support\Facades\Session::get('locale')); ?>',
                            'status': status
                        },
                        success: function(data) {
                            if (data.status === true) {
                                toastr.success("Status change successfully");
                                returnStatus = true;
                                return;
                            } else {
                                toastr.warning(data.message);
                                returnStatus = false;
                                return;
                            }
                        },
                        error: function () {
                            toastr.success("Oops something went wrong!");
                            returnStatus = false;
                            return false;
                        }
                    });
                    <?php endif; ?>
                    return returnStatus;
                        //return asdasd;
                },
                boards: []
            });

            $.ajax({
                url: '<?php echo e(route('voyager.'.$dataType->slug.'.browse')); ?>',
                type: 'POST',
                data: {
                    'lang': '<?php echo e(\Illuminate\Support\Facades\Session::get('locale')); ?>',
                },
                success: function (data) {
                    kanban.addBoards(data.data)
                },
                error: function () {
                    console.log('request error');
                    toastr.success("Oops something went wrong!");
                }
            });
            <?php endif; ?>
            <?php if(auth()->user()->role->slug != 'customer'): ?>

            $('#messageBox').click(function(){
                $('#saveBtn').show();
            });

            $('#saveBtn').click(function(){
                var message = $('#messageBox').val();
                $.ajax({
                    url: "<?php echo e(route('voyager.'.$dataType->slug.'.ajaxMessageActivity')); ?>",
                    type: 'POST',
                    data: {
                        'message': message,
                        'data_id': $('#msgBoxId').val(),
                        'lang': '<?php echo e(\Illuminate\Support\Facades\Session::get('locale')); ?>',
                    },
                    success: function (data) {
                        toastr.clear();
                        if (data.status === true) {
                            $('#activityList').prepend(data.html);
                            $('#messageBox').val('');
                        } else {
                            console.log(data.message);
                            toastr.warning(data.message);
                        }
                    },
                    error: function () {
                        console.log('request error');
                        toastr.error("Oops something went wrong!");
                    }
                });
            });

            $('body').on('change','.checkPoint',function(){
                id = $(this).attr('data-id');
                if($(this).prop( "checked")){
                    checkPointStatus("<?php echo e(\App\Models\ServiceInquiryCheckPoint::TRUE); ?>",id,'<?php echo e(\Illuminate\Support\Facades\Session::get('locale')); ?>');
                    $(this).next().addClass('lineThrough')
                }else{
                    checkPointStatus("<?php echo e(\App\Models\ServiceInquiryCheckPoint::FALSE); ?>",id,'<?php echo e(\Illuminate\Support\Facades\Session::get('locale')); ?>');
                    $(this).next().removeClass('lineThrough')
                }
            });

            $('body').on('change','.checkPointComment',function(){
                comment = 'null';
                if($(this).val() !== ''){
                    comment = $(this).val();
                }
                $.ajax({
                    url: "<?php echo e(route('voyager.'.$dataType->slug.'.checkPointComment')); ?>",
                    type: 'POST',
                    data: {
                        'data_id': $(this).attr('data-id'),
                        'comment' : comment,
                        'lang': '<?php echo e(\Illuminate\Support\Facades\Session::get('locale')); ?>',
                    },
                    success: function (data) {
                        console.log(data);
                        toastr.clear();
                        if (data.status === true) {
                            $('#activityList').prepend(data.html);
                        } else {
                            console.log(data.message);
                        }
                    },
                    error: function () {
                        console.log('request error');
                        toastr.error("Oops something went wrong!");
                    }
                });
            });

            $('body').on('click','.inviteButton',function(){
                var emails = $('.inviteUsers').val();
                showLoading();
                $.ajax({
                    url: "<?php echo e(route('voyager.'.$dataType->slug.'.ajaxInviteUsers')); ?>",
                    async:false,
                    type: 'POST',
                    data: {
                        'emails': emails,
                        'data_id': $(this).attr('data-id'),
                        'lang': '<?php echo e(\Illuminate\Support\Facades\Session::get('locale')); ?>',
                    },
                    success: function (data) {
                        toastr.clear();
                        if (data.status === true) {
                            toastr.success(data.message);
                        } else {
                            toastr.warning(data.message);
                            console.log(data.message);
                        }
                    },
                    error: function () {
                        console.log('request error');
                        toastr.error("Oops something went wrong!");
                    }
                });
                $('.inviteUsers').tokenfield('destroy');
                $('.inviteUsers').val('');
                hideLoading();
            });
            <?php endif; ?>
            var select_previous = '';

            $('body').on('focus', '.changeStatus', function () {
                select_previous = $(this).val();
            });

            $('body').on('change', '.changeStatus', function(){
                var status = $(this).val();
                returnStatus = false;
                if(status != '' && status != null){
                    $.ajax({
                    url: "<?php echo e(route('voyager.'.$dataType->slug.'.ajaxStatusUpdate')); ?>",
                    type: 'POST',
                    async:false,
                    data: {
                        'data_id': $(this).data('id'),
                        'lang': '<?php echo e(\Illuminate\Support\Facades\Session::get('locale')); ?>',
                        'status': status
                    },
                    success: function(data) {
                        if (data.status === true) {
                            toastr.clear();
                            $('#toast-container').remove();
                            toastr.success("<?php echo e(__('toaster.status change successfully')); ?>");
                            returnStatus = true;
                            return;
                        } else {
                            toastr.clear();
                            $('#toast-container').remove();
                            toastr.warning(data.message);
                            returnStatus = false;
                            return;
                        }
                    },
                    error: function () {
                        toastr.clear();
                        $('#toast-container').remove();
                        toastr.success("<?php echo e(__('toaster.oops something went wrong')); ?>");
                        returnStatus = false;
                        return false;
                    }
                });
                }
                if(returnStatus == false){
                    $('#table').DataTable().draw();
                }
            });

            $('body').on('click', '.openClass', function(){
                $('#saveBtn').hide();
                toastr.clear();
                getDetails($(this).data('id'));
                $('#pdfClick').attr('href','<?php echo e(route('voyager.serviceInquiries.exportServicePdf')); ?>?type=pdf&id='+$(this).data('id'));
                $('#csvClick').attr('href','<?php echo e(route('voyager.serviceInquiries.exportServicePdf')); ?>?type=csv&id='+$(this).data('id'));
//                $('#detail_modal').modal().show();
            });


            function getDetails(data_id) {
                $.ajax({
                    url: "<?php echo e(route('voyager.'.$dataType->slug.'.ajaxDetailedRequest')); ?>",
                    type: 'POST',
                    async:false,
                    data: {
                        'data_id': data_id,
                        'lang': '<?php echo e(\Illuminate\Support\Facades\Session::get('locale')); ?>',
                    },
                    success: function (data) {
                        $('#k-m-title').html(data.productTitle);
                        $('#msgBoxId').val(data_id);
                        if (data.status === true) {
                            $('#content_detail').html(data.formHtml);
                            $('#content_body').html(data.activityHtml);
                        } else {
                            console.log(data.message);
                            toastr.warning(data.message);
                        }
                    },
                    error: function () {
                        console.log('request error');
                        toastr.error("Oops something went wrong!");
                    }
                });
            }

            function checkPointStatus(status, id, lang){
                $.ajax({
                    url: "<?php echo e(route('voyager.'.$dataType->slug.'.checkPointStatus')); ?>",
                    type: 'POST',
                    data: {
                        'data_id': id,
                        'status': status,
                        'lang': lang,
                    },
                    success: function (data) {
                        console.log(data);
                        toastr.clear();
                        if (data.status === true) {
                            $('#activityList').prepend(data.html);
                        } else {
                            console.log(data.message);
                        }
                    },
                    error: function () {
                        console.log('request error');
                        toastr.error("Oops something went wrong!");
                    }
                });
            }
        });

        $(document).on('click','#applyNow',function(){
            var form_fields = JSON.stringify(formRender.userData);
            $('#formFields').val(form_fields);
            var status = "<?php echo e(\App\Models\ServiceInquiries::APPLICATION_RECEIVED); ?>";
            $('#status_of_request').val(status);
            if ($('#apply').valid()) {
                $('form').submit();
            }
            else {
                return false;
            }
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('voyager::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/baiterek_web/resources/views/vendor/voyager/service-inquiries/browse.blade.php ENDPATH**/ ?>