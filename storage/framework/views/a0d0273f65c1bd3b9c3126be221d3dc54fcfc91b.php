<?php $__env->startSection('page_title', __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular); ?>

<?php $__env->startSection('css'); ?>
    <?php app()->setLocale(\Session::get('locale')) ?>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <style>
        .permissions-table {
            width: 75% !important;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_header'); ?>
    <h1 class="page-title">
        <i class="<?php echo e($dataType->icon); ?>"></i>
        <?php echo e(__('portal.roles.roles_'.(isset($dataTypeContent->id) ? 'edit' : 'add'))); ?>


    </h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="page-content container-fluid">
        <?php echo $__env->make('voyager::alerts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form class="form-edit-add" id="frmRole" role="form"
                          action="<?php if(isset($dataTypeContent->id)): ?><?php echo e(route('voyager.'.$dataType->slug.'.update', $dataTypeContent->id)); ?><?php else: ?><?php echo e(route('voyager.'.$dataType->slug.'.store')); ?><?php endif; ?>"
                          method="POST" enctype="multipart/form-data">

                        <!-- PUT Method if we are editing -->
                        <?php if(isset($dataTypeContent->id)): ?>
                            <?php echo e(method_field("PUT")); ?>

                        <?php endif; ?>

                        <!-- CSRF TOKEN -->
                        <?php echo e(csrf_field()); ?>


                        <div class="panel-body">

                            <?php if(count($errors) > 0): ?>
                                <div class="alert alert-danger">
                                    <ul>
                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li><?php echo e($error); ?></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>

                            <?php $__currentLoopData = $dataType->addRows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="form-group">
                                    <label for="name"><?php echo e(__('portal.roles.'.$row->display_name)); ?></label>

                                    <?php echo Voyager::formField($row, $dataType, $dataTypeContent); ?>


                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            <div class="form-group clearfix">
                                <label for="permission"><?php echo e(__('portal.roles.permissions')); ?></label><br>
                                <a id="selectAll" class="permission-select-all radio-link active" style="cursor: pointer"><?php echo e(__('portal.roles.add.select_all')); ?></a>
                                <a id="deselectAll" class="permission-deselect-all radio-link" style="cursor: pointer"><?php echo e(__('portal.roles.add.deselect_all')); ?></a>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover dataTable no-footer permissions-table">
                                    <thead>
                                        <tr role="row">
                                            <th class="border-right"><?php echo e(__('portal.roles.add.section')); ?></th>
                                            <th class="border-right"><?php echo e(__('portal.roles.add.page')); ?></th>
                                            <th class="text-center"><?php echo e(__('portal.roles.add.browse')); ?></th>
                                            <th class="text-center"><?php echo e(__('portal.roles.add.read')); ?></th>
                                            <th class="text-center"><?php echo e(__('portal.roles.add.add')); ?></th>
                                            <th class="text-center"><?php echo e(__('portal.roles.add.edit')); ?></th>
                                            <th class="text-center"><?php echo e(__('portal.roles.add.delete')); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $role_permissions = (isset($dataTypeContent)) ? $dataTypeContent->permissions->pluck('key')->toArray() : []; ?>
                                    <?php $__currentLoopData = $permissionTree; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr class="parent">
                                            <td>
                                                <?php if(count($item['children']) > 0): ?>
                                                    <a href="#" class="parent-control" data-id="<?php echo e($item['id']); ?>"><i class="fas fa-minus-circle"></i></a>
                                                <?php endif; ?>
                                                <?php echo e($item['title']); ?>

                                            </td>
                                            <td></td>
                                            <?php $__empty_1 = true; $__currentLoopData = $item['permission_keys']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                                <td class="text-center">
                                                    <div class="custom-checkbox">
                                                        <input type="checkbox" data-parent="<?php echo e($item['id']); ?>" id="permission-<?php echo e($item['id']); ?>" name="permissions[]"
                                                               <?php echo e((isset($rolePermissions) && in_array($key, $rolePermissions)) ? 'checked="checked" ' : ''); ?>

                                                               class="the-permission" value="<?php echo e($key); ?>">
                                                        <label></label>
                                                    </div>
                                                </td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            <?php endif; ?>
                                        </tr>
                                        <?php $__empty_1 = true; $__currentLoopData = $item['children']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $children): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                            <tr class="children children-<?php echo e($item['id']); ?>">
                                                <td></td>
                                                <td><?php echo e($children['title']); ?></td>
                                                <?php $__empty_2 = true; $__currentLoopData = $children['permission_keys']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $childKey): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_2 = false; ?>
                                                    <td class="text-center">
                                                        <div class="custom-checkbox">
                                                            <input type="checkbox" data-parent="<?php echo e($children['id']); ?>" id="permission-<?php echo e($children['id']); ?>" name="permissions[]"
                                                                   <?php echo e((isset($rolePermissions) && in_array($childKey, $rolePermissions)) ? 'checked="checked" ' : ''); ?>

                                                                   class="the-permission" value="<?php echo e($childKey); ?>">
                                                            <label></label>
                                                        </div>
                                                    </td>
                                                    <?php if(count($children['permission_keys']) == 1): ?>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_2): ?>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                <?php endif; ?>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>

                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- panel-body -->
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary submit save"><?php echo e(__('portal.roles.submit')); ?></button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="<?php echo e(route('voyager.upload')); ?>" target="form_target" method="post"
                          enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <?php echo e(csrf_field()); ?>

                        <input name="image" id="upload_file" type="file"
                               onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="<?php echo e($dataType->slug); ?>">
                    </form>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script>
        $('document').ready(function () {

            // $('#frmRole').validate({
            //     rules: {
            //         name:'required',
            //         display_name:'required'
            //     }
            // });

            // $('.toggleswitch').bootstrapToggle();


            $('.permission-select-all').on('click', function(){
                $('.permissions-table').find("input[type='checkbox']").prop('checked', true);
            });

            $('.permission-deselect-all').on('click', function(){
                $('.permissions-table').find("input[type='checkbox']").prop('checked', false);
                return false;
            });

            // $('.custom-checkbox').on('click', function (e) {
            //     e.preventDefault();
            //     if($(this).find('input[type="checkbox"]').is(":checked")){
            //         $(this).find('input[type="checkbox"]').prop('checked', false);
            //     } else {
            //         $(this).find('input[type="checkbox"]').prop('checked', true);
            //     }
            // });

            // $('.submit').unbind('click').click(function() {
            //     var isvalid = $("#frmRole").valid();
            //     if (isvalid) {
            //         $('#frmRole')[0].submit();
            //     }
            //     else {
            //         return false;
            //     }
            // });

        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('voyager::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/baiterek_web/resources/views/vendor/voyager/roles/edit-add.blade.php ENDPATH**/ ?>