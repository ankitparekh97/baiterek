<?php if(!empty(\App\Helper\Helper::getBannerCategoryBySlug('home-banner'))): ?>
<?php  $homeBanner =\App\Helper\Helper::getBannerCategoryBySlug('home-banner'); ?>
<div class="owl-carousel owl-theme homebanner">
    <?php if(count(\App\Helper\Helper::getBannersImages('home-banner'))>0): ?>
        <?php $__currentLoopData = \App\Helper\Helper::getBannersImages('home-banner'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $homebanners): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="item banner_item">
        <div class="slider-content" style="width:<?php echo e($homeBanner->width); ?>px;height:<?php echo e($homeBanner->height); ?>px;background-image: url(<?php echo e(asset('storage/'. $homebanners->image)); ?>);">
                <div class="content">
                    <h3 style="color:<?php echo e($homebanners->title_color); ?>"><?php echo e($homebanners->title); ?></h3>
                    <h1><?php echo $homebanners->primary_text; ?> </h1>
                    <p><?php echo $homebanners->secondary_text; ?></p>
                    <a href="<?php echo e($homebanners->url); ?>" target="_blank" class="btn" style="color:<?php echo e($homebanners->url_text_color); ?>; border: 2px solid <?php echo e($homebanners->url_color); ?>;"><?php if($homebanners->url_text != ''): ?> <?php echo e($homebanners->url_text); ?> <?php else: ?> <?php echo app('translator')->get('auth.giveItTry'); ?> <?php endif; ?></a>
                </div>
            </div>
        </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
</div>

<script type="application/javascript">
// home banner
$(function () {
 $('.homebanner').owlCarousel({
     loop:true,
     margin:0,
     nav:true,
     items: 1,
     responsive:{
         0:{
             items:1
         },
         600:{
             items:1
         },
         1000:{
             items:1
         }
     }
 })
})

</script>
<?php endif; ?>
<?php /**PATH /var/www/html/baiterek_web/resources/views/frontend/layouts/homebanner.blade.php ENDPATH**/ ?>