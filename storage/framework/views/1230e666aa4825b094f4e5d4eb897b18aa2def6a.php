<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <meta name="_url" content="<?php echo e(URL('')); ?>">
    <link rel="icon" href="<?php echo e(asset('images/Frame.png')); ?>" type="image/png" >

    
    <link rel="stylesheet" href="<?php echo e(asset('css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/owl.carousel.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/owl.theme.default.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/toastr.min.css')); ?>">

    <script src="<?php echo e(asset('js/jquery-3.3.1.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/jquery-ui.min.js')); ?>"></script>

    <link rel="stylesheet" href="<?php echo e(asset('fonts/icomoon/style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/custom.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/trix.css')); ?>">

    <style>
        #toast-container {
            position: fixed;
            z-index: 999999;
            pointer-events: none;
            left:50% !important;
            transform: translate(-50%, 0px);
        }
        .toast-top-right {
            right: auto !important;
        }
    </style>
    <?php echo $__env->yieldContent('css'); ?>

    <title><?php echo e(__('content.title_statement')); ?></title>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>

<body>
<header>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="/">
                <?php $site_logo_img = Voyager::setting('site.logo', ''); ?>
                <?php if($site_logo_img == ''): ?>
                    <img src="<?php echo e(asset('images/logo.svg')); ?>" alt="<?php echo e(Voyager::setting("site.title")); ?>" title="<?php echo e(__('content.title_statement')); ?>" >
                <?php else: ?>
                    <img src="<?php echo e(Voyager::image($site_logo_img)); ?>" alt="<?php echo e(Voyager::setting("site.title")); ?>" title="<?php echo e(__('content.title_statement')); ?>" >
                <?php endif; ?>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

             <?php echo $__env->make('frontend.layouts.menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
             <?php echo $__env->make('frontend.layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        </nav>
    </div>
</header>
<div id="loader" style="display: none;">
    <img src="<?php echo e(asset('images/loader.gif')); ?>" alt="Loader">
</div>
<?php if(url('/')==\Illuminate\Support\Facades\Request::url()): ?>
    <main class="">
        <section class="hero_banner">
            <?php echo $__env->make('frontend.layouts.homebanner', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </section>

        <section class="our_services">
            <div class="container-fluid">
                <div class="row">
                    <?php echo $__env->make('frontend.layouts.categories', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
            </div>
        </section>

        <section class="latest_news">
            <div class="container-fluid">
                <div class="row">
                    <?php echo $__env->make('frontend.layouts.news', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
            </div>
        </section>

        <section class="business_div">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo $__env->make('frontend.layouts.homepagemiddleblock', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="fact_cards">
            <div class="container-fluid">
                <div class="row">
                     <?php echo $__env->make('frontend.layouts.graphimages', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
            </div>
        </section>

        <section class="logos_display">
            <div class="container-fluid">
                <div class="row">
                    <?php echo $__env->make('frontend.layouts.companylogos', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
            </div>
        </section>
    </main>
<?php else: ?>
    <main class="main-body">
         <?php echo $__env->yieldContent('content'); ?>
    </main>
<?php endif; ?>

<footer>
    <script src="<?php echo e(asset('js/popper.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/owl.carousel.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/moment.js')); ?>"></script>
    <script src="<?php echo e(asset('js/jquery.validate.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/toastr.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/trix.js')); ?>"></script>
    <?php echo $__env->make('frontend.layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</footer>

<script defer src="<?php echo e(asset('js/frontend.js')); ?>"></script>

<script type="application/javascript">
    var baseUrl = $('meta[name="_url"]').attr('content');
    var assetBaseUrl = "<?php echo e(asset('')); ?>";
    var disabledBtn = "<?php echo app('translator')->get('fields.Coming soon'); ?>";

    <?php if(session('success')): ?>
        toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-center",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    toastr.success("<?php echo e(\Illuminate\Support\Facades\Session::get('success')); ?>");
    <?php endif; ?>

            <?php if(session('error')): ?>
        toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-center",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    toastr.error("<?php echo e(\Illuminate\Support\Facades\Session::get('error')); ?>");
    <?php endif; ?>
</script>
</body>
</html>

<?php /**PATH /var/www/html/baiterek_web/resources/views/frontend/master.blade.php ENDPATH**/ ?>