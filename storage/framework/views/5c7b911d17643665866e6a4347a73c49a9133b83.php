<div class="container">
    <div class="row">
        <div class="col-md-12">
            <ul class="footer-links">
                <li class="footer-links-li">
                    <?php if(!empty(\App\Helper\Helper::getFooterWidget('digital-baiterek'))): ?>
                        <p><?php echo \App\Helper\Helper::getFooterWidget('digital-baiterek')->content; ?></p>
                    <?php endif; ?>
                </li>
                <li class="footer-links-li">
                    
                    <?php echo e(menu('Footer menu Block 1','bootstrap')); ?>

                </li>
                <li class="footer-links-li">
                    <?php if(!empty(\App\Helper\Helper::getFooterWidget('visit'))): ?>
                        
                        <p><?php echo \App\Helper\Helper::getFooterWidget('visit')->content; ?></p>
                    <?php endif; ?>
                </li>
                
                <li class="footer-links-li">
                    
                    <?php echo e(menu('Footer menu Block 2','bootstrap')); ?>

                </li>
                <li class="footer-links-li">
                    <?php if(!empty(\App\Helper\Helper::getFooterWidget('contact'))): ?>
                        
                        <p><?php echo \App\Helper\Helper::getFooterWidget('contact')->content; ?></p>
                    <?php endif; ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="footer-copyright">
                <p>© <?php echo app('translator')->get('content.copy_rights'); ?> <?php echo e(Carbon\Carbon::createFromFormat('Y-m-d H:i:s', \Carbon\Carbon::now())->year); ?> <?php echo app('translator')->get('content.all_rights'); ?></p>
                
            </div>
        </div>
    </div>
</div>
<?php /**PATH /var/www/html/baiterek_web/resources/views/frontend/layouts/footer.blade.php ENDPATH**/ ?>