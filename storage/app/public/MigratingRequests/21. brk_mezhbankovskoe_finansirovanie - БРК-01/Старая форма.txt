{
    "uuid": "163364",
    "oldUuid": "d252cf7e-8bed-463d-a532-b129c75d5b17",
    "version": 2,
    "form": "ba4adee7-60f9-4214-bfa3-29b56ccd8baf",
    "formVersion": 1,
    "modified": "2019-01-11 16:17:41.0",
    "nodeUUID": "65f5f9c0-158a-11e9-bc7b-00505602002b",
    "data": [
        {
            "id": "e_z",
            "type": "label",
            "label": "e_z"
        },
        {
            "id": "app",
            "type": "textbox"
        },
        {
            "id": "label-dulhgd",
            "type": "label",
            "label": "Номер заявки: "
        },
        {
            "id": "app_num",
            "type": "counter",
            "value": "18538-11-01-19"
        },
        {
            "id": "label-qx7pdb",
            "type": "label",
            "label": "Номер подачи:"
        },
        {
            "id": "regnumber",
            "type": "textbox"
        },
        {
            "id": "label-qx7pdb_copy3",
            "type": "label",
            "label": "Дата подачи заявки:"
        },
        {
            "id": "request_date_start",
            "type": "date",
            "value": "2019-01-11 16:19",
            "key": "2019-01-11 16:19:37"
        },
        {
            "id": "label-qx7pdb_copy2",
            "type": "label",
            "label": "Дата отправки заявки:"
        },
        {
            "id": "send_date",
            "type": "date"
        },
        {
            "id": "label-qx7pdb_copy1",
            "type": "label",
            "label": "Автор заявки:"
        },
        {
            "id": "request_author",
            "type": "entity",
            "value": "Егеубаева Жанерке ",
            "key": "eaccab55-c063-4596-baa1-5f5ea67319a9"
        },
        {
            "id": "label-k3nlcq",
            "type": "label",
            "label": " "
        },
        {
            "id": "label-ewboyw",
            "type": "label",
            "label": "Межбанковское финансирование "
        },
        {
            "id": "label-h3muy4",
            "type": "label",
            "label": " "
        },
        {
            "id": "label_stats",
            "type": "label",
            "label": "Статус заявки:"
        },
        {
            "id": "refuse_desicion_status",
            "type": "listbox",
            "value": "Черновик",
            "key": "0"
        },
        {
            "id": "label-pfusif",
            "type": "label",
            "label": " "
        },
        {
            "id": "h1",
            "type": "label",
            "label": "1. Общая информация о заявителе"
        },
        {
            "id": "label-e2558e",
            "type": "label",
            "label": "Наименование организации заявителя:"
        },
        {
            "id": "company_name",
            "type": "textbox"
        },
        {
            "id": "label-0ftouc",
            "type": "label",
            "label": "ФИО первого руководителя и (или) иных работников:"
        },
        {
            "id": "FIO",
            "type": "textbox"
        },
        {
            "id": "label-13xbva",
            "type": "label",
            "label": "Контакты:"
        },
        {
            "id": "contacts",
            "type": "textarea",
            "value": "\n"
        },
        {
            "id": "label-13xbva_copy1",
            "type": "label",
            "label": "e-mail:"
        },
        {
            "id": "email",
            "type": "textbox"
        },
        {
            "id": "h2",
            "type": "label",
            "label": "2. Информация о запрашиваемом продукте "
        },
        {
            "id": "label-ab3nlg",
            "type": "label",
            "label": "Запрашиваемая сумма финансирования:"
        },
        {
            "id": "sum",
            "type": "numericinput"
        },
        {
            "id": "label-ab3nlg_copy1",
            "type": "label",
            "label": "Валюта финансирования:"
        },
        {
            "id": "listbox-78p9oa",
            "type": "listbox",
            "value": "тенге",
            "key": "1"
        },
        {
            "id": "label-ab3nlg_copy3",
            "type": "label",
            "label": "Запрашиваемый общий срок финансирования:"
        },
        {
            "id": "numericinput-vgfh6y",
            "type": "numericinput"
        },
        {
            "id": "label-zsbhfk",
            "type": "label",
            "label": "лет"
        },
        {
            "id": "label-ab3nlg_copy2",
            "type": "label",
            "label": "Запрашиваемая ставка финансирования:"
        },
        {
            "id": "numericinput-xzziy1",
            "type": "numericinput"
        },
        {
            "id": "label-kt8u0i",
            "type": "label",
            "label": "%"
        },
        {
            "id": "grouped-panel",
            "type": "custom"
        }
    ]
}