<?php

return [
    'pdf' => 'pdf',
    'doc' => 'word',
    'docx' => 'word',
    'csv' => 'excel',
    'xls' => 'excel',
    'xlsx' => 'excel',
    'ppt' => 'powerpoint',
    'pptx' => 'powerpoint',
    'txt' => 'text',
    'png' => 'image',
    'jpg' => 'image',
    'jpeg' => 'image',
];
