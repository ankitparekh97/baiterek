<?php

return [
    'serviceInquiries' => [
        0 => 'Draft',
        1 => 'Application received',
        2 => 'Control',
        3 => 'Data/Documents are missing',
        4 => 'In process',
        5 => 'Rejected',
        6 => 'Accepted',
    ],
    'serviceInquiriesCustomer' => [
        0 => 'Draft',
        1 => 'Applied',
        2 => 'On control',
        3 => 'Data/Documents are missing',
        4 => 'In process',
        5 => 'Rejected',
        6 => 'Accepted',
    ],
];
