<?php
/**
 * Created by PhpStorm.
 * User: shruti
 * Date: 23/12/19
 * Time: 11:04 AM
 */

return [
    'urlSegments' => [
        'services' => 'services',
        'categories' => 'categories',
        'news' => 'news',
        'contact' => 'contact',
        'login' => 'login',
        'logout' => 'logout',
        'register' => 'register',
        'authCheck' => 'authCheck',
        'checkEmail' => 'checkEmail',
        'checkVAT_no' => 'checkVAT_no',
        'save' => 'save',
        'submitRequest' => 'submitRequest',
        'applyNotification' => 'applyNotification',
        'resetpassword' => 'resetpassword',
        'updatepassword' => 'updatepassword',
        'forgotPassword' => 'forgotPassword',
        'emailResetPasswordLink' => 'emailResetPasswordLink',
        'resetPasswordForm' => 'resetPasswordForm',
        'savePassword' => 'savePassword',
    ],
    'roles' => [
        'admin' => 'admin',
        'supervisor' => 'supervisor',
        'product-manager' => 'product-manager',
        'content-manager' => 'content-manager',
        'customer' => 'customer',
    ],
    'locales' => [
        'EN' => 'en',
        'RU' => 'ru',
        'KZ' => 'kz',
    ],
];
