<?php
/**
 * Created by PhpStorm.
 * User: shruti
 * Date: 11/12/19
 * Time: 3:15 PM
 */

use Illuminate\Support\Facades\App;

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function (){
    Route::get('/', 'HomeController@index')->name('home.index');

    Route::get('news/{id}', function ($id) {
        return view('frontend.news.newsDetail', ['id'=> $id]);
    });

    Route::get('category/{id}', function ($id) {
        return App::call('App\Http\Controllers\Frontend\ProductController@getCategory', ['id' => $id]);
    });

    Route::get('services/{id}', function ($id) {
        return App::call('App\Http\Controllers\Frontend\ProductController@getProduct', ['id' => $id]);
    });

    Route::get('projects/{id}', function ($id) {
        return App::call('App\Http\Controllers\Frontend\ProjectsController@getProjects', ['id' => $id]);
    });

    Route::get('projectDetail/{id}', function ($id) {
        return App::call('App\Http\Controllers\Frontend\ProjectsController@projectDetail', ['id' => $id]);
    });

    Route::get('apply/{id}', function ($id) {
        return App::call('App\Http\Controllers\Frontend\ServiceRequestController@apply', ['id' => $id]);
    });

    Route::any('categories', 'ProductController@getAllCategories')->name('categories');
    Route::any('services', 'ProductController@index')->name('products');
    Route::any('projects', 'ProjectsController@index')->name('projects');
    Route::get('news', 'NewsController@index')->name('news');

    Route::get('login', 'CustomerController@index')->name('login');
    Route::post('authCheck', 'CustomerController@login')->name('authCheck');

    Route::get('register', 'CustomerController@register')->name('register');
    Route::get('checkEmail', 'CustomerController@checkEmail')->name('checkEmail');
    Route::get('checkVAT_no', 'CustomerController@checkVAT_no')->name('checkVAT_no');
    Route::post('save', 'CustomerController@save')->name('save');

    Route::get('forgot-password', 'ForgotPasswordController@forgotPassword')->name('forgotPassword');
    Route::post('email-reset-password-link', 'ForgotPasswordController@emailResetPasswordLink')->name('emailResetPasswordLink');
    Route::get('reset-password-form/{token}', 'ForgotPasswordController@resetPasswordForm')->name('resetPasswordForm');
    Route::post('save-password', 'ForgotPasswordController@savePassword')->name('savePassword');

    // request access
    Route::group(['middleware' => ['FrontendAuth','Security']], function(){

        Route::get('resetpassword', 'CustomerController@resetpassword')->name('resetpassword');
        Route::post('updatepassword', 'CustomerController@updatepassword')->name('updatepassword');

        Route::post('submitRequest', 'ServiceRequestController@submitRequest')->name('submitRequest');
        Route::get('applyNotification', 'ServiceRequestController@applyNotification')->name('applyNotification');

        Route::post('getDraftData', 'ServiceRequestController@getDraftData')->name('getDraftData');
        Route::post('applyService', 'ServiceRequestController@applyService')->name('applyService');
        Route::post('deleteDocument','ServiceRequestController@deleteDocument')->name('deleteDocument');

        Route::get('home-logout', 'CustomerController@logout')->name('logout');
    });

    Route::get('/{slug}', 'SegmentController@index')->name('cms.index');
});