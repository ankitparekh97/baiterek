<?php

use App\Helper\Helper;
use App\Models\ServiceInquiries;
use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('clear:all', function () {
    Artisan::call('config:clear');
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    Artisan::call('config:cache');
    $this->comment('Clearing Cache,View,Route,Config');
})->describe('Clearing Cache,View,Route,Config');

Artisan::command('setData', function () {
    $serviceInquiries = \App\Models\ServiceInquiries::where('is_migrated', 1)->where('product_id', 88)->get();
    foreach ($serviceInquiries as $serviceInquiry) {
        $jsonData = collect(json_decode($serviceInquiry->old_json_data));
        $requestAuthor = $jsonData->where('id', 'request_author')->first()->value ?? null;
        $serviceInq = ServiceInquiries::where('id', $serviceInquiry->id)->first();
        // textarea-1584877102190
        $data = json_decode($serviceInq->fields);
        $data = collect($data)->map(function ($item) use ($requestAuthor) {
            if (isset($item->name)) {
                if ($item->name == 'textarea-1584877102190') {
                    $item->userData = [$requestAuthor];
                }
            }
            return $item;
        });
        $serviceInq->fields = json_encode($data);
        $serviceInq->save();
        $this->comment($requestAuthor . ' ====== ' . $serviceInq->id);
    }
    dd('done');
});

Artisan::command('setData2', function () {
    $serviceInquiries = \App\Models\ServiceInquiries::where('is_migrated', 1)->where('product_id', 86)->get();
    foreach ($serviceInquiries as $serviceInquiry) {
        $jsonData = collect(json_decode($serviceInquiry->old_json_data));
        $requestAuthor = $jsonData->where('id', 'request_author')->first()->value ?? null;
        $serviceInq = ServiceInquiries::where('id', $serviceInquiry->id)->first();
        // textarea-1584877102190
        $data = json_decode($serviceInq->fields);
        $data = collect($data)->map(function ($item) use ($requestAuthor) {
            if (isset($item->name)) {
                if ($item->name == 'textarea-1584387613847') {
                    if (array_first($item->userData) == null) {
                        $item->userData = [$requestAuthor];
                    }
                }
            }
            return $item;
        });
        $serviceInq->fields = json_encode($data);
        $serviceInq->save();
        $this->comment($requestAuthor . ' ====== ' . $serviceInquiry->id);
    }
    dd('done');
});

Artisan::command('service', function () {
    $registryCode = 'bd_zayavlenie_po_expo';
    $serviceInquiries = \App\Models\ServiceInquiries::where('is_migrated', 0)->get();
    $error = [];
    foreach ($serviceInquiries as $k => $serviceInquiry) {
        try {
            if ($serviceInquiry->request_id == 'undefined') {
                $this->comment('SKIP');
                continue;
            }
            $client = new \GuzzleHttp\Client();
            $registryResponse = $client->request('GET', 'https://digital.baiterek.gov.kz/Synergy/rest/api/registry/data_ext', [
                'timeout' => 300,
                'auth' => ['administrator', 'Heinkb1737!'],
                'query' => [
                    'field' => 'app_num',
                    'value' => $serviceInquiry->request_id,
                    'registryCode' => $registryCode,
                    'condition' => 'TEXT_EQUALS'
                ]
            ]);
            $registryResponse = $registryResponse->getBody()->getContents();
            $dataUUID = json_decode($registryResponse)->result[0]->dataUUID;


            $client = new \GuzzleHttp\Client();
            $formResponse = $client->request('GET', 'https://digital.baiterek.gov.kz/Synergy/rest/api/asforms/data/' . $dataUUID, [
                'auth' => ['administrator', 'Heinkb1737!'],
            ]);
            $formResponse = $formResponse->getBody()->getContents();
            $data = collect(json_decode($formResponse)->data);
            $data2 = collect(json_decode($formResponse)->data)->where('type', 'appendable_table');
            $combined = [];
            foreach ($data2 as $item) {
                if (isset($item->data)) {
                    $combined[] = $item->data;
                } else {
                    continue;
                }
            }
            $combined = Helper::mergeArrayofArrays($combined);
            $combined = $data->merge($combined);
            if ($combined->count()) {
                $serviceInquiry->old_json_data = json_encode($combined);
                $serviceInquiry->dataUUID = $dataUUID;
                $serviceInquiry->is_migrated = 1;
                $serviceInquiry->save();
            }
            $this->comment('KEY => ' . $k . ' || ' . $serviceInquiry->id);
        } catch (\Exception $e) {
            $this->comment($serviceInquiry->request_id . ' <=> ' . $e->getMessage());
            $error[] = $serviceInquiry->id;
            continue;
        }
    }
    dump('ERRORS', $error);
    dd('done');
});