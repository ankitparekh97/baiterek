<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\ServiceInquiries;
use App\Models\ServiceInquiriesDocuments;
use App\Models\ServiceInquiryActivity;
use App\Models\ServiceInquiryVerification;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Filesystem;
use League\Flysystem\Sftp\SftpAdapter;

Route::get('/', function () {
    return view('welcome');
});

Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});

Route::get('view_inquiries/{view}', function ($view){
    Session::put('view_inquiries_view', $view);
    return redirect()->back();
})->name('view_inquiries');

Route::group(['prefix' => 'admin'], function () {

    Route::get('forgotpassword','Voyager\\ForgotPasswordController@forgotPassword')->name('forgotpassword');
    Route::post('email-reset-password-link', 'Voyager\\ForgotPasswordController@emailResetPasswordLink')->name('emailResetPasswordLink');
    Route::get('reset-password-form/{token}', 'Voyager\\ForgotPasswordController@resetPasswordForm')->name('resetPasswordForm');
    Route::post('save-password', 'Voyager\\ForgotPasswordController@savePassword')->name('savePassword');

    //    Route::group(['middleware' => 'CheckLogin'], function(){
//        Route::get('change-pass', 'Voyager\\UserController@changePassword')->name('voyager.change-pass');
//    });
//
//    Route::group(['middleware' => 'FirstLogin'], function(){
//
//        Route::post('update-pass', 'Voyager\\UserController@updatePassword')->name('voyager.update-pass');
//    });

    Route::any('service-inquiries/export-service-pdf',['uses' => 'Voyager\\ServiceInquiryController@exportToPdf','as' => 'voyager.serviceInquiries.exportServicePdf']);

    // request access
    Route::group(['middleware' => ['AdminAuth','Security']], function(){
        Voyager::routes();


        // Profile
        Route::get('profiles','Voyager\\ProfileController@index')->name('voyager.profiles');
        Route::get('edit-my-profile/{id?}','Voyager\\ProfileController@editProfile')->name('voyager.edit-my-profile');
        Route::post('imageUpload','Voyager\\ProfileController@imageUpload')->name('voyager.image-upload');
        Route::post('update','Voyager\\ProfileController@update')->name('voyager.update');
        Route::get('my-documents','Voyager\\ProfileController@migratedDocuments')->name('voyager.migratedDocuments');
        Route::post('my-documents','Voyager\\ProfileController@migratedDocumentsbrowse')->name('voyager.my-documents.browse');

        // Banner Category
        Route::post('banner-masters','Voyager\\BannerController@index')->name('voyager.banner-masters');
        Route::post('banner-masters/browse','Voyager\\BannerController@browse')->name('voyager.banner-masters.browse');
        Route::get('banner-masters/create','Voyager\\BannerController@create')->name('voyager.banner-masters.create');
        Route::post('banner-masters/store','Voyager\\BannerController@store')->name('voyager.banner-masters.store');
        Route::get('banner-masters/edit/{id}','Voyager\\BannerController@edit')->name('voyager.banner-masters.edit');
        Route::put('banner-masters/update/{id}','Voyager\\BannerController@update')->name('voyager.banner-masters.update');
        Route::delete('banner-masters/destroy/{id}','Voyager\\BannerController@destroy')->name('voyager.banner-masters.destroy');

        // Users
        Route::post('users','Voyager\\UserController@index')->name('voyager.users');
        Route::any('userStatus','Voyager\\UserController@userStatus')->name('voyager.userStatus');
        Route::post('users/browse','Voyager\\UserController@browse')->name('voyager.users.browse');
        Route::get('users/create','Voyager\\UserController@create')->name('voyager.users.create');
        Route::post('users/store','Voyager\\UserController@store')->name('voyager.users.store');
        Route::get('users/edit/{id}','Voyager\\UserController@edit')->name('voyager.users.edit');
        Route::put('users/update/{id}','Voyager\\UserController@update')->name('voyager.users.update');
        Route::delete('users/destroy/{id}','Voyager\\UserController@destroy')->name('voyager.users.destroy');

        Route::get('product-managers','Voyager\\ProductManagerController@index')->name('product-managers');
        Route::post('product-managers/browse','Voyager\\ProductManagerController@browse')->name('voyager.product-managers.browse');
        Route::get('product-managers/create','Voyager\\ProductManagerController@create')->name('voyager.product-managers.create');
        Route::post('product-managers/store','Voyager\\ProductManagerController@store')->name('voyager.product-managers.store');
        Route::get('product-managers/edit/{id}','Voyager\\ProductManagerController@edit')->name('voyager.product-managers.edit');
        Route::put('product-managers/update/{id}','Voyager\\ProductManagerController@update')->name('voyager.product-managers.update');
        Route::get('product-managers/read/{id}','Voyager\\ProductManagerController@show')->name('voyager.product-managers.read');
        Route::delete('product-managers/destroy/{id}','Voyager\\ProductManagerController@destroy')->name('voyager.product-managers.destroy');
        Route::delete('product-managers/{id}','Voyager\\ProductManagerController@destroy')->name('voyager.product-managers.destroy');


        // Banners
        Route::post('banner-images/browse','Voyager\\BannerImagesController@browse')->name('voyager.banner-images.browse');



        Route::get('roles','Voyager\\VoyagerRoleController@index')->name('voyager.roles.index');
        Route::post('roles/browse','Voyager\\VoyagerRoleController@browse')->name('voyager.roles.browse');
        Route::get('roles/create','Voyager\\VoyagerRoleController@create')->name('voyager.roles.create');
        Route::post('roles/store','Voyager\\VoyagerRoleController@store')->name('voyager.roles.store');
        Route::get('roles/edit/{id}','Voyager\\VoyagerRoleController@edit')->name('voyager.roles.edit');
        Route::put('roles/update/{id}','Voyager\\VoyagerRoleController@update')->name('voyager.roles.update');
        Route::delete('roles/destroy/{id}','Voyager\\VoyagerRoleController@destroy')->name('voyager.roles.destroy');
        Route::post('roles/builder','Voyager\\VoyagerRoleController@builder')->name('voyager.roles.builder');


        //Roles routes
        // Route::resource('roles', 'Voyager\\VoyagerRoleController');
        // Route::get('roles/',['uses' => 'Voyager\\VoyagerRoleController@index',  'as' => 'voyager.roles.index']);
        Route::get('roles/create',['uses' => 'Voyager\\VoyagerRoleController@create',  'as' => 'voyager.roles.create']);
        Route::get('roles/{id}/edit',['uses' => 'Voyager\\VoyagerRoleController@edit',  'as' => 'voyager.roles.edit']);
        Route::get('roles/{id}',['uses' => 'Voyager\\VoyagerRoleController@show',  'as' => 'voyager.roles.show']);
        Route::post('roles/',['uses' => 'Voyager\\VoyagerRoleController@store',  'as' => 'voyager.roles.store']);
        Route::put('{id}',['uses' => 'Voyager\\VoyagerRoleController@update',  'as' => 'voyager.roles.update']);
        Route::delete('roles/{id}',['uses' => 'Voyager\\VoyagerRoleController@destroy',  'as' => 'voyager.roles.destroy']);
        // Route::post('roles/browse',['uses' => 'Voyager\\VoyagerRoleController@browse',  'as' => 'voyager.roles.browse']);


        Route::get('products','Voyager\\ProductController@index')->name('voyager.products.index');
        Route::post('products/browse','Voyager\\ProductController@browse')->name('voyager.products.browse');

        Route::post('products/saveGeneralInfo',['uses' => 'Voyager\\ProductController@saveGeneralInfo',  'as' => 'saveGeneralInfo']);
        Route::post('getLabelTranslations',['uses' => 'Voyager\\ProductController@getLabelTranslations',  'as' => 'getLabelTranslations']);
        Route::get('getLangTranslations/{lang}',['uses' => 'Voyager\\ProductController@getLangTranslations',  'as' => 'getLangTranslations']);
        Route::post('remove-checkpoint-document',['uses' => 'Voyager\\ProductController@removeCheckpointDocument',  'as' => 'removeCheckpointDocument']);



//        Route::any('service-inquiries/index','Voyager\\ServiceInquiryController@index')->name('voyager.service-inquiries.browse');
        Route::get('service-inquiries/additional/data','Voyager\\ServiceInquiryController@additionalData')->name('voyager.service-inquiries.additionalData');

        Route::post('service-inquiries/browse','Voyager\\ServiceInquiryController@browse')->name('voyager.service-inquiries.browse');
        Route::post('service-inquiries/ajaxDocuments','Voyager\\ServiceInquiryController@ajaxDocuments')->name('voyager.service-inquiries.ajaxDocuments');
        Route::post('service-inquiries/ajaxStatusUpdate','Voyager\\ServiceInquiryController@ajaxStatusUpdate')->name('voyager.service-inquiries.ajaxStatusUpdate');
        Route::post('service-inquiries/ajaxDetailedRequest','Voyager\\ServiceInquiryController@ajaxDetailedRequest')->name('voyager.service-inquiries.ajaxDetailedRequest');
        Route::post('service-inquiries/ajaxMessageActivity','Voyager\\ServiceInquiryController@ajaxMessageActivity')->name('voyager.service-inquiries.ajaxMessageActivity');
        Route::post('service-inquiries/checkPointStatus','Voyager\\ServiceInquiryController@checkPointStatus')->name('voyager.service-inquiries.checkPointStatus');
        Route::post('service-inquiries/checkPointComment','Voyager\\ServiceInquiryController@checkPointComment')->name('voyager.service-inquiries.checkPointComment');
        Route::post('service-inquiries/ajaxInviteUsers','Voyager\\ServiceInquiryController@ajaxInviteUsers')->name('voyager.service-inquiries.ajaxInviteUsers');

//        Route::post('service-inquiries/ajaxCityList','Voyager\\ServiceInquiryController@ajaxCityList')->name('voyager.service-inquiries.ajaxCityList');
        Route::put('service-inquiries/update/{id}',['uses' => 'Voyager\\ServiceInquiryController@update',  'as' => 'voyager.service-inquiries.update']);
        Route::get('service-inquiries/draftApply/{id}','Voyager\\ServiceInquiryController@draftApply')->name('voyager.service-inquiries.draftApply');

        // pages
        Route::get('pages','Voyager\\PagesController@index')->name('voyager.pages.index');
        Route::post('pages/browse','Voyager\\PagesController@browse')->name('voyager.pages.browse');
        Route::get('pages/create','Voyager\\PagesController@create')->name('voyager.pages.create');
        Route::post('pages/store','Voyager\\PagesController@store')->name('voyager.pages.store');
        Route::get('pages/edit/{id}','Voyager\\PagesController@edit')->name('voyager.pages.edit');
        Route::put('pages/update/{id}','Voyager\\PagesController@update')->name('voyager.pages.update');
        Route::delete('pages/destroy/{id}','Voyager\\PagesController@destroy')->name('voyager.pages.destroy');

        // news
        Route::get('news','Voyager\\NewsController@index')->name('voyager.news.index');
        Route::post('news','Voyager\\NewsController@index')->name('voyager.news');
        Route::post('news/browse','Voyager\\NewsController@browse')->name('voyager.news.browse');
        Route::get('news/create','Voyager\\NewsController@create')->name('voyager.news.create');
        Route::post('news/store','Voyager\\NewsController@store')->name('voyager.news.store');
        Route::get('news/edit/{id}','Voyager\\NewsController@edit')->name('voyager.news.edit');
        Route::put('news/update/{id}','Voyager\\NewsController@update')->name('voyager.news.update');
        Route::delete('news/destroy/{id}','Voyager\\NewsController@destroy')->name('voyager.news.destroy');

        // Graph Blocks
        Route::get('graph-blocks','Voyager\\GraphBlocksController@index')->name('voyager.graph-blocks.index');
        Route::post('graph-blocks','Voyager\\GraphBlocksController@index')->name('voyager.graph-blocks');
        Route::post('graph-blocks/browse','Voyager\\GraphBlocksController@browse')->name('voyager.graph-blocks.browse');
        Route::get('graph-blocks/create','Voyager\\GraphBlocksController@create')->name('voyager.graph-blocks.create');
        Route::post('graph-blocks/store','Voyager\\GraphBlocksController@store')->name('voyager.graph-blocks.store');
        Route::get('graph-blocks/edit/{id}','Voyager\\GraphBlocksController@edit')->name('voyager.graph-blocks.edit');
        Route::put('graph-blocks/update/{id}','Voyager\\GraphBlocksController@update')->name('voyager.graph-blocks.update');
        Route::delete('graph-blocks/destroy/{id}','Voyager\\GraphBlocksController@destroy')->name('voyager.graph-blocks.destroy');

        // subsidiaries
        Route::get('subsidiaries','Voyager\\SubsidiaryController@index')->name('voyager.subsidiaries.index');
        Route::post('subsidiaries','Voyager\\SubsidiaryController@index')->name('voyager.subsidiaries');
        Route::post('subsidiaries/browse','Voyager\\SubsidiaryController@browse')->name('voyager.subsidiaries.browse');
        Route::get('subsidiaries/create','Voyager\\SubsidiaryController@create')->name('voyager.subsidiaries.create');
        Route::post('subsidiaries/store','Voyager\\SubsidiaryController@store')->name('voyager.subsidiaries.store');
        Route::get('subsidiaries/edit/{id}','Voyager\\SubsidiaryController@edit')->name('voyager.subsidiaries.edit');
        Route::put('subsidiaries/update/{id}','Voyager\\SubsidiaryController@update')->name('voyager.subsidiaries.update');
        Route::delete('subsidiaries/destroy/{id}','Voyager\\SubsidiaryController@destroy')->name('voyager.subsidiaries.destroy');

        // consulting service product
        Route::get('consulting-service-products','Voyager\\ConsultingServiceProductController@index')->name('voyager.consulting-service-products.index');
        Route::post('consulting-service-products','Voyager\\ConsultingServiceProductController@index')->name('voyager.consulting-service-products');
        Route::post('consulting-service-products/browse','Voyager\\ConsultingServiceProductController@browse')->name('voyager.consulting-service-products.browse');
        Route::get('consulting-service-products/create','Voyager\\ConsultingServiceProductController@create')->name('voyager.consulting-service-products.create');
        Route::post('consulting-service-products/store','Voyager\\ConsultingServiceProductController@store')->name('voyager.consulting-service-products.store');
        Route::get('consulting-service-products/edit/{id}','Voyager\\ConsultingServiceProductController@edit')->name('voyager.consulting-service-products.edit');
        Route::put('consulting-service-products/update/{id}','Voyager\\ConsultingServiceProductController@update')->name('voyager.consulting-service-products.update');
        Route::delete('consulting-service-products/destroy/{id}','Voyager\\ConsultingServiceProductController@destroy')->name('voyager.consulting-service-products.destroy');

        // Projects
        Route::get('projects','Voyager\\ProjectsController@index')->name('voyager.projects.index');
        Route::post('projects','Voyager\\ProjectsController@index')->name('voyager.projects');
        Route::post('projects/browse','Voyager\\ProjectsController@browse')->name('voyager.projects.browse');
        Route::get('projects/create','Voyager\\ProjectsController@create')->name('voyager.projects.create');
        Route::post('projects/store','Voyager\\ProjectsController@store')->name('voyager.projects.store');
        Route::get('projects/edit/{id}','Voyager\\ProjectsController@edit')->name('voyager.projects.edit');
        Route::put('projects/update/{id}','Voyager\\ProjectsController@update')->name('voyager.projects.update');
        Route::delete('projects/destroy/{id}','Voyager\\ProjectsController@destroy')->name('voyager.projects.destroy');

        // Footer Widgets
        Route::get('footer-widgets','Voyager\\FooterWidgetsController@index')->name('voyager.footer-widgets.index');
        Route::post('footer-widgets','Voyager\\FooterWidgetsController@index')->name('voyager.footer-widgets');
        Route::post('footer-widgets/browse','Voyager\\FooterWidgetsController@browse')->name('voyager.footer-widgets.browse');
        Route::get('footer-widgets/create','Voyager\\FooterWidgetsController@create')->name('voyager.footer-widgets.create');
        Route::post('footer-widgets/store','Voyager\\FooterWidgetsController@store')->name('voyager.footer-widgets.store');
        Route::get('footer-widgets/edit/{id}','Voyager\\FooterWidgetsController@edit')->name('voyager.footer-widgets.edit');
        Route::put('footer-widgets/update/{id}','Voyager\\FooterWidgetsController@update')->name('voyager.footer-widgets.update');
        Route::delete('footer-widgets/destroy/{id}','Voyager\\FooterWidgetsController@destroy')->name('voyager.footer-widgets.destroy');


        /* ProductCategoriesController */
        Route::get('product-categories','Voyager\\ProductCategoriesController@index')->name('voyager.product-categories.index');
        Route::post('product-categories','Voyager\\ProductCategoriesController@index')->name('voyager.product-categories');
        Route::post('product-categories/browse','Voyager\\ProductCategoriesController@browse')->name('voyager.product-categories.browse');
        Route::get('product-categories/create','Voyager\\ProductCategoriesController@create')->name('voyager.product-categories.create');
        Route::post('product-categories/store','Voyager\\ProductCategoriesController@store')->name('voyager.product-categories.store');
        Route::get('product-categories/edit/{id}','Voyager\\ProductCategoriesController@edit')->name('voyager.product-categories.edit');
        Route::put('product-categories/update/{id}','Voyager\\ProductCategoriesController@update')->name('voyager.product-categories.update');
        Route::delete('product-categories/destroy/{id}','Voyager\\ProductCategoriesController@destroy')->name('voyager.product-categories.destroy');

        // Admin Media
//        Route::get('media/', ['uses' => 'Voyager\\VoyagerMediaController@index',              'as' => 'index']);
//        Route::post('files', ['uses' => 'Voyager\\VoyagerMediaController@files',              'as' => 'files']);
//        Route::post('new_folder', ['uses' => 'Voyager\\VoyagerMediaController@new_folder',         'as' => 'new_folder']);
//        Route::post('delete_file_folder', ['uses' => 'Voyager\\VoyagerMediaController@delete', 'as' => 'delete']);
//        Route::post('move_file', ['uses' => 'Voyager\\VoyagerMediaController@move',          'as' => 'move']);
//        Route::post('rename_file', ['uses' => 'Voyager\\VoyagerMediaController@rename',        'as' => 'rename']);
//        Route::post('upload', ['uses' => 'Voyager\\VoyagerMediaController@upload',             'as' => 'upload']);
//        Route::post('crop', ['uses' => 'Voyager\\VoyagerMediaController@crop',             'as' => 'crop']);

    });

});
Route::get('service-inquiries/ajaxCityList','Frontend\\CustomerController@ajaxCityList')->name('voyager.service-inquiries.ajaxCityList');
Route::get('invitation_check_dock/{id}', function($id){
//    try {
        $service_data = ServiceInquiryVerification::where('link', decrypt($id))->first();
        $serviceInquiryData = ServiceInquiries::where('id',$service_data->service_inquiry_id)->first();
        $service_form_data = [];
        $filesList = json_decode($service_data->path);
        foreach(json_decode($serviceInquiryData->fields) as $key => $form_data){
            if ($form_data->type == 'file'){
                $fileName = ServiceInquiriesDocuments::where('service_inquiries_id', $serviceInquiryData->id)->where('document', 'LIKE', $form_data->name.'%')->first();

                $customerFolder = $serviceInquiryData->user->name .'_'. $serviceInquiryData->user->id;
                $serviceInquiryVerificationId = $serviceInquiryData->id;
                $docPath = 'Documents/' . $customerFolder . '/' . $serviceInquiryVerificationId . '/' . $fileName->document;

                if($fileName){
                    if(in_array($fileName->document,array_values((Array)$filesList))) {
                        array_push($service_form_data, [
                            'type' => $form_data->type,
                            'title' => $form_data->label,
                            'value' => url('customerAttachmentDownload'). '?path='.$docPath
//                            'value' => asset('storage/documents/' . $fileName->document)
                        ]);
                    }
                }
            } else if($form_data->type == 'paragraph' || $form_data->type == 'header' || $form_data->type == 'hidden' || $form_data->type == 'button'){}else{
                try{
                    array_push($service_form_data,[
                        'type' => $form_data->type,
                        'title'  => $form_data->label,
                        'value'  => implode(', ', $form_data->userData)
                    ]);
                } catch (\Exception $s){
                    continue;
                }
            }
        }
        return view('frontend.tempUrl.verification', compact('service_data', 'service_form_data'));
//    }catch (Exception $e){
//        return abort(404);
//    }
})->name('invitationServiceInquiryVerification');

Route::any('action-services-inquiry-verification/{id}', function(\Illuminate\Http\Request $request, $id){
    app()->setLocale('kz');
    $docName = [];
    if($request->external_doc) {
        foreach ($request->external_doc as $key => $extDoc) {
            $image = $extDoc;
            $path = 'public/serviceInquiry/externalVerification/';
            $fileName = \Carbon\Carbon::now()->timestamp.$key. '.' . $image->getClientOriginalExtension();
            $storeFileName = 'serviceInquiry/externalVerification/' . $fileName;
            Storage::disk('local')->put($path . $fileName, file_get_contents($image));
            array_push($docName, $storeFileName);
        }
    }
    $service_data = ServiceInquiryVerification::where('id',$id)->first();
    $service_data->is_verified = $request->status == 1 ? ServiceInquiryVerification::VERIFICATION_YES : ServiceInquiryVerification::VERIFICATION_NO;
    $service_data->action_taken_at = \Carbon\Carbon::now();
    $service_data->comment = $request->comment;
    $service_data->documents = json_encode($docName);
    $service_data->save();

    ServiceInquiryActivity::create([
        'service_inquiry_id' => $service_data->service_inquiry_id,
        'user_id' => 0,
        'activity_type' => ServiceInquiryActivity::ACTIVITY_VERIFICATION,
        'message' => json_encode(['is_verified' => $service_data->is_verified, 'action_taken_at' => $service_data->action_taken_at, 'comment' => $service_data->comment, 'email' => $service_data->email, 'documents' => json_encode($docName)]),
        'requested_param' => serialize($request->except(['external_doc'])),
    ]);

    return back();
})->name('actionServiceInquiryVerification');

Route::post('CustomerFile', 'Voyager\\ServiceInquiryController@customerFile')->name('customerFile');

//Route::get('admin/login', ['uses' => 'Voyager\\VoyagerAuthController@login',     'as' => 'voyager.login']);
Route::get('admin/login', 'Frontend\CustomerController@index')->name('voyager.login');
Route::post('admin/login', ['uses' => 'Voyager\\VoyagerAuthController@postLogin', 'as' => 'voyager.postlogin']);

Route::get('customerAttachmentDownload',function(\Illuminate\Http\Request $request){

    /* DOWNLOAD FILE */
    try {
        $files = Storage::disk('sftp')->download($request->path);
        return $files;
    } catch (\Exception $e) {
        \Illuminate\Support\Facades\Log::notice('FILE DOWNLOAD ERROR '. $e->getMessage());
        return back();
    }
})->name('customerAttachmentDownload');

Route::get('download-file',function(\Illuminate\Http\Request $request){
//    dd($request->path);
//    $file = '/mig/savan.txt';
//    $content = 'BAITEREK 2020 PROJECT';
    /* STORE FILE */
//    Storage::disk('sftp')->put($file, $content);
    /****************END *****************/

    /* DOWNLOAD FILE */

    try {
        $files = Storage::disk('sftp')->download($request->path);
        return $files;
    } catch (\Exception $e) {
        \Illuminate\Support\Facades\Log::notice('FILE DOWNLOAD ERROR '. $e->getMessage());
        return back();
    }
    /****************END *****************/
})->name('document.download');