<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::group(['namespace' => 'API'], function(){
    Route::post('login', 'UserController@login')->name('api.login');
    Route::get('roles', 'RoleController@UserRoles')->name('roles');
    Route::post('register', 'UserController@register');

    Route::get('categories/{num}', 'ProductCategoryController@index');
    Route::get('product-category/{num}', 'ProductController@index');

    Route::post('forgotPassword', 'UserController@forgotPassword');
    Route::get('news/{num}', 'NewsController@getNews')->name('getNews');
    Route::get('products/{num}', 'ProductController@getProducts')->name('getProducts');
    Route::get('locale/{locale}', function ($locale){
        Session::put('locale', $locale);
        return response(['status' => 'Successfully Change Language'], 200);
    });

    Route::any('FileSync/{dir}', 'FileTransferController@fileSync');
    Route::any('dirSync/{dir}', 'FileTransferController@dirSync');
    Route::any('fileSynct', 'FileTransferController@fileSynct');

    Route::group(['middleware' => 'auth:api','prefix' => 'auth'], function(){
        Route::get('userDetails', 'UserController@getUserDetails');
        Route::post('setUserDetails', 'UserController@setUserDetails');
        Route::post('changePassword', 'UserController@changePassword');
        Route::get('requests/{num}', 'ServiceRequestController@getRequestList')->name('requests');

    });
});
